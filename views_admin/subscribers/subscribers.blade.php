@extends('admin.layout.auth')
@section('title', 'Coupons' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->         
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                   <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                                    @include('admin.includes.realtime-search')
                       </div>
                      <div class="col-sm-12">
                        <textarea id="res" style="display:  none ;" ></textarea>
                  
                  <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">           
                           <p >Calling all data...</p>
                        </div>
                  <!-------------Loader Ends here------------->
                   
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Subscribers</h2>
                                 </div>
                                 <div class="col-sm-5 text-right">
                                  
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12" >
                                     <div ng-view></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/subscribers.js')}}"></script> 

<!------>
@endsection