<?php

return array (
  'view_logistics' => '1',
  'view_items' => '1',
  'add_items' => '1',
  'view_stores' => '1',
  'view_coupons' => '1',
  'view_customers' => '1',
  'view_vendors' => '1',
  'view_store_managers' => '1',
  'add_customers' => '1',
  'add_vendors' => '1',
  'add_store_managers' => '1',
  'view_orders' => '1',
  'add_orders' => '0',
  'view_account_settings' => '1',
  'view_business_logo_settings' => '1',
  'view_faqs_settings' => '1',
  'view_timeslots_settings' => '1',
  'view_tax_settings' => '1',
  'view_locations_settings' => '1',
  'view_areas_settings' => '1',
  'view_plugins_settings' => '1',
  'view_categories_settings' => '1',
  'view_tags_settings' => '1',
  'approve_edit_request' => '1',
  'send_mass_notifications' => '1',
  'switch_store_availability' => '0',
  'store_edit_by_store_manager' => '0',
  'add_categories' => '0',
  'edit_categories' => '0',
  'view_freshdesk_enquiries' => '1',
  'view_subscribers' => '1',
  'view_payment_request_sent' => '0',
  'view_payment_request' => '1',
  'add_order_cancel_reasons' => '1',
  'edit_order_cancel_reasons' => '1',
  'filter_reviews' => '1',
  'view_basic_settings' => '1',
  'view_order_cancel_reasons' => '1',
  'view_reviews' => '1',
  'view_store_settings' => '0',
  'view_translations' => '1',

  'view_privacy_policy' => '1',
  'view_terms_and_conditions' => '1',
  'view_web_banners' => '1',
  
  'view_store_banners' => '1',
  
  
  'store_info_sidebar'=>'0',
  'store_menu_sidebar'=>'0',
  'manager_filters'=>'1',
  'sort_stores'=>'0'
  
  
);
