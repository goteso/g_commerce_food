@extends('web-food.layout.web')
@section('title', 'Login' )  
 
@section('content')


@section('header')
@include('web-food.includes.header')
@show
 
 
 
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
               <div class="container-fluid"> 
                  <div class="row" >
                      <div class="col-sm-12"  >
                                     
                   
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="login" ng-controller="forgotPassController" ng-cloak >
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-12 col-lg-10 col-lg-offset-1 text-center">   
                         <div class="panel">
						 <div class="panel-body">
						   <div class="row">
						     <div class="col-sm-12 col-md-10 col-md-offset-1  ">
							   
							   <div id="email-block"> 
							   <h3 class="header  text-center">Forgot Password</h3>
							   <br> 
							   <p>We will send a link or an otp to your registered email and mobile using which you will be able to create a new password.</p>
							   <br>
							     <div class="form-group">
								   <input type="email" class="form-control" id="user_email" name="email" ng-model="email" placeholder="Email">
								 </div>  
							    
							   <md-button type="button" ng-click="sendEmail()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Submit</md-button> 
							   </div>
							   
							   <div id="otp-block">
							   <h3 class="header  text-center">Verify OTP</h3>
							   <br> 
							     <div class="form-group">
								   <input type="text" class="form-control" id="otp" name="otp" ng-model="otp" placeholder="Enter Otp">
								 </div>  
							    
							   <md-button type="button" ng-click="verifyOtp()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Submit</md-button> 
							   </div>
							   
							   <div id="password-block">
							   <h3 class="header  text-center">Create New Password</h3>
							   <br> 
							     <div class="form-group">
								   <input type="password" class="form-control" id="password" name="password" ng-model="password" placeholder="Password">
								 </div>  
							    
							   <md-button type="button" ng-click="setPassword()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Submit</md-button> 
							   </div>
							   
							 </div> 
						   </div>
						 </div>
						 </div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
		
		  				  
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>

<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/forgot-password.js')}}"></script> 
@endsection