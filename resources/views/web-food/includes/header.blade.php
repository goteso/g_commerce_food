   <header>
   <nav class="navbar navbar-inverse  " style="border-radius:0px;">
      <div class="container" ng-controller="headerController" ng-cloak>
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" style="padding:7px 15px;" href="{{URL::to('/')}}">
		   <!-- <img src="{{URL::asset('/images/logo')}}/@{{logo}}" class="img-responsive" style="height:45px;">-->
			 <img src="{{URL::asset('/admin/assets/images/logo-white.png')}}" class="img-responsive" style="height:55px;">
		  </a>
          
        </div>
        <div id="navbar" class="navbar-collapse collapse" >

          <div class="navbar-form navbar-right"> 
		  <div class="dropdown" id="header-account">
            <md-button type="button"  class="btn md-raised bg-color md-submit md-button md-ink-ripple  dropdown-toggle" type="button" data-toggle="dropdown">Account <span class="caret"></span></md-button>
			<ul class="dropdown-menu">
				<li>
				  <a href="{{URL::to('web-login')}}">
				    <img src="{{URL::asset('web-food/assets/images/shopping-bag.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/shopping-bag-active.svg')}}" class="img-active">
				 Login</a>
				</li>
				<li><a href="{{URL::to('register')}}">
				 <img src="{{URL::asset('web-food/assets/images/address.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/address-active.svg')}}" class="img-active">
					Register</a>
				</li>
			</div>
			
				<div class="dropdown"   id="header-login" >
			  <md-button class="btn md-raised bg-color md-submit md-button md-ink-ripple dropdown-toggle" type="button" data-toggle="dropdown">Hi, @{{customerDataFirstName}}
			  <span class="caret"></span></md-button>
			  <ul class="dropdown-menu">
				<li>
				  <a href="{{URL::to('my-orders')}}">
				    <img src="{{URL::asset('web-food/assets/images/shopping-bag.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/shopping-bag-active.svg')}}" class="img-active">
				  My Orders</a>
				</li>
				<li><a href="{{URL::to('addresses')}}">
				 <img src="{{URL::asset('web-food/assets/images/address.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/address-active.svg')}}" class="img-active">
					Manage Addresses</a></li>
					<li><a href="{{URL::to('loyalty-points')}}">
				 <img src="{{URL::asset('web-food/assets/images/address.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/address-active.svg')}}" class="img-active">
					Loyalty Points History</a></li>
				<li><a href="{{URL::to('reviews')}}">
				 <img src="{{URL::asset('web-food/assets/images/feedback.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/feedback-active.svg')}}" class="img-active">
					My Reviews</a></li>
				<li><a href="{{URL::to('favourites')}}">
				 <img src="{{URL::asset('web-food/assets/images/favorites.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/favorites-active.svg')}}" class="img-active">
					Favourites</a></li>
				<li><a href="{{URL::to('account')}}">
				 <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
					Edit Profile</a></li>
					<li><a href="{{URL::to('change-password')}}">
				 <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
					Change Password</a></li>
				<li ng-controller="logoutController"><a href="#" ng-click="logout();">
				 <img src="{{URL::asset('web-food/assets/images/sign-out.svg')}}" class="img-normal">
					<img src="{{URL::asset('web-food/assets/images/sign-out-active.svg')}}" class="img-active">
					Signout</a></li>
			  </ul>
			</div>
				
          </div>
          
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
	
	</header>
	
	 
