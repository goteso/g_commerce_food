@extends('web-food.layout.web')
@section('title', 'Reviews' )  
 
@section('content')


@section('header')
@include('web-food.includes.header')
@show
  
 
						   
 <div  ng-controller="passwordController" ng-cloak>
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-8"   >
                           <md-list layout-padding class="profile-data" >
							<md-list-item class="md-3-line"     >
								<img  ng-src="{{URL::asset('images/users')}}/@{{customerDataPhoto}}" class="md-avatar" ng-show="customerDataPhoto">
								<img  ng-src="{{URL::asset('admin/assets/images/user-placeholder.png')}}" class="md-avatar" ng-show="customerDataId && !customerDataPhoto">
								<div class="md-list-item-text">
								  <h2 >@{{customerDataFirstName}} @{{customerDataLastName}}</h2>
								  <h4>
									@{{customerDataEmail}}
								  </h4>
								</div>
							</md-list-item>
						  </md-list>    
                      </div>
                      <div class="col-sm-4">
                          <br>
                         <a href="{{URL::to('/')}}"> <button class="btn md-raised bg-color md-submit md-button md-ink-ripple   md-button" ng-show="customerDataId">Place Order</button>
                         </a>
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="orders">
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-4 text-center" ng-controller="logoutController">   
                         <div class="panel left">
						 <div class="panel-body">
						   <table  class="table left-nav-links">
						    <tr>
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/shopping-bag.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/shopping-bag-active.svg')}}" class="img-active">
							  </td>
							  <td > <a href="{{URL::to('my-orders')}}">My Orders</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/address.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/address-active.svg')}}" class="img-active">
							  </td>
							  <td> <a href="{{URL::to('addresses')}}">Manage Addresses</a></td>
							</tr>
							<tr  class="">
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('loyalty-points')}}"> Loyalty Points History</a></td>
							</tr>
							<tr >
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/feedback.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/feedback-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('reviews')}}"> My Reviews</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/favorites.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/favorites-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('favourites')}}"> Favourites</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('account')}}/@{{profileData.data.user_id}}"> Edit Profile</a></td>
							</tr>
							
						<tr class="active">
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('change-password')}}"> Change Password</a></td>
							</tr>
							<tr class="">
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('transfer-loyalty-points')}}"> Transfer Loyalty Points</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/sign-out.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/sign-out-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="#" ng-click="logout();">Signout</a></td>
							</tr>
						   </table>
						 </div>
						 </div>
                     </div> 
					 
					 
					 <div class="col-sm-8 text-center">   
                         <div class="panel">
						 <div class="panel-body">
						 
						     <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('web-food/assets/images/89.svg')}}" class="img-responsive center-block">				 
                              <p >Loading...</p>
                           </div>
						   <div class="row category-add">
						     <div class="col-sm-8 col-sm-offset-2" ><br>
                                                      <md-input-container  class="md-block text-left"  >
                                                         <label>Old Password</label>
                                                         <input type="password" id="old_password" name="old_password"  ng-model="old_password" value="" />
                                                      </md-input-container>
													  
													  <md-input-container  class="md-block text-left"  >
                                                         <label>New Password</label>
                                                         <input type="password" id="new_password" name="new_password"  ng-model="new_password" value="" />
                                                      </md-input-container>
													  
													  <md-input-container  class="md-block text-left"  >
                                                         <label>Confirm New Password</label>
                                                         <input type="password" id="confirm_new_password" name="confirm_new_password"  ng-model="confirm_new_password" value="" />
                                                      </md-input-container>
                                                     </div>
												   <div class="col-lg-8 col-sm-offset-2 submit text-right">
												    
                                                   <button   ng-click="changePassword()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Change Password</button>
                                                </div>
												</div>
						 </div>
						 </div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
	 </div>
		  				  
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>

<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/change-password.js')}}"></script> 
@endsection