@extends('web-food.layout.web')
@section('title', 'Home' )  
 <link rel="stylesheet" href=" {{URL::asset('web-food/assets/css/style1.css')}}">
     <link rel="stylesheet" href="{{URL::asset('web-food/assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('web-food/assets/css/main.css')}}">
<style>

#main-banner .form-group{padding:5px 8px;}
@media(min-width:768px){#main-banner .form-inline{display:flex;}}
   .spinner {
   margin: 100px auto 0;
   width: 70px;
   text-align: center;
   }
   .spinner > div {
   width: 18px;
   height: 18px;
   background-color: #333;
   border-radius: 100%;
   display: inline-block;
   -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   }
   .spinner .bounce1 {
   -webkit-animation-delay: -0.32s;
   animation-delay: -0.32s;
   }
   .spinner .bounce2 {
   -webkit-animation-delay: -0.16s;
   animation-delay: -0.16s;
   }
   @-webkit-keyframes sk-bouncedelay {
   0%, 80%, 100% { -webkit-transform: scale(0) }
   40% { -webkit-transform: scale(1.0) }
   }
   @keyframes sk-bouncedelay {
   0%, 80%, 100% { 
   -webkit-transform: scale(0);
   transform: scale(0);
   } 40% { 
   -webkit-transform: scale(1.0);
   transform: scale(1.0);
   }
   }
   .foodbakery-button-loader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background: #f97456;
   text-align: center;
   border-radius: 3px;
   background-color: #c33332 !important;
   }
   .foodbakery-button-loader .spinner {
   width: 25px;
   height: 25px;
   position: absolute;
   display: inline-block;
   top: 0;
   bottom: 0;
   left: 0;
   right: 0;
   margin: auto;
   }
   .foodbakery-button-loader .double-bounce1, .foodbakery-button-loader .double-bounce2 {
   width: 100%;
   height: 100%;
   border-radius: 50%;
   background-color: #fff;
   opacity: 0.6;
   position: absolute;
   top: 0;
   left: 0;
   -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
   animation: sk-bounce 2.0s infinite ease-in-out;
   }
   .foodbakery-button-loader .double-bounce2 {
   -webkit-animation-delay: -1.0s;
   animation-delay: -1.0s;
   }
   md-checkbox.md-default-theme.md-checked .md-ink-ripple, md-checkbox.md-checked .md-ink-ripple{color:#c33332}
   md-checkbox.md-default-theme.md-checked .md-icon, md-checkbox.md-checked .md-icon{background-color:#c33332;}
   
   md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}
   
   .promoCode  md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:140px!important;max-height:150px!important;}
   #pickupModal  md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:300px!important}
   .carousel-control.left, .carousel-control.right{background:transparent!important;}
   .reviewData.celebrity {
    background: #fff0f0;
}




.rating {
    color: #d7d7d7;
    margin: 0;
    padding: 0;
}
ul.rating {
    display: inline-block;
}
.rating li {
	font-size: 18px;
    list-style-type: none;
    display: inline-block;
    padding: 1px;
    text-align: center;
    font-weight: bold;
    cursor: pointer;
}
.rating .filled {
    color: #fbad00;
}



.btn-icon {
  padding: 0;
  background: transparent;
  border-color: transparent;
  box-shadow: none;
  cursor: pointer;
  border-radius: 0;
}
.btn-icon:active,
.btn-icon:focus {
  -webkit-box-shadow: none;
  -moz-box-shadow: none;
  box-shadow: none;
  border-color: transparent;
}
.btn-icon:active {
  -webkit-transform: scale(0.9);
  -moz-transform: scale(0.9);
  -ms-transform: scale(0.9);
  -o-transform: scale(0.9);
  transform: scale(0.9);
}
.btn-icon:focus {
  outline: 0 none !important;
}
.btn-icon .glyphicon {
  -webkit-transition: color 0.1s ease;
  -moz-transition: color 0.1s ease;
  -o-transition: color 0.1s ease;
  transition: color 0.1s ease;
}
.btn-icon .glyphicon-star {
  color: #666;
}
.btn-icon .glyphicon-star:hover {
  color: rgba(102, 102, 102, 0.85);
}
.btn-icon .glyphicon-star.active {
  color: #FECF15;
}
.btn-icon .glyphicon-star.active:hover {
  color: rgba(254, 207, 21, 0.85);
}
.btn-icon .glyphicon-heart {
  color: #666;
}
.btn-icon .glyphicon-heart:hover {
  color: rgba(102, 102, 102, 0.85);
}
.btn-icon .glyphicon-heart.active {
  color: #FE4365;
}
.btn-icon .glyphicon-heart.active:hover {
  color: rgba(254, 67, 101, 0.85);
}
.btn-icon .glyphicon-bookmark {
  color: #666;
}
.btn-icon .glyphicon-bookmark:hover {
  color: rgba(102, 102, 102, 0.9);
}
.btn-icon .glyphicon-bookmark.active {
  color: #A6D478;
}
.btn-icon .glyphicon-bookmark.active:hover {
  color: rgba(166, 212, 120, 0.9);
}
   </style>
@section('content')


@section('header')
@include('web-food.includes.header')
@show
 
  <div ng-view></div>
 
       
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>


<script src="{{URL::asset('web-food/assets/js/owl.carousel.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        })
    })
</script>
<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/abc8.js')}}"></script> 

@endsection