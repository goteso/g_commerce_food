@extends('admin.layout.auth')
@section('title', 'Dashboard' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
<style>
   @media(min-width:1200px){
   .card{height:300px;overflow-y:auto} 
   }
   @media (min-width: 992px){
   .col-md-3.top-data {
   width: 20%;
   }}
   @media(min-width:2200px){
   .top-value{display:none;}
   .bottom-value{display:block;} 
   }
   @media(max-width:2199px) {
   .top-value{display:block;}
   .bottom-value{display:none;}  
   }
   @media(min-width:766px) and (max-width:1199px){
   .card{height:330px;overflow-y:auto} 
   } 
   hc-chart {
   padding-top:5px;
   width: 100%; 
   display: block;
   } 
   .highcharts-container {width:100%!important;}
   svg:not(:root){width:100%;}
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper">
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->         
      @section('sidebar')
      @include('admin.includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                     @include('admin.includes.realtime-search')
                  </div>
                  <div class="col-sm-7 text-right " style="display:inline-block;" ng-controller="dashboardController">
                     <?php          $auth_user_type = Auth::user()->user_type;   
                        $auth_user_id =   Auth::id();  
                        ?> 
                     @if(trans('permission'.$auth_user_type.'.switch_store_availability') == '1')
                     <label>Store Availability</label>:  
                     <md-switch class="text-center md-primary" ng-change="switchStoreStatus(busy);" ng-model="busy" value="@{{busy}}"  ng-true-value="1" ng-false-value="0"   style="display:inline-block;margin:0;padding:10px;"> </md-switch>
                     @endif
                  </div>
                  <textarea id="res" style="display: none " ></textarea>
                  <textarea id="res1" style="display: none " ></textarea>
                  <textarea id="res2" style="display:none  " ></textarea>
                  <div id="loading" class="loading" >
                     <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">        
                     <p >Calling all data...</p>
                  </div>
                  <div class="col-sm-12">
                     <div class="text-right">
                        <br> 
                     </div>

                           <?php 

   $currency_symbol = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;

         ?>


                     <div class="tab-content" >
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div ng-controller="dashboardController" ng-cloak>
                           <div class="container-fluid" id="dashboard">
                              <div class="row" >
                                 <div class="col-sm-12" >
                                    <div class="row header-cols"  >
                                       <div class="col-sm-6 col-md-3 top-data"   >
                                          <div class="panel products"  >
                                             <a href="{{URL::to('v1/orders#/regular')}}">
                                                <div class="panel-data  ">
                                                   <ul class="list-inline"  >
                                                      <li><i class=" fa fa-shopping-cart" ></i> </li>
                                                      <li style="float:right">
                                                         <h2 class="top-value" style="margin:0px">@{{dashboardData.counts[0].total_order}}</h2>
                                                      </li>
                                                   </ul>
                                                   <ul class="list-inline"  >
                                                      <li>
                                                         <h5 class=" text-uppercase">Total Orders</h5>
                                                      </li>
                                                      <li style="float:right">
                                                         <h2 class=" bottom-value" style=" margin-top:-6px">@{{dashboardData.counts[0].total_order}}</h2>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="progress progress-sm">
                                                   <div class="progress-bar bg-color" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                                                      <span class="sr-only">70% Complete</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col-sm-6 col-md-3 top-data"  ng-hide="!dashboardData.counts[0].total_users" >
                                          <div class="panel categories" style="padding: 10px 15px !important;" >
                                             <a href="{{URL::to('v1/customers')}}">
                                                <div class="panel-data  ">
                                                   <ul class="list-inline"  >
                                                      <li><i class=" fa fa-user"></i> </li>
                                                      <li style="float:right">
                                                         <h2 class="top-value" style="margin:0px">@{{dashboardData.counts[0].total_users}}</h2>
                                                      </li>
                                                   </ul>
                                                   <ul class="list-inline"  >
                                                      <li>
                                                         <h5 class=" text-uppercase" >Total Users</h5>
                                                      </li>
                                                      <li style="float:right">
                                                         <h2 class=" bottom-value" style=" margin-top:-6px">@{{dashboardData.counts[0].total_users}}</h2>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="progress progress-sm">
                                                   <div class="progress-bar bg-color" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:20%">
                                                      <span class="sr-only">20% Complete</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col-sm-6 col-md-3 top-data"   ng-hide="!dashboardData.counts[0].total_tasks" >
                                          <div class="panel customers"  >
                                             <a href="{{URL::to('v1/logistics')}}">
                                                <div class="panel-data  ">
                                                   <ul class="list-inline"  >
                                                      <li><i class=" fa fa-tag" ></i> </li>
                                                      <li style="float:right">
                                                         <h2 class="top-value" style="margin:0px">@{{dashboardData.counts[0].total_tasks}}</h2>
                                                      </li>
                                                   </ul>
                                                   <ul class="list-inline"  >
                                                      <li>
                                                         <h5 class=" text-uppercase">Total Tasks</h5>
                                                      </li>
                                                      <li style="float:right">
                                                         <h2 class=" bottom-value" style=" margin-top:-6px">@{{dashboardData.counts[0].total_tasks}}</h2>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="progress progress-sm">
                                                   <div class="progress-bar bg-color" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%">
                                                      <span class="sr-only">70% Complete</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col-sm-6 col-md-3 top-data"    ng-hide="!dashboardData.counts[0].total_vendors">
                                          <div class="panel orders"  >
                                             <a href="{{URL::to('v1/vendors')}}">
                                                <div class="panel-data  ">
                                                   <ul class="list-inline"  >
                                                      <li><i class=" fa fa-truck"></i> </li>
                                                      <li style="float:right">
                                                         <h2 class="top-value" style="margin:0px">@{{dashboardData.counts[0].total_vendors}}</h2>
                                                      </li>
                                                   </ul>
                                                   <ul class="list-inline" >
                                                      <li>
                                                         <h5 class=" text-uppercase">Total Restaurants </h5>
                                                      </li>
                                                      <li style="float:right">
                                                         <h2 class=" bottom-value" style=" margin-top:-6px">@{{dashboardData.counts[0].total_vendors}}</h2>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="progress progress-sm">
                                                   <div class="progress-bar bg-color" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                                      <span class="sr-only">80% Complete</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col-sm-6 col-md-3 top-data"    ng-hide="!dashboardData.counts[0].total_drivers">
                                          <div class="panel drivers"  >
                                             <a href="{{URL::to('v1/logistics')}}">
                                                <div class="panel-data  ">
                                                   <ul class="list-inline"  >
                                                      <li><i class=" fa fa-truck"></i> </li>
                                                      <li style="float:right">
                                                         <h2 class="top-value" style="margin:0px">@{{dashboardData.counts[0].total_drivers}}</h2>
                                                      </li>
                                                   </ul>
                                                   <ul class="list-inline" >
                                                      <li>
                                                         <h5 class=" text-uppercase">Total Drivers </h5>
                                                      </li>
                                                      <li style="float:right">
                                                         <h2 class=" bottom-value" style=" margin-top:-6px">@{{dashboardData.counts[0].total_drivers}}</h2>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="progress progress-sm">
                                                   <div class="progress-bar bg-color" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                                      <span class="sr-only">80% Complete</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                       <div class="col-sm-6 col-md-3 top-data"    ng-hide="!dashboardData.counts[0].total_store">
                                          <div class="panel drivers"  >
                                             <a href="{{URL::to('v1/stores')}}">
                                                <div class="panel-data  ">
                                                   <ul class="list-inline"  >
                                                      <li><i class=" fa fa-truck"></i> </li>
                                                      <li style="float:right">
                                                         <h2 class="top-value" style="margin:0px">@{{dashboardData.counts[0].total_store}}</h2>
                                                      </li>
                                                   </ul>
                                                   <ul class="list-inline" >
                                                      <li>
                                                         <h5 class=" text-uppercase">Total Stores </h5>
                                                      </li>
                                                      <li style="float:right">
                                                         <h2 class=" bottom-value" style=" margin-top:-6px">@{{dashboardData.counts[0].total_store}}</h2>
                                                      </li>
                                                   </ul>
                                                </div>
                                                <div class="progress progress-sm">
                                                   <div class="progress-bar bg-color" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%">
                                                      <span class="sr-only">80% Complete</span>
                                                   </div>
                                                </div>
                                             </a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12 col-md-12 col-lg-12"  >
                                    <div class="row" >
                                        <!-----------------------PIE CHARTS FOR  ORDERS BLOCK--------------------------------->
                                       <div class="col-sm-12 col-md-6 col-lg-6" ng-repeat="item in dashboardData.blocks"  > 
                                          <div class="panel order"   style="height:450px;padding:8px 0 ;" ng-repeat="items in item" ng-show="items.type=='orders_count'"  >
                                             <h3 class="block-title" style="padding:8px 25px ;">Orders</h3>
 
                                             <hc-pie-chart   data="pieData" ng-show="items.data[0]">Placeholder for pie chart</hc-pie-chart>
                                              <div class="container-fluid innerData" ng-show="!items.data[0]">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br> 
												    <img class="img-responsive center-block" src="{{URL::to('web-food/assets/images/placeholders/orders-placeholder.png')}}">
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>
                                          </div>
                                       </div>
                                       <!-----------------------PIE CHARTS FOR  ORDERS BLOCK--------------------------------->
 
                                       
                       
                                        <!----------------------------ORDERS CHART BLOACK STARTS HERE-----------------------------> 
                                       <div class="col-sm-12 col-md-6 col-lg-6" ng-repeat="item in dashboardData.blocks">
                                          <div class="panel order"  ng-repeat="items in item" ng-show="items.type=='orders'"  >
                                             <h3 class="block-title">Orders</h3>
                                             <h4>@{{items.desc}}</h4>
                                             <hc-chart options="ordersChartOptions">Placeholder for generic chart</hc-chart> 
                                          </div>
                                       </div>
                                         <!----------------------------ORDERS CHART BLOACK ENDS HERE-----------------------------> 
                                    </div>

                                       
                                    <div class="row" >
									 <!-----------------------TASKS BLOCK STARTS HERE--------------------------------->
                                       <div class="col-sm-12 col-md-6 col-lg-6" ng-repeat="item in dashboardData.blocks" >
                                          <div class="panel top-users" ng-repeat="items in item" ng-show="items.type=='tasks'" style="height:450px;">
                                             <h3 class="block-title">Tasks</h3>
                                             <table class="table items-detail table-responsive">
                                                <thead>
                                                   <tr  >
                                                      <th >ORDER ID</th>
                                                      <th> PICKUP DETAIL</th>
                                                      <th>DROPOFF DETAIL</th>
                                                      <th >CREATED</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <tr ng-repeat="items in items.data.data">
                                                      <td class="user-image" style="width:17%"><a href="{{ URL::to('v1/order_detail')}}/@{{items.order_id}}">#@{{items.order_id }}</a></td>
                                                      <td class="user-name" style="width:50%">
                                                         @{{items.pickup_contact_title}} <br>@{{items.pickup_address}} <br>@{{items.pickup_time}} 
                                                      </td>
                                                      <td class="user-createdTime text-left" style="width:50%">  @{{items.dropoff_contact_title}} <br>@{{items.dropoff_address_string}} <br>@{{items.dropoff_time}} </td>
                                                      <td class="user-createdTime text-right" style="width:100%">
                                                         <pre>@{{items.created_at_formatted}}</pre>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table> 
                                          
                                            <div class="text-center" ng-show="!items.data.data[0]"> 
                                                 <br> 
                                                <p>No Records found.</p>
                                            </div>
                                         
                                        
                                          </div>
                                       </div>
                                       <!-----------------------TASKS BLOCK ENDS HERE--------------------------------->
									 <!-----------------------RECENT ORDERS BLOCK--------------------------------->
                                       <div class="col-sm-12 col-md-6 col-lg-6"ng-repeat="item in dashboardData.blocks" >
                                          <div class="panel rOrders" ng-repeat="items in item" ng-show="items.type=='recent_orders'" style="height:450px;">
                                             <h3 class="block-title">Recent Orders</h3>
                                             <table class="table items-detail">
                                                <thead>
                                                   <tr  >
                                                      <th > ID</th>
                                                      <th> STATUS</th>
													  <th>CUSTOMER NAME</th>
                                                      <th>CREATED</th>
                                                      <th >TOTAL</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <tr ng-repeat="items in items.data ">
                                                      <td class="order-id">  <a href="{{ URL::to('v1/order_detail')}}/@{{items.order_id}}">#@{{items.order_id}} </a></td>
                                                      <td> <span class="state-color" style="background-color:@{{items.status_details[0].label_colors}}; color:#fff ">@{{items.order_status }}</span> </td>
													  <td>@{{items.customer_details[0].first_name}}</td>
                                                      <td>@{{items.created_at_formatted  }}</td>
                                                      <td class="order-total  "><?php echo @$currency_symbol;?> @{{items.total}}</td>
                                                   </tr>
                                                </tbody>
                                             </table>
											 <div class="text-center" ng-show="!items.data[0]"> 
                                                 <br> <br>
												   <img class="img-responsive center-block" src="{{URL::to('web-food/assets/images/placeholders/orders-placeholder.png')}}">
                                                <p>No Records found.</p>
                                            </div>
                                         
                                          </div>
                                       </div>
                                       <!-------------------------------RECENT ORDERS BLOACK ENDS HERE-------------------------------->
									 
                                     
                                     
                                    </div>

                                    <div class="row" >
									 <!-----------------------TOP USERS BLOCK STARTS HERE--------------------------------->
                                       <div class="col-sm-12 col-md-6 col-lg-6" ng-repeat="item in dashboardData.blocks" >
                                          <div class="panel top-users" ng-repeat="items in item" ng-show="items.type=='top_users'" style="height:450px;" >
                                             <h3 class="block-title">Top Users</h3>
                                             <table class="table items-detail">
                                                <tbody>
                                                   <tr ng-repeat="item in items.data">
                                                      <td class="user-image">
													  <img src="{{URL::to('admin/assets/images/boy.png')}}" style="width:50px;" ng-show="!item.photo">
														 <img src="{{URL::to('images/users')}}/@{{item.photo}}" style="width:45px;height:45px;" ng-show="item.photo">
														 </td>
                                                      <td class="user-name">
                                                         <a href="{{url::to('v1/customer-profile')}}/@{{item.user_id}}">@{{item.first_name}} @{{item.last_name}}</a><br>
                                                         <pre class="user-desc">@{{item.email}}</pre>
                                                      </td>
                                                      <td class="user-createdTime text-right">@{{item.total_orders }} Orders </td>
                                                      <td class="user-createdTime text-right">
                                                         <pre>@{{item.created_at_formatted}}</pre>
                                                         <span>@{{item.user_type_title }}</span>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                              <div class="container-fluid innerData" ng-show="!items.data[0]">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br> 
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>
                                          </div>
                                       </div>
                                        <!-----------------------TOP USERS BLOCK ENDS HERE--------------------------------->
                                       <!----------------------RECENT USERS BLOCK STARTS HERE--------------------------------->
                                       <div class="col-sm-12 col-md-6 col-lg-6" ng-repeat="item in dashboardData.blocks" >
                                          <div class="panel rUsers" ng-repeat="items in item" ng-show="items.type=='recent_users'" style="height:450px;">
                                             <h3 class="block-title">Recent Users</h3>
                                             <table class="table items-detail">
                                                <tbody>
                                                   <tr ng-repeat="item in items.data">
                                                      <td class="user-image"> 
													  <img src="{{URL::to('admin/assets/images/boy.png')}}" style="width:50px;" ng-show="!item.photo">
														 <img src="{{URL::to('images/users')}}/@{{item.photo}}" style="width:45px;height:45px;" ng-show="item.photo">
														 </td>
                                                      <td class="user-name">
                                                         <a href="{{url::to('v1/customer-profile')}}/@{{item.user_id}}">@{{item.first_name}} @{{item.last_name}}</a><br>
                                                         <pre class="user-desc">@{{item.email}}</pre>
                                                      </td>
                                                      <td class="user-createdTime text-right">
                                                         <pre>@{{item.created_at_formatted}}</pre>
                                                         <span>@{{item.user_type_title }}</span>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
											 
											 <div class="text-center" ng-show="!items.data[0]"> 
                                                 <br> 
                                                <p>No Records found.</p>
                                            </div>
                                         
                                          </div>
                                       </div>
                                       <!-----------------------RECENT USERS BLOCK ENDS HERE--------------------------------->

                                      
                                       
                                    </div>

                                 </div>
                              </div>
                              <!-----------------------------------------------STORE BUSY ON OFF MODAL STARTS HERE-------------------------------------------------------------------->
                              <div id="myModal" class="modal fade" data-backdrop="false" style="z-index:20" role="dialog">
                                 <div class="modal-dialog modal-lg category-add"  >
                                    <!-- Modal content-->
                                    <div class="modal-content "   >
                                       <div class="modal-header  "  >
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title text-uppercase" > Add <b>Busy Store</b> Data</h4>
                                       </div>
                                       <div class="modal-body "  >
                                          <div class="row"   >
                                             <div class="col-sm-6">
                                                <div class="form-group" >
                                                   <label for="title"> Start Time</label>
                                                   <input type="text" mdc-datetime-picker="" date="false" time="true" class="form-control  md-input" name="start_time" id="start_time"  ng-model="start_time"  placeholder="Enter Start time"   format="YYYY-MM-DD HH:mm:ss" required readonly="readonly"> 
                                                </div>
                                                <div class="form-group" >
                                                   <label for="title"> End Time</label>
                                                   <input type="text" mdc-datetime-picker="" date="false" time="true" class="form-control  md-input" name="end_time" id="end_time"  ng-model="end_time"   value=""  placeholder="Enter End time"  format="YYYY-MM-DD HH:mm:ss" required readonly="readonly"> 
                                                </div>
                                                <br>                    
                                                <md-input-container  class="md-block">
                                                   <label>Reason</label>
                                                   <textarea id="reason" name="reason" rows="5" max-rows="5"  md-no-autogrow > </textarea>
                                                </md-input-container>
                                             </div>
                                             <div class="col-sm-6 text-right">
                                                <md-button ng-click="storeStatusBusy()" class="md-raised bg-color md-submit" style="bottom:0px;">Submit</md-button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-----------------------------------------------STORE BUSY ON OFF MODAL ENDS HERE-------------------------------------------------------------------->
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/dashboard.js')}}"></script> 
<!----assets for pdf download--------->
<!------>
@endsection