@extends('admin.layout.auth')
@section('title', 'Timeslots' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        @include('admin.includes.realtime-search')
                     </div>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                        <div class="tab-content"  ng-controller="timeslotsController as ctrl" ng-cloak  >
                           <!--------------------------- Angular App Starts ---------------------------->
                           <textarea id="res" style="display: none   ;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-10" >
                                    <h2 class="header">Timeslots</h2>
                                 </div>
                                 <div class="col-sm-2">
                                    <div class="text-right"> 
                                    </div>
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12" >
                                    <div class="" >
                                       <ul class="nav nav-tabs" >
                                          <li ng-repeat="data in ctrl.timeslots.data" ng-class="{active: $index == selected}" ng-click="changeTab();"><a href="#@{{data.day}}" data-toggle="tab" ng-click="tab = @{{$index}}">@{{data.day}}</a></li>
                                       </ul>
                                       <div class="tab-content  tab-content-data" >
                                          <div id="@{{data.day}}" class="tab-pane fade in " ng-class="{active: $index == selected}" ng-repeat="data in ctrl.timeslots.data" >
                                             <div class="panel">
                                                <div class="panel-body user-info">
												  <div class="text-right"> 
                                                      <button ng-click="add($index,data.day)" class="btn md-raised bg-color md-add md-button md-ink-ripple"> Add New</button> 
													 </div>
													 
                                                   <table class="table   tableAdd">
                                                      <tbody >
                                                         <tr  class="" ng-repeat="values in data.slots">
                                                            <td  style="border:0px;" > 
															  <input type="hidden" class="form-control" name="id@{{ $index }}@{{ data.day}} " id="id " ng-model="values.id"   value="@{{values.id}}" style="width:80%"  > 
                                                               <div class="form-group" >
                                                                  <label for="title"> Start Time</label>
                                                                  <input type="text" mdc-datetime-picker="" date="false" time="true" class="form-control  md-input" name="from_time@{{ $index }}@{{ data.day}} " id="from_time1 " ng-model="values.from_time"   value="" style="width:80%" placeholder="Enter Start time" min-date="minDate" format="HH:mm:ss" required readonly="readonly"> 
                                                               </div>
															   
															   

                                                            </td  >
                                                            <td  style="border:0px;" >
															<input type="hidden" class="form-control" name="weekday@{{ $index }}@{{ data.day}} " id="weekday " ng-model="values.weekday"   value="@{{values.weekday}}" style="width:80%"  > 
                                                               <div class="form-group"  >
                                                                  <label for="value">  End Time  </label>
                                                                  <input type="text"mdc-datetime-picker="" date="false" time="true" class="form-control  md-input"  name="to_time@{{ $index }}@{{ data.day}}" id="to_time1" ng-model="values.to_time"  value=""  style="width:80%" placeholder="Enter end time" min-date="minDate" format="HH:mm:ss" required readonly="readonly"> 
                                                               </div>
                                                            </td>
                                                            <td style="border:0px;"> 
															    <button ng-click="ctrl.deleteTimeslot(values.id)"  class="btn   btn-add md-raised  md-add md-button md-ink-ripple" style="margin-top:18px;">Delete</button> 
															<button ng-click="ctrl.updateTimeslot($index,data.day)"  class="btn btn-save btn-add md-raised bg-color md-add md-button md-ink-ripple">Update</button> 
                                                            </td>
                                                         </tr>
														 
														 
														 <tr  class="" id="newSlotRow" ng-show="showval">
                                                            <td  style="border:0px;" ng-repeat="values in ctrl.timeslots.form_data">  
                                                               <div class="form-group" >
                                                                  <label for="title">@{{values.title}}</label>
                                                                  <input type="@{{values.type}}" mdc-datetime-picker="" date="false" time="true" class="form-control  md-input" name="@{{values.identifier}} " id="@{{values.identifier}} " ng-model="values.value"   value="" style="width:80%" placeholder="Enter @{{values.title}}" min-date="minDate" format="HH:mm:ss" required readonly="readonly"> 
                                                               </div> 
                                                            </td  >
                                                            <!--<td  style="border:0px;" >
                                                               <div class="form-group"  >
                                                                  <label for="value">  End Time  </label>
                                                                  <input type="text"mdc-datetime-picker="" date="false" time="true" class="form-control  md-input"  name="to_time@{{ $index }}@{{ data.day}}" id="to_time" ng-model="values.to_time"  value=""  style="width:80%" placeholder="Enter end time" min-date="minDate" format="HH:mm:ss" required readonly="readonly"> 
                                                               </div>
                                                            </td>-->
                                                            <td style="border:0px;"> 
                                                               <button ng-click="ctrl.storeTimeslot($index,data.day)"  class="btn btn-save btn-add md-raised bg-color md-add md-button md-ink-ripple">Save</button>
                                                            </td>
                                                         </tr>
														 </tr>
                                                      </tbody>
													  
													  
                                                   </table>
												
												    <div class="container-fluid innerData" ng-show="!data.slots[0] && showval == false">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br> 
												  <img src="{{URL::asset('web-food/assets/images/placeholders/address-placeholder.png')}}" class="img-responsive center-block"  >
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>

                                                </div>
                                             </div>
                                          </div>
										  
										   
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------- Angular App Ends ---------------------------->
                         
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/timeslots.js')}}"></script> 
@endsection