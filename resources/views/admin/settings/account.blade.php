@extends('admin.layout.auth')
@section('title', 'Account' ) 
	<link href="{{URL::to('admin/assets/editor/bootstrap3-wysihtml5.min.css')}}">
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">

<style type="text/css" media="screen">
  .btn.jumbo {
    font-size: 20px;
    font-weight: normal;
    padding: 14px 24px;
    margin-right: 10px;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px;
  }
  ul.wysihtml5-toolbar {
    margin: 0;
    padding: 0;
  display: block;}
  
  ul.wysihtml5-toolbar>li{
      float: left;
    display: list-item;
    list-style: none;
    margin: 0 5px 10px 0;
	}
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper" >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      @section('sidebar')
      @include('admin.includes.sidebar')
      @show
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper"  >
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
                     @include('admin.includes.realtime-search')
                  </div>
                  <div class="col-sm-12">
                     <div class="text-right">
                     </div>
                     <div class="tab-content"   >
					  <input type="file" id="file" style="display: none "/>
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!--------------------------- Angular App Starts ---------------------------->
                        <textarea id="res" style="display:  none  ;" ></textarea>
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <div  >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-10" >
                                    <h2 class="header"> </h2>
                                 </div>
                                 <div class="col-sm-2">
                                    <div class="text-right">  
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="container-fluid" ng-cloak>
                              <div class="row">
                                 <div class="col-sm-12" > 
									<!------------------------NG VIEW STARTS HERE------------------------->
								     <div ng-view></div> 
									<!------------------------NG VIEW ENDS HERE--------------------------->
                                 </div>
                              </div> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
         </section>
         </div>
      </div>
   </div>
</div>

 <!-- Include external CSS. -->
   
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/account.js')}}"></script> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
   function changeProfile() {
       $('#file').click();
   }
   $('#file').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-users')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
					 
                    //$('#preview_image').attr('src', '{{asset('images/users/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
                    var data = data.filename; 
                 $('#item_photo').val(data).trigger("change");
   
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
   
               
               //document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '{{asset('images/users')}}/' + data);
					$('#preview_image1').attr('src', '{{asset('images/users')}}/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
					 
                alert(xhr.responseText);
                //$('#preview_image').attr('src', '{{asset('admin/assets/images/placeholder.jpg')}}');
            }
        });
    }
   
   
   
   
   function removeFile() {
       if ($('#item_photo').val() != '')
		   alert("{{url('ajax-remove-image-users')}}/" + $('#item_photo').val());
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '{{csrf_token()}}');
               $.ajax({
                   url: "{{url('ajax-remove-image-users')}}/" + $('#item_photo').val(),
                   data: '',
                   type: 'DELETE',
                   contentType: false,
                   processData: false,
                   success: function (data) {
					   alert('s');
                       $('#preview_image').attr('src', '{{asset('assets/images/placeholder.jpg')}}');
                       $('#item_photo').val('');
                       $('#loading1').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
   
   
   
   
</script>

 
  
  
  
@endsection