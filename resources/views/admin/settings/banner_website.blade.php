@extends('admin.layout.auth')
@section('title', 'Upload Banners' )
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
<style>
   .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
   }
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper"  >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
                        @include('admin.includes.realtime-search')
                     </div>
                     <?php          $auth_user_type = Auth::user()->user_type;   
                        $auth_user_id =   Auth::id();  
                        
                        ?>
                     <div class="col-sm-12">
                        <div class="text-right">
                        </div>
                        <div class="tab-content"   >
                           <textarea id="res" style="display:  none  ;" ></textarea>
                           <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                              <p >Calling all data...</p>
                           </div>
                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <!--------------------------- Angular App Starts ---------------------------->
                           <div ng-controller="bannerController" ng-cloak>
                              <div class="container-fluid" >
                                 <div class="row">
                                    <div class="col-sm-10" >
                                       <h2 class="header">Upload Banner</h2>
                                    </div>
                                    <div class="col-sm-2">
                                       <div class="text-right">
                                          <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>  
                                       </div>
                                    </div>
                                 </div>
                                 <div class="row" >
                                    <div class="col-sm-12" >
                                       <div class="" >
                                          <div id="tableToExport" class="products-table table-responsive"  >
                                             <table class="table" class="table table-striped" id="exportthis" >
                                                <thead>
                                                   <tr>
                                                      <th>ID</th> 
                                                      <th>BANNER PHOTO</th>
                                                      <th>CREATED</th>
                                                      <th id="action">ACTION</th>
                                                   </tr>
                                                </thead>
                                                <tbody>
                                                   <tr ng-repeat="values in banners.data">
                                                      <td>#@{{values.id}}</td> 
                                                      <td><img style="height:50px;width:50px" class="img-responsive center-block" ng-hide="!values.photo" src="{{URL::asset('/images/banners')}}/@{{values.photo}}" />
                                                         <img style="height:50px;width:50px"class="img-responsive center-block"ng-show="!values.photo" src="{{URL::asset('/admin/assets/images/placeholder.jpg')}}" />
                                                      </td>
                                                      <td>@{{values.created_at_formatted}}</td>
                                                      <td> 
                                                         <a class="btn btn-xs delete-product" ng-click="deleteBanner(values.id, $index)"><i class="fa fa-trash"></i></a>              
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>

                                             <div class="container-fluid innerData" ng-show="!banners.data[0]">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br> 
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>


                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!--------------------------- Angular App Ends ---------------------------->
							  
							  
							  
                              <!-----------------------------------------------BANNER ADD MODAL STARTS HERE-------------------------------------------------------------------->
                              <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
                                 <div class="modal-dialog modal-lg category-add"  >
                                    <!-- Modal content-->
                                    <div class="modal-content "   >
                                       <div class="modal-header  "  >
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title text-uppercase"  >Add <b>Banner</b></h4>
                                       </div>
                                       <div class="modal-body "  >
                                          <div class="row"   >
                                             <div class="col-sm-10 col-sm-offset-1">
                                                <div class="img-upload" style="background:#f5f5f5;">
                                                   <div class="form-group text-center"   >
                                                      <input type="file" id="file" style="display:none "/> 
                                                      <input type=" " ng-model="item_photo" value="@{{item_photo}}" id="item_photo" style="display:none;"/>
                                                      <div style="text-align: center;position: relative;    width: 100%;" id="image">
                                                         <img width="100%"  id="preview_image" src="{{asset('admin/assets/images/banner-placeholder.png')}}" style="padding:30px 40px; width:100%"/> 
                                                         <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="text-right" style="font-size: " >
                                                   <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;position:absolute;bottom: 40px; right:50px">
                                                   <i class="fa fa-edit"></i> 
                                                   </a>&nbsp;&nbsp; 
                                                </div>
                                             </div>
                                             <div class="col-sm-12 text-center">
                                                
                                                
                                                <md-button ng-click="store_banner(data)" class="md-raised bg-color md-submit" style="bottom:0px">Add</md-button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-----------------------------------------------BANNER ADD MODAL ENDS HERE-------------------------------------------------------------------->
							  
							  
							  
							    <!-----------------------------------------------BANNER edit MODAL STARTS HERE-------------------------------------------------------------------->
                              <div id="editBan" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
                                 <div class="modal-dialog modal-lg category-add"  >
                                    <!-- Modal content-->
                                    <div class="modal-content "   >
                                       <div class="modal-header  "  >
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title text-uppercase"  >Edit <b>Banner</b></h4>
                                       </div>
                                       <div class="modal-body "  >
                                          <div class="row"   >
                                             <div class="col-sm-10 col-sm-offset-1">
                                                <div class="img-upload" style="background:#f5f5f5;">
                                                   <div class="form-group text-center"   >
                                                      <input type="file" id="file" style="display:none "/> 
                                                      <input type=" " ng-model="item_photo1" value="@{{item_photo}}" id="item_photo1" style="display:none;"/>
                                                      <div style="text-align: center;position: relative" id="image">
                                                         <img width="100%"  id="preview_image1" src="{{asset('admin/assets/images/banner-placeholder.png')}}" style="padding:40px 60px;" ng-hide="item_photo1"/>
                                                         <img class="img-responsive"   id="preview_image2" src="<?php echo env('APP_URL')."/images/banners/";?>@{{item_photo1}}"  ng-hide="!item_photo1" style="background:#f5f5f5;border:1px solid #f5f5f5;min-height:200px;width:100%"/>
                                                         <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="text-right" style="font-size: " >
                                                   <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;position:absolute;bottom: 40px; right:50px">
                                                   <i class="fa fa-edit"></i> 
                                                   </a>&nbsp;&nbsp; 
                                                </div>
                                             </div>
                                             <div class="col-sm-12 text-center">
                                                
                                                <md-button ng-click="update_banner()" class="md-raised bg-color md-submit" style="bottom:0px">Update</md-button>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <!-----------------------------------------------BANNER EDIT MODAL ENDS HERE-------------------------------------------------------------------->
                           </div>
                        </div>
                     </div>
                  </div>
            </section>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- SCRIPTS -->
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/bannerWeb.js')}}"></script>  
<!-- JavaScripts -->  
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
   function changeProfile() {
       $('#file').click();
   }
   $('#file').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-banner')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/banners/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
                    var data = data.filename;    
                 $('#item_photo').val(data).trigger("change");
    $('#item_photo1').val(data).trigger("change");
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
   
               
               //document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '{{asset('images/banners')}}/' + data);
   		$('#preview_image1').attr('src', '{{asset('images/banners')}}/' + data);
		$('#preview_image2').attr('src', '{{asset('images/banners')}}/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/logo/noimage.jpg')}}');
            }
        });
    }
   
    
   function removeFile() {
       if ($('#item_photo').val() != '')
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '{{csrf_token()}}');
               $.ajax({
                   url: "ajax-remove-image-banner/" + $('#item_photo').val(),
                   data: form_data,
                   type: 'POST',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                       $('#item_photo').val('');
                       $('#loading1').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
   
   
   
   
</script>
@endsection