@extends('admin.layout.auth')
@section('title', 'Areas' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
 <style>
 .ui-autocomplete {
z-index: 999999 !important;} 
 </style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper" >
<div id="layout-static">
   <!---------- Static Sidebar Starts------->			
   @section('sidebar')
   @include('admin.includes.sidebar')
   @show
   <!---------- Static Sidebar Ends------->
   <div class="static-content-wrapper"  >
      <section id="main-header">
         <div class="container-fluid">
            <div class="row">
               <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                  @include('admin.includes.realtime-search')
               </div>
               <div class="col-sm-12">
                  <div class="text-right">
                  </div>
                  <div class="tab-content"  ng-controller="areaController as ctrl" ng-cloak  >
                     <!--------------------------- Angular App Starts ---------------------------->
                     <textarea id="res" style="display:  none  ;" ></textarea>
                     <div id="loading" class="loading" style="display:none ;">
                        <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                        <p >Calling all data...</p>
                     </div>
                     <div class="container-fluid" >
                        <div class="row">
                           <div class="col-sm-10" >
                              <h2 class="header">Areas</h2>
                           </div>
                           <div class="col-sm-2">
                              <div class="text-right">
                                 <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>  
                              </div>
                           </div>
                        </div>
                        <div class="row" >
                           <div class="col-sm-12" >
                              <div class="" >
                                 <div id="tableToExport" class="products-table table-responsive"  >
                                    <table class="table" class="table table-striped" id="exportthis" >
                                       <thead>
                                          <tr>
                                             <th>ID</th>
                                             <th>TITLE</th>
                                             <th>LOCATION</th>
                                             <th>PINCODE</th>
                                             <th>CREATED</th>
                                             <th id="action">ACTION</th>
                                          </tr>
                                       </thead>
                                       <tbody>
                                          <tr ng-repeat="values in ctrl.areas.data.data  ">
                                             <td>#@{{values.area_id}}</td>
                                             <td>@{{values.title}}</td>
                                             <td>Lat : @{{values.latitude}}<br> Lon : @{{values.longitude}}</td>
                                             <td>@{{values.pincode}}</td>
                                             <td>@{{values.created_at_formatted}}</td>
                                             <td>
                                                <a class="btn btn-xs edit-product" ng-click="ctrl.editArea(values, $index)" ><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-xs delete-product" ng-click="ctrl.deleteArea(values.area_id, $index)"><i class="fa fa-trash"></i></a>               
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>

                                    <div class="container-fluid innerData" ng-show="!ctrl.areas.data.data">
                                          <div class="row">
                                            <div class="col-sm-12"> 
                                                 <br>
                                                <img class="img-responsive center-block" src="web-food/assets/images/placeholders/address-placeholder.png">
                                                <p>No Address found.</p>
                                            </div>
                                          </div>
                                        </div>
                                 </div>
								  <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.areas.data.first_page_url != null"ng-click="ctrl.pagination(ctrl.areas.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.areas.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.areas.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.areas.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.areas.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.areas.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.areas.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.areas.data.last_page_url);">Last</button> 
                                       </div>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                     <!--------------------------- Angular App Ends ---------------------------->
                     <!-----------------------------------------------AREA ADD MODAL STARTS HERE-------------------------------------------------------------------->
                     <div id="add" class="modal fade" data-backdrop="false"  style="z-index:20;" role="dialog">
                        <div class="modal-dialog modal-lg address-add"  >
                           <!-- Modal content-->
                           <div class="modal-content "   >
                              <div class="modal-header  "  >
                                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                                 <h4 class="modal-title text-uppercase"  >Add <b>Area</b></h4>
                              </div>
                              <div class="modal-body "  >
                                 <div class="row"   >
                                    <div class="col-sm-12 col-md-6">
                                       <div class="form-horizontal"  >
                                          <h5 class="">Search</h5>
                                          <div class="search-input">
                                             <input type="text" class="form-control" id="us3-address" placeholder="Type area, city or street"  />
                                          </div>
                                          <br>
                                          <!--Map by element. Also it can be attribute-->
                                          <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;" ></locationpicker>
                                          <div class="clearfix" >&nbsp;</div>
                                         
                                          <div class="clearfix"></div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <div class="row">
                                    <div class="col-sm-12">
									 <div class="m-t-small  "> 
                                            <h4><b>Coordinates</b></h4>
										   <label> Latitude :</label> <input type="text" class="form-control"   id="us3-lat"  readonly style="width:80%"> <br>
                                            <label> Longitude :</label><input type="text" class="form-control"   id="us3-lon" readonly style="width:80%"/>
                                          </div><br>
									</div>
                                          <div class="col-sm-12">
                                             <md-input-container  class="md-block">
                                                <label>Title</label>
                                                <input type="text"  id="title" name="title"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12">
                                             <md-input-container  class="md-block">
                                                <label>Pincode</label>
                                                <input type="text"  id="pincode" name="pincode"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12">
                                             <md-button ng-click="ctrl.storeArea()" class="md-raised bg-color md-submit">Add Area</md-button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   </div>
                           </div>
                           <!-----------------------------------------------AREA ADD MODAL ENDS HERE-------------------------------------------------------------------->
                           <!-----------------------------------------------AREA EDIT MODAL STARTS HERE-------------------------------------------------------------------->
                           <div id="edit" class="modal fade" data-backdrop="false" style="z-index:20" role="dialog">
                              <div class="modal-dialog modal-lg address-add"  >
                                 <!-- Modal content-->
                                 <div class="modal-content "   >
                                    <div class="modal-header  "  >
                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       <h4 class="modal-title text-uppercase"  >Edit <b>Area</b></h4>
                                    </div>
                                    <div class="modal-body "  >
                                       <div class="row"   >
                                          <div class="col-sm-12 col-md-6">
                                       <div class="form-horizontal"  >
                                          <label class="control-label">Search</label>
                                          <div class="search-input">
                                             <input type="text" class="form-control" id="us3-address_edit" placeholder="Type area, city or street"  />
                                          </div>
                                          <br>
                                          <!--Map by element. Also it can be attribute-->
                                          <locationpicker options="ctrl.editLocationpickerOptions" style="width:98%;padding:20px 5px;" ></locationpicker>
                                          <div class="clearfix" >&nbsp;</div>
                                         
                                          <div class="clearfix"></div>
                                       </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                       <div class="row">
                                          
										  <div class="col-sm-12">
										     <div class="m-t-small  "> 
										  <h4><b>Coordinates</b></h4>
										  
                                             <label>Latitude :</label> <input type="text" class="form-control"   id="us3-lat_edit"  ng-model="ctrl.latitude" style="width:80%" readonly ><br>
                                            <label> Longitude :</label> <input type="text" class="form-control"   id="us3-lon_edit" ng-model="ctrl.longitude" readonly style="width:80%"/>
                                          </div><br>
										  </div>
										  
                                          <div class="col-sm-12">
                                             <md-input-container  class="md-block">
                                                <label>Title</label>
                                                <input type="text"  ng-model="ctrl.area_title" id="title_edit" name="title_edit"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12">
                                             <md-input-container  class="md-block">
                                                <label>Pincode</label>
                                                <input type="text"  ng-model="ctrl.pincode" id="pincode_edit" name="pincode_edit"/>
                                             </md-input-container>
                                          </div>
                                          <div class="col-sm-12"> 
                                             <md-button ng-click="ctrl.updateArea(ctrl.area_id)" class="md-raised bg-color md-submit">Update Area</md-button>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
						   </div>
                     </div>
                           <!-----------------------------------------------AREA EDIT MODAL ENDS HERE-------------------------------------------------------------------->
                        </div>
                     </div>
                  </div>
               </div>
      </section>
      </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/areas.js')}}"></script> 
@endsection