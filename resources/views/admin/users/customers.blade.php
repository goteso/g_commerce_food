@extends('admin.layout.auth')
@section('title', 'Customers' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                       <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                                    @include('admin.includes.realtime-search')
                        </div>
                     <div class="col-sm-12">
					 
                        <textarea id="res" style="display:none;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						 
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Customers</h2>
                                 </div>


								  <div class="col-sm-5 text-right">
                                     <a href="{{URL::to('v1/customer')}}"><button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right" >Add New</button></a>
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12" ng-controller="userController as ctrl" ng-cloak >
                                    <div class="" >
                                       <br> 

                        
                                       <div id="tableToExport" class="products-table table-responsive "  >
                                          <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr> 
                                                   <th>ID</th>
                                                   <th>NAME</th>
                                                   <th>PHOTO</th>
                                                   <th>EMAIL</th>
                        												   <th>MOBILE</th>
                                                   <th>TYPE</th>
                        												   <th>BLOCK</th>
                                                   <th>CREATED</th>
                                                   <th>ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody >
                                                <tr  ng-repeat="values in ctrl.users.data.data  "> 
                                                   <td><a href="{{ URL::to('v1/customer-profile')}}/@{{values.user_id}}" ><b>#@{{values.user_id}}</b></a> </td>
                                                   <td>@{{values.first_name}} @{{values.last_name}}</td>
                                                   <td>

                                                  
  <img class="center-block" src="<?php echo url('/');?>/images/users/@{{values.thumb_photo}}" style="height:40px;width:40px" ng-hide="!values.thumb_photo"> <img class="img-responsive center-block" style="height:40px;width:40px" src="<?php echo url('/').'/admin/assets/images/img-placeholder.png';?>" ng-show="!values.thumb_photo"> </td>
                                                   </td>
                                                   <td > @{{values.email}} </td> 
                                                   <td > @{{values.phone}} </td>
                                                   <td >  
                                                      <select class="form-control"  ng-model="customer_type" id="customer_type@{{values.user_id}}" ng-change="update_customer_type(values.user_id , this , $index)">

                                                        <option value=""  >Select Customer Type</option>
                                                        <option value="regular" ng-selected="values.customer_type == 'regular'" >Regular</option>
                                                        <option value="celebrity" ng-selected="values.customer_type == 'celebrity'">Celebrity</option>
                                                        
                                                      </select>
                                                   </td>
                                                 <td ng-show="values.status == '1'"><a href="#" ng-click="blockUser(values.user_id, $index)" ><span style="color:red"> Block</span></a></td>
                                                 <td ng-show="values.status == '0'" style="color:red" ><a href="#" ng-click="unblockUser(values.user_id, $index)"  ><span style="color:green"> UnBlock</span></a></td>
                                                   
                                                   <td>@{{values.created_at_formatted}}</td>
                                                   <td class="actions">
                                                      <a class="btn btn-xs edit-product" href="{{ URL::to('v1/customer')}}/@{{values.user_id}}" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.deleteUser(values.user_id, $index)"><i class="fa fa-trash"></i></a>
                                                      <!--  <a class="btn btn-xs edit-product" href="{{ URL::to('product_edit')}}/@{{values.id}}" ><img src="{{url::to('admin/images/edit.png')}} " style="height:16px;width:16px"/></a>
                                                         <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><img src="{{url::to('admin/images/bin.png')}} " style="height:16px;width:13px"/></a>-->
                                                   
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>


                                            <div class="container-fluid innerData" ng-show="!ctrl.users.data.data">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br> 
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>

                                       </div> 
                                       <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.users.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.first_page_url);">First</button> 
                                           <button class="btn" ng-show="ctrl.users.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.users.data.current_page}}</span>
                                          <button class="btn" ng-show="ctrl.users.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="ctrl.users.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.last_page_url);">Last</button> 
                                       </div>



                                                               <!-- Modal -->
<div id="replyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Block User</h4>
      </div>
      <div class="modal-body">
         <input type="hidden" id="user_id">
        <p>Type a reason to block below</p>
        
        <textarea  id="message" placeholder="Enter your message here" rows="5" class="form-control"></textarea>

        <button style="margin-top:2%"class="btn btn-success" ng-click="block_user()" >Block</button>

             </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<div id="unblockModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">UnBlock User</h4>
      </div>
      <div class="modal-body">
         <input type="hidden" id="user_id_unblock">
        <p>Type a reason to UnBlock below</p>
        
        <textarea  id="message_unblock" placeholder="Enter your message here" rows="5" class="form-control"></textarea>

        <button style="margin-top:2%"class="btn btn-success" ng-click="unblock_user()" >UnBlock</button>

             </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>











                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                        


                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/customers.js')}}"></script> 

<!------>
@endsection