@extends('admin.layout.auth')
@section('title', 'Customer Enquiries' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
  
    @media(max-width:767px){ .table{table-layout:fixed}
 .table-responsive>.table>tbody>tr>td, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>td, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>thead>tr>th{ white-space:pre-wrap!important; width: 100px;  }
   .table>tbody>tr>td.desc,  .table>thead>tr>th.desc{   width: 200px!important;}
  }
   @media(min-width:768px){
   .table>tbody>tr>td.desc{   width: 50%!important;}
   }
 </style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            @include('admin.includes.realtime-search')
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
			
            <div class="tab-content"  ng-controller="FreshdeskEnquiriesController" ng-cloak  >




 

               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Enquiries</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                    <th >Sr</th> 
									 <th class="desc">Message</th>
                                       <th>User</th>
                                       <th>Created</th>
                                        <th>Reply</th>
										 
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in freshdesk_enquiries.data.data  "> 
                                       <td>@{{ $index + 1 }}</td>
										         <td class="desc">@{{ values.description }}</td>
                                       <td> @{{ values.user_details[0].first_name }} @{{ values.user_details[0].last_name }} </td>
                                       <td> @{{ values.created_at_formatted }}  </td>
                                       <td ng-show='values.status=="0"'><a href="#" ng-click="replyUser(values.id, $index)" >Reply</a></td>
                                       <td ng-show='values.status=="1"'> <span style="color:green">Completed</span></td>
									        
                                    
                                 </tbody>
                              </table>

                              <div class="container-fluid innerData" ng-show="!freshdesk_enquiries.data.data">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br>  
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>

                           </div>
						   <div class="pagination text-right" style="display:block" >
                                           <button class="btn" ng-show="reviews.data.first_page_url" ng-click="pagination(reviews.data.first_page_url);">First</button> 
                                           <button class="btn" ng-show="reviews.data.prev_page_url" ng-click="pagination(reviews.data.prev_page_url);">Previous</button> 
                                          <span>@{{reviews.data.current_page}}</span>
                                          <button class="btn" ng-show="reviews.data.next_page_url" ng-click=".pagination(reviews.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="reviews.data.last_page_url" ng-click="pagination(reviews.data.last_page_url);">Last</button> 
                                       </div>
						 
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
<!-- Modal -->
<div id="replyModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Enquiry Reply</h4>
      </div>
      <div class="modal-body">
         <input type="hidden" id="enquiry_id">
        <p>Type a message below</p>
        
        <textarea  id="message" placeholder="Enter your message here" rows="5" class="form-control"></textarea>

        <button style="margin-top:2%"class="btn btn-success" ng-click="reply()" >Reply</button>

             </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>






  
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>






<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/freshdesk_enquiries.js')}}"></script> 
@endsection