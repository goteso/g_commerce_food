@extends('admin.layout.auth')
@section('title', 'Restaurant Menu' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/app.css')}}">
<style>
  .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
.StepProgress {
  position: relative;
  padding-left: 20px;
  list-style: none;
}
  .StepProgress::before {
    display: inline-block;
    content: '';
    position: absolute;
    top: 2px;
    left: 0px;
    width: 10px;
    height: 50%;
    border-left: 1px solid #CCC;
  }
  
  .StepProgress-item {
    position: relative;
    counter-increment: list;
	      padding-left: 10px;
  }
    .StepProgress-item:not(:last-child) {
      padding-bottom: 10px;
    }
    
    .StepProgress-item-first::before {
      display: inline-block;
      content: '';
      position: absolute;
      left: -20px;
      height: 100%;
      width: 10px;
	    top: 2px;
    }
    
    .StepProgress-item::after {
      content: '';
      display: inline-block;
      position: absolute;
      top: 0px;
	   padding-top: 6px;
      left: -34px;
      width: 30px;
      height: 30px;
      border: 0px solid #CCC;
      border-radius: 50%;
      background-color: #D8E1ED;
    }
    
    
      .StepProgress-item.is-done::before {
        border-left: 2px dotted #D8E1ED;
      }
      .StepProgress-item.is-done::after {
        content: "P";
		 padding-top: 6px;
        font-size: 12px;
        color: #FFF;
        text-align: center;
        border: 0px solid #488de4;
        background-color: #D8E1ED;
      }
	  .unassigned .StepProgress-item.StepProgress-item-first.is-done::after {
        /*content: counter(list);*/
		content: "P";
        padding-top: 6px;
        width: 30px;
        height: 30px;
        top: 0px;
        left: -34px;
        font-size: 12px;
        text-align: center;
        color: #fff; 
        background-color: #D8E1ED; 
        border: 0px solid #488de4;
      }
    
     
      .StepProgress-item.current::before {
        border-left: 2px dotted #D8E1ED;
      }
      
      .StepProgress-item.current::after {
        /*content: counter(list);*/
		content: "D";
        padding-top: 6px;
        width: 30px;
        height: 30px;
        top: 2px;
        left: -35px;
        font-size: 12px;
        text-align: center;
        color: #fff;
        border: 0px solid #488de4;
        background-color: D8E1ED;
      }
	  
	  .completed .StepProgress-item.StepProgress-item-last.is-done::after { 
		content: "D"!important;
         font-size: 12px;
        color: #FFF;
        text-align: center;
        border: 0px solid #488de4;
        background-color: #D8E1ED;
      }
    
   
  
  strong {
    display: block;
  }
  
  md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}

  
  .container-2{
  width: auto;
  vertical-align: middle;
  white-space: nowrap;
  position: absolute!important;
  right:0;
}

.container-2 input#search{
	margin-top:5px;
  width: 50px;
  height: 30px;
  background: #fff;
  border: none;
  font-size: 10pt;
  float: right;
  color: #262626;
  padding-right: 35px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 0px;
  color: #fff;
 opacity:0;
  -webkit-transition: width .55s ease;
  -moz-transition: width .55s ease;
  -ms-transition: width .55s ease;
  -o-transition: width .55s ease;
  transition: width .55s ease;
}

.container-2 input#search::-webkit-input-placeholder {
   color: #65737e;
}
 
.container-2 input#search:-moz-placeholder { /* Firefox 18- */
   color: #65737e;  
}
 
.container-2 input#search::-moz-placeholder {  /* Firefox 19+ */
   color: #65737e;  
}
 
.container-2 input#search:-ms-input-placeholder {  
   color: #65737e;  
}

.container-2 .icon{
  position: absolute;
  right:0;
  top: 10%;
  margin-left: 10px;
  margin-top: 10px;
  z-index: 1;
  color: #fff;
}

.container-2 input#search:focus, .container-2 input#search:active{
  outline:none;
  width: 150px;
   color: #65737e;  
}
 
.container-2:hover input#search{
width: 150px;
  opacity:1;
}
 
.container-2:hover .icon{
  color: #fff;
}
</style>
@section('content')
@section('header')
@include('admin.includes.header')
@show
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
				  <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                                    @include('admin.includes.realtime-search')
                                 </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						 
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content" id="tab-content"  ng-controller="restaurantController as ctrl" ng-cloak>
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Restaurant </h2>
                                 </div>
                                  <div class="col-sm-5"  >
                                                      <h5>Select Store</h5>
                                                      <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedStore" md-search-text-change="ctrl.searchStoreChange(searchStore)" md-search-text="searchStore" md-selected-item-change="ctrl.selectedStoreChange(values, field)" md-items="values in ctrl.storeSearch(searchStore)" md-item-text="values.store_title" md-min-length="0" placeholder="Select Store " md-menu-class="autocomplete-custom-template"    >
                                                         <md-item-template>
                                                            <span class="item-title"> 
                                                            <span> @{{values.store_title}} </span> 
                                                            </span> 
                                                         </md-item-template>
                                                      </md-autocomplete>
                            <input type=" " id="store_id" name="store_id" ng-model="ctrl.store_id" value="@{{ctrl.store_id}}" style="display:none;"/>
                                                      <br> 
                                                   </div> 
                              </div>
                              <div class="row"   style="display: flex; ">
                                 
								 <div class="col-sm-12"style="padding:0px" >
								 
								  <!--------------------------TAB hEADER STRATS HERE-------------------------------->
									<ul class="nav nav-tabs" >
									   <li class="active"><a href="#menu" data-toggle="tab" ng-click="ctrl.menu();">Menu</a></li>
									   <li class=""><a href="#categories" data-toggle="tab"  ng-click="ctrl.categoriesFilter();">Categories</a></li>
									    <li class=""><a href="#items" data-toggle="tab" ng-click="ctrl.items();">Items</a></li> 
									</ul>
									<!--------------------------TAB HEADER ENDS HERE-------------------------------->
									<div class="tab-content  tab-content-data" >
									
									    <!-------------------------task panel starts here--------------->
									   <div id="menu" class="tab-pane fade in active" >
									      <div class=" user-info" >
										  
										  <div class="panel"> 
										    <div class="panel-body user-info">
										 <div class="row" ng-repeat="values in ctrl.stores"  ng-show="ctrl.stores">
										  
										 <div class="col-sm-3">
			   <img src="{{URL::asset('images/stores')}}/@{{values.store_photo}}" ng-show="values.store_photo" class="img-responsive">
			    <img src="{{URL::asset('web-food/assets/images/placeholders/store-placeholder.png')}}" ng-show="!values.store_photo" class="img-responsive">
				</div>
				 <div class="col-sm-3">
				
				<h3>@{{values.store_title}}</h3>
				</div>
				</div>
				  
				
				<div class="row" ng-show="ctrl.categoriesData">
				<div class="col-sm-12" >
				<hr>
				<h3>Categories</h3> 
				 <!-- <ul>
				  <li  ng-repeat="cat in ctrl.categoriesData.data">@{{cat.category_title}}</li>
				  </ul>-->
				  
				  <div class="row">
				      <div class="col-sm-12" ng-repeat="cat in ctrl.categoriesData.data">
				          <h4>@{{cat.category_title}}</h4>
				          
				          @{{cat}}
				          <ul>
				              <li ng-repeat="item in cat.items">@{{item.item_title}}</li>
				          </ul>
				      </div>
				  </div>
				</div>
				</div>
										
										  
										  
										  </div>
   
      </div>
									     </div>
									   </div>
									    <!----------------task panel ends here--------------------------->
									   <div id="categories" class="tab-pane fade"  > 
									     <div class="panel">
               <div class="panel-body user-info" >
			   <div >
			   <div class="row">
			    <div class="col-sm-12 text-right">
				   <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>
				</div>
			   </div>
			   <br>
									        <div id="tableToExport" class="products-table table-responsive" >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>CATEGORY NAME</th>
                                       <th>CATEGORY PHOTO</th>
                                       <th>Status</th>
									    <th>CREATED</th>
                                       <th id="action">ACTION</th>  
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.categories.data.data  ">
                                       <td>#@{{values.category_id}}</td>
                                       <td>@{{values.category_title}}</td>
									   <td>
										 <img style="height:40px;width:40px" class="img-responsive center-block" ng-hide="!values.category_photo" src="{{URL::asset('images/categories')}}/@{{values.category_photo}}" />
										 <img  style="height:40px;width:40px" class="img-responsive center-block" ng-show="!values.category_photo" src="{{URL::asset('admin/assets/images/placeholder.jpg')}}" />
									   </td>
					                   <td class="text-center" layout="row" layout-align="center center">   <md-switch class="text-center md-primary" ng-change="ctrl.switchStatus(values.status,values.category_id);" ng-model="values.status" value="@{{values.status}}"  ng-true-value="1 " ng-false-value="0"  >  </md-switch></td>
                                       <td>@{{values.created_at_formatted}}</td>
                                       


                                       <td>
                                          
                                        
                                            <a class="btn btn-xs edit-product" ng-click="ctrl.editCategory(values, $index)" ><i class="fa fa-edit"></i></a>
                                           
                                          
                                           <a class="btn btn-xs delete-product" ng-click="ctrl.deleteCategoryId(values.category_id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                      
                                    </tr>
                                    
                                 </tbody>
                              </table>
                              
                                <div class="container-fluid innerData" ng-show="!ctrl.products.data.data">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br>
                                                <img class="img-responsive center-block" src="../web-food/assets/images/placeholders/food-placeholder.png">
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>

                           </div>
                <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.categories.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.categories.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.categories.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.categories.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.categories.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.categories.data.last_page_url);">Last</button> 
                                       </div>
									   </div>
									   
									   
									   
									   </div>
									   </div>
	  
									   </div>
									   
									   
									   
									   
									    <div id="items" class="tab-pane fade"  > 
									        <div class="panel">
               <div class="panel-body user-info">
                   
                   <div class="row">
                         <div class="col-sm-12 text-right"  >
	        <a href="{{URL::to('v1/item')}}?regular"><button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right" >Add Regular Item</button></a>
            <a href="{{URL::to('v1/item')}}?catering"><button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right" >Add Catering Item</button></a>
	      
	  </div>
                   </div>
                  <div id="tableToExport" class="products-table table-responsive"  >
                                            <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr> 
                                                   <th>ID</th>
                                                   <th>TITLE</th>
                                                   <th>PRICE</th>
                                                   <th>PHOTO</th>
												    <th>STATUS</th>
                                                   <th>CREATED</th>
                                                   <th>ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody >
                                                <tr  ng-repeat="values in ctrl.products.data.data  "> 
                                                   <td><a href="{{URL::to('v1/item')}}/@{{values.item_id}}" ><b>#@{{values.item_id}}</b></a> </td>
                                                   <td>@{{values.item_title}}</td>
                                                   <td ><span ng-show='values.item_price > 0'><?php echo env("CURRENCY_SYMBOL", "");?>@{{values.item_price}}</span></td>
                                                   <td>

                                                      <img class="center-block" src="{{URL::asset('images/items/')}}@{{values.item_thumb_photo}}" style="height:40px;width:40px" ng-hide="!values.item_thumb_photo"> <img class="img-responsive center-block" style="height:40px;width:40px" src="<?php echo url('/').'/admin/assets/images/item-placeholder.png';?>" ng-show="!values.item_thumb_photo"> </td>

                                                    <td class="text-center" layout="row" layout-align="center center">   <md-switch class="text-center md-primary" ng-change="ctrl.switchStatus(values.item_active_status,values.item_id);" ng-model="values.item_active_status" value="@{{values.item_active_status}}"  ng-true-value="'active' " ng-false-value="'inactive'"  >    </md-switch></td>
                                                   <td>@{{values.created_at_formatted}}</td>
                                                   <td class="actions">
                                                      <a class="btn btn-xs edit-product" title="Edit" href="{{URL::asset('v1/item')}}/@{{values.item_id}}?@{{values.type}}" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" title="Delete" ng-click="ctrl.deleteItem(values.item_id)"><i class="fa fa-trash"></i></a>
                                                      <!--  <a class="btn btn-xs edit-product" href="{{ URL::to('product_edit')}}/@{{values.id}}" ><img src="{{url::to('admin/images/edit.png')}} " style="height:16px;width:16px"/></a>
                                                         <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><img src="{{url::to('admin/images/bin.png')}} " style="height:16px;width:13px"/></a>-->
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div> 
									   <div class="pagination text-right" style="display:block" >
                                           <button class="btn" ng-show="ctrl.products.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.products.data.first_page_url);">First</button> 
                                           <button class="btn" ng-show="ctrl.products.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.products.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.products.data.current_page}}</span>
                                          <button class="btn" ng-show="ctrl.products.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.products.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="ctrl.products.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.products.data.last_page_url);">Last</button> 
                                       </div>
									   
                                       
               </div>
            </div>
									   </div>
									</div>
									   
                                 <!--   <div ng-view></div>-->
                                 </div>
								 
								 
								  
  
     </div>
                           </div>
  
  
  
  
  
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Category</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
                  <div class="col-sm-6">
             

                     <div class="img-upload" style="background:#f5f5f5;">
                        <div class="form-group text-center"   >
            
              <input type="file" id="file" style="display:none "/>
                           <input type="" id="file_name" style="display:none "/>
                           <br><br>      
                           <div style="text-align: center;position: relative" id="image">
                              <img width="300px"  id="preview_image" src="{{URL::asset('admin/assets/images/placeholder.jpg')}}"/>
                              <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                           </div>
                           <br><br>
                           <p class="text-right">
                              <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;">
                              <i class="fa fa-edit"></i> 
                              </a>&nbsp;&nbsp; 
                           </p>
                           <br>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <h5>Parent Category</h5>
             <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="selectedItem" md-search-text-change="searchCategoryChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedCategoryChange(values)" md-items="values in ctrl.categorySearch(searchText)" md-item-text="values.category_title" md-min-length="0" placeholder="Select Parent Category"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> @{{values.category_title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
            <input type="hidden" id="category_id" name="category_id" ng-model="category_id" value="@{{category_id}}" />
                     
                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"     id="category_title" name="category_title"/>
                     </md-input-container>
                     
             
                     <md-button ng-click="ctrl.storeCategory()" class="md-raised bg-color md-submit" style="bottom:0px;">Add Category</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	
	
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="editCat" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Edit <b>Category</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
                  <div class="col-sm-6">
                      <div class="img-upload" style="background:#f5f5f5;">
                        <div class="form-group text-center"   >
            
              <input type="file" id="file" style="display:none "/>
                           <input type="" id="file_name1" style="display:none "/>
                           <br><br>      
                           <div style="text-align: center;position: relative" id="image">
                              <img    class="img-responsive center-block"  id="preview_image1" ng-show="!ctrl.category_photo" src="{{URL::asset('admin/assets/images/placeholder.jpg')}}"/>
                  <img    class="img-responsive center-block" id="preview_image2" ng-show="ctrl.category_photo"  src="{{URL::asset('images/categories')}}/@{{ctrl.category_photo}}" style="padding:0px 20px;"/> 
                              <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                           </div>
                           <br><br>
                           <p class="text-right">
                              <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;">
                              <i class="fa fa-edit"></i> 
                              </a>&nbsp;&nbsp; 
                           </p>
                           <br>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <h5>Parent Category</h5>
           <md-autocomplete ng-disabled="ctrl.isDisabled" md-no-cache="ctrl.noCache" md-selected-item="ctrl.parent_category_title" md-search-text-change="ctrl.searchEditCategoryChange(searchText)" md-search-text="searchText" md-selected-item-change="ctrl.selectedEditCategoryChange(data)" md-items="data in ctrl.editCategorySearch(searchText)" md-item-text="data.category_title" md-min-length="0" placeholder="Select Parent Category"  md-menu-class="autocomplete-custom-template">
                        <md-item-template>
                            <span class="item-title"> 
                                   <span> @{{data.category_title}} </span> 
                             </span> 
                        </md-item-template>
                     </md-autocomplete>
            <input type="hidden" id="category_edit_id" name="category_edit_id"  ng-model="ctrl.parent_id" value="@{{ctrl.parent_id}}" />
         
                     <br>
                     <md-input-container  class="md-block">
                        <label>Title</label>
                        <input type="text"  ng-model="ctrl.category_edit_title"  value="" id="category_edit_title" name="category_edit_title"/>
                     </md-input-container>
           
      
                     <br>
                     <md-button ng-click="ctrl.updateCategory(ctrl.category_id)" class="md-raised bg-color md-submit" style="bottom:0;">Update Category</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
	
	
                           
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/restaurants.js')}}"></script> 
<script>
 

$(function() {

$("#wrapper")
.width($(window).width()).height($(window).height());
});
</script>
<!------>
@endsection