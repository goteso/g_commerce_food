 
 <div class="static-sidebar-wrapper sidebar-default">
   <div class="static-sidebar">
      <div class="sidebar">
        <div class="widget stay-on-collapse" id="widget-sidebar">
            <nav role="navigation" class="widget-body">
               <ul class="acc-menu"> 
               
               <?php @$auth_user_type = @Auth::user()->user_type;   
                     @$auth_user_id =   @Auth::id();
                     App::setLocale('settings');  
                     if($auth_user_id =='' || $auth_user_id == null)
                     {
                           Auth::logout();
                           echo '<script>document.getElementById("logout").click();</script>';
                     }

                     if($auth_user_type == '4')
                     {
                     	$auth_store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;

                     }
                     else
                     {
                     	$auth_store_id = '';
                     }

                ?>
                  <input type="hidden" id="auth_user_type" value="<?php echo $auth_user_type;?>">
                  <input type="hidden" id="auth_user_id" value="<?php echo $auth_user_id;?>">
                  <input type="hidden" id="auth_store_id" value="<?php echo $auth_store_id;?>">

   
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'dashboard_data') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('/v1/dashboard_data') }}"><i class="fas fa-tachometer-alt"></i><span>Dashboard  </span></a></li>
 	         
 

                    @if(trans('permission'.$auth_user_type.'.view_orders') == '1')
				    <li  class=" <?php if(request()->segment(count(request()->segments())) == 'orders#/') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/orders#/regular') }}"><i class="fa fa-shopping-cart" ></i><span>Orders</span> </a></li>
				   @endif


				   @if(trans('permission'.$auth_user_type.'.view_logistics') == '1')
 
			       <li class=" <?php if(request()->segment(count(request()->segments())) == 'logistics') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/logistics') }}" ><i class="fa fa-truck"></i><span>Delivery Boy</span></a></li>
 
		           @endif

  
				    @if(trans('permission'.$auth_user_type.'.view_stores') == '1')
 
				    <li class=" <?php  if(request()->segment(count(request()->segments())) == 'restaurant') { echo 'active';} else { echo '';}?>" ><a href="{{ URL::to('v1/restaurant') }}/<?php echo @$auth_user_id;?>"><i class="fa fa-home"></i><span>Stores</span></a></li> 
 
				    @endif


@if(trans('permission'.$auth_user_type.'.store_menu_sidebar') == '1')
					<li  ><a href="{{ URL::to('v1/store_menu') }}/<?php echo @$auth_store_id;?>"  ><i class="fas fa-ticket-alt"></i><span>Menu</span></a></li>
@endif

@if(trans('permission'.$auth_user_type.'.store_info_sidebar') == '1')
					<li ><a href="{{ URL::to('v1/store_info') }}/<?php echo @$auth_store_id;?>?id=<?php echo @$auth_user_id;?>"  ><i class="fas fa-ticket-alt"></i><span>Store Settings</span></a></li>
	@endif	 
				    <!--<li><a href="#"><i class="fa fa-money"></i><span>Accounting</span></a></li>-->
					  			@if(trans('permission'.$auth_user_type.'.view_subscribers') == '1')
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'subscribers') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/subscribers#/') }}"  ><i class="fas fa-ticket-alt"></i><span>Subscribers</span></a></li>
					@endif
					

					@if(trans('permission'.$auth_user_type.'.view_coupons') == '1')
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'coupons') { echo 'active';} else { echo '';}?>" ><a href="{{ URL::to('v1/coupons#/') }}" ><i class="fas fa-ticket-alt"></i><span>Coupons</span></a></li>
					@endif


					@if(trans('permission'.$auth_user_type.'.view_customers') == '1')   
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'customers') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/customers') }}"  ><i class="fa fa-user"></i><span>Customers</span></a></li>
					@endif   


                <?php $vendor_plugins = @\App\Setting::where('key_title','vendors')->first(['key_value'])->key_value;
                 if($vendor_plugins == 1)
                   { ?>
					@if(trans('permission'.$auth_user_type.'.view_vendors') == '1')   
 
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'vendors') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/vendors') }}" ><i class="fa fa-user"></i><span>Restaurants</span></a></li>
 
				 
					@endif
				<?php } ?>
 

 
					
					  @if(trans('permission'.$auth_user_type.'.view_reviews') == '1')
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'reviews') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/reviews') }}"  ><i class="fa fa-home"></i><span>Reviews</span></a></li>
				    @endif


		  
				      @if(trans('permission'.$auth_user_type.'.view_freshdesk_enquiries') == '1')
				    <li  class=" <?php if(request()->segment(count(request()->segments())) == 'freshdesk') { echo 'active';} else { echo '';}?>" ><a href="{{ URL::to('v1/freshdesk') }}"><i class="fa fa-question-circle"></i><span>Customers Enquiries</span></a></li>
				    @endif


		 
		 
		       @if(trans('permission'.$auth_user_type.'.view_payment_request') == '1')
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'payment-requests') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/payment-requests') }}" ><i class="fa fa-question-circle"></i><span>Payment Requests</span></a></li>
 @endif


 @if(trans('permission'.$auth_user_type.'.view_payment_request_sent') == '1')
				     <li class=" <?php if(request()->segment(count(request()->segments())) == 'payment-requests-sent') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/payment-requests-sent') }}" ><i class="fa fa-question-circle"></i><span>Payment Requests Sent</span></a></li>
	 @endif	 


		 
				    
                  @if(trans('permission'.$auth_user_type.'.send_mass_notifications') == '1')
				   <li style="padding-right:0;margin-left:0">
                     <a href="javascript:;"><i class="fa fa-bell"></i> <span>Send Notifications</span></a>	
                     <ul class="acc-menu" style="padding-right:0;margin-left:0">
                                 <li style="padding-right:0;margin-left:0"><a href="{{ URL::to('v1/emails-to-customers') }}"><i style="font-size:14px" class="fa fa-envelope"></i><span style="font-size:14px"> Emails to Customers</span></a></li>
				    			 <li style="padding-right:0;margin-left:0"><a href="{{ URL::to('v1/push-to-customers') }}"><i style="font-size:14px" class="fa fa-bell"></i><span style="font-size:14px"> Push Notifications</span></a></li>
				   				 <li style="padding-right:0;margin-left:0"><a href="{{ URL::to('v1/emails-to-stores') }}"><i style="font-size:14px" class="fa fa-envelope"></i><span style="font-size:14px"> Emails to Stores</span></a></li>
				   				  <li style="padding-right:0;margin-left:0"><a href="{{ URL::to('v1/emails-to-subscribers') }}"><i style="font-size:14px" class="fa fa-envelope"></i><span style="font-size:14px"> Emails to Subscribers</span></a></li>
                     </ul>
                  </li>
                  @endif

 

					<li class=" <?php if(request()->segment(count(request()->segments())) == 'reports') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/reports') }}"  ><i class="fa fa-file"></i><span>Reports</span></a></li>
						 
					<!--<li><a href="#"><i class="fa fa-tasks"></i><span>Roles</span></a></li>-->
			 
				    <li class=" <?php if(request()->segment(count(request()->segments())) == 'settings') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('settings')}}"  ><i class="fa fa-wrench"></i><span>Settings</span></a></li>
					
					@if(trans('permission'.$auth_user_type.'.approve_edit_request') == '1')
					 <li class=" <?php if(request()->segment(count(request()->segments())) == 'store-requests') { echo 'active';} else { echo '';}?>"><a href="{{ URL::to('v1/store-requests')}}"  ><i class="fa fa-wrench"></i><span>Approval Requests</span></a></li>
					@endif

               </ul>
            </nav>
         </div>
      </div>
   </div>
</div>