@extends('admin.layout.auth')
@section('title', 'Payment Requests' ) 
<link rel="stylesheet" href="{{ URL::asset('admin/assets/css/custom.css')}}">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
 </style>
@section('content')
@section('header')
@include('admin.includes.header')
@show

                 <?php @$auth_user_type = @Auth::user()->user_type;   
                     @$auth_user_id =   @Auth::id();
                     App::setLocale('settings');  
                     if($auth_user_id =='' || $auth_user_id == null)
                     {
                           Auth::logout();
                           echo '<script>document.getElementById("logout").click();</script>';
                     }

                     if($auth_user_type == '4')
                     {
                      $auth_store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                     }
                     else
                     {
                      $auth_store_id = '';
                     }

                     if($auth_user_type == '4' || $auth_user_type == 4 )
                     {
                        $type = 'manager_vendor';
                     }

                        if($auth_user_type == '3' || $auth_user_type == 3 )
                     {
                        $type = 'vendor_admin';
                     }

                     if($auth_user_type == '1' || $auth_user_type == 1 )
                     {
                        $type = '';
                     }


                ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->     
         @section('sidebar')
         @include('admin.includes.sidebar')
         @show
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
     
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            @include('admin.includes.realtime-search')
         </div>
       <?php          $auth_user_type = Auth::user()->user_type;   
                      $auth_user_id =   Auth::id();  
 
                 ?>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
       <input type="file" id="file" style="display:none "/>
            <div class="tab-content"  ng-controller="PaymentRequestsController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="{{URL::asset('admin/assets/images/89.svg')}}" class="img-responsive center-block">         
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Payment Requests(Received)</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">

   <?php if( $type != '1' && $type != '')
   {    ?>
                           <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Make Payment Request</button>

                         <?php } ?>
                       
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
 
                     
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>NOTES</th>
                                       <th>SENDER</th>
                                       <th>STATUS</th>
                                       <th>CREATED</th>
                                       <th id="action">ACTION</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.payment_requests.data.data  ">
                                       <td>#@{{values.id}}</td>
                                       <td>@{{values.notes}}</td>
                                       <td>@{{values.sender_name}}</td>
                                       <td>@{{values.status_name}}</td>
                                       <td>@{{values.created_at_formatted}}</td>
                                       


                           
                                       <td>
                                            <a href="" ng-click="ctrl.updateStatusOpen(values.id)">ACTION</a>          
                                       </td>
                                      
                                    </tr>
                                    
                                 </tbody>
                              </table>

                                <div class="container-fluid innerData" ng-show="!ctrl.payment_requests.data.data">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br> 
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>
                                        
                           </div>

                           
                <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.payment_requests.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.payment_requests.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.payment_requests.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.payment_requests.data.prev_page_url);">Previous</button> 
                                          <span>@{{ctrl.payment_requests.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.payment_requests.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.payment_requests.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.payment_requests.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.payment_requests.data.last_page_url);">Last</button> 
                                       </div>
                     
                           
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Payment Request</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
 
                  <div class="col-sm-12">
                     <h5>Notes</h5>
                     <textarea id="notes" rows="5" class="form-control"></textarea>
                     <input type="hidden" id="type" name="type" value="<?php echo @$type; ?>">
                    <md-button ng-click="ctrl.storePaymentRequests()" class="md-raised bg-color md-submit" style="bottom:0px;">Submit</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
  
  
  
  
  
  
  
  
   <!-----------------------------------------------CATEGORY ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="actionModel" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  > <b>Accept/Reject Payment Request</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   >
              
                  <div class="col-sm-12">
                      <h5>Accept/Reject Payment Request</h5>

                      <textarea id="notes_update" rows="5" class="form-control" placeholder="Enter Notes Here"></textarea>
                      <br>
                      <select class="form-control" id="status">
                             <option value=''>Select Action</option>
                             <option value='1'>Mark Accepted</option>
                             <option value='2'>Mark Payment Sent</option>
                             <option value='3'>Mark Rejected</option>
                      </select>
                     <br>

                     <md-button ng-click="ctrl.updateStatus(ctrl.id)" class="md-raised bg-color md-submit" style="bottom:0;">Update Status</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CATEGORY ADD MODAL ENDS HERE-------------------------------------------------------------------->
   </div>
  </div>
      </div>
   </div>
  
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="{{ URL::asset('admin/angular-controllers/payment_requests.js')}}"></script> 

<!-- JavaScripts --> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);
 
        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "{{url('image-upload-categories')}}",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '{{asset('images/categories/noimage.jpg')}}');
                    alert(data.errors['file']);
                }
                else {
             
           var data = data.filename;
          
                     $('#file_name').val(data);          
                    $('#file_name1').val(data);
                    //$('#photo').val(data);
          $('#preview_image').attr('src', '{{asset('images/categories')}}/'+data);
                    $('#preview_image2').attr('src', '{{asset('images/categories')}}/'+data);
          $('#preview_image1').attr('src', '{{asset('images/categories')}}/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '{{asset('images/categories/noimage.jpg')}}');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '{{csrf_token()}}');
                $.ajax({
                    url: "ajax-remove-image-categories/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '{{asset('images/noimage.jpg')}}');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>


@endsection