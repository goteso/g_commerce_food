@extends('web-food.layout.web')
@section('title', 'About' )  
 
@section('content')


@section('header')
@include('web-food.includes.header')
@show
  
 
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-12 text-center"  >
                          <h2 class="header">About Us</h2>
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id=""  >
               <div class="container">
			   <div class="row">  
                     <div class="col-sm-12  ">   
					 </div>
				</div>
                  <div class="row">  
                     <div class="col-sm-12  ">   
                         <div class=" ">
						 <div class="panel-body">
						 <h4 class="header">Who We Are</h4>
						    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  </p>
							 
							  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
						 </div>
						 </div>
                     </div> 
					 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
	 
		  				    <!---------------------------------------MOBILE APP SECTION STARTS HERE-------------------------------------------->
            <section id="mobile-app" >
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-7 text-center"> 
					   <img src="web-food/assets/images/mobile-app.png" class="img-responsive center-block" style="width:60%;">
                      </div>
					  <div class="col-sm-5 right" > 
					     <h3 class="header">Download our App Now!</h3>
						 <p>Find it on Appstore and Playstore</p><br>
						 <ul class="list-inline">
						 <li> <a href=""><img src="web-food/assets/images/app_store.png" class="img-responsive center-block"></a></li>
						 <li> <a href=""><img src="web-food/assets/images/google_play.png" class="img-responsive center-block"></a></li>
						 </ul>
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------MOBILE APP SECTION ENDS HERE-------------------------------------------->		
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>
 
@endsection