@extends('web-food.layout.web')
@section('title', 'Favourites' )  
 
@section('content')


@section('header')
@include('web-food.includes.header')
@show
 
   
  <div ng-controller="favouriteController" ng-cloak>
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-8"   >
                           <md-list layout-padding class="profile-data" >
							<md-list-item class="md-3-line"     >
								<img  ng-src="{{URL::asset('images/users')}}/@{{customerDataPhoto}}" class="md-avatar" ng-show="customerDataPhoto">
								<img  ng-src="{{URL::asset('admin/assets/images/user-placeholder.png')}} " class="md-avatar" ng-show="customerDataId && !customerDataPhoto">
								<div class="md-list-item-text">
								  <h2 >@{{customerDataFirstName}} @{{customerDataLastName}}</h2>
								  <h4>
									@{{customerDataEmail}}
								  </h4>
								</div>
							</md-list-item>
						  </md-list>    
                      </div>
                      <div class="col-sm-4">
                          <br>
                         <a href="{{URL::to('/')}}"> <button class="btn md-raised bg-color md-submit md-button md-ink-ripple dropdown-toggle md-button" ng-show="customerDataId">Place Order</button>
                         </a>
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="orders" >
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-4 text-center" ng-controller="logoutController">   
                         <div class="panel left">
						 <div class="panel-body">
						   <table  class="table left-nav-links">
						    <tr >
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/shopping-bag.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/shopping-bag-active.svg')}}" class="img-active">
							  </td>
							  <td > <a href="{{URL::to('my-orders')}}">My Orders</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/address.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/address-active.svg')}}" class="img-active">
							  </td>
							  <td> <a href="{{URL::to('addresses')}}">Manage Addresses</a></td>
							</tr>
							<tr  class="">
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('loyalty-points')}}"> Loyalty Points History</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/feedback.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/feedback-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('reviews')}}"> My Reviews</a></td>
							</tr>
							<tr class="active">
							  <td>  
							     <img src="{{URL::asset('web-food/assets/images/favorites.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/favorites-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('favourites')}}"> Favourites</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('account')}}/@{{profileData.data.user_id}}"> Edit Profile</a></td>
							</tr>
							<tr >
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/settings.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/settings-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="{{URL::to('change-password')}}"> Change Password</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="{{URL::asset('web-food/assets/images/sign-out.svg')}}" class="img-normal">
					             <img src="{{URL::asset('web-food/assets/images/sign-out-active.svg')}}" class="img-active">
							  </td>
							  <td><a href="#" ng-click="logout();">Signout</a></td>
							</tr>
						   </table>
						 </div>
						 </div>
                     </div> 
					 
					 
					 <div class="col-sm-8 text-center">   
                         <div class="panel">
						 <div class="panel-body">
						  <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('web-food/assets/images/89.svg')}}" class="img-responsive center-block">				 
                              <p >Loading...</p>
                           </div>
						   
						     <div id="tableToExport" class="products-table table-responsive"  >
                                          <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr>  
                                                   <th>STORE</th>
                                                   <th>ADDRESS</th> 
                                                   <th>TIME</th>
                                                  <th>ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody >
                                                <tr   class="innerData" ng-repeat="values in favourite.data  ">  
                                                   <td>@{{values.store_details[0].store_title}}  </td> 
                                                   <td> @{{values.order_status}}</td>
                                                   <td>@{{values.created_at_formatted}}</td>
                                                    <td class="actions"> 
													  <a class="btn btn-xs edit-product" href="#" ng-click="favourite_store_delete(values.id,$index);"><i class="fa fa-trash"></i></a> 
                                                   </td> 
                                                </tr>
                                             </tbody>
                                          </table>

                                           <div class="container-fluid innerData" > 
                                          <div class="row" ng-show="favourite.data == ''">
                                            <div class="col-sm-12"> 
                                          	    <br>
                                             	<img class="img-responsive center-block" src="web-food/assets/images/placeholders/favourite-placeholder.png">
                                             	<p>No Favourites found.</p>
                                            </div>
                                          </div>
                                        </div>


                                       </div> 
						 </div>
						 </div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
		
	  	</div>			  
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>

<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/favourites.js')}}"></script> 
@endsection