@extends('web-food.layout.web')
@section('title', 'Items' )  
<link rel="stylesheet" href=" {{URL::asset('web-food/assets/css/style1.css')}}">
<style>
   .spinner {
   margin: 100px auto 0;
   width: 70px;
   text-align: center;
   }
   .spinner > div {
   width: 18px;
   height: 18px;
   background-color: #333;
   border-radius: 100%;
   display: inline-block;
   -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   }
   .spinner .bounce1 {
   -webkit-animation-delay: -0.32s;
   animation-delay: -0.32s;
   }
   .spinner .bounce2 {
   -webkit-animation-delay: -0.16s;
   animation-delay: -0.16s;
   }
   @-webkit-keyframes sk-bouncedelay {
   0%, 80%, 100% { -webkit-transform: scale(0) }
   40% { -webkit-transform: scale(1.0) }
   }
   @keyframes sk-bouncedelay {
   0%, 80%, 100% { 
   -webkit-transform: scale(0);
   transform: scale(0);
   } 40% { 
   -webkit-transform: scale(1.0);
   transform: scale(1.0);
   }
   }
   .foodbakery-button-loader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background: #f97456;
   text-align: center;
   border-radius: 3px;
   background-color: #c33332 !important;
   }
   .foodbakery-button-loader .spinner {
   width: 25px;
   height: 25px;
   position: absolute;
   display: inline-block;
   top: 0;
   bottom: 0;
   left: 0;
   right: 0;
   margin: auto;
   }
   .foodbakery-button-loader .double-bounce1, .foodbakery-button-loader .double-bounce2 {
   width: 100%;
   height: 100%;
   border-radius: 50%;
   background-color: #fff;
   opacity: 0.6;
   position: absolute;
   top: 0;
   left: 0;
   -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
   animation: sk-bounce 2.0s infinite ease-in-out;
   }
   .foodbakery-button-loader .double-bounce2 {
   -webkit-animation-delay: -1.0s;
   animation-delay: -1.0s;
   }
   
   md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}
    md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}
	
</style>
@section('content')
@section('header')
@include('web-food.includes.header')
@show
<!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
<section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
   <div class="container">
      <div class="row" >
          <div class="col-sm-12"   >
                           <md-list layout-padding class="profile-data" >
							<md-list-item class="md-3-line"     >
								<img  ng-src="{{URL::asset('images/users')}}/@{{customerDataPhoto}}" class="md-avatar" ng-show="customerDataPhoto">
								<img  ng-src="{{URL::asset('admin/assets/images/user-placeholder.png')}}/@{{customerDataPhoto}}" class="md-avatar" ng-show="customerDataId && !customerDataPhoto">
								<div class="md-list-item-text">
								  <h2 >@{{customerDataFirstName}} @{{customerDataLastName}}</h2>
								  <h4>
									@{{customerDataEmail}}
								  </h4>
								</div>
							</md-list-item>
						  </md-list>    
                      </div>
      </div>
   </div>
</section>
<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
<section id="view-product" >
   <div class="container">
        <div ng-view></div>
   </div>
   </div>
   </div>
</section>
<!------>
@section('footer')
@include('web-food.includes.footer')
@show
<!------>
<script type="text/javascript" src='https://rawgithub.com/gsklee/ngStorage/master/ngStorage.js'></script>

<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/place_order.js')}}"></script> 

@endsection