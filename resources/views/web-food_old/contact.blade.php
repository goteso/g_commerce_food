@extends('web-food.layout.web')
@section('title', 'Contact' )  
 
@section('content')


@section('header')
@include('web-food.includes.header')
@show
 
 
   <div  ng-controller="contactController" ng-cloak>
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
               <div class="container-fluid"> 
                  <div class="row" >
                      <div class="col-sm-12 text-center"  >
                    <h2 class="header">Contact Us</h2>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="contact"    >
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-12 ">   
                         <br><br>
						 
						 <div class="row">
						    <div class="contact-info col-xs-6 col-sm-4">
                     <div class="info-icon text-center">
                        <i class="fa fa-map-marker-alt"></i>
                     </div>
                     <h5 class="info-title text-center">
                        Address
                     </h5>
                     <div class="info-text">
                        <p class="text-center">
                           Doha - Qatar 
                        </p>
                     </div>
                  </div>
				  
				     <div class="contact-info col-xs-6 col-sm-4">
                     <div class="info-icon text-center">
                        <i class="fa fa-envelope"></i>
                     </div>
                     <h5 class="info-title text-center">
                        Email
                     </h5>
                     <div class="info-text">
                        <p class="text-center">
                          Info@mataemonline.comr<br>
						   
                        </p>
                     </div>
                  </div>
				  
				     <div class="contact-info col-xs-6 col-sm-4">
                     <div class="info-icon text-center">
                        <i class="fa fa-phone"></i>
                     </div>
                     <h5 class="info-title text-center">
                        Phone
                     </h5>
                     <div class="info-text">
                        <p class="text-center">
                           44161607<br> 
                        </p>
                     </div>
                  </div>
				   <br>
						 </div>
						 
						 <hr>
						   <div class="row">
						     <div class="col-sm-12  col-lg-10 col-lg-offset-1">
							   <h2 class="header  text-center">Contact Us</h2>
							   <br>
							   <form>
							    <div class="row">
							   <div class="col-sm-6">
							     <div class="form-group"> 
								   <input type="text" class="form-control" id="first_name" name="first_name" ng-model="first_name" placeholder="First Name">
								 </div>
								 </div>
								 <div class="col-sm-6"> 
								   <div class="form-group">
								   <input type="text" class="form-control" id="last_name" name="last_name" ng-model="last_name" placeholder="Last Name">
								 </div>
								 </div> 
								 <div class="col-sm-12">
							     <div class="form-group">
								   <input type="email" class="form-control" id="contact_email" name="email" ng-model="contact_email" placeholder="Email">
								 </div>
								  </div>
								 <div class="col-sm-12">
								 <div class="form-group">
								   <input type="text" class="form-control" id="contact_mobile" name="email" ng-model="contact_mobile" placeholder="Mobile">
								 </div>
								  </div>
								 <div class="col-sm-12">
								 <div class="form-group">
								  <textarea id="comment" class="form-control" name="comment" rows="5" placeholder="comment"></textarea>
								 </div> 
							 </div>
							   <div class="text-center">
							   <md-button type="button" ng-click="contact()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Submit</md-button>
							   </div>
							   </form>
							 </div>
							 </div>
							 
						   
						 </div>
						 <div>&nbsp;</div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
		</div>
		  				  
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>

<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/contact.js')}}"></script> 
@endsection