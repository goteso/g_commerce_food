@extends('web-food.layout.web')
@section('title', 'Terms & Conditions' )  
 
@section('content')


@section('header')
@include('web-food.includes.header')
@show
  
 
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-12 text-center"  >
                            <h2 class="header">Terms & Conditions</h2> 
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="" ng-controller="termsController">
               <div class="container">
			   <div class="row">  
                     <div class="col-sm-12  ">  
					   <br>
					 </div>
				</div> 
                  <div class="row">  
                     <div class="col-sm-12  " >   
                         <div class=" left">
						 <div class="panel-body" ng-repeat="terms in termsData">
						    <p>@{{terms.key_value}}</p>
							</div>
						 </div>
						 </div>
                     </div> 
					 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
	 
		  				  
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>

<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/terms.js')}}"></script> 
@endsection