<footer>
  <section>
	 <div class="container ">
		<div class="row">
		  <div class="col-sm-12" ng-controller="newsletterController">
		  <h3 class="foot-title text-center text-uppercase">Subscribe to our newsletter</h3>
		  <form class="form-inline text-center">
			<div class="form-group">
			  <input type="email" class="form-control" id="newsletter-email" name="newsletter-email" placeholder="Enter your Email Address...">
			</div> 
			<md-button  ng-click="submitNewsletter()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">sign up</md-button>
		  </form>
		  </div>
		  
		</div>
	 </div>
   </section>
   
   <section>
	 <div class="container" ng-controller="footerController">
		<div class="row"  >
		  <div class="col-sm-3">
		    <div class="widget">
		      <ul>

			    <li><a href="{{URL::to('about')}}">About Us</a></li>
				<li><a href="{{URL::to('terms-and-conditions')}}">Terms & Conditions</a></li>
				<li><a href="{{URL::to('privacy-policy')}}">Privacy Policy</a></li>
				<li><a href="{{URL::to('faq')}}">FAQ</a></li>
			  </ul>
			</div>
		  </div>
		  
		  <div class="col-sm-3">
		    <div class="widget">
		      <ul>
			    <li><a href="{{URL::to('contact')}}">Contact Us</a></li>
				<li>@{{footer_email}}</li>
				<li>@{{footer_phone}}</li> 
			  </ul>
			</div>
		  </div>
		   <div class="col-sm-3">
		   </div>
		  <div class="col-sm-3">
		    <div class="widget">
		      <ul>
			    <li style="padding-bottom:10px;"><a href="">Follow Us</a></li>
				<ul class="list-inline" > 
				   <li style="padding: 5px 10px;"><a href="@{{footer_facebook_link}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
				   <li style="padding: 5px 10px;"><a href="@{{footer_twitter_link}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
				   <li style="padding: 5px 10px;"><a href="@{{footer_google_link}}" target="_blank"><i class="fab fa-google"></i></a></li>
				</ul>
			  </ul>
			</div>
		  </div> 
		</div>
	 </div>
   </section>
   
   
   <section>
      <div class="container">
		<div class="row">
		  <div class="col-sm-12  ">
		     <p class="text-center text-uppercase">2018 Mataem Online. All rights reserved. powered by <a href="#" target="_blank" style="text-decoration:none;">Mataem Online</a></p>
		  </div> 
		</div>
	 </div>
   </section>
</footer>