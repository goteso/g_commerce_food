@extends('web-food.layout.web')
@section('title', 'Home' )  
 <link rel="stylesheet" href=" {{URL::asset('web-food/assets/css/style1.css')}}">
<style>

#main-banner .form-group{padding:5px 8px;}
@media(min-width:768px){#main-banner .form-inline{display:flex;}}
   .spinner {
   margin: 100px auto 0;
   width: 70px;
   text-align: center;
   }
   .spinner > div {
   width: 18px;
   height: 18px;
   background-color: #333;
   border-radius: 100%;
   display: inline-block;
   -webkit-animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   animation: sk-bouncedelay 1.4s infinite ease-in-out both;
   }
   .spinner .bounce1 {
   -webkit-animation-delay: -0.32s;
   animation-delay: -0.32s;
   }
   .spinner .bounce2 {
   -webkit-animation-delay: -0.16s;
   animation-delay: -0.16s;
   }
   @-webkit-keyframes sk-bouncedelay {
   0%, 80%, 100% { -webkit-transform: scale(0) }
   40% { -webkit-transform: scale(1.0) }
   }
   @keyframes sk-bouncedelay {
   0%, 80%, 100% { 
   -webkit-transform: scale(0);
   transform: scale(0);
   } 40% { 
   -webkit-transform: scale(1.0);
   transform: scale(1.0);
   }
   }
   .foodbakery-button-loader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   background: #f97456;
   text-align: center;
   border-radius: 3px;
   background-color: #c33332 !important;
   }
   .foodbakery-button-loader .spinner {
   width: 25px;
   height: 25px;
   position: absolute;
   display: inline-block;
   top: 0;
   bottom: 0;
   left: 0;
   right: 0;
   margin: auto;
   }
   .foodbakery-button-loader .double-bounce1, .foodbakery-button-loader .double-bounce2 {
   width: 100%;
   height: 100%;
   border-radius: 50%;
   background-color: #fff;
   opacity: 0.6;
   position: absolute;
   top: 0;
   left: 0;
   -webkit-animation: sk-bounce 2.0s infinite ease-in-out;
   animation: sk-bounce 2.0s infinite ease-in-out;
   }
   .foodbakery-button-loader .double-bounce2 {
   -webkit-animation-delay: -1.0s;
   animation-delay: -1.0s;
   }
   md-checkbox.md-default-theme.md-checked .md-ink-ripple, md-checkbox.md-checked .md-ink-ripple{color:#c33332}
   md-checkbox.md-default-theme.md-checked .md-icon, md-checkbox.md-checked .md-icon{background-color:#c33332;}
   
   md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:650px!important;}
   
   .promoCode  md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:140px!important;max-height:150px!important;}
   #pickupModal  md-tabs:not(.md-no-tab-content):not(.md-dynamic-height){min-height:300px!important}
   .carousel-control.left, .carousel-control.right{background:transparent!important;}
   .reviewData.celebrity {
    background: #fff0f0;
}




.rating {
    color: #d7d7d7;
    margin: 0;
    padding: 0;
}
ul.rating {
    display: inline-block;
}
.rating li {
	font-size: 18px;
    list-style-type: none;
    display: inline-block;
    padding: 1px;
    text-align: center;
    font-weight: bold;
    cursor: pointer;
}
.rating .filled {
    color: #fbad00;
}

   </style>
@section('content')


@section('header')
@include('web-food.includes.header')
@show
 
  <div ng-view></div>
 
       
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>


<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/abc8.js')}}"></script> 

@endsection