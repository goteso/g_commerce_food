@extends('web-food.layout.web')
@section('title', 'FAQ' )  
 <style>
     @keyframes minus {
    from {
        transform:rotate(0deg);
    }
    to {
        transform:rotate(360deg);
    }
}


 </style>
@section('content')


@section('header')
@include('web-food.includes.header')
@show
  
 
 
   
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('{{URL::asset('web-food/assets/images/food-back-small.jpg')}}')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-12 text-center"  >
                             <h2 class="header">You are UnSubscribed</h2>
                      </div>
                 </div>
               </div>
            </section>
    <!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="faq" ng-controller="faqController">
               <div class="container">
         <div class="row">  
                     <div class="col-sm-12  ">  
                      <div id="loading" class="loading" style="display:none ;">
                              <img src="{{URL::asset('web-food/assets/images/89.svg')}}" class="img-responsive center-block">        
                              <p >Loading...</p>
                           </div>
               
             <br>
           </div>
        </div>
                  <div class="row">  
                     <div class="col-sm-12  ">   
                         <div class="  left">
             <div class="panel-body">
               <!-- <div class="" ng-repeat="data in faqData.data.data">
              <h4>Q@{{$index+1}}: @{{data.question}}</h4>
              <p>Ans. @{{data.answer}}</p>
              <hr>
              </div>--->

            <?php 
                    if(isset($_GET['email']) && $_GET['email'] != '' && $_GET['email'] != null)
                    {
                      @\App\Newsletters::where('email' , $_GET['email'])->update(['status' => 0]);
                        ?> <p>You have been successfully unsubscribed from our Newsletter , you can again subscribe it anytime !</p>

                        <?php
                    }
            ?>
              

  
             </div>
             </div>
                     </div> 
           
                  </div> 
               </div>
            </section>
    <!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
    
   
                
      
@section('footer')
@include('web-food.includes.footer')
@show
<!------>

<script type="text/javascript" src="{{ URL::asset('web-food/angular-controllers/faq.js')}}"></script> 
@endsection