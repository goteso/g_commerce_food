<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTransactionTax extends Model
{
        protected $fillable = [ 'order_id', 'setting_tax_id','setting_tax_title','order_transaction_tax_amount' ];
		protected $table = 'order_transaction_tax';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 public function getCurrencySymbolAttribute($value) {
     
      $currency_symbol = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;

      if($currency_symbol == '' || $currency_symbol == null || $currency_symbol == 'null')
      {
             return 'QAR';
      }
      return $currency_symbol;

 
    }

 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}