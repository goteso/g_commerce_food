<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreDeliverySettings extends Model
{
        protected $fillable = [ 'store_id' , 'area_id', 'start_time','end_time','delivery_fee'];
		protected $table = 'store_delivery_settings';
		
 


	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
 
 
 
 



 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
}