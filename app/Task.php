<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
        protected $fillable = [ 'task_id' , 'order_id', 'driver_id' , 'store_id' , 'pickup_contact_title' , 'pickup_contact_phone' , 'pickup_time', 'picked_up_time' , 'pickup_address' , 'dropoff_contact_title' , 'dropoff_contact_phone', 'dropoff_time' , 'dropped_off_time' , 'dropoff_address' , 'status' , 'task_type' , 'signature'];
		protected $table = 'task';
 
 public function getDriverDetailsAttribute($value) {
         return  @\App\User::where('user_id',$this->driver_id)->get(['user_id','first_name','email','phone','last_name','photo']);
    }
 
 public function getDropoffLatitudeAttribute($value) {
        return @json_decode($this->dropoff_address)->latitude;
    }

 public function getDropoffLongitudeAttribute($value) {
         return @json_decode($this->dropoff_address)->longitude;
    }
    
    
    

     public function getCustomerDetailsAttribute($value) {


          $customer_id = @\App\Order::where('order_id',$this->order_id)->first(['customer_id'])->customer_id;
       return  $customer_details = @\App\User::where('user_id',$customer_id)->get(['user_id','first_name','last_name','photo','thumb_photo']);
    }
    
    
 
   public function getPickupLatitudeAttribute($value) {
       return @\App\Store::where('store_id',$this->store_id)->first(['latitude'])->latitude;
    }


     public function getDriverNameAttribute($value) {
       return @\App\User::where('user_id',$this->driver_id)->first(['first_name'])->first_name." ".@\App\User::where('user_id',$this->driver_id)->first(['last_name'])->last_name;
    }


     public function getStoreTitleAttribute($value) {
       return @\App\Stores::where('store_id',$this->store_id)->first(['store_title'])->store_title;
    }




     public function getPickupTimeFormattedAttribute($value) {
      
      $pickup_time = @\Carbon\Carbon::parse($this->pickup_time);
      $now = @\Carbon\Carbon::now();
      
      $diff_in_days = $now->diffInDays($pickup_time);

      return $diff_in_days;

    



    }



 
 public function getPickupLongitudeAttribute($value) {
        return @\App\Store::where('store_id',$this->store_id)->first(['longitude'])->longitude;
    }
 
 public function getDropoffAddressStringAttribute($value) {
         return @json_decode($this->dropoff_address)->address_line1." ".@json_decode($this->dropoff_address)->address_line2;
    }
 
 public function getStoreDetailsAttribute($value) {
         return  @\App\Store::where('store_id',$this->store_id)->get([ 'store_id','store_title','store_photo','store_rating' ]);
    }
 	
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}