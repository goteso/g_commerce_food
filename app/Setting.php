<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
        protected $fillable = [
        'id','key_title','key_display_title','key_value','type'];

        protected $table = 'setting';



         public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
     
 
}
