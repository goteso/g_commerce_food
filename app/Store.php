<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
        protected $fillable = [ 'store_title' , 'store_photo', 'store_thumb_photo', 'sort_index','store_rating' , 'latitude' , 'longitude' , 'vendor_id' , 'manager_id' , 'address' , 'status','featured','popular_count','busy_end_time','catering_status','catering_commission','store_tax','minimum_delivery'];
		protected $table = 'stores';
		
    protected $casts = [ 'store_id'=>'int' , 'status'=>'status' ];


	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
      public function getManagerNameAttribute($value) {
                 $v =@\App\User::where('user_id',$this->manager_id)->first(['first_name'])->first_name;
      if($v == null ||  $v == '' || $v == ' ')
      {
        $v = '';
      }
      return $v;
    }

         public function getCuisinesAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
      $store_meta_type_id = @\App\StoreMetaType::where('identifier','cuisines')->first(['store_meta_type_id'])->store_meta_type_id;

      $value = @\App\StoreMetaValue::where('store_meta_type_id',$store_meta_type_id)->where('store_id',$this->store_id)->first(['value'])->value;

      if($value == '' || $value == null)
      {
        return '';
      }
      return $value;
    }

         public function getVendorNamesAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
      $v = @\App\User::where('user_id',$this->vendor_id)->first(['first_name'])->first_name;
      if($v == null ||   $v== '' || $v == ' ')
      {
        $v = '';
      }
      return $v;
    }

         public function getStoreMetaAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
      return  @\App\StoreMetaValue::where('store_id',$this->store_id )->get(['store_meta_value_id','store_meta_type_id','value']);
    }


             public function getBusyStatusAttribute($value) {
       
             $now = @\Carbon\Carbon::now();
             $now =\Carbon\Carbon::parse($now)->format('Y-m-d');
             $count = @\App\Store::where('store_id',$this->store_id)->where('busy_end_time','<>','')->where('busy_end_time','<>',null)->where('busy_end_time','>',$now)->count();
             if($count > 0)
             {
                 return 1;
             }
             else { return 0;}
    }

                



                 public function getBusyStatusTitleAttribute($value) {
  $now = @\Carbon\Carbon::now();
             $now =\Carbon\Carbon::parse($now)->format('Y-m-d');
             $count = @\App\Store::where('store_id',$this->store_id)->where('busy_end_time','<>','')->where('busy_end_time','<>',null)->where('busy_end_time','>',$now)->count();
             if($count > 0)
             {
                 return 'Busy';
             }
             else { return 'Open';}
    }


         public function getFavouriteAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
  
      if(isset($_GET['user_id']) == null ||   $_GET['user_id'] == '' || $_GET['user_id']  == ' ')
      {
      return 0;
      }
      
      $v =  @\App\FavouriteStore::where('store_id',$this->store_id)->where('user_id',$_GET['user_id'])->count();

      if($v > 0)
      {
          return 1;
      }
      else
      {
        return 0;
      }
    }



             public function getFavouriteStoreIdAttribute($value) 
             {
                      //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();  
                      if(isset($_GET['user_id']) == null ||   $_GET['user_id'] == '' || $_GET['user_id']  == ' ')
                      {
                          return '';
                      }
                      
                      $v =  @\App\FavouriteStore::where('store_id',$this->store_id)->where('user_id',$_GET['user_id'])->count();

                      if($v > 0)
                      {
                        return  @\App\FavouriteStore::where('store_id',$this->store_id)->where('user_id',$_GET['user_id'])->first(['id'])->id;
                      }
                      else
                      {
                          return '';
                      }

             }






      public function getCloseStatusAttribute($value) {
   
             $now = @\Carbon\Carbon::now();
             $now =\Carbon\Carbon::parse($now)->format('H:i:s');

             $now_day =\Carbon\Carbon::parse($now)->format('D');

             $store_working_hours_details = @\App\StoreWorkingHours::where('store_id',$this->store_id)->where('weekday',$now_day)->get(['start_time','end_time']);

               if(count($store_working_hours_details) < 1)
               {
                return 0;
               }

                $start_time = @$store_working_hours_details[0]['start_time'];

                $start_time = @\Carbon\Carbon::parse($start_time)->format('H:i:s');
   
                $end_time = @$store_working_hours_details[0]['end_time'];
                $end_time = @\Carbon\Carbon::parse($end_time)->format('H:i:s');

                if (strtotime($now) < strtotime($start_time)) 
                 {
                       return 1;
                 } 
                  if (strtotime($now) > strtotime($end_time)) 
                 {
                       return 1;
                 }   
              return 0;
    }

          public function getCloseStatusTitleAttribute($value) {
           
             $now = @\Carbon\Carbon::now();
             $now =\Carbon\Carbon::parse($now)->format('H:i:s');
             $now_day =\Carbon\Carbon::parse($now)->format('D');
             $store_working_hours_details = @\App\StoreWorkingHours::where('store_id',$this->store_id)->where('weekday',$now_day)->get(['start_time','end_time']);
               if(count($store_working_hours_details) < 1)
               {
                return 'Open';
               }

                $start_time = @$store_working_hours_details[0]['start_time'];
                $start_time = @\Carbon\Carbon::parse($start_time)->format('H:i:s');

                $end_time = @$store_working_hours_details[0]['end_time'];
                $end_time = @\Carbon\Carbon::parse($end_time)->format('H:i:s');
                if (strtotime($now) < strtotime($start_time)) 
                 {
                       return 'Closed';
                 } 
                  if (strtotime($now) > strtotime($end_time)) 
                 {
                       return 'Closed';
                 }   
              return 'Open';
    }






          public function getStoreWorkingHoursAttribute($value) {
             $now = @\Carbon\Carbon::now();
             $now =\Carbon\Carbon::parse($now)->format('H:i:s');
             $now_day =\Carbon\Carbon::parse($now)->format('D');
             $store_working_hours_details = @\App\StoreWorkingHours::where('store_id',$this->store_id)->where('weekday',$now_day)->get(['start_time','end_time']);
             $start_time = @$store_working_hours_details[0]['start_time'];
             $end_time = @$store_working_hours_details[0]['end_time'];

             if($start_time == '' || $start_time == ' ' || $start_time == null || $end_time == '' || $end_time == ' ' || $end_time == null )
             {
                return '';
             }
          
            return $start_time.'-'.$end_time;
    }







 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
}