<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationsApp extends Model
{
	 protected $fillable = [
         'notification_type','receiver_id','recipient_id','sub_title', 'linked_id','is_read','status'   ];
       
 protected $table = 'notifications_app';
 
			    public function getCreatedAtAttribute($value) {
         $v = \Carbon\Carbon::parse($value)->diffforhumans();
	 
        $v = str_replace([' seconds', ' second'], 'sec', $v);
        $v = str_replace([' minutes', ' minute'], 'min', $v);
        $v = str_replace([' hours', ' hour'], 'h', $v);
        $v = str_replace([' months', ' month'], 'm', $v);
		$v = str_replace([' days', ' month' , ' day'], 'd', $v);
		$v = str_replace([' weeks', ' week'], 'w', $v);
		$v =  str_replace([' ago', ' ago'], '', $v);

        if(preg_match('(years|year)', $v)){
            $v = $v->toFormattedDateString();
        }

        return $v;
		
		
    }

     public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	 public function getTitleAttribute($value) {

 
					        $notification_type = $this->notification_type;
					        $notification_title = @\App\NotificationsPush::where('notification_type',$notification_type)->first(['title'])->title;
					        return $notification_title;
		 }
	
 
}
