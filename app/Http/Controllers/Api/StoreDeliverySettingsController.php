<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class StoreDeliverySettingsController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route- ============================================================== Store Store Working Hours=========================================> 
   public function store(Request $request)
   {
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'store_id' => 'required',
					'start_time' => 'required',
					'end_time' => 'required',
					'delivery_fee' => 'required',
					'area_id' => 'required',
				  ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			        $exist_count = @\App\StoreDeliverySettings::where('store_id',$request->store_id )->where('area_id',$request->area_id )->count();
                    if($exist_count > 0)
                    {
                    	  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'delivery setting for this area already exist';
                          $data['data']      =   @\App\StoreDeliverySettings::where('store_id',$request->store_id )->where('area_id',$request->area_id )->get(); 
                          return $data;
                    }
 
					$store_delivery_settings = new App\StoreDeliverySettings;
					$store_delivery_settings->store_id = @$request->store_id;
				    $store_delivery_settings->start_time = $this->validate_string($request->start_time);
				    $store_delivery_settings->end_time = $this->validate_string($request->end_time);
				    $store_delivery_settings->area_id = $this->validate_string($request->area_id);
				    $store_delivery_settings->delivery_fee = $this->validate_integer($request->delivery_fee);
				    $store_delivery_settings->save();
					
				    if($store_delivery_settings != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Store Delivery Setting Added';
                          $data['data']      =   $store_delivery_settings;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add ';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
  
  // Route- ============================================================== Get Categories List =========================================> 
   public function get_list()
   {


        $per_page = $this->get_variable_per_page(); 
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();

		$store_id = $this->get_variable_store_id();
		$area_id = $this->get_variable_area_id();
 
	    $model = new \App\StoreDeliverySettings;
	   	$model = $model::where('id' ,'<>', '0');  
	    
	    if($store_id != '' && $store_id != null)
		{   $model = $model->where('store_id' , $store_id);  }

	    if($area_id != '' && $area_id != null)
		{   $model = $model->where('area_id' , $area_id);  }

        $model = $model->orderBy($orderby,$order);
	   
        $result = $model->get();
        
     
	   
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Delivery Settings Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Result Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  



  // Route-8.3 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id)
   {
	   
					$validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'store_id' => 'required',
					'start_time' => 'required',
					'end_time' => 'required',
					'delivery_fee' => 'required',
					'area_id' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
				
	               //check existance of category with ID in categories table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Delivery Setting with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
				    
					$title =$request->title;
	                $parent_id = $this->validate_integer($request->parent_id);
				 

				    $other_exist_count = @\App\StoreDeliverySettings::where('store_id',$request->store_id )->where('area_id',$request->area_id )->where('id','<>',$id )->count();
				    if($other_exist_count > 0)
                    {
                    	  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Other Delivery Setting for this Area already exist';
                          $data['data']      =   [];
                          return $data;
                    }



	                App\StoreDeliverySettings::where('id', $id)->update([
                            'store_id' => @$request->store_id,
				    		'start_time' => $this->validate_string($request->start_time),
				    		'end_time' => $this->validate_string($request->end_time),
				    		'area_id' => $this->validate_string($request->area_id),
				    		'delivery_fee' => $this->validate_string($request->delivery_fee),
				    	 
	                ]);
	               
				    $result = @\App\StoreDeliverySettings::where('id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Delivery Setting Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update Working Hour';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   
 
  // Route-  ============================================================== Get Items List =========================================> 
  public function destroy($id)
   {
   	 
   	         //check existance of item with ID in items table
				 	$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Record with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}

                    @\App\StoreDeliverySettings::where('id',$id)->delete();
                    
   	 	                  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Delivery Setting deleted Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
   }


 













 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\StoreDeliverySettings::where('id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_store_id()
{
	 if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
					{ $store_id = $_GET['store_id']; }
					else 
					{ $store_id = ''; }
    return $store_id;
}	

   public function get_variable_area_id()
{
	 if(isset($_GET['area_id']) && $_GET['area_id'] != null && $_GET['area_id'] != '')
					{ $area_id = $_GET['area_id']; }
					else 
					{ $area_id = ''; }
    return $area_id;
}	


	 
	 
  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}