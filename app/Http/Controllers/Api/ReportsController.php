<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class ReportsController extends Controller 
{
	

use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
 /**
    Route::get('/v1/reports/orders', array('uses' => 'ReportsController@reports_orders'));//Route-
    Route::get('/v1/reports/cancelled_orders', array('uses' => 'ReportsController@reports_cancelled_orders'));//Route-
    Route::get('/v1/reports/customers', array('uses' => 'ReportsController@reports_customers'));//Route-
    Route::get('/v1/reports/customers_most_sales', array('uses' => 'ReportsController@reports_customers_most_sales'));//Route-
    Route::get('/v1/reports/tasks', array('uses' => 'ReportsController@reports_tasks'));//Route-
 **/

 
 
 // Route-23.1 ============================================================== Get Reports List =========================================> 
   public function get_list(Request $request  )
   {

 
 
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();

    $model = new \App\Reports;
    $model = $model::where('report_id' ,'<>', '0');  
    $model = $model->where('user_type' , $user_type );  
  
  $result = $model->get();
         if(sizeof($result) > 0)
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Reports List Fetched';
                          $data['data']      =         $result;  
				    }
					else
					{
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
					}
				   return $data;
				 
  }
  








   //Route-   1 =========================================================================
   public function reports_best_selling_items(Request $request  )
   {
      $order_items = DB::table("order_item")
      ->select(DB::raw("* , COUNT(*) as count , sum(item_price) as total_order_amount , sum(order_item_quantity) as order_qty  "))
      ->orderBy("created_at")
      ->groupBy(DB::raw("item_id"))
      ->get();
 
 
      $data_array = array();
      foreach($order_items as $order_item)
      {
             $d= array();
             $d[] = @\App\Items::where('item_id',$order_item->item_id)->first(['item_title'])->item_title;
             $d[] = @\App\OrderItem::where('item_id',$order_item->item_id)->count();
             $d[] =  $order_item->total_order_amount;
             $d[] =  $order_item->order_qty;
             @\App\OrderItem::where('item_id',$order_item->item_id)->count();
             $data_array[] = $d;
       }
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Items Name';
        $columns_array[] = 'Orders';
        $columns_array[] = 'Total Orders Amount';
        $columns_array[] = 'Order Qty';

 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Best Selling Items';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    0;  
                          $data['data']      =         $main;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }
  

 

//filters = store and time
    //Route-  2 =========================================================================
   public function reports_busy_restaurants(Request $request  )
   {

            //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Restaurant Name';
        $columns_array[] = 'Busy Time';
        $columns_array[] = 'Date';
        $columns_array[] = 'Duration';
        $store_id = $this->get_variable_store_id();



 	    $model = new \App\StoreBusyLog;
	   	$model = $model::where('id' ,'<>', '0'); 

        if(isset($_GET['date_from']) && $_GET['date_from'] != '' && isset($_GET['date_to']) && $_GET['date_to'] != '' )
        {
            $date_from= $_GET['date_from'];
              $date_to = $_GET['date_to'];
            $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
            $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
            $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
        }
        
        if($store_id != '' && $store_id != null)
        {
            $model = $model->where('store_id',@$store_id);
        }
        
 
        $store_busy_log = $model->get();
 
      $data_array = array();
      foreach($store_busy_log as $busy_log)
      {
             $d= array();
             $d[] = @\App\Stores::where('store_id',$busy_log->store_id)->first(['store_title'])->store_title;
             $d[] = @\Carbon\Carbon::parse($busy_log->start_time)->format('h:i A')." To ".@\Carbon\Carbon::parse($busy_log->end_time)->format('h:i A');
             $d[] = @\Carbon\Carbon::parse($busy_log->start_time)->format('d-m-Y');
             $difference = strtotime($busy_log->end_time) - strtotime($busy_log->start_time) ;
             $d[] = gmdate("H:i:s", $difference);
             $data_array[] = $d;
       }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Restaurant Busy Report';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['date_filter']      =     1;  
                          $data['store_filter']      =    1;  
                          $data['data']      =         $main;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }





//filterss = store,date
    //Route-  2 =========================================================================
   public function reports_rejected_orders(Request $request  )
   {

            //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Order#';
        $columns_array[] = 'Date';
        $columns_array[] = 'Order Amount';
        $columns_array[] = 'Customer Name';
        $columns_array[] = 'Contact Number';

        $store_id = $this->get_variable_store_id();
        $model = new \App\Order;
	   	$model = $model::where('order_id' ,'<>', '0')->where('order_status' , 'cancelled'); 

        if(isset($_GET['date_from']) && $_GET['date_from'] != '' && isset($_GET['date_to']) && $_GET['date_to'] != '' )
        {
            $date_from= $_GET['date_from'];
              $date_to = $_GET['date_to'];
            $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
            $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
            $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
        }
        if($store_id != '' && $store_id != null)
        {
            $model = $model->where('store_id',@$store_id);
        }
        $result = $model->get();
		
 
      $data_array = array();
      foreach($result as $e)
      {
             $d= array();
             $d[] = $e->order_id;
             $d[] = @\Carbon\Carbon::parse($e->created_at)->format('h:i A');
             $d[] = $e->total;

             $customer_id = $e->customer_id;
             $customer_details = @\App\User::where('user_id',$customer_id)->get(['first_name','last_name','phone']);
             $d[] = @$customer_details[0]['first_name']." ".@$customer_details[0]['last_name'];
             $d[] = @$customer_details[0]['phone'];
             $data_array[] = $d;
       }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Cancelled Orders';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main;  
                             $data['date_filter']      =     1;  
                          $data['store_filter']      =    1;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }







    //Route-  2 =========================================================================
   public function best_selling_area(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Area Name';
        $columns_array[] = 'Orders';
        $columns_array[] = 'Total Order Amount';
        $columns_array[] = '%Sales';

    


        $result = DB::table("orders")
      ->select(DB::raw("* , COUNT(*) as count , sum(total) as total_order_amount "))
      ->orderBy("created_at")
      ->groupBy(DB::raw("area_id"))->where('area_id','<>','null')->where('order_status','completed')
      ->get();
 
      
 
      $data_array = array();
      foreach($result as $e)
      {


      	if($e->area_id != '' &&  $e->area_id != null )
      	{
             $d= array();
             $d[] = $this->validate_string(@\App\Areas::where('area_id',$e->area_id)->where('area_id','<>','')->first(['title'])->title);

               $total_order_in_area = @\App\Order::where('area_id',$e->area_id)->where('area_id','<>','')->where('order_status','completed')->count();
             $d[] = $e->count;
             $d[] = $e->total_order_amount;

             $store_id = $e->store_id;
              $total_order_in_store = @\App\Order::where('store_id',$store_id)->where('store_id','<>','')->where('order_status','completed')->count();
 
               $percentage = $total_order_in_area / $total_order_in_store * 100;

             $d[] = $percentage."%";
             $data_array[] = $d;
         }
       }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Best Selling Area';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    0;  

                          $data['data']      =         $main;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }






      //Route-  2 =========================================================================
   public function best_selling_restaurant(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Date';
        $columns_array[] = 'Month';
        $columns_array[] = 'Restaurant Name';
        $columns_array[] = 'Orders';
        $columns_array[] = 'Pickup Orders';
        $columns_array[] = 'Delivery Orders';
        $columns_array[] = 'Total Orders Amount';
        $columns_array[] = 'Cash Orders';
        $columns_array[] = 'Credit Card Orders';
 
         $result = DB::table("orders")
      ->select(DB::raw("* , COUNT(*) as count , sum(total) as total_order_amount"))
      ->orderBy("created_at")
      ->groupBy(DB::raw("store_id"))->where('area_id','<>','null')->where('order_status','completed')
      ->get();


 
      $data_array = array();
      foreach($result as $e)
      {
      	if($e->area_id != '' &&  $e->area_id != null )
      	{
		      		if($e->store_id != '' && $e->store_id != null)
		      		{
		             $d= array();
		             $d[] = ''; //Date
		             $d[] = ''; //Month
		             $d[] = @\App\Stores::where('store_id',$e->store_id)->first(['store_title'])->store_title;
		             $d[] = @\App\Order::where('store_id',$e->store_id)->where('order_status','completed')->count();

		             $d[] = @\App\Order::where('store_id',$e->store_id)->where('order_status','completed')->where('delivery_type','restaurant_pickup')->count();//pickup order
		             $d[] = @\App\Order::where('store_id',$e->store_id)->where('order_status','completed')->where('delivery_type','home_delivery')->count();//delivr orders
		             $d[] =$e->total_order_amount;
		             $d[] = @\App\Order::where('store_id',$e->store_id)->where('order_status','completed')->where('payment_mode','cash')->sum('total');//delivr orders
		             $d[] = @\App\Order::where('store_id',$e->store_id)->where('order_status','completed')->where('payment_mode','card')->sum('total');//delivr orders

		               //$total_order_in_area = @\App\Order::where('area_id',$e->area_id)->where('area_id','<>','')->where('order_status','completed')->count();
	 
		             $data_array[] = $d;
		           }
         }
       }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Best Selling Restaurant';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    0;  
                          $data['data']      =         $main;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }













//filters = store

      //Route-  2 =========================================================================
   public function delivery_boy_report(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Driver Name';
        $columns_array[] = 'Staff';
        $columns_array[] = 'Orders';
        $columns_array[] = 'Orders Amount';
        $columns_array[] = 'Late Delivery';
		
		
		
		$store_id = $this->get_variable_store_id();
		
        $model = new \App\User;
	   	$model = $model::where('user_id' ,'<>', '0'); 
 
        if($store_id != '' && $store_id != null)
        {
            $model = $model->where('store_id',@$store_id);
        }
       
		
		
 
          $result = $model->where('user_type','5')->distinct('user_id')->pluck('user_id');
          

 
      $data_array = array();
      foreach($result as $driver_id)
      {

      	if($driver_id != '' &&  $driver_id != null )
      	{
		      		 
		             $d= array();
		             $d[] = @\App\User::where('user_id', $driver_id)->first(['first_name'])->first_name." ".@\App\User::where('user_id', $driver_id)->first(['last_name'])->last_name;
		             $d[] = $driver_id; //Month

		               $order_ids_from_task = @\App\Task::where('driver_id',$driver_id)->pluck('order_id');

		             $orders_count = @\App\Order::whereIn('order_id',$order_ids_from_task)->where('order_status','completed')->count();
		             $orders_amount = @\App\Order::whereIn('order_id',$order_ids_from_task)->where('order_status','completed')->sum('total');
		             $d[] = $orders_count;
		             $d[] = $orders_amount;

		             $d[] ='';
		   
		               //$total_order_in_area = @\App\Order::where('area_id',$e->area_id)->where('area_id','<>','')->where('order_status','completed')->count();
	 
		             $data_array[] = $d;
		           
         }
       }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Delivery Boy Reports';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    1;  
                          $data['data']      =         $main;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }




//filter date,store

      //Route-  2 =========================================================================
   public function daily_sales_report(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Date';
        $columns_array[] = 'Month';
        $columns_array[] = 'Orders';
        $columns_array[] = 'Pickup Orders';
        $columns_array[] = 'Delivery Orders';
        $columns_array[] = 'Total Orders Amount';
        $columns_array[] = 'Cash Orders';
        $columns_array[] = 'Credit Card Orders';
		
		$store_id = $this->get_variable_store_id();
		


        if(isset($_GET['date_from']) && $_GET['date_from'] != '' && isset($_GET['date_to']) && $_GET['date_to'] != '' )
        {
            $date_from= $_GET['date_from'];
            $date_to = $_GET['date_to'];
       }
	   else{
		   $date_from= @\Carbon\Carbon::now()->addMonths(-1)->format('Y-m-d');
              $date_to = @\Carbon\Carbon::now()->format('Y-m-d');
	   }
	   
	    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	    $date_array = $this->date_range($date_from , $date_to);
 
 
      $data_array = array();
      foreach($date_array as $date)
      {
                     $d= array();
		             $d[] =$date;
					 
			 if($store_id != '' && $store_id != null)
            {
                     $d[] = @\Carbon\Carbon::parse($date)->format('M');
                     $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('delivery_type','restaurant_pickup')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('delivery_type','home_delivery')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('payment_mode','cash')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('payment_mode','card')->sum('total');
            }
			else{
				     $d[] = @\Carbon\Carbon::parse($date)->format('M');
                     $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('delivery_type','restaurant_pickup')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('delivery_type','home_delivery')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('payment_mode','cash')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('payment_mode','card')->sum('total');
		        }
       
	   
	   
		             
		   
		               //$total_order_in_area = @\App\Order::where('area_id',$e->area_id)->where('area_id','<>','')->where('order_status','completed')->count();
	 
		             $data_array[] = $d;
		           
          
       }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Daily Sales Report';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main;  
                             $data['date_filter']      =     1;  
                          $data['store_filter']      =    1;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }





//filter = store
      //Route-  2 =========================================================================
   public function monthly_sales_report(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Date';
        $columns_array[] = 'Month';
        $columns_array[] = 'Orders';
        $columns_array[] = 'Pickup Orders';
        $columns_array[] = 'Delivery Orders';
        $columns_array[] = 'Total Orders Amount';
        $columns_array[] = 'Cash Orders';
        $columns_array[] = 'Credit Card Orders';
        $store_id = $this->get_variable_store_id();
		

/**

	    $date_from= $_GET['date_from'];
	    $date_to = $_GET['date_to'];
	    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	    $date_array = $this->date_range($date_from , $date_to);
**/



	  $dates3 = array();
      for($t = 0; $t <= 12; $t++) 
      {
        $dates_array[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
      }
 
 
      $total3 = 0;

      foreach(array_reverse($dates3)  as $date)
      {
        $ud3['label'] = \Carbon\Carbon::parse($date)->format('M');
        $ud3['date'] = $date;
        $ud3['y'] = \App\Order::whereDate('created_at', 'like', "%".$date."%")->count();
        $data[] = $ud3;
      }


	 
 
      foreach($dates_array as $date)
      {
                     $d= array();
		             $d[] =$date;
		             $d[] = @\Carbon\Carbon::parse($date)->format('M');
 
 
            if($store_id != '' && $store_id != null)
            {
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('delivery_type','restaurant_pickup')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('delivery_type','home_delivery')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('payment_mode','cash')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('store_id',$store_id)->where('order_status','completed')->where('payment_mode','card')->sum('total');
			}
			
			else
			{
				     $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('delivery_type','restaurant_pickup')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('delivery_type','home_delivery')->count();
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('payment_mode','cash')->sum('total');
		             $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->where('payment_mode','card')->sum('total');
			}
		               //$total_order_in_area = @\App\Order::where('area_id',$e->area_id)->where('area_id','<>','')->where('order_status','completed')->count();
	 
		             $data_array[] = $d;
		           
          
       }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Monthly Sales Report';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main;
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    1;    
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }

  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
      //Route-  2 =========================================================================
   public function mataem_revenue_report(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Registered Restaurant';
        $columns_array[] = 'Month';
        $columns_array[] = 'Total Orders Amount';
        $columns_array[] = 'Mataem Commission';
   


/**

	    $date_from= $_GET['date_from'];
	    $date_to = $_GET['date_to'];
	    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	    $date_array = $this->date_range($date_from , $date_to);
**/
	  $dates3 = array();
      for($t = 0; $t <= 12; $t++) 
      {
        $dates_array[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
      }
 
      $store_ids = @\App\Stores::pluck('store_id');
   
      foreach($dates_array as $date)
      {
                     $d= array();
		             $d[] = @\App\Stores::where('created_at','like','%'.$date.'%')->count();
		             $d[] = @\Carbon\Carbon::parse($date)->format('M');
                     $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->sum('total');
					 $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->sum('store_commission');
		             $data_array[] = $d;
	  }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Mataem Revenue Report';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main;  
                          $data['date_filter']      =     0;  
                          $data['store_filter']      =    0;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }


      //Route-  2 =========================================================================
   public function restaurant_performance_report(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Registered Restaurant';
        $columns_array[] = 'Month';
        $columns_array[] = 'Total Orders Amount';
        $columns_array[] = 'Orders';
   


/**

	    $date_from= $_GET['date_from'];
	    $date_to = $_GET['date_to'];
	    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	    $date_array = $this->date_range($date_from , $date_to);
**/
	  $dates3 = array();
      for($t = 0; $t <= 12; $t++) 
      {
        $dates_array[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
      }
 
      $store_ids = @\App\Stores::pluck('store_id');
   
      foreach($dates_array as $date)
      {
                     $d= array();
		             $d[] = @\App\Stores::where('created_at','like','%'.$date.'%')->count();
		             $d[] = @\Carbon\Carbon::parse($date)->format('M');
                     $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->where('order_status','completed')->sum('total');
					 $d[] = @\App\Order::where('created_at','like','%'.$date.'%')->count();
		             $data_array[] = $d;
	  }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Restaurant Performance Report';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main;  
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    0;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }

  
  
  
  
  
  
  
      //Route-  2 =========================================================================
   public function cookies_reward_report(Request $request  )
   {
        //columns array starts 
        $columns_array = array();
        $columns_array[] = 'Month';
        $columns_array[] = 'Year';
		$columns_array[] = 'Registered Restaurants';
        $columns_array[] = 'Cookies Earned';
        $columns_array[] = 'Redeemed Cookies';
   


/**

	    $date_from= $_GET['date_from'];
	    $date_to = $_GET['date_to'];
	    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
	    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
	    $date_array = $this->date_range($date_from , $date_to);
**/
	  $dates3 = array();
      for($t = 0; $t <= 12; $t++) 
      {
        $dates_array[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
      }
 
      $store_ids = @\App\Stores::pluck('store_id');
   
      foreach($dates_array as $date)
      {
                     $d= array();
					 $d[] = @\Carbon\Carbon::parse($date)->format('M');
		             $d[] = @\Carbon\Carbon::parse($date)->format('Y');
		             $d[] = @\App\Stores::where('created_at','like','%'.$date.'%')->count();
                     $d[] = @\App\LoyaltyPoints::where('created_at','like','%'.$date.'%')->where('type','order_earned')->sum('points');
					 $order_spent_points =  @\App\LoyaltyPoints::where('created_at','like','%'.$date.'%')->where('type','order_spent')->pluck('points');
					 $t=0;
					 foreach($order_spent_points as $p)
					 {
						$t = $t + abs($p); 
					 }
					 $d[] = $t;
		             $data_array[] = $d;
	  }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Cookies Reward Report';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main; 
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    0;   
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }




















        //Route-  2 =========================================================================
   public function due_payment_report(Request $request  )
   {

    $store_id = $this->get_variable_store_id();
        //columns array starts 
        $columns_array = array();
    
        $columns_array[] = 'Month';
        $columns_array[] = 'Total Orders Amount';
        $columns_array[] = 'Mataem Commission';
        $columns_array[] = 'Due';
   


/**

      $date_from= $_GET['date_from'];
      $date_to = $_GET['date_to'];
      $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
      $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
      $date_array = $this->date_range($date_from , $date_to);
**/
    $dates3 = array();
      for($t = 0; $t <= 12; $t++) 
      {
        $dates_array[] = date('Y-m', strtotime( date( 'Y-m-01' )." -".$t." months"));
      }
 
      $store_ids = @\App\Stores::pluck('store_id');
   
      foreach($dates_array as $date)
      {
                     $d= array();

                     if($store_id != '' && $store_id != null && $store_id != ' ')
                     {

                                 $d[] = @\Carbon\Carbon::parse($date)->format('M');
                    
                                 $total = @\App\Order::where('order_status','completed')->where('store_id',$store_id)->where('created_at','like','%'.$date.'%')->sum('total');
                                 $store_commission = @\App\Order::where('order_status','completed')->where('store_id',$store_id)->where('created_at','like','%'.$date.'%')->sum('store_commission');

                                 $due = $this->validate_integer(floatval($total)) - $this->validate_integer(floatval($store_commission));
                                 $d[] = round($total,2);
                                 $d[] = round($store_commission,2); 
                                 $d[] = abs(round($due,2));

                     }
                     else
                     {
                                 $d[] = @\Carbon\Carbon::parse($date)->format('M');
                    
                                 $total = @\App\Order::where('order_status','completed')->where('created_at','like','%'.$date.'%')->sum('total');
                                 $store_commission = @\App\Order::where('order_status','completed')->where('created_at','like','%'.$date.'%')->sum('store_commission');

                                 $due = $this->validate_integer(floatval($total)) - $this->validate_integer(floatval($store_commission));
                                 $d[] = round($total,2);
                                 $d[] = round($store_commission,2); 
                                 $d[] = abs(round($due,2));

                     }
           
                     $data_array[] = $d;
    }


 
        // create actual json
        $main = array();
        $main_obj['header'] = 'Due Payments Report';
        $main_obj['columns'] = $columns_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = @\Carbon\Carbon::now()->format('d M, Y h:i A');
        $main[] = $main_obj;
 
        if(sizeof($main) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Order Reports List Fetched';
                          $data['data']      =         $main;  
                             $data['date_filter']      =     0;  
                          $data['store_filter']      =    1;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Reports Found';
                          $data['data']           =   [];  
          }
           return $data;
         
  }

  
  
  
  























public function date_range($sd,$ed)
{
			$tmp = array();
			$sdu = strtotime($sd);
			$edu = strtotime($ed);
			while ($sdu <= $edu) {
			$tmp[] = date('Y-m-d',$sdu);
			$sdu = strtotime('+1 day',$sdu);
			}

			return $tmp;
}















//Route-23.4 =============================================================
   public function reports_customers(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$request_type = $this->get_variable_request_type();
		$user_type = $this->get_variable_user_type();
 
		$table_fields = 'user_id,first_name,last_name,email,phone,facebook_id,status,referral_code,longitude,latitude,loyalty_points,created_at_formatted';
	  
	    if($table_fields !='' && $table_fields != null)
        { $table_fields_array = explode(",",$table_fields); }
        else  { $table_fields_array = [];  }



 
	    $model = new \App\User;
	    $model = $model::where('user_id' ,'<>', '0');  


		if($user_type != '' && $user_type != null)
		{  $model = $model->where('user_type' , $user_type);  }	


  ///auth filter starts
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
 
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
  ///auth filter starts Ends

    if($search != '' && $search != null)
		{  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(email,' ',phone,' ',first_name,' ',last_name,' ',user_id)"),'like', '%'.$search.'%'); });  }

        $model = $model->orderBy($orderby,$order);	

 	 
        $date_from= $_GET['date_from'];
        $date_to = $_GET['date_to'];
        $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
        $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
        $user_data = \App\User::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();
 
                $data_array = array();
        foreach($user_data as $order_d)
        {
        	$data_obj_array = array();
        	for($i=0;$i<sizeof($table_fields_array);$i++)
        	{

        		$data_obj_array[] = $order_d[$table_fields_array[$i]];
        	}
        	$data_array[] = $data_obj_array;
        }
        

        // create actual json
        $main = array();
        $main_obj['header'] = 'Users  Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;


	    	   
	              if(sizeof($main) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Users List Fetched Successfully';
                          $data['data']      =   $main;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No User Found';
                          $data['data']      =   [];  
					}
				   return $data;
         
  }

  



//Route-23.5 ================================================
   public function reports_tasks(Request $request  )
   {
    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$request_type = $this->get_variable_request_type();
		$user_type = $this->get_variable_user_type();
 
		$table_fields = 'task_id,driver_id,order_id,pickup_contact_title,pickup_contact_phone,pickup_time,picked_up_time,store_title,driver_name,dropoff_contact_title,dropoff_contact_phone,dropoff_time,status,task_type,created_at_formatted';
	  
	  if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }

	  $model = new \App\Task;
	  $model = $model::where('id' ,'<>', '0');  

		if($user_type != '' && $user_type != null)
		{ $model = $model->where('user_type' , $user_type); }	

    ///auth filter starts
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
 
    if($user_type == '3') //vendor
    {
      $model = $model->where('vendor_id' , $auth_user_id );   
    }
    ///auth filter starts Ends

    if($search != '' && $search != null)
		{ $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(task_id,' ',order_id,' ',driver_id,' ',store_id)"),'like', '%'.$search.'%'); });  }

        $model = $model->orderBy($orderby,$order);	
 
        $date_from= $_GET['date_from'];
        $date_to = $_GET['date_to'];
        $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
        $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
        $user_data = \App\Task::where('created_at','<',$date_to)->where('created_at','>',$date_from)->get();

 
        $data_array = array();
        foreach($user_data as $order_d)
        {
        	$data_obj_array = array();
        	for($i=0;$i<sizeof($table_fields_array);$i++)
        	{

        		$data_obj_array[] = $order_d[$table_fields_array[$i]];
        	}
        	$data_array[] = $data_obj_array;
        }
     
        // create actual json
        $main = array();
        $main_obj['header'] = 'Task  Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;

	      if(sizeof($main) > 0)
					{
					 	              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Report Fetched Successfully';
                          $data['data']      =   $main;  
				  }
					else
					{
						              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
					}
				   return $data;
         
  }




//Route-23.6 ===========================================================================================================
     public function reports_items(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();
		$category = $this->get_variable_category();
		$tag = $this->get_variable_tag();
		$status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
    $table_fields = 'item_id,item_title,item_price,item_discount,item_discount_expiry_date,store_title,vendor_name,item_active_status,item_stock_count';
	  
	  if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }
        
		
		$model = new \App\Items;
		$model = $model::where('item_id' ,'<>', '0');  
	 

    //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================

 
		if($status != '' && $status != null)
		{  $model = $model->where('item_active_status' , $status);  }	


    if($store_id != '' && $store_id != null)
    {  $model = $model->where('store_id' , $store_id);  } 
	   
	  if($search != '' && $search != null)
		{  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(item_id,' ',item_title)"),'like', '%'.$search.'%'); });  }
      
	  if($category != '' && $category != null)
		{   $model = $model->whereRaw("FIND_IN_SET('".$category."',items.item_categories)");  }	
	
		if($tag != '' && $tag != null)
		{   $model = $model->whereRaw("FIND_IN_SET('".$tag."',items.item_tags)");  }

 
 
 
    $date_from= $_GET['date_from'];
    $date_to = $_GET['date_to'];
    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
    $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
    $model = $model->orderBy($orderby,$order);	
    $result = $model->get();



 	 
    $data_array = array();
    foreach($result as $order_d)
    {
     	$data_obj_array = array();
      	for($i=0;$i<sizeof($table_fields_array);$i++)
        	{

        		$data_obj_array[] = $order_d[$table_fields_array[$i]];
        	}
     	$data_array[] = $data_obj_array;
    }
        
        // create actual json
        $main = array();
        $main_obj['header'] = 'Items Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
	    	   
	        if(sizeof($main) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Report Fetched Successfully';
                          $data['data']      =   $main;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
					}
				   return $data;
         
  }
















//Route-23.7 ===========================================================================================================
     public function reports_coupons(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
    $search = $this->get_variable_search();
    $status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
      
       $table_fields = 'coupon_id,coupon_title,coupon_desc,coupon_code,discount,valid_from,expiry,type,max_discount,limit_total,limit_user,minimum_order_amount,maximum_order_amount,active_status,vendor_name,store_title';
    
    if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }
    $include_consumed = $this->get_variable_include_consumed();
    $include_expired  = $this->get_variable_include_expired();
  
    
    $model = new \App\Coupons;
    $model = $model::where('coupon_id' ,'<>', '0');  


    //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================



    

      if($store_id != '' && $store_id != null)
    {   $model = $model->where('store_id' , $store_id);  } 
 
    //user type_filter check ends

        if($status != 'any' && $status != '' && $status != null)
        {
          $model->where('active_status' , $status); 
        }
 
        if(   $include_expired == 'true')
        {

            $model->whereDate('expiry', '<', date("Y-m-d"));
        }
              else
        {

            $model->whereDate('expiry', '>', date("Y-m-d"));
        }
 

      if($search != '' && $search != null)
    {  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(coupon_id,' ',coupon_code,' ',coupon_desc,' ', coupon_title)"),'like', '%'.$search.'%'); });  }
        

    $model = $model->orderBy($orderby,$order);
         
    $date_from= $_GET['date_from'];
    $date_to = $_GET['date_to'];
    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
    $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
    $model = $model->orderBy($orderby,$order);  
      $result = $model->get();


 
   
    $data_array = array();
    foreach($result as $order_d)
    {
      $data_obj_array = array();
        for($i=0;$i<sizeof($table_fields_array);$i++)
          {

            $data_obj_array[] = $order_d[$table_fields_array[$i]];
          }
      $data_array[] = $data_obj_array;
    }
        
        // create actual json
        $main = array();
        $main_obj['header'] = 'Coupons Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
           
          if(sizeof($main) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Coupons Report Fetched Successfully';
                          $data['data']      =   $main;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
          }
           return $data;
         
  }












//Route-23.8 ===========================================================================================================
     public function reports_reviews(Request $request  )
   {

    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
    $search = $this->get_variable_search();
    $category = $this->get_variable_category();
    $tag = $this->get_variable_tag();
    $status = $this->get_variable_status();
    $store_id = $this->get_variable_store_id();
    $table_fields = $this->get_variable_table_fields();
    
    if($table_fields !='' && $table_fields != null)
    { $table_fields_array = explode(",",$table_fields); }
    else  { $table_fields_array = [];  }
        
    
    $model = new \App\Items;
    $model = $model::where('item_id' ,'<>', '0');  
   

    //user types filters ==========================================
    $auth_user_id = $this->get_auth_user_id();
    $user_type = $this->get_auth_user_type();
    if($user_type == '4') //store manager_id
    {
       $store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
       if($store_id == '' || $store_id == null) { $store_id = 'NA';}
    }
     if($user_type == '3') //vendor
    {   $model = $model->where('vendor_id' , $auth_user_id );   
    }
    //user type_filter check ends =================================

 
    if($status != '' && $status != null)
    {  $model = $model->where('item_active_status' , $status);  } 


    if($store_id != '' && $store_id != null)
    {  $model = $model->where('store_id' , $store_id);  } 
     
    if($search != '' && $search != null)
    {  $model = $model->where(function($q) use ($search) { $q->where( DB::raw("CONCAT(item_id,' ',item_title)"),'like', '%'.$search.'%'); });  }
      
    if($category != '' && $category != null)
    {   $model = $model->whereRaw("FIND_IN_SET('".$category."',items.item_categories)");  } 
  
    if($tag != '' && $tag != null)
    {   $model = $model->whereRaw("FIND_IN_SET('".$tag."',items.item_tags)");  }

 
 
 
    $date_from= $_GET['date_from'];
    $date_to = $_GET['date_to'];
    $date_from =\Carbon\Carbon::parse($date_from)->format('Y-m-d');
    $date_to =\Carbon\Carbon::parse($date_to)->format('Y-m-d');
    $model = $model->where('created_at','<',$date_to)->where('created_at','>',$date_from);
    $model = $model->orderBy($orderby,$order);  
    $result = $model->get();

   

   
    $data_array = array();
    foreach($result as $order_d)
    {
      $data_obj_array = array();
        for($i=0;$i<sizeof($table_fields_array);$i++)
          {

            $data_obj_array[] = $order_d[$table_fields_array[$i]];
          }
      $data_array[] = $data_obj_array;
    }
        
        // create actual json
        $main = array();
        $main_obj['header'] = 'Items Report';
        $main_obj['columns'] = $table_fields_array;
        $main_obj['data'] = $data_array;
        $main_obj['footer'] = 'Footer Text Goes Here';
        $main[] = $main_obj;
           
          if(sizeof($main) > 0)
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Task Report Fetched Successfully';
                          $data['data']      =   $main;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Task Found';
                          $data['data']      =   [];  
          }
           return $data;
         
  }







 


















 
   
//==========================================================================misc functions===================================================================//   

public function get_variable_user_type()
{
	 if(isset($_GET['user_type']) && $_GET['user_type'] != null && $_GET['user_type'] != '')
					{ $user_type = $_GET['user_type']; }
					else 
					{ $user_type = ''; }
    return $user_type;
}	
 public function get_variable_request_type()
 {
 	   if(isset($_GET['request_type']) && $_GET['request_type'] != null && $_GET['request_type'] != '')
					{ $request_type = $_GET['request_type']; }
					else 
					{ $request_type = ''; }
       return $request_type;
 }
 
public function get_variable_search()
{
   if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
          { $search = $_GET['search']; }
          else 
          { $search = ''; }
    return $search;
} 

public function get_variable_store_id()
{
   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
          { $store_id = $_GET['store_id']; }
          else 
          { $store_id = ''; }
    return $store_id;
} 



public function get_variable_driver_id()
{
   if(isset($_GET['driver_id']) && $_GET['driver_id'] != null && $_GET['driver_id'] != '')
          { $var = $_GET['driver_id']; }
          else 
          { $var = ''; }
    return $var;
} 

    

   public function get_variable_include_expired()
{
   if(isset($_GET['include_expired']) && $_GET['include_expired'] != null && $_GET['include_expired'] != '')
          { $include_expired = $_GET['include_expired']; }
          else 
          { $include_expired = 'false'; }
    return $include_expired;
}

   public function get_variable_include_consumed()
{
   if(isset($_GET['include_consumed']) && $_GET['include_consumed'] != null && $_GET['include_consumed'] != '')
          { $include_consumed = $_GET['include_consumed']; }
          else 
          { $include_consumed = 'false'; }
    return $include_consumed;
}





   




   public function get_variable_expiry($request)
{
   if(  $request['expiry'] != null && $request['expiry'] != '')
          { $expiry = $request['expiry']; }
          else 
          { $expiry = \Carbon\Carbon::now()->addYears(19); }
    return $expiry;
}



   public function get_variable_valid_from($request)
{
   if(  $request['valid_from'] != null && $request['valid_from'] != '')
          { $valid_from = $request['valid_from']; }
          else 
          { $valid_from = \Carbon\Carbon::now(); }
    return $valid_from;
}

   public function get_variable_category()
{
	 if(isset($_GET['category']) && $_GET['category'] != null && $_GET['category'] != '')
					{ $category = $_GET['category']; }
					else 
					{ $category = ''; }
    return $category;
}	

   public function get_variable_tag()
{
	 if(isset($_GET['tag']) && $_GET['tag'] != null && $_GET['tag'] != '')
					{ $tag = $_GET['tag']; }
					else 
					{ $tag = ''; }
    return $tag;
}	

   public function get_variable_table_fields()
{
   if(isset($_GET['table_fields']) && $_GET['table_fields'] != null && $_GET['table_fields'] != '')
          { $table_fields = $_GET['table_fields']; }
          else 
          { $table_fields = ''; }
    return $table_fields;
}

public function get_variable_status()
{
   if(isset($_GET['status']) && $_GET['status'] != null && $_GET['status'] != '')
          { $var = $_GET['status']; }
          else 
          { $var = ''; }
    return $var;
} 

public function get_variable_task_type()
{
   if(isset($_GET['task_type']) && $_GET['task_type'] != null && $_GET['task_type'] != '')
          { $var = $_GET['task_type']; }
          else 
          { $var = ''; }
    return $var;
} 


//check item existence by id
public function item_exist($id)
{
	$count = @\App\Items::where('item_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	

 public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

//validate all the request variables if they are null or empty , it will return values
public function validate_datetime($var)
{
	 if(!isset($var) or $var == null or $var == '' or $var == ' ')
	 {
	 	 $date = @\Carbon\Carbon::now();
	 	 $date = @\Carbon\Carbon::parse($date);
		 return $this->add_days($date , '7' , 'Y-m-d h:i:s');
	 }
	 else{
		 return $var;
	 }
}


 
  
 
  
  public function get_item_meta_type_id($identifer)
  {
	  $item_meta_type_id = @\App\ItemMetaType::where('identifier',$identifer)->first(['item_meta_type_id'])->item_meta_type_id;
	  return $item_meta_type_id;
  }
  



///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

 

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}
 





public function get_variable_user_id()
{
   if(isset($_GET['user_id']) && $_GET['user_id'] != null && $_GET['user_id'] != '')
          { $user_id = $_GET['user_id']; }
          else 
          { $user_id = ''; }
    return $user_id;
} 
public function get_variable_order_id()
{
   if(isset($_GET['order_id']) && $_GET['order_id'] != null && $_GET['order_id'] != '')
          { $order_id = $_GET['order_id']; }
          else 
          { $order_id = ''; }
    return $order_id;
} 
 
      
  

   public function get_variable_fields()
{
   if(isset($_GET['table_fields']) && $_GET['table_fields'] != null && $_GET['table_fields'] != '')
          { $table_fields = $_GET['table_fields']; }
          else 
          { $table_fields = ''; }
    return $table_fields;
}



 



  
 


 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}