<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class PaymentRequestsController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route- ============================================================== Store Item to Items table =========================================> 
   public function store(Request $request)
   {


     ///auth filter starts
        $auth_user_id = $this->get_auth_user_id();
        $auth_user_type = $this->get_auth_user_type();

         $auth_user_id = $this->validate_string($auth_user_id);
        $auth_user_type = $this->validate_string($auth_user_type);



        if($auth_user_type == '0' || $auth_user_type == 0)
        {
        	        $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'User Type not defined';
                    return $data;	
        }

        if($auth_user_id == '0' || $auth_user_id == 0)
        {
        	        $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Authentication failed';
                    return $data;	
        }


 


                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'type' => 'required',

			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
	                $type = $this->validate_string($request->type);
	                $notes = $this->validate_string($request->notes);
	       

                   if($type == 'vendor_admin')
                   {
                   	  $sender_id = $auth_user_id;
                   	  $receiver_id = @\App\User::where('user_type','1')->first(['user_id'])->user_id;


                   }
                   else if($type == 'manager_vendor')
                   {
                   	  $sender_id = $auth_user_id;
                      $receiver_id = @\App\Stores::where('manager_id',$auth_user_id)->first(['vendor_id'])->vendor_id;

                      $receiver_id = $this->validate_string($receiver_id);
                      if($receiver_id == '' || $receiver_id == null)
                      {
                      	        $data['status_code']    =   0;
			                    $data['status_text']    =   'Receiver Not Found';             
			                    $data['message']        =   '';
			                    return $data;
                      }

                   }
                   else
                   {
                        $data['status_code']    =   0;
	                    $data['status_text']    =   'Failed';             
	                    $data['message']        =   'Invalid Request Type';
	                    return $data;	
                    }
			 
						$location = new \App\PaymentRequests;
						$location->notes = @$request->notes;
						$location->sender_id = @$sender_id;
						$location->receiver_id = @$receiver_id;
						$location->status = '0';
						$location->type = @$request->type;
	                    $location->save();
						
					    if($location != '')
						{



							    
                                     $receiver_details = @App\User::where('user_id', $receiver_id)->get(["email","first_name","last_name"]);
                                     $sender_details = @App\User::where('user_id', $sender_id)->get(["email","first_name","last_name"]);
       
                                     $receiver_email = @$receiver_details[0]['email'];
 
         
  
 
 
 
           
             $data2=array('notes' => $request->notes , 'sender_details'=>$sender_details , 'receiver_details'=>$receiver_details );

           
             \Mail::send('emails.payment_requests', $data2, function($message) use ($receiver_email , $receiver_details , $sender_details) {
             $message->to($receiver_email, 'Mataem')->subject('Mataem - Payment Request');
             $message->from('noreply@mateam.com', 'Mataem');
         });
            

 
							  $data['status_code']    =   1;
	                          $data['status_text']    =   'Success';             
	                          $data['message']        =   'Request Sent Successfully';
	                          $data['data']      =   $location;  
					    }
						else
						{
							  $data['status_code']    =   0;
	                          $data['status_text']    =   'Failed';             
	                          $data['message']        =   'Unable to Submit Request';
	                          $data['data']      =   [];  
						}
				   
				  return $data;
				 
  }
   

   
  
  // Route ============================================================== Get Categories List =========================================> 
   public function get_list()
   {

   	     ///auth filter starts
        $auth_user_id = $this->get_auth_user_id();
        $auth_user_type = $this->get_auth_user_type();

         $auth_user_id = $this->validate_string($auth_user_id);
        $auth_user_type = $this->validate_string($auth_user_type);



        if($auth_user_type == '0' || $auth_user_type == 0)
        {
        	        $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'User Type not defined';
                    return $data;	
        }

        if($auth_user_id == '0' || $auth_user_id == 0)
        {
        	        $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Authentication failed';
                    return $data;	
        }


	   
        $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
 
	 
	    $model = new \App\PaymentRequests;
		$model = $model::where('id' ,'<>', '0');  

		if($auth_user_type != '1' && $auth_user_type != 1)
		{
            $model = $model->where('receiver_id' , $auth_user_id);  
		}
 
		$model = $model->orderBy($orderby,$order);
        
 	   
        $result = $model->paginate($per_page);
	   
	              if(sizeof($result) > 0)
					{




						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Payment Requests Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Requests Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  




     // Route ============================================================== Get Categories List =========================================> 
   public function get_list_sent()
   {
 
   	     ///auth filter starts
        $auth_user_id = $this->get_auth_user_id();
        $auth_user_type = $this->get_auth_user_type();

         $auth_user_id = $this->validate_string($auth_user_id);
        $auth_user_type = $this->validate_string($auth_user_type);



        if($auth_user_type == '0' || $auth_user_type == 0)
        {
        	        $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'User Type not defined';
                    return $data;	
        }

        if($auth_user_id == '0' || $auth_user_id == 0)
        {
        	        $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   'Authentication failed';
                    return $data;	
        }


	   
        $per_page = $this->get_variable_per_page(); //ASC or DESC
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
 
	 
	    $model = new \App\PaymentRequests;
		$model = $model::where('id' ,'<>', '0');  
 

		if($auth_user_type != '1' && $auth_user_type != 1)
		{
            $model = $model->where('sender_id' , $auth_user_id);  
		}
 
		$model = $model->orderBy($orderby,$order);
        
 	   
        $result = $model->paginate($per_page);
	   
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Payment Requests Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Requests Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  






  // Route-  ============================================================== Get Items List =========================================> 
   public function update_status(Request $request , $id)
   {
	    
					$validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'status' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
			 
	                $status = $this->validate_integer($request->status);
				 
	                App\PaymentRequests::where('id', $id)->update(['status' => $status ]);
	               


$receiver_id = @\App\PaymentRequests::where('id',$id)->first(['receiver_id'])->receiver_id;
$sender_id = @\App\PaymentRequests::where('id',$id)->first(['sender_id'])->sender_id;


	                                                    $receiver_details = @App\User::where('user_id', $receiver_id)->get(["email","first_name","last_name"]);
                                     $sender_details = @App\User::where('user_id', $sender_id)->get(["email","first_name","last_name"]);
       
                                     $receiver_email = @$receiver_details[0]['email'];
 
         
  
 
 
 
           
             $data2=array('notes' => $request->notes , 'sender_details'=>$sender_details , 'receiver_details'=>$receiver_details , 'status'=>$request->status);

           
             \Mail::send('emails.payment_action', $data2, function($message) use ($receiver_email , $receiver_details , $sender_details) {
             $message->to($receiver_email, 'Mataem')->subject('Mataem - Payment Request Response');
             $message->from('noreply@mateam.com', 'Mataem');
         });





		 
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Process Successfull';
                          $data['data']      =  [];  
	 
				   return $data;
   }  


   
 
 
 
 
 // Route-  ========================================================To delete Location =========================================> 
 public function destroy($id)
 {   	 
   	         //check existance of item with ID in items table
				 	$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Location with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
                             @\App\Locations::where('location_id',$id)->delete();
						     @\App\Locations::where('parent_id', $id )->update(['parent_id' => '0']);
						     @\App\Areas::where('location_id', $id )->update(['location_id' => '0']);
 
                             $data['status_code']    =   1;
	                         $data['status_text']    =   'Success';             
	                         $data['message']        =   'Location Deleted Successfully';
	                         $data['data']      =   [];  
	                         return $data;
 }





 
 
 
 
 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\Locations::where('location_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_parent_id()
{
	 if(isset($_GET['parent_id']) && $_GET['parent_id'] != null && $_GET['parent_id'] != '')
					{ $parent_id = $_GET['parent_id']; }
					else 
					{ $parent_id = ''; }
    return $parent_id;
}	

   public function get_variable_parents_only()
{
	 if(isset($_GET['parents_only']) && $_GET['parents_only'] != null && $_GET['parents_only'] != '')
					{ $parents_only = $_GET['parents_only']; }
					else 
					{ $parents_only = 'false'; }
    return $parents_only;
}	

  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}