<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class SendOfferPushNotificationController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
 

   
  
  // Route-22.1 ============================================================== Get Setting List =========================================> 
   public function push_to_customers(Request $request)
   {

                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'title' => 'required',
					'sub_title' => 'required'
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			 
 
				    	$this->notification_to_all([] , $request->title , $request->sub_title , []);
				 
				   
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Notifications Sent Successfully';
                          $data['data']      =   [];  
			              return $data;
   }  




      public function email_to_customers(Request $request)
   {

                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'email_body' => 'required',
					'subject' => 'required'
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }

                $customers = @\App\User::where('user_type','2')->get();

                $main = array();
                $main['email_body'] = $request->email_body;
                $main['email_subject'] = $request->subject;
                $main['details'] = [];
                $main['id'] = '';
                $main['notification_type'] = 'emails_to_customers';
 
               foreach($customers as $c)
               {
                $main['email'] = $c['email'];
                $d = dispatch(new \App\Jobs\SendEmailTest($main));
               }
           

          	              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Emails Sent Successfully';
                          $data['data']      =   [];  
			              return $data;
   }  







      public function email_to_subscribers(Request $request)
   {

                $validator = Validator::make($request->all(), [
          //'title' => 'required|unique:posts|max:255',
          'email_body' => 'required',
          'subject' => 'required'
            ]);
     
        if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;         
                }

                $customers = @\App\Newsletters::where('status','1')->get();

              

                $main = array();
                $main['email_body'] = $request->email_body;
                
                $main['email_subject'] = $request->subject;
                $main['details'] = [];
                $main['id'] = '';
                $main['notification_type'] = 'emails_to_subscribers';
 
               foreach($customers as $c)
               {
             
               	$main['email_body'] = $request->email_body.'<p style="font-size:14px">if you want to unsubscribe our Newsletter , click below link</p><p><a style="font-size:12px" href="'.env('APP_URL').'/unsubscribe?email='.$c['email'].'">UNSUBSCRIBE</p>';
                $main['email'] = $c['email'];
                $d = dispatch(new \App\Jobs\SendEmailTest($main));
               }
           

                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Emails Sent Successfully';
                          $data['data']      =   [];  
                    return $data;
   }  












         public function email_to_stores(Request $request)
   {

                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'email_body' => 'required',
					'subject' => 'required'
			      ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }

                $customers = @\App\User::where('user_type','4')->get();

                $main = array();
                $main['email_body'] = $request->email_body;
                $main['email_subject'] = $request->subject;
                $main['details'] = [];
                $main['id'] = '';
                $main['notification_type'] = 'emails_to_customers';
 
               foreach($customers as $c)
               {
                $main['email'] = $c['email'];
                $d = dispatch(new \App\Jobs\SendEmailTest($main));
               }
           

          	              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Emails Sent Successfully';
                          $data['data']      =   [];  
			              return $data;
   }  





 
 
 
 
 






 



 
 
 
 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}

 

  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
  
 
 
 
 


}