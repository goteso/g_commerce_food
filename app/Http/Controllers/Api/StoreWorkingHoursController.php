<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class StoreWorkingHoursController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
   
   
 // Route- ============================================================== Store Store Working Hours=========================================> 
   public function store(Request $request)
   {
                $validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
					'store_id' => 'required',
					'start_time' => 'required',
					'end_time' => 'required',
					'weekday' => 'required',
				  ]);
	   
				if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;					
                }
			        $exist_count = @\App\StoreWorkingHours::where('store_id',$request->store_id )->where('weekday',$request->weekday )->count();
                    if($exist_count > 0)
                    {
                    	  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Working Hours for this day already Set';
                          $data['data']      =   @\App\StoreWorkingHours::where('store_id',$request->store_id )->where('weekday',$request->weekday )->get(); 
                          return $data;
                    }
 
					$store_working_hours = new App\StoreWorkingHours;
					$store_working_hours->store_id = @$request->store_id;
				    $store_working_hours->start_time = $this->validate_string($request->start_time);
				    $store_working_hours->end_time = $this->validate_string($request->end_time);
				    $store_working_hours->weekday = $this->validate_string($request->weekday);
				    $store_working_hours->save();
					
				    if($store_working_hours != '')
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Store Working Hours Added';
                          $data['data']      =   $store_working_hours;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add ';
                          $data['data']      =   [];  
					}
				   
				  return $data;
				 
  }
   

   
  
  // Route- ============================================================== Get Categories List =========================================> 
   public function get_list()
   {


        $per_page = $this->get_variable_per_page(); 
		$orderby = $this->get_variable_orderby();
		$order = $this->get_variable_order();
		$search = $this->get_variable_search();

		$store_id = $this->get_variable_store_id();
		$weekday = $this->get_variable_weekday();
 
	    $model = new \App\StoreWorkingHours;
	   	$model = $model::where('id' ,'<>', '0');  
	    
	    if($store_id != '' && $store_id != null)
		{   $model = $model->where('store_id' , $store_id);  }

	    if($weekday != '' && $weekday != null)
		{   $model = $model->where('weekday' , $weekday);  }

        $model = $model->orderBy($orderby,$order);
	   
        $result = $model->get();
        
    
	   
	              if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Working Hours Fetched Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Result Found';
                          $data['data']      =   [];  
					}
				   return $data;
   }  



  // Route-8.3 ============================================================== Get Items List =========================================> 
   public function update(Request $request , $id)
   {
	   
					$validator = Validator::make($request->all(), [
					//'title' => 'required|unique:posts|max:255',
						'store_id' => 'required',
						'start_time' => 'required',
						'end_time' => 'required',
						'weekday' => 'required',
					]);
	   
					if($validator->errors()->all()) 
					{
						$data['status_code']    =   0;
						$data['status_text']    =   'Failed';             
						$data['message']        =   $validator->errors()->first();
						return $data;					
					}				
				
	               //check existance of category with ID in categories table
					$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Working Hours with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}
				    
					$title =$request->title;
	                $parent_id = $this->validate_integer($request->parent_id);
				 

				    $other_exist_count = @\App\StoreWorkingHours::where('store_id',$request->store_id )->where('weekday',$request->weekday )->where('id','<>',$id )->count();
				    if($other_exist_count > 0)
                    {
                    	  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Other Working Hour for this store and weekday already exist';
                          $data['data']      =   [];
                          return $data;
                    }



	                App\StoreWorkingHours::where('id', $id)->update([
                            'store_id' => @$request->store_id,
				    		'start_time' => $this->validate_string($request->start_time),
				    		'end_time' => $this->validate_string($request->end_time),
				    		'weekday' => $this->validate_string($request->weekday),
				    	 
	                ]);
	               
				    $result = @\App\StoreWorkingHours::where('id',$id)->get();
			 			
	                if(sizeof($result) > 0)
					{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Working Hour Updated Successfully';
                          $data['data']      =   $result;  
				    }
					else
					{
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Update Working Hour';
                          $data['data']      =   [];  
					}
				   return $data;
   }  


   
 
  // Route-  ============================================================== Get Items List =========================================> 
  
   public function destroy($id)
   {
   	 
   	         //check existance of item with ID in items table
				 	$exist = $this->model_exist($id);	
                    if($exist == 0 or $exist == '0')
                    {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Record with this ID does not exist';
                          $data['data']      =   [];
                          return $data;						  
					}

                    @\App\StoreWorkingHours::where('id',$id)->delete();
                    
 

   	 	                  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Working Hour Deleted Successfully';
                          $data['data']      =   [];  
                          return $data;
   }






 
 
  




  //Route-8.5 ============================================================= Get Areas Nearest to Get Latitude and Longitude
     public function get_list_nearby()
   {
   	$latitude = $_GET['latitude'];
   	$longitude = $_GET['longitude'];
   	$distance_limit =  $_GET['distance_limit'];

if(isset($_GET['distance_limit']) && $_GET['distance_limit'] != '' && $_GET['distance_limit'] != null)
{
}
else { $distance_limit = 5000; }
 

		$d = DB::table("areas")
 
	->select("*" , "areas.area_id"
		,DB::raw("6371 * acos(cos(radians(" . $latitude . ")) 
		* cos(radians(areas.latitude)) 
		* cos(radians(areas.longitude) - radians(" . $longitude . ")) 
		+ sin(radians(" .$latitude. ")) 
		* sin(radians(areas.latitude))) AS distance"))
		->groupBy("areas.area_id")
		->take(1)->get();





$d = $result = json_decode($d, true);
$distance = intval($d[0]['distance']);


if($distance > $distance_limit  )
{
	 					  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to find any area in this distance_limit';
                          $data['data']      =   [];  
                          return $data;
}
 else
{
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Area Fetched Successfully';
                          $data['data']      =   $d;  
 }
				 
				   return $data;
   }  

 
 
 













 
   
//==========================================================================misc functions===================================================================//   
//check item existence by id
public function model_exist($id)
{
	$count = @\App\StoreWorkingHours::where('id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}


public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	
      
   
   public function get_variable_store_id()
{
	 if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
					{ $store_id = $_GET['store_id']; }
					else 
					{ $store_id = ''; }
    return $store_id;
}	

   public function get_variable_weekday()
{
	 if(isset($_GET['weekday']) && $_GET['weekday'] != null && $_GET['weekday'] != '')
					{ $weekday = $_GET['weekday']; }
					else 
					{ $weekday = ''; }
    return $weekday;
}	


	 
	 
  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}