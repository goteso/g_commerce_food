<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
 
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
 
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class ContactUsEmailController extends Controller 
{
	
  
   
   
 

   
  
  // Route-27.1 ============================================================== Get Setting List =========================================> 
public function send_contact_us_email(Request $request)
{

          $phone = $request->phone;
          $first_name = $request->first_name;
          $last_name = $request->last_name;
          $email = $request->email;
          $comment = $request->comment;

            $to_email = @\App\Setting::where('key_title','email')->first(['key_value'])->key_value;
          $main = array();

          $email_body = addcslashes($comment , '\`*[]()#+-.!');

          $email_body = str_replace("\\","",$email_body);
          $email_body = str_replace('\\', '', $email_body);
         

          $main['email_body'] = $email_body;
          $main['first_name'] = $first_name;
          $main['last_name'] = $last_name;
          $main['email'] = $email;
          $main['comment'] = $comment;
          $main['phone'] = $phone;
           

          $main['email_subject'] = 'MaTaem:Enquiry';
          $main['details'] = [];
          $main['email'] =  $to_email;
          $main['id'] = '';
          $main['notification_type'] = 'enquiry';;


          
          $notification_type = 'enquiry';
          $id = '';
          $email_subject = 'Mataem:Enquiry';
 
  try{
        \Mail::send('emails.contact_us', $main, function($message) use (  $notification_type , $to_email , $id,  $email_subject , $main ) 
                            {   $message->to( $to_email, env('APP_NAME'))->subject( env('APP_NAME').':'. $email_subject );
                                $message->from('harvindersingh@goteso.com', env('APP_NAME'));
                            });

 }catch (\Exception $e){

 }
  

 
 
                    $data['status_code']    =   1;
                    $data['status_text']    =   'Success';             
                    $data['message']        =  'Enquiry Submitted, We will get back to you as soon as possible';
                    $data['data'] = [];
                    return $data; 
 
}  


  


///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}




public function get_variable_per_page()
{
   if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
          { $type = $_GET['per_page']; }
          else 
          { $type = '50'; }
    return $type;
}






public function get_variable_keys()
{
   if(isset($_GET['keys']) && $_GET['keys'] != null && $_GET['keys'] != '')
          { $keys = $_GET['keys']; }
          else 
          { $keys = ''; }
    return $keys;
}



 

 public function get_variable_exclude_type()
{
   if(isset($_GET['exclude_type']) && $_GET['exclude_type'] != null && $_GET['exclude_type'] != '')
          { $exclude_type = $_GET['exclude_type']; }
          else 
          { $exclude_type = ''; }
    return $exclude_type;
}
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 


}