<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class LoyaltyPointsController extends Controller 
{
	
  use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
    
   
   
  public function check_loyalty_points_range( $request  , $maximum_loyalty_points_transfer , $minimum_loyalty_points_transfer)
  {
     $points = $request->points;

    if($points > $minimum_loyalty_points_transfer && $points < $maximum_loyalty_points_transfer)
     {
        return 1;
     }
     else
     {
        return 0;
     }
  } 
 


public function get_setting_key_value($key_title , $settings)
{
    foreach($settings as $setting)
    {
      if($setting['key_title'] == $key_title)
         {

             return $setting['key_value'];
         }
    }
          
}
   
  
  // Route-28.1============================================================== Get Setting List =========================================> 
public function store(Request $request , $create_request = '')

{

  $type = $this->validate_string($request->type);

  if($this->validate_string($create_request) != '')
  {
    $request = $create_request;
    $type = $request['type'];
  }
 
          //type === order , referral , transfer , promotion
 
          $settings = @\App\Setting::get();
 
          if($type == 'order_earned' || $type == 'order_spent' || $type == 'referral_from' || $type == 'referral_registered'  || $type == 'promotion')
          {

                  //'user_id','type','points','expiry_date','source
                  
                  if( $type == 'order_earned' || $type == 'order_spent' )
                   {
                     $order_loyalty_points_expiry_day_count = $this->get_setting_key_value('order_loyalty_points_expiry_day_count' , $settings);
                     if($order_loyalty_points_expiry_day_count > 0)
                      {
                         $today = @\Carbon\Carbon::now();
                         $expiry_date = $today->addDays($order_loyalty_points_expiry_day_count);
                      }
                      else
                      {
                        $expiry_date = '';
                      }
                   }
 
 
                   if($type == 'referral_from' || $type == 'referral_registered')
                   {
                     $referral_loyalty_points_expiry_day_count = $this->get_setting_key_value('referral_loyalty_points_expiry_day_count' , $settings);
                     if($referral_loyalty_points_expiry_day_count > 0)
                      {
                         $today = @\Carbon\Carbon::now();
                         $expiry_date = $today->addDays($referral_loyalty_points_expiry_day_count);
                      }
                      else
                      {
                        $expiry_date = '';
                      }

                    $LoyaltyPoints = new \App\LoyaltyPoints;
                    $LoyaltyPoints->user_id = $this->validate_string(@$request['user_id']);
                    $LoyaltyPoints->points = $request['points'];
                    $LoyaltyPoints->expiry_date = $this->validate_string($expiry_date);
                    $LoyaltyPoints->type = $this->validate_string($request['type']);
                    $LoyaltyPoints->source = $this->validate_string($request['source']);
                    $LoyaltyPoints->save();

                    }



                   if($type == 'promotion')
                   {
                     $promotion_loyalty_points_expiry_day_count = $this->get_setting_key_value('promotion_loyalty_points_expiry_day_count' , $settings);
                     if($promotion_loyalty_points_expiry_day_count > 0)
                      {
                         $today = @\Carbon\Carbon::now();
                         $expiry_date = $today->addDays($promotion_loyalty_points_expiry_day_count);
                      }
                      else
                      {
                        $expiry_date = '';
                      }
                   }
 

            
          }

         else if(  $type == 'transfer'  )
         {
         
                  $available_loyalty_points = @\App\LoyaltyPoints::where('user_id',$request->user_id)->sum('points');
 

                 if($available_loyalty_points < 1)
                 {
                 	      $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'You do not have Loyalty Points';
                          $data['data']      =   [];  
                          return $data;
                 }

                 if($available_loyalty_points < $request->points)
                 {
                 	      $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'You do not have Enough Loyalty Points,Points Needed are '.$request->points.', your Bal is '.$available_loyalty_points;
                          $data['data']      =   [];  
                          return $data;
                 }

                 $maximum_loyalty_points_transfer = $this->get_setting_key_value('maximum_loyalty_points_transfer' , $settings);
                 $minimum_loyalty_points_transfer =$this->get_setting_key_value('minimum_loyalty_points_transfer' , $settings);
              
                 $points_valid = $this->check_loyalty_points_range($request , $maximum_loyalty_points_transfer , $minimum_loyalty_points_transfer );
                 if($points_valid == 1)
                 {    
                       $email = $request->email;
                       $email_exist_count = @\App\User::where('email', $email)->count();   

                      if($email_exist_count < 1)
                      {   
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Email Not Found !';
                          $data['data']      =   [];  
                          return $data;
                      }
                      
                      $user_id =  @\App\User::where('email', $email)->first(['user_id'])->user_id;

                      $transfer_loyalty_points_expiry_day_count = $this->get_setting_key_value('transfer_loyalty_points_expiry_day_count' , $settings);

                      if($transfer_loyalty_points_expiry_day_count > 0)
                      {

                      $today = @\Carbon\Carbon::now();
                      $expiry_date = $today->addDays($transfer_loyalty_points_expiry_day_count);
                      }
                      else
                      {
                        $expiry_date = '';
                      }

                      $LoyaltyPoints = new \App\LoyaltyPoints;
                      $LoyaltyPoints->user_id = $this->validate_string(@$user_id);
                      $LoyaltyPoints->points = $this->validate_integer($request->points);
                      $LoyaltyPoints->expiry_date = $this->validate_string($expiry_date);
                      $LoyaltyPoints->type = 'transfer_earned';
                      $LoyaltyPoints->source = $this->validate_string(@$request->user_id);
                      $LoyaltyPoints->save();

                      $LoyaltyPoints = new \App\LoyaltyPoints;
                      $LoyaltyPoints->user_id = $this->validate_string(@$request->user_id);
                      $LoyaltyPoints->points = "-".abs($request->points);
                      $LoyaltyPoints->expiry_date = '';
                      $LoyaltyPoints->type = 'transfer_spent';
                      $LoyaltyPoints->source = $this->validate_string($request->sourcessssss);
                      $LoyaltyPoints->save();

                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Transferred Successfully';
                          $data['data']      =   $LoyaltyPoints;  
                          return $data;
                       
                 }
                 else
                 {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Value is out of the transfer Limit , Maximum Transfer limit is '.$maximum_loyalty_points_transfer.", while Minimum is ".$minimum_loyalty_points_transfer;
                          $data['data']      =   [];  
                          return $data;
                  }
         }
         else
         {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Wrong Type';
                          $data['data']      =   [];  
                          return $data;
         }


        
          
          if(  @$LoyaltyPoints != '')
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Loyalty Points Added Successfully';
                          $data['data']      =   $LoyaltyPoints;  
            }
          else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Add Loyalty Points';
                          $data['data']      =   [];  
          }
           
          return $data;

}  


 




public function apply(Request $request , $user_id , $store_id = '')
{ 
//max_loyalty_points_to_use_order_place
//loyalty_points_single_currency



  $items = $request->items;

  $order_total = $this->total_order_amount_from_items($items);


  $store_id;

  $store_meta_type_id_earn = @\App\StoreMetaType::where('identifier','store_loyalty_points_spent_order')->first(['store_meta_type_id'])->store_meta_type_id;
      $store_meta_value_earn_value = @\App\StoreMetaValue::where('store_id',$store_id)->where('store_meta_type_id',$store_meta_type_id_earn)->first(['value'])->value;




      $max_loyalty_points_to_use_order_place = @\App\Setting::where('key_title','max_loyalty_points_to_use_order_place')->first(['key_value'])->key_value;

 if($store_meta_value_earn_value != '' && $store_meta_value_earn_value != null && $store_meta_value_earn_value != ' ')
 {

 
    $max_loyalty_points_to_use_order_place = $store_meta_value_earn_value;
 }


 

  $loyalty_points_single_currency = @\App\Setting::where('key_title','loyalty_points_single_currency')->first(['key_value'])->key_value;
  $users_loyalty_points = @\App\LoyaltyPoints::where('user_id' , $user_id )->sum('points');

  $applicable_users_loyalty_points = intval($max_loyalty_points_to_use_order_place *  $users_loyalty_points / 100);

  $applicable_users_loyalty_points_in_cash = $applicable_users_loyalty_points / $loyalty_points_single_currency;
if($users_loyalty_points < 1)
{

         if(intval($applicable_users_loyalty_points_in_cash) < 1)
              {
                $applicable_users_loyalty_points_in_cash = 0;
              }

               if(intval($applicable_users_loyalty_points) < 1)
              {
                $applicable_users_loyalty_points = 0;
              }

   $d['applicable_points'] = $applicable_users_loyalty_points;
   $d['discount'] = $applicable_users_loyalty_points_in_cash;
   $d['status_code'] ="0";
   $d["status_text"]=  "Failed";
   $d["message"]=  "No Loyalty Points are available";
   return  $d;
}
  


         if(intval($applicable_users_loyalty_points_in_cash) < 1)
              {
                $applicable_users_loyalty_points_in_cash = 0;
              }

               if(intval($applicable_users_loyalty_points) < 1)
              {
                $applicable_users_loyalty_points = 0;
              }
 $currency_symbol = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;

if( intval($applicable_users_loyalty_points_in_cash) < 1 || intval($applicable_users_loyalty_points_in_cash) < 1  )
{
  $message = 'No Loyalty Points are available';
}
else
{
   $message = $applicable_users_loyalty_points." points are applied with worth of ".$currency_symbol."".$applicable_users_loyalty_points_in_cash;
}
 
 $d['applicable_points'] = $applicable_users_loyalty_points;
 $d['discount'] = $applicable_users_loyalty_points_in_cash;
 $d['status_code'] ="1";
 $d["status_text"]=  "Success";

 $d["message"]=  $message;

return  $d;
  
}
 















  // Route-17.2 ============================================================== Get Users List =========================================> 
   public function get_list()
   {
    $per_page = $this->get_variable_per_page(); //ASC or DESC
    $orderby = $this->get_variable_orderby();
    $order = $this->get_variable_order();
 
    $user_id = $this->get_variable_user_id();
 
    
    $model = new \App\LoyaltyPoints;
    $model = $model::where('id' ,'<>', '0');  


    if($user_id != '' && $user_id != null)
    {  $model = $model->where('user_id' , $user_id)->where('points' ,'<>', 0)->where('points' ,'<>', '')->where('points' ,'<>', 'null');  } 

 
 
    $model = $model->orderBy($orderby,$order);  
    $result = $model->paginate($per_page); 
      
 
        if(sizeof($result) > 0)
          {
                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Loyalty Points List Fetched Successfully';
                          $data['data']      =   $result;  
          }
        else
          {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Loyalty Points';
                          $data['data']      =   [];  
          }
          
          return $data;
   }  



 
 
 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}


public function get_variable_keys()
{
   if(isset($_GET['keys']) && $_GET['keys'] != null && $_GET['keys'] != '')
          { $keys = $_GET['keys']; }
          else 
          { $keys = ''; }
    return $keys;
}



 

 public function get_variable_exclude_type()
{
   if(isset($_GET['exclude_type']) && $_GET['exclude_type'] != null && $_GET['exclude_type'] != '')
          { $exclude_type = $_GET['exclude_type']; }
          else 
          { $exclude_type = ''; }
    return $exclude_type;
}



 public function get_variable_user_id()
{
   if(isset($_GET['user_id']) && $_GET['user_id'] != null && $_GET['user_id'] != '')
          { $user_id = $_GET['user_id']; }
          else 
          { $user_id = ''; }
    return $user_id;
}

 

  
public function get_variable_per_page()
{
   if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
          { $per_page = $_GET['per_page']; }
          else 
          { $per_page = 2000; }
    return $per_page;
}

public function get_variable_orderby()
{
   if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
          { $orderby = $_GET['orderby']; }
          else 
          { $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
   if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
          { $order = $_GET['order']; }
          else 
          { $order = 'DESC'; }
    return $order;
}
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
  
 
 
 
 


}