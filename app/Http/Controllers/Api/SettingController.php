<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class SettingController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
   
 

   
  
  // Route-21.1 ============================================================== Get Setting List =========================================> 
   public function get_list()
   {
	    $type = $this->get_variable_type();
      $exclude_type = $this->get_variable_exclude_type();
      $keys = $this->get_variable_keys();

      if($type != '')
      {
       $type = explode(",", $type );
      }
      else
      {
        $type = [];
      }


      if($exclude_type != '')
      {
       $exclude_type = explode(",", $exclude_type );
      }
      else
      {
        $exclude_type = [];
      }


      
 
     $model = new \App\Setting;
		 $model = $model::where('id' ,'<>', '0'); 



	   
	   if(sizeof($type) > 0 )
		{
     $model = $model->whereIn('type' , $type); 
    }



    if($keys != '' )
    {  

     $keys_array = explode(",",$keys);
     $model = $model->whereIn('key_title' , $keys_array); 
    }



    if(sizeof($exclude_type) > 0 )
    {   
     $model = $model->whereNotIn('type' , $exclude_type); 
    }
 
     $result = $model->get(); 

        if(sizeof($result) > 0)
		 {
						  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Setting List Fetched Successfully';
                          $data['data']      =   $result;  
		 }
		 else
		 {
						  $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'No Setting Found';
                          $data['data']      =   [];  
		 }
				   return $data;
   }  



 
   //Route-21.2 =======================================================
    public function update(Request $request , $id='')
   {
               $user =  \App\Setting::where('id',$id)->update(['key_value' => $this->validate_string($request->key_value) ]);
               $data['status_code']    =   1;
               $data['status_text']    =   'Success';             
               $data['message']        =   'Settings Updated Successfully';
               $data['data']      =   [];  
	           return $data;
   }



 



 
 
 
 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_type()
{
	 if(isset($_GET['type']) && $_GET['type'] != null && $_GET['type'] != '')
					{ $type = $_GET['type']; }
					else 
					{ $type = ''; }
    return $type;
}


public function get_variable_keys()
{
   if(isset($_GET['keys']) && $_GET['keys'] != null && $_GET['keys'] != '')
          { $keys = $_GET['keys']; }
          else 
          { $keys = ''; }
    return $keys;
}



 

 public function get_variable_exclude_type()
{
   if(isset($_GET['exclude_type']) && $_GET['exclude_type'] != null && $_GET['exclude_type'] != '')
          { $exclude_type = $_GET['exclude_type']; }
          else 
          { $exclude_type = ''; }
    return $exclude_type;
}

 

  
 
 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
  
 
 
 
 


}