<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use App\Traits\notifications;

use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class StoreSortedController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
use notifications; // <-- ...and also this line. 
   
   
   
 

   
  
  // Route- ============================================================== Get Users List =========================================> 
   public function get_list(Request $request)
   {	 
  
				    $model = new \App\Store;
				    $model = $model::where('store_id' ,'<>', '0');  
			        $model = $model->where('sort_index','<>','0'); 
			        $model = $model->orderBy('sort_index','ASC');  
			        $result = $model->get();

	                   if(sizeof($result) > 0)
						{
							  $data['status_code']    =   1;
	                          $data['status_text']    =   'Success';             
	                          $data['message']        =   'Sorted Stores List Fetched Successfully';
	                          $data['data']      =   $result;  
					    }
						else
						{
							  $data['status_code']    =   0;
	                          $data['status_text']    =   'Failed';             
	                          $data['message']        =   'No Sorted Stores Found';
	                          $data['data']      =   [];  
						}
					   return $data;
   }  


  
 




     // Route-12.6 ============================================================== Delete a Address =========================================> 
public function sort_stores(Request $request )
{
                   $stores = $request;
                   $stores = $stores->toArray();
                   if(sizeof($stores) > 0 )
                   {
                      	$index = 0;
	                   	for($i=0;$i<sizeof($stores);$i++)
	                   	{
	                   		$index++;
	                   		  $store_id =  $stores[$i]['store_id'];
	                   		@\App\Store::where('store_id', $store_id)->update(['sort_index' => $index]);
	                   	}
                   }  
                  $data['status_code']    =   1;
	              $data['status_text']    =   'Success';             
	              $data['message']        =   'Stores Sorted Successfully';
	              $data['data']      =   [];  
	              return $data;
}
 
 
  

 
















 
 
   
//==========================================================================misc functions===================================================================//   
//check user existence by id
public function model_exist($id)
{
	$count = @\App\Store::where('store_id',$id)->count();
	if($count < 1) {
		return 0;
	}
	else{
		return 1;
	}
}	


 
 

///================================ function to check GET variable's and Defaults ====================================================//
public function get_variable_per_page()
{
	 if(isset($_GET['per_page']) && $_GET['per_page'] != null && $_GET['per_page'] != '')
					{ $per_page = $_GET['per_page']; }
					else 
					{ $per_page = 20; }
    return $per_page;
}

public function get_variable_orderby()
{
	 if(isset($_GET['orderby']) && $_GET['orderby'] != null && $_GET['orderby'] != '')
					{ $orderby = $_GET['orderby']; }
					else 
					{ $orderby = 'created_at'; }
    return $orderby;
}

public function get_variable_order()
{
	 if(isset($_GET['order']) && $_GET['order'] != null && $_GET['order'] != '')
					{ $order = $_GET['order']; }
					else 
					{ $order = 'DESC'; }
    return $order;
}

public function get_variable_search()
{
	 if(isset($_GET['search']) && $_GET['search'] != null && $_GET['search'] != '')
					{ $search = $_GET['search']; }
					else 
					{ $search = ''; }
    return $search;
}	


public function get_variable_vendor_id()
{
	 if(isset($_GET['vendor_id']) && $_GET['vendor_id'] != null && $_GET['vendor_id'] != '')
					{ $vendor_id = $_GET['vendor_id']; }
					else 
					{ $vendor_id = ''; }
    return $vendor_id;
}	
 
      
   
   public function get_variable_linked_id()
{
	 if(isset($_GET['linked_id']) && $_GET['linked_id'] != null && $_GET['linked_id'] != '')
					{ $linked_id = $_GET['linked_id']; }
					else 
					{ $linked_id = ''; }
    return $linked_id;
}	
 

 public function get_variable_meta_filters()
 {
 		 if(isset($_GET['meta_filters']) && $_GET['meta_filters'] != null && $_GET['meta_filters'] != '')
					{ $meta_filters = $_GET['meta_filters']; }
					else 
					{ $meta_filters = ''; }
    return $meta_filters;
 }
  
 
   public function get_variable_include_meta()
{
	 if(isset($_GET['include_meta']) && $_GET['include_meta'] != null && $_GET['include_meta'] != '')
					{ $include_meta = $_GET['include_meta']; }
					else 
					{ $include_meta = 'true'; }
    return $include_meta;
}	


 public function get_variable_request_type()
 {
 	   if(isset($_GET['request_type']) && $_GET['request_type'] != null && $_GET['request_type'] != '')
					{ $request_type = $_GET['request_type']; }
					else 
					{ $request_type = ''; }
       return $request_type;
 }



public function get_variable_featured()
{
	if(isset($_GET['featured']) && $_GET['featured'] != null && $_GET['featured'] != '')
					{ $featured = $_GET['featured']; }
					else 
					{ $featured = ''; }
       return $featured;
}


  public function get_variable_store_id()
 {
 	   if(isset($_GET['store_id']) && $_GET['store_id'] != null && $_GET['store_id'] != '')
					{ $store_id = $_GET['store_id']; }
					else 
					{ $store_id = ''; }
       return $store_id;
 }



  public function get_variable_exclude_busy_stores()
 {
 	   if(isset($_GET['exclude_busy_stores']) && $_GET['exclude_busy_stores'] != null && $_GET['exclude_busy_stores'] != '')
					{ $exclude_busy_stores = $_GET['exclude_busy_stores']; }
					else 
					{ $exclude_busy_stores = 'true'; }
       return $exclude_busy_stores;
 }

  public function get_variable_user_id()
 {
 	   if(isset($_GET['user_id']) && $_GET['user_id'] != null && $_GET['user_id'] != '')
					{ $user_id = $_GET['user_id']; }
					else 
					{ $user_id = ''; }
       return $user_id;
 }

  public function get_variable_approved_status()
 {
 	   if(isset($_GET['approved_status']) && $_GET['approved_status'] != null && $_GET['approved_status'] != '')
					{ $approved_status = $_GET['approved_status']; }
					else 
					{ $approved_status = '0'; }
       return $approved_status;
 }




 

 
 ///================================ function to check GET variable's and Defaults Ends ====================================================//
 
 
 
 
 
  
 
 
 
 
	
	
	public function paginateWithoutKey($items, $perPage = 15, $page = null, $options = [])
    {

        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        $lap = new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);

        return [
            'current_page' => $lap->currentPage(),
            'data' => $lap ->values(),
            'first_page_url' => $lap ->url(1),
            'from' => $lap->firstItem(),
            'last_page' => $lap->lastPage(),
            'last_page_url' => $lap->url($lap->lastPage()),
            'next_page_url' => $lap->nextPageUrl(),
            'per_page' => $lap->perPage(),
            'prev_page_url' => $lap->previousPageUrl(),
            'to' => $lap->lastItem(),
            'total' => $lap->total(),
        ];
    }
	
	
	
	 public function paginate($items, $perPage = 15, $page = null, $options = [])
{
	$page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
	$items = $items instanceof \Collection ? $items : Collection::make($items);
	return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
}
	 
	 	public function add_trades_feedback(Request $request)
{
	 
        $validator = Validator::make($request->all(), [
           
	 
        ]);
        if ($validator->errors()->all())
        {
            $data['status_code']    =   0;
            $data['status_text']    =   'Failed';
            $data['message']        =   $validator->errors()->first();
        }
        else
        {
        
		
			$trade_id = $request->trade_id;
		    $user_id = $request->user_id;
			$feedback = $request->feedback;
			$ratings = $request->ratings;
		  
		  
		    $order_seller_id = @\App\Trades::where('id',$request->trade_id)->first(['seller_id'])->seller_id;
			$order_buyer_id = @\App\Trades::where('id',$request->trade_id)->first(['buyer_id'])->buyer_id;
			
			if($user_id == $order_seller_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'seller_ratings' => $ratings , 'seller_feedback' => $feedback]);
			}
			
		 
			if($user_id == $order_buyer_id)
			{
				 
				 \App\Trades::where('id',$request->trade_id)->update([ 'buyer_ratings' => $ratings , 'buyer_feedback' => $feedback]);
			}
		  
 
		
 
            $data['status_code']    =   1;
            $data['status_text']    =   'Success';
            $data['message']        =   'Submitted successfully';

        }
        return $data;
    }
	
	
	
	
 
 
	
 
	

    
 
   public function make_thumb($src, $dest, $desired_width) 
   {

    /* read the source image */
    $source_image = imagecreatefromjpeg($src);
    $width = imagesx($source_image);
    $height = imagesy($source_image);
    
    /* find the "desired height" of this thumbnail, relative to the desired width  */
    $desired_height = floor($height * ($desired_width / $width));
    
    /* create a new, "virtual" image */
    $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
    /* copy source image at a resized size */
    imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
    /* create the physical thumbnail image to its destination */
    imagejpeg($virtual_image, $dest);
    }

    /**
     * @author Dikshant
     * set user language by default
     */
 
 
 


}