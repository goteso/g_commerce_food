<?php
namespace App\Http\Controllers\Api; //admin add
use App;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller; // using controller class
use Auth;
use Session;
use DB;
use Validator;
use App\User;
use Carbon\Carbon;
use App\Otp;
use App\UserSocialLinks;
use App\UserLikesDislikes;
use App\UserRatings;
use App\Country;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Traits\one_signal; // <-- you'll need this line...
use App\Traits\bitcoin_price;
use App\Traits\trait_functions;
use Illuminate\Support\Arr;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use Hash;
use Mail;
use File;
 
 


class NewslettersController extends Controller 
{
	
use one_signal; // <-- ...and also this line.
use bitcoin_price; // <-- ...and also this line.
use trait_functions; // <-- ...and also this line. 
   
   
 
 

  //Route-30.1 ============================================================== Get Categories List =========================================> 
   public function store(Request $request)
   {
     
           $validator = Validator::make($request->all(), [
          //'title' => 'required|unique:posts|max:255',
          'email' => 'required',
            ]);
     
        if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;         
                }
       

       $exist = @\App\Newsletters::where('email',$request->email)->where('status',1)->count();

       if($exist > 0)
       {
        
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'You are already Subscribed';
                          $data['data']      =   [];  
                          return $data;
       }
       
       
        $exist = @\App\Newsletters::where('email',$request->email)->where('status',0)->count();

       if($exist > 0)
       {
           
            $exist = @\App\Newsletters::where('email',$request->email)->update(['status'=>1]);
             $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Subscribed Successfully';
                          $data['data']      =   [];  
                          return $data;
       }


          $data1 = new \App\Newsletters;
          $data1->email = $this->validate_string($request->email);
          $data1->status = '1';
                    $data1->save();

                    $main[] = $data1;
          
            if($data1 != '')
          {
              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Subscribed Successfully';
                          $data['data']      =   $main;  
            }
          else
          {
              $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Unable to Subscribe';
                          $data['data']      =   [];  
          }
           
          return $data;
   }  

   

     //Route-30.1 ============================================================== Get Categories List =========================================> 
   public function unsubscribe(Request $request)
   {
     
           $validator = Validator::make($request->all(), [
          //'title' => 'required|unique:posts|max:255',
          'email' => 'required',
            ]);
     
        if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;         
                }
       



       $exist = @\App\Newsletters::where('email',$request->email)->count();

       if($exist < 1)
       {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Email not Subscribed';
                          $data['data']      =   [];  
                          return $data;
       }
 
                         App\Newsletters::where('email', $request->email) ->update(['status' => 0]);

                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'UnSubscribed Successfully';
                          $data['data']      =   [];  
 
           
          return $data;
   }  






   public function newsletters_subscribe_again(Request $request)
   {
     
           $validator = Validator::make($request->all(), [
          //'title' => 'required|unique:posts|max:255',
          'email' => 'required',
            ]);
     
        if($validator->errors()->all()) 
                {
                    $data['status_code']    =   0;
                    $data['status_text']    =   'Failed';             
                    $data['message']        =   $validator->errors()->first();
                    return $data;         
                }
       



       $exist = @\App\Newsletters::where('email',$request->email)->count();

       if($exist < 1)
       {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Email not Subscribed';
                          $data['data']      =   [];  
                          return $data;
       }
 
                         App\Newsletters::where('email', $request->email) ->update(['status' => 1]);

                          $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Subscribed Successfully';
                          $data['data']      =   [];  
 
           
          return $data;
   }  





   



  
  // Route-30.2 ============================================================== Get Users List =========================================> 
   public function get_list()
   {
	   
    $model = new \App\Newsletters;
    $model = $model->orderBy('id','DESC');	

    
       $status = $this->get_default('status' , 1);

       if($status != '' && $status != null )
       {
          $model = $model->where('status',$status);
       }

    $result = $model->paginate(20);
	    
      $subscribers_count = @\App\Newsletters::where('status','1')->count();
      $unsubscriber_count = @\App\Newsletters::where('status','0')->count();
 
	      if(sizeof($result) > 0)
					{
						               $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Subscribers List Fetched Successfully';
                          $data['data']      =   $result;  
                          $data['subscribers_count']      =   $subscribers_count; 
                          $data['unsubscriber_count']      =   $unsubscriber_count;  
				  }
				else
					{
						              $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'No Subscriber Found';
                          $data['data']      =   [];  
                            $data['subscribers_count']      =   $subscribers_count; 
                          $data['unsubscriber_count']      =   $unsubscriber_count;  
					}
				  
          return $data;
   }  








//Route-30.3 ==========================================
     public function destroy($id)
   {
   	  
                          @\App\Newsletters::where('id', $id )->delete();
 

   	 	                  $data['status_code']    =   1;
                          $data['status_text']    =   'Success';             
                          $data['message']        =   'Unsubscribed Successfully';
                          $data['data']      =   [];  
                          return $data;
   }




// Route-30.4 =====================================================
public function send_newsletters(Request $request)
{

  $html_content = $this->validate_string($request->html_content);

  if($html_content == '')
  {
                          $data['status_code']    =   0;
                          $data['status_text']    =   'Failed';             
                          $data['message']        =   'Html Content Missing';
                          $data['data']      =   [];  
                          return $data;
  }
   
    $emails = @\App\Newsletters::pluck('email');
    $emails = implode(",",json_decode($emails));
    $config = array();
    $config['api_key'] = "d67417b40bc9837f0149666c0c41b2b3-a5d1a068-7de666bf";
    $config['api_url'] = "https://api.mailgun.net/v3/gomeatservices.com/messages";
    $message = array();
    $message['from'] = "harvindersingh@goteso.com";
    $message['to'] = $emails;
    $message['h:Reply-To'] = "harvindersingh25000@gmail.com";
    $message['subject'] = "MultiTest";
    $message['html'] = "<h2>Email Sent to multiple 3 users</h2>";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['api_url']);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;

}
 
 


}