<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMetaType extends Model
{
        protected $fillable = [ 'user_meta_type_id' , 'title', 'identifier' , 'type' , 'count_limit' , 'field_options' , 'user_type' ];
		protected $table = 'user_meta_type';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}