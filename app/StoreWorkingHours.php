<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreWorkingHours extends Model
{
        protected $fillable = [ 'store_id' , 'start_time','end_time','weekday'];
		protected $table = 'store_working_hours';
		
 


	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
 

    
 
 



 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

	
	
}