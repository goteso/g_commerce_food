<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FilterOptions extends Model
{
        protected $fillable = [ 'identifier', 'value' , 'type'];
		protected $table = 'store_filter_options';
		


	protected $casts = [ 'id'=>'int' ];

	
        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }




 public function getLabelAttribute($value) {
       return $this->identifier;
    }


 
  
  public function getIsSelectedAttribute($value) {
         return  0;
    }
    
  
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}