<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushLog extends Model
{
        protected $fillable = [ 'notification_type' , 'log1', 'log2' , 'receiver_id' ];
		protected $table = 'push_log';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}