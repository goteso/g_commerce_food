<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use \Danjdewhurst\PassportFacebookLogin\FacebookLoginTrait;
use \Adaojunior\Passport\SocialGrantException;
use \Adaojunior\Passport\SocialUserResolverInterface;

class User extends Authenticatable implements SocialUserResolverInterface
{
	
	use HasApiTokens, Notifiable;
	
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
'first_name', 'last_name', 'email', 'phone' , 'password' , 'photo' ,'thumb_photo' , 'user_type' , 'otp' , 'status' ,'vendor_id' , 'remember_token' , 'email_token','longitude','latitude','commission','referral_code','facebook_id','store_id','customer_type'
    ];

protected $table = 'user';
	
	protected $primaryKey = 'user_id';
	    public function resolve($network, $accessToken, $accessTokenSecret = null)
    {
        switch ($network) {
            case 'facebook':
                return $this->authWithFacebook($accessToken);
                break;
            default:
                throw SocialGrantException::invalidNetwork();
                break;
        }
    }


 

         public function getLoyaltyPointsShareStringAttribute($value) {

         $registration_loyalty_points = @\App\Setting::where('key_title' , 'registration_loyalty_points' )->first(['key_value'])->key_value;
         $string = '';

                if( $registration_loyalty_points == '' || $registration_loyalty_points == null || $registration_loyalty_points == '0' || $registration_loyalty_points == 0 || $registration_loyalty_points == ' '  )
                {
$string =  '';
               }
                else
                {
                   
                        $string =  'Hi , You can use my reference code '.$this->referral_code.' and avail '.$registration_loyalty_points.' loyalty points.';
               
                }

        return $string;
         
 
    }
	
	
	
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password' 
    ];
	
	
 
		public function orders()
    {
        return $this->hasMany('App\Orders','user_id');
    }

	
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

     public function getLoyaltyPointsAttribute($value) {
         $loyalty_points = @\App\LoyaltyPoints::where('user_id',$this->user_id)->sum('points');
         if($loyalty_points < 1)
         {
            return 0;
         }
         else
         {

            $loyalty_points = floatval($loyalty_points);
            return round($loyalty_points,2);
         }
    }


     public function getTotalTasksAttribute($value) {
        if($this->user_type == '5')
        {
            return  @\App\Task::where('driver_id',$this->user_id)->count();
        }
        else
        {
            return  0;
        }
        
    }
	
	
	
         public function getLatestTaskAttribute($value) {
        if($this->user_type == '5')
        {
            return  @\App\Task::where('driver_id',$this->user_id)->orderBy('task_id','DESC')->take(1)->get();
        }
        else
        {
            return  [];
        }
        
    }
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }

 



    public function addNew($input)
    {
        $check = static::where('facebook_id',$input['facebook_id'])->first();


        if(is_null($check)){
            return static::create($input);
        }


        return $check;
    }


	
	

}
