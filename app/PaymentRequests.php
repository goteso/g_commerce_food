<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentRequests extends Model
{
        protected $fillable = [
        'type','receiver_id','sender_id','sender_notes','receiver_notes','status'
    ];

    protected $table ='payment_requests';
 



  public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	

      public function getSenderNameAttribute($value) {
         
         $user_id = @$this->sender_id;
         $user_type = @\App\User::where('user_id',$user_id)->first(['user_type'])->user_type;

         if($user_type == '3' || $user_type == 3)
         {
            return @\App\User::where('user_id',$user_id)->first(['first_name'])->first_name." ".@\App\User::where('user_id',$user_id)->first(['last_name'])->last_name;

         }

           if($user_type == '4' || $user_type == 4)
         {
            return @\App\Stores::where('manager_id',$user_id)->first(['store_title'])->store_title;
         }

           if($user_type == '1' || $user_type == 1)
         {
              return 'Mataem Online';
        }

    }


          public function getReceiverNameAttribute($value) {
         
         $user_id = @$this->receiver_id;
         $user_type = @\App\User::where('user_id',$user_id)->first(['user_type'])->user_type;

         if($user_type == '3' || $user_type == 3)
         {
            return @\App\User::where('user_id',$user_id)->first(['first_name'])->first_name." ".@\App\User::where('user_id',$user_id)->first(['last_name'])->last_name;

         }

           if($user_type == '4' || $user_type == 4)
         {
            return @\App\Stores::where('manager_id',$user_id)->first(['store_title'])->store_title;
         }

           if($user_type == '1' || $user_type == 1)
         {
              return 'Mataem Online';
        }

    }
    
 

      public function getStatusNameAttribute($value) {
         $n = 'Pending';

         if($this->status == '1' || $this->status == 1)
         {
               return 'Request Accepted';
         }

           if($this->status == '2' || $this->status == 2)
         {
              return 'Payment Sent';
         }

           if($this->status == '3' || $this->status == 3)
         {
             return 'Request Rejected';
         }

         return $n;
    }
    



 
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }



    
}
