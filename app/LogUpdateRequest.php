<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUpdateRequest extends Model
{
        protected $fillable = [ 'question', 'answer'];
		protected $table = 'log_update_request';
		


	protected $casts = [ 'id'=>'int','store_id'=>'int','user_id' => 'int' ,'approved_status' => 'int' ,'request_data' => 'string'   ];

	
        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }


     public function getRequestDataObjectAttribute($value) {
         return  json_decode($this->request_data);
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }


     public function getStoreDetailsAttribute($value) {
         return @\App\Stores::where('store_id',$this->store_id)->get();
    }

  public function getUserDetailsAttribute($value) {
         return @\App\User::where('user_id',$this->user_id)->get(['user_id','email','phone','user_type','first_name','last_name']);
    }


	public function getStoreMetaAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
      return  @\App\StoreMetaValue::where('store_id',$this->store_id )->get(['store_meta_value_id','store_meta_type_id','value']);
    }
	




        public function getStatusTitleAttribute($value) {
         //return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
      if($this->approved_status == '1' || $this->approved_status == 1)
      {
          return 'Approved';
      }
      if($this->approved_status == '0' || $this->approved_status == 0)
      {
          return 'Pending';
      }

       if($this->approved_status == '2' || $this->approved_status == 2)
      {
          return 'Rejected';
      }
      return 'Pending';
    }
    



	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}