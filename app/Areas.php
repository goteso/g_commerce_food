<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Areas extends Model
{
        protected $fillable = [ 'area_id', 'title' , 'latitude' , 'longitude' , 'pincode'  ];
		protected $table = 'areas';
		
		protected $casts = [ 'area_id'=>'int'  ];

        
 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }

 public function getCreatedAtFormatted2Attribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->format('M d, Y');
    }
	

 




 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
}