<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reports extends Model
{
        protected $fillable = [ 'identifier' , 'display_title', 'api' ,'user_type' ,'store_filter' , 'date_filter'];
		protected $table = 'reports';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }
	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}