<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderReview extends Model
{
        protected $fillable = [ 'order_id', 'store_id','rating','review','user_id','celebrity' ,'status' ];
		protected $table = 'order_review';
		
 
	
	 public function getCreatedAtFormattedAttribute($value) {
         return  \Carbon\Carbon::parse($this->created_at)->diffforhumans();
    }



         public function getUserDetailsAttribute($value) {
         return @\App\User::where('user_id',$this->user_id)->get(['user_id','first_name','last_name','customer_type']);
    }




         public function getCelebrityReviewAttribute($value) {

         	$user_id = $this->user_id;
         	$customer_type =  @\App\User::where('user_id',$this->user_id)->first(['customer_type'])->customer_type;

         	if($customer_type == 'celebrity')
         	{
               return 1;
         	}
         	else
         	{
         		return 0;
         	}
    }






             public function getStoreDetailsAttribute($value) {
         return @\App\Store::where('store_id',$this->store_id)->get([ 'store_id' , 'store_title', 'store_photo' , 'store_rating']);
    }


	
	
	
 
 public function toArray()
    {
        $array = parent::toArray();
        foreach ($this->getMutatedAttributes() as $key)
        {
            if ( ! array_key_exists($key, $array)) {
                $array[$key] = $this->{$key};   
            }
        }
        return $array;
    }
	
	
}