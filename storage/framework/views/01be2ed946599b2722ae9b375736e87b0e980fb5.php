<?php $__env->startSection('title', 'Add Store' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
<style>
   .panel{padding: 20px;} .m-t-small .form-control{ border: 0px;
   box-shadow: none;
   width: 80%;}
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper" >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->      
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
               <div class="row">
                  <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                     <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                  </div>
                  <?php          $auth_user_type = Auth::user()->user_type;   
                     $auth_user_id =   Auth::id();  
                     
                     ?>
                  <div class="col-sm-12">
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
                        <!--------------------------- Angular App Starts ---------------------------->
                        <textarea id="res" style="display:  none  ;" ></textarea>
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">        
                           <p >Calling all data...</p>
                        </div>
                        <input type="file" id="file" style="display:none "/>
                        <!--------------------------Angular App Starts ------------------------------------>
                        <div  ng-controller="addStoreController" ng-cloak>
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-12" >
                                    <h2 class="header">Add Store</h2>
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12" >
                                    <div class="panel">
                                       <div class="row">
                                          <div class="col-sm-12 col-md-12">
                                             <div class="form-horizontal"  >
                                                <label class="control-label">Search</label>
                                                <div class="search-input">
                                                   <input type="text" class="form-control" id="us3-address" placeholder="Type area, city or street"  />
                                                </div>
                                                <br>
                                                <!--Map by element. Also it can be attribute-->
                                                <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;" ></locationpicker>
                                                <div class="clearfix" >&nbsp;</div>
                                                <div class="clearfix" >&nbsp;</div>
                                                <div class="row">
                                                   <div class="m-t-small form-inline col-sm-5 col-lg-4">
                                                      <h5><b>Coordinates : </b></h5>
                                                      <label>Latitude : </label> <input type="text" class="form-control" id="us3-lat" ><br>
                                                      <label>Longitude : </label> <input type="text" class="form-control"  id="us3-lon" />
                                                   </div>
                                                   <div class="col-sm-7 col-lg-8">
                                                      <md-input-container  class="md-block text-left" >
                                                         <label>Address</label>
                                                         <textarea id="address" name="address" rows="3" max-rows="3"  md-no-autogrow  >  </textarea>
                                                      </md-input-container>
                                                   </div>
                                                </div>
                                                <div class="clearfix"></div>
                                             </div>
                                          </div>
                                          <div class="col-sm-5 col-lg-4 category-add">
                                             <div class="img-upload" style="background:#f5f5f5;max-width:350px;">
                                                <div class="form-group text-center"   >
                                                   <input type="file" id="file" style="display:none "/>
                                                   <input type="hidden" id="file_name" style="display:none "/>
                                                   <div style="text-align: center;position: relative" id="image">
                                                      <img width="100%"  id="preview_image" src="<?php echo e(asset('admin/assets/images/img-placeholder.png')); ?>"/>
                                                      <i id="loading1" class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: absolute;left: 40%;top: 40%;display: none"></i>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="text-right" style="font-size:;max-width:350px; " >
                                                <a href="javascript:changeProfile()" title="edit" style="text-decoration: none;background:#e2e2e2;padding:8px;position:relative;bottom: 50px; right:10px">
                                                <i class="fa fa-edit"></i> 
                                                </a>&nbsp;&nbsp;
                                                <a href="javascript:removeFile()" title="delete" style="color: red;text-decoration: none;background:#e2e2e2;padding:8px; position:relative;bottom: 50px;right:10px ">
                                                <i class="fa fa-trash-o"></i>
                                                </a>
                                             </div>
                                          </div>
                                          <div class="col-sm-12 col-md-7 col-lg-8">
                                             <div class="row">
                                                <div class="col-sm-12">
                                                   <md-input-container  class="md-block text-left" >
                                                      <label>Title</label>
                                                      <input type="text" id="title" name="title"   />
                                                   </md-input-container>
                                                </div>
                                                <div class="col-sm-12">
                                                   <md-input-container  class="md-block text-left" >
                                                      <label>Comission</label>
                                                      <input type="number" id="commission" name="commission"   />
                                                   </md-input-container>
                                                </div>
                                                <div class="col-sm-12">
                                                   <md-input-container  class="md-block text-left" >
                                                      <label>Store Tax</label>
                                                      <input type="number" id="store_tax" name="store_tax"   />
                                                   </md-input-container>
                                                </div>

                                                <div class="col-sm-12">
                                                   <md-input-container  class="md-block text-left" >
                                                      <label>Minimum Delivery Amount</label>
                                                      <input type="number" id="minimum_delivery" name="minimum_delivery"   />
                                                   </md-input-container>
                                                </div>


                                                <div class="col-sm-12">
                                                   <md-input-container  class="md-block text-left" >
                                                      <label>Catering Comission</label>
                                                      <input type="number" id="catering_commission" name="catering_commission"   />
                                                   </md-input-container>
                                                </div>
                                                <div class="col-sm-12">
                                                   <label>Featured</label>
                                                   <select class="form-control" id="featured">
                                                      <option value="0" >No</option>
                                                      <option value="1">Yes</option>
                                                   </select>
                                                   <br>
                                                </div>
                                                
												<div class="col-sm-12">
                                                   <md-input-container  class="md-block text-left" >
                                                      <label>Catering Status</label>
                                                     <md-switch ng-change="switch(catering_status);" id="catering_status" ng-model="catering_status" value="{{catering_status}}"class="md-primary" ng-true-value="'1' " ng-false-value="'0'">    </md-switch>

                                                   </md-input-container>
                                                </div>
            
                                                <div class="col-lg-12 col-sm-12 submit text-right">
                                                   <button   ng-click="addStore()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Add Store</button>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!--------------------------Angular App Ends ------------------------------------>
                        </div>
                     </div>
                  </div>
               </div>
         </section>
         </div>
      </div>
   </div>
   <!------>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/storeAdd.js')); ?>"></script>  
<!-- JavaScripts --> 
<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
   function changeProfile() {
       $('#file').click();
   }
   $('#file').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
   function upload(img) {
       var form_data = new FormData();
       form_data.append('file', img.files[0]);
       form_data.append('_token', '<?php echo e(csrf_token()); ?>');
       $('#loading1').css('display', 'block');
       $.ajax({
           url: "<?php echo e(url('image-upload-stores')); ?>",
           data: form_data,
           type: 'POST',
           contentType: false,
           processData: false,
           success: function (data) {
               if (data.fail) {
                   $('#preview_image').attr('src', '<?php echo e(asset('images/stores/noimage.jpg')); ?>');
                   alert(data.errors['file']);
               }
               else {
     var data = data.filename;
     console.log(data);
                   $('#file_name').val(data);
                   $('#photo').val(data);
                   $('#preview_image').attr('src', '<?php echo e(asset('images/stores')); ?>/'+data);
   
               }
               $('#loading1').css('display', 'none');
           },
           error: function (xhr, status, error) {
               alert(xhr.responseText);
               $('#preview_image').attr('src', '<?php echo e(asset('images/stores/noimage.jpg')); ?>');
           }
       });
   }
   function removeFile() {
       if ($('#file_name').val() != '')
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '<?php echo e(csrf_token()); ?>');
               $.ajax({
                   url: "ajax-remove-image-stores/" + $('#file_name').val(),
                   data: form_data,
                   type: 'POST',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                       $('#file_name').val('');
                       $('#loading').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }
   }
   
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>