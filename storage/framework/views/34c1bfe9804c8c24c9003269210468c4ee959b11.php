<?php $__env->startSection('title', 'Report Details' ); ?>
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>"> 
 <style>
 .caret:before{top:10px!important;}
 </style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>

<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
<div id="wrapper"  >
   <div id="layout-static">
      <!---------- Static Sidebar Starts------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!---------- Static Sidebar Ends------->
	  
 

      <div class="static-content-wrapper">
         <section id="main-header">
            <div class="container-fluid">
			  <div class="row">
                     <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
					 </div>
               <div class="row">
                  <div class="col-sm-12"> 
                     <div class="text-right">
                     </div>
                     <div class="tab-content" >
					 
					 <div id="loading" class="loading" >
                    <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">			 
					 <p >Calling all data...</p>
					 </div>
                        <!--------------------------Angular App Starts ------------------------------------>
      <div  ng-controller="reportController" ng-cloak>
         <div class="container-fluid" >
            <div class="row" >
               <div class="col-sm-12" >
			    
	 
		    <br>

		 	 <div class="row">
					   <div class="col-sm-4" style="display:flex;" ng-show="store_filter == 1">
					     
						 <h5> Filter By : &nbsp;</h5>
						  <div class="dropdown">
						  
						  <select class="form-control" name="store_id"  id="store_id">
						  	<option  value=""> Select Store </option>
                           <?php 

                              @$auth_user_type = @Auth::user()->user_type;   
                              @$auth_user_id =   @Auth::id();

                              if($auth_user_type == '3' || $auth_user_type == 3)
                              {
                                      $stores = @\App\Stores::where('vendor_id' , $auth_user_id )->get();
                              }
                              else if($auth_user_type == '4' || $auth_user_type == 4)
                              {
                              	      $stores =[];
                              }
                              else
                              {
                                    $stores = @\App\Stores::get();
                              }


                              foreach($stores as $store)
                              {
                                     echo  '<option  value="'.$store['store_id'].'"> '.$store['store_title'].' </option>';
                              } 

                              ?>
                    </select>
			                
						</div>
					 
					   </div> 
					   <div class="col-sm-6 form-inline" ng-show='date_filter==1'>
 
					      <div class="form-group">
					  <md-input-container  class="md-block text-left"  >
                                                      <label>From Date </label>
                                                    <datepicker
 
                                                         date-format="yyyy-MM-dd"  
                                                         button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                         button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                         <input ng-model="date_from" type="text" id="date_from"  name="date_from" class=" font-fontawesome font-light radius3" value="{{date_from}}" placeholder="Enter Start Date" />
                                                      </datepicker>
                                                   </md-input-container>
						      </div>


						  
						 <div class="form-group">
						   <md-input-container  class="md-block text-left"  >
						  <label> - To Date</label>
					                                  <datepicker
                                                         date-format="yyyy-MM-dd"  
                                                         button-prev='<i class="fa fa-arrow-circle-left"></i>'
                                                         button-next='<i class="fa fa-arrow-circle-right"></i>'>
                                                         <input ng-model="date_to" type="text"  id="date_to" name="date_to"  class="  font-fontawesome font-light radius3" value="{{date_to}}" placeholder="Enter End Date" />
                                                      </datepicker>
 </md-input-container>

						 </div>
					   </div>
					    <div class="col-sm-2" ng-show='store_filter==1 || date_filter == 1'>
						  <button class="btn btn-success" ng-click="generateReport();">Generate</button>
					   </div>
					   </div>




					
                  <div class="panel reports-panel">
                     <div class="panel-header">
					 <div class="row">
					   <div class="col-sm-6"> 

					     <button class="btn btn-info edit" ng-click="ShowHide()" style="display:none">Edit Notes <i class="fa fa-pencil"></i></button> 
						 </div>
					   <div class="col-sm-6 text-right">
					     <i class="fa fa-envelope-o" ng-click="export()" style="display:none"></i>
						Export As csv <i class="fa fa-file-excel-o" id="export" data-export="export" style="display: " title="Export Excel as CSV"></i> <br> 
					     Export aS XLS: <i class="fa fa-file-excel-o" ng-click="exportToExcel('#tableToExport')" style="display: " title="Export Excel"></i> 
					     <i class="fa fa-cog" style="display:none"></i>
						 <i class="fa fa-print" onclick="myFunction()" style="display:none"></i>
					   </div>
					   </div>
                     </div>
					 
                     <div class="panel-body"   ng-repeat="data in reports.data">
                        <h2 class="text-center title" >{{data.header}}</h2>
                        <h4 class="text-center text-uppercase subtitle"> {{data.subTitle}} </h4>
                        <h4 class="text-center date"> {{data.dateRange}}</h4>
						<div id="tableToExport" class="table-responsive"> 
                        <table class="table" id="exportthis">
						<thead>
                           <tr>
                              <th ng-repeat="c in data.columns  track by $index"  > {{c}}</th>
                           </tr>
						   </thead>
                           <tbody >
                              <tr  ng-repeat="value in data.data">
                               <td  ng-repeat="values in value track by $index" >  {{values}} </td> 
                              </tr>
                           </tbody>
                        </table>
						
						 
										
						</div>
             <div class="container-fluid innerData" ng-show="!data.data[0]">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br>
                                                 
                                                <p style="font-size:15px;">No Records found.</p>
                                            </div>
                                          </div>
                                        </div>
						
						<div class="row" > 
						 <div class="col-sm-12" ng-show= "IsVisible"> 
						  <p >{{notes}}</p>
						  </div>
						 <form ng-show = "IsHidden" ng-submit="submit()"> 
						  <div class="col-sm-8 col-md-9 col-lg-10"> 
						      <textarea class="form-control" ng-model="notes" id="notes" name="notes">{{data.notes}}</textarea>
						  </div>
						   <div class="col-sm-4 col-md-3 col-lg-2">
						     <button type="submit" class="btn btn-success">Save</button>
						   </div>
						 </form>
						</div>
						
                     </div>
                     <div class="panel-footer">
                        <p class="text-center">{{data.footer}}</p>
                     </div>
                  </div> 
               </div>
            </div>
         </div>
      </div>
	  
	                      
                        <!--------------------------Angular App Ends ------------------------------------>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
   </div>
</div>
</div>
<!------>


		<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/reportDetail.js')); ?>"></script> 
		
		
		<script>
		
		jQuery.fn.tableToCSV = function() {
    
    var clean_text = function(text){
        text = text.replace(/"/g, '\\"').replace(/'/g, "\\'");
        return '"'+text+'"';
    };
    
	$(this).each(function(){
			var table = $(this);
			var caption = $(this).find('caption').text();
			var title = [];
			var rows = [];

			$(this).find('tr').each(function(){
				var data = [];
				$(this).find('th').each(function(){
                    var text = clean_text($(this).text());
					title.push(text);
					});
				$(this).find('td').each(function(){
                    var text = clean_text($(this).text());
					data.push(text);
					});
				data = data.join(",");
				rows.push(data);
				});
			title = title.join(",");
			rows = rows.join("\n");

			var csv = title + rows;
			var uri = 'data:text/csv;charset=utf-8,' + encodeURIComponent(csv);
			var download_link = document.createElement('a');
			download_link.href = uri;
			var ts = new Date().getTime();
			if(caption==""){
				download_link.download = ts+".csv";
			} else {
				download_link.download = caption+"-"+ts+".csv";
			}
			document.body.appendChild(download_link);
			download_link.click();
			document.body.removeChild(download_link);
	});
    
};
</script>
 <script>
        $(function(){
            $("#export").click(function(){
                $("#exportthis").tableToCSV();
            });
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>