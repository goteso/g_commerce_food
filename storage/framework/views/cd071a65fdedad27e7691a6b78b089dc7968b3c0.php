<?php $__env->startSection('title', 'Terms & Conditions' ); ?> 

	<link href="<?php echo e(URL::to('admin/assets/editor/bootstrap3-wysihtml5.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">

<style type="text/css" media="screen">
  .btn.jumbo {
    font-size: 20px;
    font-weight: normal;
    padding: 14px 24px;
    margin-right: 10px;
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px;
  }
  ul.wysihtml5-toolbar {
    margin: 0;
    padding: 0;
  display: block;}
  
  ul.wysihtml5-toolbar>li{
      float: left;
    display: list-item;
    list-style: none;
    margin: 0 5px 10px 0;
	}
</style>
<?php $__env->startSection('content'); ?>


<!------
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=zl00639cxqfb9dlwusi00sjd71beh2jmk90t7quzey8o69t4"></script>
 
<script>tinymce.init({
  selector: "textarea",  // change this value according to your HTML
  
 
});</script>
----->

<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="termsController" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Terms & Conditions</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           
                        </div>
                     </div>
                  </div>
                  <div class="row" >
				 
	
                     <div class="col-sm-12 panel" >
                        <div class="panel-body  category-add" >
						    <div class="row" >
						        <div class="col-lg-8"  >
								 <br> 
                        
                                   <textarea id="terms" name="terms"   class="textarea"  placeholder="Enter text ..." style="width: 100%; height: 200px; font-size: 14px; line-height: 18px;"> <?php echo e($result); ?></textarea>
							 
							     <br><br> 
					           </div>
					 <div class="col-sm-4" >
                     <md-button ng-click="updateTerms()" class="md-raised bg-color md-submit" style="margin-top:120px;">Update</md-button>
					 <br> <br> 
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   
	
	 
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
   <!-- Include external CSS. -->
    <link rel="stylesheet" href="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/components-font-awesome/css/font-awesome.min.css">
  
	
    <script type="text/javascript" src="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/wysihtml5x/dist/wysihtml5x-toolbar.min.js"></script>
    <script type="text/javascript" src="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/components/handlebars/handlebars.runtime.min.js"></script>
 
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="http://bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/dist/bootstrap3-wysihtml5.min.js"></script>
 
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/terms_conditions.js')); ?>"></script> 
<script type="text/javascript">
		  $('.textarea').wysihtml5({
    toolbar: {
      fa: true
    }
  });
  </script>
  
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>