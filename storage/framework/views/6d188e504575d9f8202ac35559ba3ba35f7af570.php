<?php $__env->startSection('title', 'Settings'); ?>
 <link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
 
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>

 <?php $auth_user_type = Auth::user()->user_type;   
$auth_user_id =   Auth::id();
App::setLocale('settings');   
?>

<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
      <!---------- Static Sidebar Starts------------------------------------------------------------------------------------------------------------->			
      <?php $__env->startSection('sidebar'); ?>
      <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldSection(); ?>
      <!-- Modal -->
 
 <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-12">
                        <textarea id="res" style="display: none ;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						
				 
                  <div class="tab-content" >
				   <div class="container-fluid" >
				      <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Settings</h2>
                                 </div>
                                 <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                                    <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                 </div>
                              </div>
				  

 

				  <div  class="row">
				     <div class="col-sm-12">
					   <div class="panel settings"> 
					      <div class="row">

<?php if( trans('permission'.$auth_user_type.'.view_account_settings') == '1' || trans('permission'.$auth_user_type.'.view_business_logo_settings') == '1' || trans('permission'.$auth_user_type.'.view_faqs_settings') == '1') {?>
						     <div class="col-sm-12"> 
							   <h5 class="text-uppercase">General Settings</h5>
							   <ul class="list-inline">

							   	 <?php if(trans('permission'.$auth_user_type.'.view_account_settings') == '1'): ?>
							     <li><a href="<?php echo e(URL::to('v1/account')); ?>"><p class="gSetting"><i class="fa fa-user"></i></p><p class="text-center">Account</p></a></li>
							     <?php endif; ?>

							     <?php if(trans('permission'.$auth_user_type.'.view_business_logo_settings') == '1'): ?>
								 <li><a href="<?php echo e(URL::to('v1/business-logo')); ?>"><p class="gSetting"><i class="fa fa-image"></i></p><p class="text-center">Business Logo</p></a></li>
								 <?php endif; ?> 
								 

								 
								 
								<?php if(trans('permission'.$auth_user_type.'.view_web_banners') == '1'): ?>
								  <li><a href="<?php echo e(URL::to('v1/web-banners')); ?>"><p class="gSetting"><i class="fa fa-image"></i></p><p class="text-center">Web Banners</p></a></li> 
								<?php endif; ?>   

								<?php if(trans('permission'.$auth_user_type.'.view_terms_and_conditions') == '1'): ?>
								    <li><a href="<?php echo e(URL::to('v1/terms')); ?>"><p class="gSetting"><i class="fa fa-image"></i></p><p class="text-center">Terms & Conditions</p></a></li> 
								<?php endif; ?> 	

								<?php if(trans('permission'.$auth_user_type.'.view_privacy_policy') == '1'): ?>
								  <li><a href="<?php echo e(URL::to('v1/privacy-policy')); ?>"><p class="gSetting"><i class="fa fa-image"></i></p><p class="text-center">Privacy Policy</p></a></li> 
                                <?php endif; ?> 

								  <?php if(trans('permission'.$auth_user_type.'.view_faqs_settings') == '1'): ?>
								 <li><a href="<?php echo e(URL::to('v1/faqs')); ?>"><p class="gSetting"><i class="fa fa-question-circle"></i></p><p class="text-center">FAQs</p></a></li>
								 <?php endif; ?>
                                  <?php if(trans('permission'.$auth_user_type.'.view_store_settings') == '1'): ?>
								 <li><a href="<?php echo e(URL::to('v1/update_store')); ?>"><p class="gSetting"><i class="fa fa-building"></i></p><p class="text-center">Update Store</p></a></li>
								  <?php endif; ?>

								 

<?php if(trans('permission'.$auth_user_type.'.view_translations') == '1'): ?>
	                            <li><a href="<?php echo e(URL::to('translations')); ?>"><p class="gSetting"><i class="fa fa-building"></i></p><p class="text-center">Panel Permissions</p></a></li>
 <?php endif; ?>
							   </ul>
							 </div>
 <?php } ?>


<?php if( trans('permission'.$auth_user_type.'.view_timeslots_settings') == '1' || trans('permission'.$auth_user_type.'.view_tax_settings') == '1' ) {?>							 
							 <div class="col-sm-12"> 
							   <h5 class="text-uppercase">Admin Settings</h5>
							   <ul class="list-inline">

							   	 <?php if(trans('permission'.$auth_user_type.'.view_timeslots_settings') == '1'): ?>
							     <li><a href="<?php echo e(URL::to('v1/timeslots')); ?>"><p class="aSetting text-center"><i class="fa fa-calendar-o"></i></p><p class="text-center">Timeslots</p></a></li>
							     <?php endif; ?>

							      <?php if(trans('permission'.$auth_user_type.'.view_tax_settings') == '1'): ?>
								 <li><a href="<?php echo e(URL::to('v1/tax')); ?>"><p class="aSetting text-center"><i class="fa fa-percent"></i></p><p class="text-center">Tax</p></a></li>
								 <?php endif; ?>
								 
								 	<?php if(trans('permission'.$auth_user_type.'.view_privacy_policy') == '1'): ?>
                                <li><a href="<?php echo e(URL::to('v1/cuisine-filters')); ?>"><p class="aSetting"><i class="fa fa-image"></i></p><p class="text-center"> Cuisine Filters</p></a></li> 
<?php endif; ?>

 <?php if(trans('permission'.$auth_user_type.'.view_store_settings') == '1'): ?>
								    <li><a href="<?php echo e(URL::to('v1/store-banners')); ?>"><p class="aSetting"><i class="fa fa-image"></i></p><p class="text-center"> Store Banners</p></a></li> 
 <?php endif; ?>								  

								  
								   <?php if(trans('permission'.$auth_user_type.'.view_store_settings') == '1'): ?>
								   <li><a href="<?php echo e(URL::to('v1/stores/sort')); ?>"><p class="aSetting"><i class="fa fa-building"></i></p><p class="text-center">Sort Stores</p></a></li>
 <?php endif; ?>								   
								      
								    <?php if(trans('permission'.$auth_user_type.'.view_order_cancel_reasons') == '1'): ?>
								     <li><a href="<?php echo e(URL::to('v1/order-cancel-reasons')); ?>"><p class="aSetting text-center"><i class="fa fa-times-circle"></i></p><p class="text-center">Order Cancel Reasons</p></a></li>
								    <?php endif; ?>
			    
							   </ul>
							 </div>
<?php } ?>							 
						



<?php if( trans('permission'.$auth_user_type.'.view_locations_settings') == '1' || trans('permission'.$auth_user_type.'.view_areas_settings') == '1' || trans('permission'.$auth_user_type.'.view_plugins_settings') == '1') {?>	 
							 <div class="col-sm-12"> 
							   <h5 class="text-uppercase">Logistics Settings</h5>
							   <ul class="list-inline">

							   	 <?php if(trans('permission'.$auth_user_type.'.view_locations_settings') == '1'): ?>
							     <!--<li><a href="<?php echo e(URL::to('v1/locations')); ?>"><p class="lSetting text-center"><i class="fa fa-location-arrow"></i></p><p class="text-center">Locations</p></a></li>-->
							     <?php endif; ?>

							      <?php if(trans('permission'.$auth_user_type.'.view_areas_settings') == '1'): ?>
							     <li><a href="<?php echo e(URL::to('v1/areas')); ?>"><p class="lSetting text-center"><i class="fa fa-map-marker"></i></p><p class="text-center">Areas</p></a></li> 
							     <?php endif; ?>  

							      <?php if(trans('permission'.$auth_user_type.'.view_plugins_settings') == '1'): ?>
							     <li><a href="<?php echo e(URL::to('v1/plugins')); ?>"><p class="lSetting text-center"><i class="fa fa-plug"></i></p><p class="text-center">Plugins</p></a></li> 
							     <?php endif; ?>

							   </ul>
							 </div>
<?php } ?>							 
						

 						 
							 
							 <!---<div class="col-sm-12"> 
							   <h5 class="text-uppercase">Notification Settings</h5>
							   <ul class="list-inline">
							     <li><p class="nSetting text-center"><i class="fa fa-envelope" aria-hidden="true"></i></p><p class="text-center">Email</p></li>
								 <li><p class="nSetting text-center"><i class="fa fa-"></i></p><p class="text-center">Push Notifications</p></li>  
							     <li><p class="nSetting text-center"><i class="fa fa-comment"></i></p><p class="text-center">SMS</p></li> 
							   </ul>
							 </div>-->
							 
						  </div>
                       </div>					   
					 </div>
				  </div>
		</div>
	  </div>
	  </div> 
	     </div>
   </div>
    
   </section>
</div>

</div>

</div>

  </div>

		
		 
 

<!----manage modal ends here--->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>