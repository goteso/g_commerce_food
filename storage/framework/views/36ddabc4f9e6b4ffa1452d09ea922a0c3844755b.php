<?php $__env->startSection('title', 'Reviews' ); ?>  
 
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('header'); ?>
<?php echo $__env->make('web-food.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
  
 
						   
 <div  ng-controller="transferPointsController" ng-cloak>
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('<?php echo e(URL::asset('web-food/assets/images/food-back-small.jpg')); ?>')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-8"   >
                           <md-list layout-padding class="profile-data" >
							<md-list-item class="md-3-line"     >
								<img  ng-src="<?php echo e(URL::asset('images/users')); ?>/{{customerDataPhoto}}" class="md-avatar" ng-show="customerDataPhoto">
								<img  ng-src="<?php echo e(URL::asset('admin/assets/images/user-placeholder.png')); ?>" class="md-avatar" ng-show="customerDataId && !customerDataPhoto">
								<div class="md-list-item-text">
								  <h2 >{{customerDataFirstName}} {{customerDataLastName}}</h2>
								  <h4>
									{{customerDataEmail}}
								  </h4>
								</div>
							</md-list-item>
						  </md-list>    
                      </div>
                      <div class="col-sm-4">
                          <br>
                         <a href="<?php echo e(URL::to('/')); ?>"> <button class="btn md-raised bg-color md-submit md-button md-ink-ripple dropdown-toggle md-button" ng-show="customerDataId">Place Order</button>
                         </a>
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="orders">
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-4 text-center" ng-controller="logoutController">   
                         <div class="panel left">
						 <div class="panel-body">
						   <table  class="table left-nav-links">
						    <tr>
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/shopping-bag.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/shopping-bag-active.svg')); ?>" class="img-active">
							  </td>
							  <td > <a href="<?php echo e(URL::to('my-orders')); ?>">My Orders</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/address.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/address-active.svg')); ?>" class="img-active">
							  </td>
							  <td> <a href="<?php echo e(URL::to('addresses')); ?>">Manage Addresses</a></td>
							</tr>
							<tr  class="">
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('loyalty-points')); ?>"> Loyalty Points History</a></td>
							</tr>
							<tr >
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/feedback.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/feedback-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('reviews')); ?>"> My Reviews</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/favorites.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/favorites-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('favourites')); ?>"> Favourites</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('account')); ?>/{{profileData.data.user_id}}"> Edit Profile</a></td>
							</tr>
							
						<tr class="">
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('change-password')); ?>"> Change Password</a></td>
							</tr>
							<tr class="active">
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('transfer-loyalty-points')); ?>"> Transfer Loyalty Points</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/sign-out.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/sign-out-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="#" ng-click="logout();">Signout</a></td>
							</tr>
						   </table>
						 </div>
						 </div>
                     </div> 
					 
					 
					 <div class="col-sm-8 ">   
                         <div class="panel">
						 <div class="panel-body">
						 
						     <div id="loading" class="loading text-center" style="display:none ;">
                              <img src="<?php echo e(URL::asset('web-food/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                              <p >Loading...</p>
                           </div>
						   <h3>Transfer Loyalty Points</h3>
						   
						   <div class="row category-add text-center">
						     <div class="col-sm-12" ><br>
                                                      <md-input-container  class="md-block text-left"  >
                                                         <label>Email Id</label>
                                                         <input type="email" id="email" name="email"  ng-model="email" value="" />
                                                      </md-input-container>
													  
													  <md-input-container  class="md-block text-left"  >
                                                         <label>Points</label>
                                                         <input type="number" id="points" name="points"  ng-model="points" value="" />
                                                      </md-input-container>
													  
													  
                                                     </div>
												   <div class="col-lg-12 submit text-right">
												    
                                                   <button   ng-click="transferPoints()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">Tranfer Points</button>
                                                </div>
												</div>
						 </div>
						 </div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
	 </div>
		  				  
      
<?php $__env->startSection('footer'); ?>
<?php echo $__env->make('web-food.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<!------>

<script type="text/javascript" src="<?php echo e(URL::asset('web-food/angular-controllers/transfer-points.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web-food.layout.web', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>