<?php $__env->startSection('title', 'Restaurants' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                        <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display:none;" ></textarea>
						
                        <!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
                        <!-------------Loader Ends here------------->
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Restaurants</h2>
                                 </div>
                                 <div class="col-sm-5 text-right">
                                    <a href="<?php echo e(URL::to('v1/vendor')); ?>"><button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right" >Add New</button></a>
                                 </div>
                              </div>
                              <div class="row" >
                                 <div class="col-sm-12" ng-controller="userController as ctrl" ng-cloak >
                                    <div class="" >
                                       <br> 
                                       <div id="tableToExport" class="products-table table-responsive"  >
                                          <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr>
                                                   <th>ID</th>
                                                   <th>NAME</th>
                                                   <th>PHOTO</th>
                                                   <th>EMAIL</th>
                                                   <th>MOBILE</th>
                                                   <th>CREATED</th>
                                                   <th>ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody >
                                                <tr  ng-repeat="values in ctrl.users.data.data  ">
                                                   <td><a href="<?php echo e(URL::asset('/v1/restaurant')); ?>/{{values.user_id}}" ><b>#{{values.user_id}}</b></a> </td>
                                                   <td>{{values.first_name}} {{values.last_name}}</td>
                                                   <td><img class="center-block" src="<?php echo url('/');?>/images/users/{{values.photo}}" style="height:40px;width:40px" ng-hide="!values.photo"> 
												   <img class="img-responsive center-block" style="height:40px;width:40px" src="<?php echo url('/').'/admin/assets/images/boy.png';?>" ng-show="!values.photo"> </td>
                                                   <td > {{values.email}} </td>
                                                   <td > {{values.phone}} </td>
                                                   <td>{{values.created_at_formatted}}</td>
                                                   <td class="actions">
                                                      <a class="btn btn-xs edit-product" href="<?php echo e(URL::to('v1/restaurant')); ?>/{{values.user_id}}" ><i class="fa fa-edit"></i></a>
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.deleteUser(values.user_id, $index)"><i class="fa fa-trash"></i></a>
                                                      <!--  <a class="btn btn-xs edit-product" href="<?php echo e(URL::to('product_edit')); ?>/{{values.id}}" ><img src="<?php echo e(url::to('admin/images/edit.png')); ?> " style="height:16px;width:16px"/></a>
                                                         <a class="btn btn-xs delete-product" ng-click="ctrl.removeChoice(values.id, $index)"><img src="<?php echo e(url::to('admin/images/bin.png')); ?> " style="height:16px;width:13px"/></a>-->
                                                   </td>
                                                </tr>
                                             </tbody>
                                          </table>


                                            <div class="container-fluid innerData" ng-show="!ctrl.users.data.data">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br> 
                                                <p>No Records found.</p>
                                            </div>
                                          </div>
                                        </div>


                                       </div>
                                      <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.users.data.first_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.users.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.prev_page_url);">Previous</button> 
                                          <span>{{ctrl.users.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.users.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.next_page_url);">Next</button> 
                                          <button class="btn"  ng-show="ctrl.users.data.last_page_url != null" ng-click="ctrl.pagination(ctrl.users.data.last_page_url);">Last</button> 
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/vendors.js')); ?>"></script> 
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>