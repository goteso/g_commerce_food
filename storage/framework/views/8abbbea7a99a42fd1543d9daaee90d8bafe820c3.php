<?php $__env->startSection('title', 'Reviews' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
 <style>
 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
  
  @media(max-width:767px){ .table{table-layout:fixed}
 .table-responsive>.table>tbody>tr>td, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>td, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>thead>tr>th{ white-space:pre-wrap!important; width: 100px;  }
   .table>tbody>tr>td.review,  .table>thead>tr>th.review{   width: 200px!important;}
  }
   @media(min-width:768px){
   .table>tbody>tr>td.review{   width: 40%!important;}
   }
 </style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 

                      <?php @$auth_user_type = @Auth::user()->user_type;   
                     @$auth_user_id =   @Auth::id();
                     App::setLocale('settings');  
                     if($auth_user_id =='' || $auth_user_id == null)
                     {
                           Auth::logout();
                           echo '<script>document.getElementById("logout").click();</script>';
                     }

                     if($auth_user_type == '4')
                     {
                        $auth_store_id = @\App\Store::where('manager_id',$auth_user_id)->first(['store_id'])->store_id;
                     }
                     else
                     {
                        $auth_store_id = '';
                     }
                ?>


                
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"   ng-controller="searchController" ng-cloak > 
            <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
			
            <div class="tab-content"  ng-controller="reviewsController" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display: none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-7" >
                        <h2 class="header">Reviews</h2>
                     </div>
                     <div class="col-sm-5">

                          <?php if(trans('permission'.$auth_user_type.'.filter_reviews') == '1'): ?>
                        <div class="text-right">
                              <md-autocomplete  md-selected-item="store_title" md-search-text-change="searchStoreChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedStoreChange(store, field)" md-items="store in storeSearch(searchText)" md-item-text="store.store_title" md-min-length="0" placeholder="Select Store" md-menu-class="autocomplete-custom-template"  md-autofocus=""  id="store_id" style="min-width:100px;"> 
                      <md-item-template>
                        <span class="item-title"> 
                        <span> {{store.store_title}} </span> 
                        </span>
                        <span class="item-metadata">
                        <span> {{item.mobile}} </span>  
                        </span>
                      </md-item-template>
                      </md-autocomplete>
                        </div>
   <?php endif; ?>

                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="panel" >
						  <div class="panel-body" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr> 
										
                                       <th>ORDER ID</th>
                                       <th>STORE TITLE</th>
									   <th>CUSTOMER</th>
                              <th class="review">REVIEW</th>
									    <th>RATING</th>  
										<th>STATUS</th>
										<th>CREATED AT</th>
										<th>Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in reviews.data.data  "> 
									
                                        <td><a href="<?php echo e(URL::to('v1/order_detail')); ?>/{{values.order_id}}">#{{values.order_id}}</a></td>
                                        <td>{{values.store_details[0].store_title}}      </td>
            									   <!--<td><img src="<?php echo e(URL::asset('images/stores')); ?>/{{values.store_details[0].store_photo}}" ng-hide="!values.store_details[0].store_photo">
            									   <img src="<?php echo e(URL::asset('admin/assets/images/placeholder.jpg')); ?>" ng-hide="values.store_details[0].store_photo"></td>-->
         									    <td> {{values.user_details[0].first_name}} {{values.user_details[0].last_name}} <span style="color:green" ng-show="values.user_details[0].customer_type != ''"> / {{values.user_details[0].customer_type}}</td>
                                        <td class="review">{{values.review}}</td>
         									    <td>{{values.rating}}</td>
												<td><md-switch style="margin:0px 15px;" ng-change="switchStatus(values.status,values.id);" ng-model="values.status" value="{{values.status}}"class="md-primary" ng-true-value="1" ng-false-value="'0'">    </md-switch></td>
                                        <td>{{values.created_at_formatted}}</td>
										          <td><a ng-click="deleteReview(values.id, $index)"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
							  
							  
							    <div class="container-fluid innerData" ng-show="!reviews.data.data">
                                          <div class="row">
                                            <div class="col-sm-12 text-center"> 
                                                 <br>
                                                 <img src="<?php echo e(URL::asset('web-food/assets/images/placeholders/review-placeholder.png')); ?>"  class="img-responsive center-block">
                                                <p style="font-size:15px;">No Records found.</p>
                                            </div>
                                          </div>
                                        </div>
                           </div>
						   <div class="pagination text-right" style="display:block" >
                                           <button class="btn" ng-show="reviews.data.first_page_url" ng-click="pagination(reviews.data.first_page_url);">First</button> 
                                           <button class="btn" ng-show="reviews.data.prev_page_url" ng-click="pagination(reviews.data.prev_page_url);">Previous</button> 
                                          <span>{{reviews.data.current_page}}</span>
                                          <button class="btn" ng-show="reviews.data.next_page_url"  ng-click="pagination(reviews.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="reviews.data.last_page_url" ng-click="pagination(reviews.data.last_page_url);">Last</button> 
                                       </div>
									   
                           <!--<div class="pagination">
						                  <a href="{{reviews.data.first_page_url}}"><button class="btn">First</button></a>
                                          <a href="{{reviews.data.prev_page_url}}"><button class="btn">Previous</button></a>
                                          <span>{{reviews.data.current_page}}</span>
                                          <a href="{{reviews.data.next_page_url}}"><button class="btn" >Next</button></a>
                                          <a href="{{reviews.data.last_page_url}}"><button class="btn" >Last</button></a>
                                 
                           </div>-->
                        </div>
                     </div>
                    </div> 
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
  
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/review.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>