<?php $__env->startSection('title', 'Items' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>"> 
<style>
table>tbody>tr>td,table>thead>tr>th{text-align:center;}

 .md-autocomplete-suggestions-container{  
   z-index:100000 !important; /* any number of choice > 1050*/
  }
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
				  <div class="col-sm-5" ng-controller="searchController" ng-cloak > 
                                    <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                 </div>
                     <div class="col-sm-12">
                        <textarea id="res" style="display:none;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						  <input type="file" id="file" style="display:none "/  >
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
                              <div class="row">
                                 <div class="col-sm-7" >
                                    <h2 class="header">Store View</h2>
                                 </div>
								 <!--<div class="col-sm-5 text-right">
                                    <!-- <a href="<?php echo e(URL::to('v1/item')); ?>"><button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right" >Add New</button></a>--
                                    
                                 </div> -->
								  
                        
                                       </div> 
                              <div class="row" >
                                 <div class="col-sm-12"  >
                                    <div ng-view></div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
         </div>
         </section>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/store-menu.js')); ?>"></script> 

<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeProfile() {
        $('#file').click();
    }
    $('#file').change(function () {
        if ($(this).val() != '') {
            upload(this);
 
        }
    });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-categories')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/categories/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
             
           var data = data.filename;
          
                     $('#file_name').val(data);          
                    $('#file_name1').val(data);
                    //$('#photo').val(data);
          $('#preview_image').attr('src', '<?php echo e(asset('images/categories')); ?>/'+data);
                    $('#preview_image2').attr('src', '<?php echo e(asset('images/categories')); ?>/'+data);
          $('#preview_image1').attr('src', '<?php echo e(asset('images/categories')); ?>/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/categories/noimage.jpg')); ?>');
            }
        });
    }
    function removeFile() {
        if ($('#file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
                $.ajax({
                    url: "ajax-remove-image-categories/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                        $('#file_name').val('');
                        $('#loading').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>


<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>