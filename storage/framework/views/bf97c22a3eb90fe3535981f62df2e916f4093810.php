  <?php 

   $currency_symbol = @\App\Setting::where('key_title','currency_symbol')->first(['key_value'])->key_value;

         ?>
		 <div class="main ">
   <md-autocomplete ng-disabled="isDisabled" md-no-cache="noCache" md-selected-item="selectedItem" md-search-text-change="searchTextChange(searchText)" md-search-text="searchText" md-selected-item-change="selectedItemChange(item, field)" md-items="item in querySearch(searchText)" md-item-text="item.type" md-min-length="0" placeholder="Search by Items , customers & order id" md-menu-class="autocomplete-custom-template"  >
      <md-item-template> 

           <span style="display:inline-block;width:100%" ng-show="item.type == 'orders'">
         <span   class="text-uppercase head">{{item.type}}</span>
         <span class="main-head" ng-repeat="value in item.data"  > 
          <a href="<?php echo env("APP_URL", "");?>/v1/order_detail/{{value.order_id}}" style="line-height:0px;"  >
         <span class="item-title">
    <span>  </span>  
   <p> #{{value.order_id}}  - <span  style="background-color:{{value.status_details[0].label_colors}};border-radius:20px;padding:3px 7px;line-height:15px;">{{value.order_status}}</span>   <br> 
 <span class="subtitle">Amount - <?php echo $currency_symbol?>{{value.total}}</span>   <br>
         <span class="subtitle">({{value.created_at_formatted2}} )</span>
         </p> 
         </span>
         <span class="item-metadata">
         <span>   </span>  
         </span>
		  </a> 
         </span>
		<span ng-show="!item.data[0]">
		 <p class="text-center">No Order Found</p>
		 </span>
           
         </span>
         
		  <span  style="display:inline-block;width:100%" ng-show="item.type == 'items'"  >
            <span   class="text-uppercase head">{{item.type}} ({{item.data.length}})</span>
            <span class="main-head" ng-repeat="value in item.data">
               <a href="<?php echo env("APP_URL", "");?>/v1/item/{{value.item_id}}" style="line-height:0px;"  >
                  <span class="item-title" >
                     <span > <img src="<?php echo e(URL::asset('admin/assets/images/item-placeholder.png')); ?>" style="height:40px;width:40px;">		</span>										  
                     <p> {{value.item_title}}  <br>
                        <span class="subtitle"><?php echo $currency_symbol?> {{value.item_price}}</span>
                     </p>
                  </span>
                  <span class="item-metadata"  >
                  </span>
				    </a> 
            </span>
			 <span ng-show="!item.data[0]">
		 <p class="text-center">No Items Found</p>
		 </span>
          
         </span>
         <span  style="display:inline-block;width:100%" ng-show="item.type == 'users'"  >
            <span   class="text-uppercase head">{{item.type}} ({{item.data.length}})</span>
            <span class="main-head" ng-repeat="value in item.data">
               <a href="<?php echo env("APP_URL", "");?>/v1/customer-profile/{{value.user_id}}" style="line-height:0px;"  >
                  <span class="item-title" >
                     <span > <img src="<?php echo e(URL::asset('admin/assets/images/boy.png')); ?>" style="height:40px;width:40px;">		</span>										  
                     <p> {{value.first_name}} {{value.last_name}}  <br>
                        <span class="subtitle">{{value.email}}</span>
                     </p>
                  </span>
                  <span class="item-metadata"  >
                  </span>
            </span>
            </a> 
			<span ng-show="!item.data[0]">
		 <p class="text-center">No User Found</p>
		 </span>
         </span>
       
      </md-item-template>
   </md-autocomplete>
</div>