<?php $__env->startSection('title', 'Store Settings' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/app.css')); ?>">
 
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
            <section id="main-header">
               <div class="container-fluid">
                  <div class="row">
				       <div class="col-sm-6" ng-controller="searchController" ng-cloak > 
                           <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                       </div>
                      <div class="col-sm-12">
                        <textarea id="res" style="display:none;" ></textarea>
						
						<!-------------Loader Starts here------------->
                        <div id="loading" class="loading" style="display:none ;">
                           <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                           <p >Calling all data...</p>
                        </div>
						<!-------------Loader Ends here------------->
						  <input type="file" id="file" style="display:none "/>
						  
						    <input type="file" id="file1" style="display:none "/>
                        <!--------------------------- Angular App Starts ---------------------------->
                        <div class="tab-content"   >
                           <div class="container-fluid" >
                                                            <div class="row" ng-controller="storeDataController"  ng-cloak >
                                 <div class="col-sm-8 col-md-9 user-profile-data" >
                                    <div class="row"  >
                                       <div class="col-sm-12 col-md-8 col-lg-6"   >
                                          <div class="card info"  ng-repeat="values in storeInfo" >
                                             <img src="<?php echo e(URL::asset('web-food/assets/images/placeholders/store-placeholder.png')); ?>" style="display:block;margin-right:0;width:100px" ng-show="!values.store_photo">
                                             <img src="<?php echo e(URL::asset('images/stores')); ?>/{{values.store_photo}}" style="display:block;margin-right:0" ng-show="values.store_photo">
                                             <div>
                                                <h4>{{values.store_title}}</h4>
                                                <h5>{{values.address}}</h5>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 </div>
							  <div ng-view></div>
							  
                           </div>
                        </div>
                        <!--------------------------- Angular App Ends ---------------------------->
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>



<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/storeSettings.js')); ?>"></script> 



<script src="https://use.fontawesome.com/2c7a93b259.js"></script>
<script>
 
 
    function changeStoreProfile() {
        $('#file').click(); 
    }
    $('#file').change(function () { 
        if ($(this).val() != '') {
            upload1(this);
  
        }
    });
    function upload1(img) { 
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-stores')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_store_image').attr('src', '<?php echo e(asset('images/stores/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                var data = data.filename;
                console.log(data);
                    $('#store_file_name').val(data);
                     $('#preview_store_image1').attr('src', '<?php echo e(asset('images/stores')); ?>/'+data);
                    $('#preview_store_image').attr('src', '<?php echo e(asset('images/stores')); ?>/'+data);

                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_store_image').attr('src', '<?php echo e(asset('images/stores/noimage.jpg')); ?>');
            }
        });
    }
    function removeFile() {
        if ($('#store_file_name').val() != '')
            if (confirm('Are you sure want to remove profile picture?')) {
                $('#loading1').css('display', 'block');
                var form_data = new FormData();
                form_data.append('_method', 'DELETE');
                form_data.append('_token', '<?php echo e(csrf_token()); ?>');
                $.ajax({
                    url: "ajax-remove-image-stores/" + $('#file_name').val(),
                    data: form_data,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                        $('#store_file_name').val('');
                        $('#loading1').css('display', 'none');
                    },
                    error: function (xhr, status, error) {
                        alert(xhr.responseText);
                    }
                });
            }
    }
 
</script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.min.js"
   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>-->
<script>
   function changeProfile() {
       $('#file1').click();
   }
   $('#file1').change(function () {
       if ($(this).val() != '') {
           upload(this);
   
       }
   });
    function upload(img) {
        var form_data = new FormData();
        form_data.append('file', img.files[0]);
        form_data.append('_token', '<?php echo e(csrf_token()); ?>');
        $('#loading1').css('display', 'block');
        $.ajax({
            url: "<?php echo e(url('image-upload-users')); ?>",
            data: form_data,
            type: 'POST',
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.fail) {
                    $('#preview_image').attr('src', '<?php echo e(asset('images/users/noimage.jpg')); ?>');
                    alert(data.errors['file']);
                }
                else {
                       var data = data.filename;
                console.log(data);
                 $('#item_photo').val(data).trigger("change");
   
   //$("#photo").dispatchEvent(new Event("input", { bubbles: true }));
    
   
               
               //document.getElementById('photo').value=data;
                    $('#preview_image').attr('src', '<?php echo e(asset('images/users')); ?>/' + data);
               $('#preview_image1').attr('src', '<?php echo e(asset('images/users')); ?>/' + data);
                }
                $('#loading1').css('display', 'none');
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText);
                $('#preview_image').attr('src', '<?php echo e(asset('images/users/noimage.jpg')); ?>');
            }
        });
    }
   
   
   
   
   function removeFile() {
       if ($('#item_photo').val() != '')
 

      $('#preview_image').attr('src', '<?php echo e(asset('admin/assets/images/placeholder.jpg')); ?>');
      
      $('#preview_image1').attr('src', '<?php echo e(asset('admin/assets/images/placeholder.jpg')); ?>');
 $('#item_photo').val('');
 
 /*
           if (confirm('Are you sure want to remove profile picture?')) {
               $('#loading1').css('display', 'block');
               var form_data = new FormData();
               form_data.append('_method', 'DELETE');
               form_data.append('_token', '<?php echo e(csrf_token()); ?>');
               $.ajax({
                   url: "ajax-remove-image-users/" + $('#photo').val(),
                   data: form_data,
                   type: 'POST',
                   contentType: false,
                   processData: false,
                   success: function (data) {
                       $('#preview_image').attr('src', '<?php echo e(asset('images/noimage.jpg')); ?>');
                       $('#photo').val('');
                       $('#loading').css('display', 'none');
                   },
                   error: function (xhr, status, error) {
                       alert(xhr.responseText);
                   }
               });
           }*/
   }
   
   
   
   
</script> 
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>