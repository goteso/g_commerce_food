<?php $__env->startSection('title', 'My Orders' ); ?>  
 <style>
.rating {
    float:left;
}

/* :not(:checked) is a filter, so that browsers that don’t support :checked don’t 
   follow these rules. Every browser that supports :checked also supports :not(), so
   it doesn’t make the test unnecessarily selective */
.rating:not(:checked) > input {
    position:absolute;
    top:-9999px;
    clip:rect(0,0,0,0);
}

.rating:not(:checked) > label {
    float:right;
    width:1em;
    padding:0 .1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:200%;
    line-height:1.2;
    color:#ddd;
    text-shadow:1px 1px #bbb, 2px 2px #666, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:before {
    content: '★ ';
}

.rating > input:checked ~ label {
    color: #f70;
    text-shadow:1px 1px #c60, 2px 2px #940, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:hover,
.rating:not(:checked) > label:hover ~ label {
    color: gold;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > input:checked + label:hover,
.rating > input:checked + label:hover ~ label,
.rating > input:checked ~ label:hover,
.rating > input:checked ~ label:hover ~ label,
.rating > label:hover ~ input:checked ~ label {
    color: #ea0;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > label:active {
    position:relative;
    top:2px;
    left:2px;
}

/* end of Lea's code */

/*
 * Clearfix from html5 boilerplate
 */

.clearfix:before,
.clearfix:after {
    content: " "; /* 1 */
    display: table; /* 2 */
}

.clearfix:after {
    clear: both;
}

/*
 * For IE 6/7 only
 * Include this rule to trigger hasLayout and contain floats.
 */

.clearfix {
    *zoom: 1;
}

/* my stuff */
#status, button {
    margin: 20px 0;
}

 </style>
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('header'); ?>
<?php echo $__env->make('web-food.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
  
 <div ng-controller="orderDetailController" ng-cloak>
 
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('<?php echo e(URL::asset('web-food/assets/images/food-back-small.jpg')); ?>')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-8"   >
                           <md-list layout-padding class="profile-data" >
							<md-list-item class="md-3-line"     >
								<img  ng-src="<?php echo e(URL::asset('images/users')); ?>/{{customerDataPhoto}}" class="md-avatar" ng-show="customerDataPhoto">
								<img  ng-src="<?php echo e(URL::asset('admin/assets/images/user-placeholder.png')); ?> " class="md-avatar" ng-show="customerDataId && !customerDataPhoto">
								<div class="md-list-item-text">
								  <h2 >{{customerDataFirstName}} {{customerDataLastName}}</h2>
								  <h4>
									{{customerDataEmail}}
								  </h4>
								</div>
							</md-list-item>
						  </md-list>    
                      </div>
                      <div class="col-sm-4">
                          <br>
                         <a href="<?php echo e(URL::to('/')); ?>"> <button class="btn md-raised bg-color md-submit md-button md-ink-ripple md-button" ng-show="customerDataId">Place Order</button>
                         </a>
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="order-detail" >
			
				  
			 <div id="loading" class="loading" style="display:none ;">
                  <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container">
				   
                  <div class="row"> 
                     <div class="col-sm-12">  
					 
					   <div class="row">
					  <div class="col-sm-5"> 
                             <a href="<?php echo e(URL::to('/my-orders')); ?>"> <button class="btn md-raised bg-color md-submit md-button md-ink-ripple   md-button" ng-show="customerDataId" style="padding:2px 25px;margin:0px;"><img src="web-food/assets/images/left-arrow.png"> My Orders</button></a> 
					  </div>
					  
					   <div class="col-sm-7 text-right"> 
					   <div  ng-repeat="order in orders" ng-show="order.type =='customer_buttons'">
                            <div ng-repeat="values in order.data"><button class="btn md-raised bg-color md-submit md-button md-ink-ripple dropdown-toggle md-button" ng-disabled="values.enabled == '0'"  ng-click="updateAction(values.action);" style="padding:2px 25px;margin:0px;"  > {{values.title}}</button> </div>
					  </div>
					  </div>
					  
				   </div>
					  
                            <div class="row">
									   
									      <!-----Block Starts For Items Detail----->
                                          <div class="col-lg-8 col-md-12"  ng-repeat="order in orders" ng-show="order.type=='items'">
                                             <div class="panel" ng-show="order.type=='items'" >
											  
                                                <h3 class="block-title" style="float:left;">{{order.title}}</h3> 
                                                <table class="table items-detail">
												   <thead>
												     <th class="text-left">Item</th>
													 <th>Item Name</th>
													 <th class="text-center">Quantity*Price</th>
													 <th class="text-right">Total</th>
												   </thead>
                                                   <tbody>
                                                      <tr ng-repeat="items in order.data ">
                                                         <td class="item-image">
                                                            <img src="<?php echo e(URL::asset('admin/assets/images/placeholder.jpg')); ?>" style="height:60px;width:60px;" class="img-responsive" ng-show="!items.item_photo"> 
															<img src="<?php echo e(URL::asset('/images/items')); ?>/{{items.item_photo}}" style="height:50px;width:50px;" class="img-responsive" ng-show="items.item_photo"> 
                                                         </td>
                                                         <td class="item-title">
                                                            {{items.item_title}} 
                                                            <p ng-repeat="v in items.order_item_variant" style="font-size: 13px!important;">{{v.item_variant_title}} : {{v.order_item_variant_value}}</p> 
                                                            <br><span class="item-desc">{{items.unit}}</span>
                                                         </td>
                                                         <td class="item-units text-center">{{items.order_item_quantity}} * {{items.single_quantity_item_price}}/<span class="item-desc">{{items.item_unit}}</span></td>
                                                         <td class="item-total text-right"><?php echo env("CURRENCY_SYMBOL", "");?>{{items.item_price }}</td>
                                                         <td></td>
                                                         <td></td>
                                                        
                                                      </tr>
                                                      <tr class="total">
                                                         <td class="" width="40%">Sub Total</td>
                                                         <td></td>
                                                         <td></td>
                                                         <td class="text-right"><?php echo env("CURRENCY_SYMBOL", "");?>{{order.sub_total}} </td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div> 
                                          </div>
										    <!-----Block Ends For Items Detail----->
											
											 <!-----Block Starts For Payment Summary----->
											   <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders"  ng-show="order.type=='payment_details'" >
											   <div class="panel"  > 
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <table class="table paymentSummary" >
                                                   <tbody >
                                                      <tr  ng-repeat="items in order.data " style="border-bottom:1px solid #ccc">
                                                         <td class="" style="font-size:14px;text-transform: capitalize">{{items.title.replace('_', ' ')  }} </td>
                                                         <td class="text-right" style="font-size:14px">
                                                            <p><?php echo env("CURRENCY_SYMBOL", "");?> {{items.value}}</p>
                                                         </td>
                                                      </tr> 
													  <tr>
													    <td><b>Total</b></td>
														<td  class="text-right"><b><?php echo env("CURRENCY_SYMBOL", "");?> {{order.order_total}}</b></td>
													  </tr>
                                                   </tbody>
                                                </table>
                                               </div> 
											 </div>
											 <!-----Block Ends For Payment Summary----->
											      <!-----Block Starts For Store Information----->
                                         <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders" ng-show="order.type=='store_details'"> 
                                             <div class="panel"ng-repeat="items in order.data "  > 
                                                <h3 class="block-title">{{order.title}}</h3>
                                                <div  style="padding-top:10px;">
												<img src="<?php echo e(URL::asset('images/stores')); ?>/{{items.store_photo}}" class="img-responsive" ng-show="items.store_photo"style="width:30%">
												<img src="<?php echo e(URL::asset('web-food/assets/images/placeholders/store-placeholder.png')); ?>" ng-show="!items.store_photo" class="img-responsive" style="width:40%">
                                                 <!--<div style="padding-left:10px;">  <p class="name"><a href="#"></a>
                                                      Title : {{items.store_title}}  
                                                   </p>
                                                 
                                                   <p>Address : {{items.address}} </p>
                                                  </div>-->
                                                    
                                                </div> 
												<br>
												<div style="">  <p class="name"><a href="#"></a>
                                                      Title : {{items.store_title}}  
                                                   </p>
                                                 
                                                   <p>Address : {{items.address}} </p>
                                                  </div>
											 </div>
											  </div>
                                             <!-----Block Ends For Store Information----->
											  <!-----Block Starts For Meta Summary----->
											 <div class="col-sm-12 col-md-6 col-lg-4" ng-repeat="order in orders"  ng-show="order.type=='order_meta'" >
											   <div class="panel"  > 
                                                 <h3 class="block-title">{{order.title}}</h3>
                                                 <div ng-repeat="items in order.data" class="ordersummary">
	                                                 <h4 class="order-title">{{items.title}}</h4>
		                                             <p class="order-desc"  >{{items.value}}</p> 
		                                         </div>
                                             </div> 
                                          </div>
											 <!-----Block Ends For Meta Summary----->
											
											</div>
						  
						 </div>
						 </div>
                     </div> 
					  
                
               
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
		
		
		
		
		
			<!------------------------------------------- ORDER TRACK Modal starts ------------------------------------------------>
<div id="orderTrack" class="modal fade" role="dialog"   style="z-index:20;" data-backdrop="false">
   <div class="modal-dialog  address-add" data-backdrop="false">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">Order  <b> Track</b></h4>
         </div>
         <div class="modal-body" > 
               <div class="row">
                  <div class="col-sm-12 col-md-12">
				  
				   
				   
                     <div class="form-horizontal"  >
                       <br>
                        <!--Map by element. Also it can be attribute-->
						
						<div id="dvMap" style="height:300px;"></div>
                       <!-- <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;height:350px;" ></locationpicker>-->
                        <div class="clearfix" >&nbsp;</div>
                        
                       
                     </div>
                  </div>
                   
         </div>
      </div>
   </div>
</div>
   </div>
<!------------------------------------ORDER TRACK MODAL ends -----------------------------------> 



<!------------------------------------------- ORDER TRACK Modal starts ------------------------------------------------>
<div id="feedbackModal" class="modal fade" role="dialog"   style="z-index:20;" data-backdrop="false">
   <div class="modal-dialog  address-add" data-backdrop="false">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">Send <b> Feedback</b></h4>
         </div>
         <div class="modal-body" > 
               <div class="row">
                  <div class="col-sm-12 col-md-12">
				    <label class="control-label">Rating</label><br>
					 <fieldset class="rating"> 
        <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="Rocks!">5 stars</label>
        <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="Pretty good">4 stars</label>
        <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="Meh">3 stars</label>
        <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="Kinda bad">2 stars</label>
        <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="Sucks big time">1 star</label>
    </fieldset>
	
	 <input type="text" id="rating" value="" style="display:none;">
	
				 
	  <br><br>
	  
                     <div class="form-horizontal"  >
                        <label class="control-label">Feedback</label>
                        
                        <textarea id="feedback" class="form-control" rows="4" placeholder="Type your comment here...."></textarea>
                        
                        
                     </div>
					 
					 <br>
                  </div>
				  
				  <div class="col-sm-12  text-right">
				  <button class="btn md-raised bg-color md-submit md-button md-ink-ripple  md-button" ng-click="sendFeedback();">Submit</button>
                   </div>
         </div>
      </div>
   </div>
</div>
   </div>
<!------------------------------------ORDER TRACK MODAL ends -----------------------------------> 




		</div>
		   				  
      
<?php $__env->startSection('footer'); ?>
<?php echo $__env->make('web-food.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<!------> 
<script type="text/javascript" src="<?php echo e(URL::asset('web-food/angular-controllers/order-detail.js')); ?>"></script> 

<script>
$(document).ready(function() { 



        if ($("radio:checked").length == 0) {
            $('#status').html("nothing checked");
            return false;
        } else {
			alert($("radio:checked").length);
            $('#status').html( 'You picked ' + $('input:radio[name=rating]:checked').val() );
        }
    
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web-food.layout.web', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>