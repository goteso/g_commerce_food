<?php $__env->startSection('title', 'Login' ); ?>  
 <style>
 .eye{    position: absolute;
    right: 70px;
 bottom: 93px;}
 
 @media(max-width:767px){.eye{bottom: 100px!important;}}
 </style>
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('header'); ?>
<?php echo $__env->make('web-food.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
 
 
 
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('<?php echo e(URL::asset('web-food/assets/images/food-back-small.jpg')); ?>')no-repeat center;">
               <div class="container-fluid"> 
                  <div class="row" >
                      <div class="col-sm-12"  >
                                     
                   
                 </div>
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="login" ng-controller="registerController">
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-12 text-center">   
                         <div class="panel">
						 <div class="panel-body">
						   <div class="row">
						     <div class="col-sm-7 left">
							   <h3 class="header  text-center">Create an account</h3>
							   <br>
							   <form>
							   <div class="row">
							       <div class="form-group col-sm-6">
								   <input type="text" class="form-control" style="text-transform: capitalize;" id="first_name" name="first_name" placeholder="First Name"  >
								 </div>
								   <div class="form-group col-sm-6">
								   <input type="text" class="form-control" style="text-transform: capitalize;" id="last_name" name="last_name" placeholder="Last Name">
								 </div>
								 </div>
							     <div class="form-group">
								   <input type="email" class="form-control" id="email" name="email" placeholder="Email"  >
								 </div>
								 <div class="form-group">
								   <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="Mobile"    >
								 </div> 
								 <div class="form-group">
								   <input type="password" class="form-control" id="password" name="password" placeholder="Password"  ><span class="eye" onclick="myFunction()"><i class="fa fa-eye"></i></span>
								 </div> 
							  

							   <md-button type="button"  ng-click="register()" class="btn md-raised bg-color md-submit md-button md-ink-ripple">sign up</md-button>
 

							   </form>
							 </div>
							 <div class="col-sm-5 right">
							 <br> 
							  <h3 class="header  text-center">Already User?</h3>
							  <p> <a href="<?php echo e(URL::to('/web-login')); ?>">Login to your account</a> </p>
							  <br>
							    
							  <hr style="width:80%;">
							  
							  <span class="center-part text-center">LOGIN WITH</span><br>
							  
							 <div style="display:inline-flex;">
							   <md-button type="button"  class="btn md-raised bg-color md-submit md-button md-ink-ripple"><i class="fab fa-facebook-f"></i></md-button>
							    <md-button type="button"  class="btn md-raised bg-color md-submit md-button md-ink-ripple"><i class="fab fa-google-plus-g"></i></md-button>
								</div>
								
							 </div>
						   </div>
						 </div>
						 </div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		

			<script type="text/javascript" src="<?php echo e(URL::asset('web-food/angular-controllers/register.js')); ?>"></script>		
		  		<script>
				function myFunction() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}	  
      </script>
<?php $__env->startSection('footer'); ?>
<?php echo $__env->make('web-food.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<!------>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web-food.layout.web', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>