<?php $__env->startSection('title', 'Cuisines' ); ?> 
<link rel="stylesheet" href="<?php echo e(URL::asset('admin/assets/css/custom.css')); ?>">
<style>
@media(max-width:767px){
	.table>tbody>tr>td{width:35%;white-space:normal!important}
}
@media(min-width:768px){
.table{table-layout:fixed}
.table>tbody>tr>td{ white-space: nowrap;
  overflow: hidden;
text-overflow: ellipsis;}
}
</style>
<?php $__env->startSection('content'); ?>
<?php $__env->startSection('header'); ?>
<?php echo $__env->make('admin.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<div ng-app="mainApp" style="margin-top:61px;z-index:99999999">
   <div id="wrapper" >
      <div id="layout-static">
         <!---------- Static Sidebar Starts------->			
         <?php $__env->startSection('sidebar'); ?>
         <?php echo $__env->make('admin.includes.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->yieldSection(); ?>
         <!---------- Static Sidebar Ends------->
         <div class="static-content-wrapper"  >
		 
<section id="main-header">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-6"  ng-controller="searchController" ng-cloak > 
            <?php echo $__env->make('admin.includes.realtime-search', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         </div>
         <div class="col-sm-12">
            <div class="text-right">
            </div>
            <div class="tab-content"  ng-controller="cuisineController as ctrl" ng-cloak  >
               <!--------------------------- Angular App Starts ---------------------------->
               <textarea id="res" style="display:  none  ;" ></textarea>
                  <div id="loading" class="loading" style="display:none ;">
                  <img src="<?php echo e(URL::asset('admin/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                  <p >Calling all data...</p>
                  </div>   
               <div class="container-fluid" >
                  <div class="row">
                     <div class="col-sm-10" >
                        <h2 class="header">Cuisine Store Filter</h2>
                     </div>
                     <div class="col-sm-2">
                        <div class="text-right">
                           <button type="button" class="btn md-raised bg-color md-add md-button md-ink-ripple text-right"  data-toggle="modal" data-target="#add">Add New</button>  
                        </div>
                     </div>
                  </div>
                  <div class="row" >
                     <div class="col-sm-12" >
                        <div class="" >
                           <div id="tableToExport" class="products-table table-responsive"  >
                              <table class="table" class="table table-striped" id="exportthis" >
                                 <thead>
                                    <tr>
                                       <th>ID</th>
                                       <th>IDENTIFIER</th>
									    <th>VALUE</th>
                                       <th>CREATED</th>
                                       <th id="action">ACTION</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    
                                    <tr ng-repeat="values in ctrl.cuisines.data.data  ">
                                       <td>#{{values.id}}</td>
                                       <td  >{{values.identifier}} </span></td>
									   <td  >{{values.value}}</td>
                                       <td>{{values.created_at_formatted}}</td>
                                       <td>  
                                                      <a class="btn btn-xs delete-product" ng-click="ctrl.deleteCuisine(values.id, $index)"><i class="fa fa-trash"></i></a>               
                                       </td>
                                    </tr>
                                    
                                 </tbody>
                              </table>
							  
							   <div class="container-fluid innerData" ng-show="!ctrl.cuisines.data.data">
                                          <div class="row">
                                            <div class="col-sm-12"> 
                                                 <br>
                                                <img class="img-responsive center-block" src="web-food/assets/images/placeholders/faq-placeholder.png">
                                                <p>No Address found.</p>
                                            </div>
                                          </div>
                                        </div>
										
										
                           </div> 
						   
						    <div class="pagination text-right" style="display:block" >
                                           <button class="btn"  ng-show="ctrl.cuisines.data.first_page_url != null"  ng-click="ctrl.pagination(ctrl.cuisines.data.first_page_url);">First</button> 
                                           <button class="btn"  ng-show="ctrl.cuisines.data.prev_page_url != null" ng-click="ctrl.pagination(ctrl.cuisines.data.prev_page_url);">Previous</button> 
                                          <span>{{ctrl.cuisines.data.current_page}}</span>
                                          <button class="btn"  ng-show="ctrl.cuisines.data.next_page_url != null" ng-click="ctrl.pagination(ctrl.cuisines.data.next_page_url);">Next</button> 
                                          <button class="btn" ng-show="ctrl.cuisines.data.last_page_url != null"  ng-click="ctrl.pagination(ctrl.cuisines.data.last_page_url);">Last</button> 
                                       </div>
									   
                        </div>
                     </div>
                     
                  </div>
               </div>
           
            <!--------------------------- Angular App Ends ---------------------------->
         
   
   
   
   <!-----------------------------------------------CUISINE ADD MODAL STARTS HERE-------------------------------------------------------------------->
   <div id="add" class="modal fade" data-backdrop="false" style="z-index:9999" role="dialog">
      <div class="modal-dialog modal-lg category-add"  >
         <!-- Modal content-->
         <div class="modal-content "   >
            <div class="modal-header  "  >
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title text-uppercase"  >Add <b>Cuisine</b></h4>
            </div>
            <div class="modal-body "  >
               <div class="row"   > 
                  <div class="col-sm-9"> 
                     <md-input-container  class="md-block">
                        <label>Identifier</label>
                        <input type="text" id="cuisine_indentifier" name="cuisine_indentifier"/>
                     </md-input-container>
                     <br>
					    <md-input-container  class="md-block">
                        <label>Value</label>
                        <input type="text" id="cuisine_value" name="cuisine_value"/>
                     </md-input-container>
                     </div>
					 <div class="col-sm-3">
                     <md-button ng-click="ctrl.storeCuisines()" class="md-raised bg-color md-submit">Add Cuisine</md-button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
    <!-----------------------------------------------CUISINE ADD MODAL ENDS HERE-------------------------------------------------------------------->
	
	
	
	
	
	
	
	
	 
	
	
	 </div>
	</div>
      </div>
   </div>
	
</section>

 </div>
   </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo e(URL::asset('admin/angular-controllers/cuisineFilter.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layout.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>