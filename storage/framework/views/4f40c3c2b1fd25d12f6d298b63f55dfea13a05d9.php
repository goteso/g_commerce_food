<?php $__env->startSection('title', 'Address' ); ?> 
 <link rel="stylesheet" href=" <?php echo e(URL::asset('web-food/assets/css/style1.css')); ?>">
 
<?php $__env->startSection('content'); ?>


<?php $__env->startSection('header'); ?>
<?php echo $__env->make('web-food.includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
 
    
						   
  <div ng-controller="addressController" ng-cloak>
        <!---------------------------------------BANNER SECTION STARTS HERE-------------------------------------------->
            <section id="banner" style="background: url('<?php echo e(URL::asset('web-food/assets/images/food-back-small.jpg')); ?>')no-repeat center;">
               <div class="container"> 
                  <div class="row" >
                      <div class="col-sm-8"   >
                           <md-list layout-padding class="profile-data" >
							<md-list-item class="md-3-line"     >
								<img  ng-src="<?php echo e(URL::asset('images/users')); ?>/{{customerDataPhoto}}" class="md-avatar" ng-show="customerDataPhoto">
								<img  ng-src="<?php echo e(URL::asset('admin/assets/images/user-placeholder.png')); ?>" class="md-avatar" ng-show="customerDataId && !customerDataPhoto">
								<div class="md-list-item-text">
								  <h2 >{{customerDataFirstName}} {{customerDataLastName}}</h2>
								  <h4>
									{{customerDataEmail}}
								  </h4>
								</div>
							</md-list-item>
						  </md-list>    
                      </div>
                      <div class="col-sm-4">
                          <br>
                         <a href="<?php echo e(URL::to('/')); ?>"> <button class="btn md-raised bg-color md-submit md-button md-ink-ripple  " ng-show="customerDataId">Place Order</button>
                         </a>
                      </div>
                 </div>
               </div>
            </section>
		<!---------------------------------------BANNER SECTION ENDS HERE-------------------------------------------->
 
        <!---------------------------------------WORKS SECTION STARTS HERE-------------------------------------------->
            <section id="orders" >
               <div class="container">
                  <div class="row"> 
                     <div class="col-sm-4 text-center" ng-controller="logoutController">   
                         <div class="panel left">
						 <div class="panel-body">
						  <table  class="table left-nav-links">
						    <tr>
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/shopping-bag.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/shopping-bag-active.svg')); ?>" class="img-active">
							  </td>
							  <td > <a href="<?php echo e(URL::to('my-orders')); ?>">My Orders</a></td>
							</tr>
							<tr class="active">
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/address.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/address-active.svg')); ?>" class="img-active">
							  </td>
							  <td> <a href="<?php echo e(URL::to('addresses')); ?>">Manage Addresses</a></td>
							</tr>
							<tr  class="">
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('loyalty-points')); ?>"> Loyalty Points History</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/feedback.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/feedback-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('reviews')); ?>"> My Reviews</a></td>
							</tr>
							<tr>
							  <td>  
							     <img src="<?php echo e(URL::asset('web-food/assets/images/favorites.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/favorites-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('favourites')); ?>"> Favourites</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('account')); ?>/{{profileData.data.user_id}}"> Edit Profile</a></td>
							</tr>
							<tr >
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('change-password')); ?>"> Change Password</a></td>
							</tr>
							<tr class="">
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/settings.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/settings-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="<?php echo e(URL::to('transfer-loyalty-points')); ?>"> Transfer Loyalty Points</a></td>
							</tr>
							<tr>
							  <td>   
							     <img src="<?php echo e(URL::asset('web-food/assets/images/sign-out.svg')); ?>" class="img-normal">
					             <img src="<?php echo e(URL::asset('web-food/assets/images/sign-out-active.svg')); ?>" class="img-active">
							  </td>
							  <td><a href="#" ng-click="logout();">Signout</a></td>
							</tr>
						   </table>
						 </div>
						 </div>
                     </div> 
					 
					 
					 <div class="col-sm-8 text-center" >   
                         <div class="panel">
						 <div class="panel-body">
						 
						 <div id="loading" class="loading" style="display:none ;">
                              <img src="<?php echo e(URL::asset('web-food/assets/images/89.svg')); ?>" class="img-responsive center-block">				 
                              <p >Loading...</p>
                           </div>
						   
						   
						   <div class="text-right">
						   <button class="btn md-raised bg-color md-submit md-button md-ink-ripple " ng-show="customerDataId" data-toggle="modal" data-target="#address_add">Add Address</button>
						   </div>
						     <div id="tableToExport" class="products-table table-responsive"  >
                                          <table class="table" class="table table-striped" id="exportthis" >
                                             <thead>
                                                <tr> 
												   <th></th>
                                                   <th>TITLE</th>
                                                   <th>ADDRESS</th>
                                                   <th>ZIPCODE</th>
                                                   <th>CITY</th>
                                                   <th>CREATED</th>
                                                  <th>ACTIONS</th>
                                                </tr>
                                             </thead>
                                             <tbody >
                                                <tr  class="innerData" ng-repeat="values in addressList.data.data  "> 
												<td><input class="md-primary" id="default" type="radio" name="default" ng-model="values.default"  ng-checked="values.default == 1" ng-change="updateDefaultStatus(values.address_id,index );" /></td>
                                                   <td> {{values.address_title}}  </td>
                                                   <td>{{values.address_line1}}  </td>
                                                   <td >{{values.pincode}}</td>
                                                   <td> {{values.city}} </td>
                                                   <td>{{values.created_at_formatted}}</td>
                                                    <td class="actions">
                                                      <a class="btn btn-xs edit-product" href="#" ng-click="getEditValues(values)"><i class="fa fa-edit"></i></a> 
													  <a class="btn btn-xs delete-product" href="#" ng-click="user_address_delete(values.address_id)"><i class="fa fa-trash"></i></a> 
                                                   </td> 
                                                </tr>
                                             </tbody>
                                          </table>

                                           <div class="container-fluid innerData" ng-show="!addressList.data.data">
                                          <div class="row">
                                            <div class="col-sm-12"> 
                                                 <br>
                                                <img class="img-responsive center-block" src="web-food/assets/images/placeholders/address-placeholder.png">
                                                <p>No Address found.</p>
                                            </div>
                                          </div>
                                        </div>

                                       </div> 
						 </div>
						 </div>
                     </div> 
                  </div> 
               </div>
            </section>
		<!---------------------------------------WORKS SECTION ENDS HERE-------------------------------------------->
		
		
		
		
		
		
		
		
		
		<!------------------------------------------- Add address  Modal starts ------------------------------------------------>
<div id="address_add" class="modal fade" role="dialog"   style="z-index:20;" data-backdrop="false" >
   <div class="modal-dialog modal-lg address-add"data-backdrop="false" >
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-uppercase">Add <b> Address</b></h4>
         </div>
         <div class="modal-body" > 
               <div class="row">
                  <div class="col-sm-12 col-md-6">
                     <div class="form-horizontal"  >
                        <label class="control-label">Search</label>
                        <div class="search-input">
                           <input type="text" class="form-control" id="us3-address" placeholder="Type area, city or street"  />
                        </div>
                        <br>
                        <!--Map by element. Also it can be attribute-->
                        <locationpicker options="locationpickerOptions" style="width:98%;padding:20px 5px;height:350px;" ></locationpicker>
                        <div class="clearfix" >&nbsp;</div>
                        <h4>{{address}}</h4>
                        <div class="m-t-small  ">
                            <h4>Coordinates:</h4>						
                           Latitude : <input type="text" class="form-control"   id="us3-lat"  readonly><br>
                          Longitude : <input type="text" class="form-control"   id="us3-lon" readonly />
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
				  <br>
                     <div class="row">
					   <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Address Title</label>
                              <input type="text" id="address_title" ng-model="address_title"/>
                           </md-input-container>
                        </div>
						  <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Phone</label>
                              <input type="text" id="address_phone" ng-model="address_phone"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-12">
                           <md-input-container  class="md-block">
                              <label>Address Line 1</label>
                              <input type="text" id="address_line1" ng-model="address_line1"/>
                           </md-input-container>
                           <md-input-container  class="md-block">
                              <label>Address Line 2</label>
                              <input type="text" id="address_line2" ng-model="address_line2"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>City</label>
                              <input type="text" id="city" ng-model="city"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>State</label>
                              <input type="text" id="state" ng-model="state"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Country</label>
                              <input type="text" id="country" ng-model="country"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Zipcode</label>
                              <input type="text" id="pincode" ng-model="pincode"/>
                           </md-input-container>
                        </div>
                        
                        </div>
                     </div>
					 <div class="text-right">
                     <md-button ng-click="add_address()"  class="md-raised bg-color md-submit">Add Address</md-button>
                     <div>
            </div>
            </div>
         </div>
      </div>
   </div>
</div>
   </div>
<!------------------------------------ Edit addresses ends -----------------------------------> 
<!------------------------------------------- Edit address  Modal starts ------------------------------------------------>
<div id="address_edit" class="modal fade" role="dialog" style="z-index:20" data-backdrop="false">
   <div class="modal-dialog modal-lg  address-add"  style="width:80%; " data-backdrop="false">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Address</h4>
         </div>
         <div class="modal-body" >
            <div class="row">
                  <div class="col-sm-12 col-md-6">
                     <div class="form-horizontal"  >
                        <label class="control-label">Search</label>
                        <div class="search-input">
                           <input type="text" class="form-control" id="us3-address-edit" placeholder="Type area, city or street"  />
                        </div>
                        <br>
                        <!--Map by element. Also it can be attribute-->
                        <locationpicker options="locationpickerOptionsEdit" style="width:98%;padding:20px 5px;" ></locationpicker>
                        <div class="clearfix" >&nbsp;</div>
                        <h4>{{address}}</h4>
                        <div class="m-t-small  ">  <h4>Coordinates:</h4>						
                           Latitude :   <input type="text" class="form-control"   id="us3-lat-edit"  ng-model="latitude_edit" readonly><br>
                          Longitude :  <input type="text" class="form-control"   id="us3-lon-edit"  ng-model="longitude_edit" readonly />
                        </div>
                        <div class="clearfix"></div>
                     </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                     <div class="row">
					   <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Address Title</label>
                              <input type="text" ng-model="address_title_edit" id="address_title_edit"  value="{{address_title_edit}}"/>
                           </md-input-container>
                        </div>
						  <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Phone</label>
                              <input type="text" ng-model="address_phone_edit" id="address_phone_edit" value="{{address_phone_edit}}"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-12">
                           <md-input-container  class="md-block">
                              <label>Address Line 1</label>
                              <input type="text" ng-model="address_line1_edit" id="address_line1_edit"/>
                           </md-input-container>
                           <md-input-container  class="md-block">
                              <label>Address Line 2</label>
                              <input type="text" ng-model="address_line2_edit" id="address_line2_edit"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>City</label>
                              <input type="text" ng-model="city_edit" id="city_edit"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>State</label>
                              <input type="text" ng-model="state_edit" id="state_edit"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Country</label>
                              <input type="text" ng-model="country_edit" id="country_edit"/>
                           </md-input-container>
                        </div>
                        <div class="col-sm-6">
                           <md-input-container  class="md-block">
                              <label>Zipcode</label>
                              <input type="text" ng-model="pincode_edit" id="pincode_edit"/>
                           </md-input-container>
                        </div>
                        
                        </div>
                     </div>
                     <md-button ng-click="user_address_update()"  class="md-raised bg-color md-submit">Update </md-button>
           
            </div>
         </div> 
      </div>
   </div>
</div>
<!------------------------------------ Add addresses ends ----------------------------------->



 </div>
		
		  				  
      
<?php $__env->startSection('footer'); ?>
<?php echo $__env->make('web-food.includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldSection(); ?>
<!------>

<script type="text/javascript" src="<?php echo e(URL::asset('web-food/angular-controllers/address.js')); ?>"></script> 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('web-food.layout.web', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>