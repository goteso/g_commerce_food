 
	
 //====================================================== REVIEW CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('FreshdeskEnquiriesController', function ($http, $scope, $window,toastr) {
 
	     // get basic form to add product ============================================================== clean done
	     
 
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/freshdesk',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.freshdesk_enquiries  = data;
            $('#loading').css('display', 'none');  
			 console.log("Success Response Data = "+JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
		     console.log("Error Response Data = "+JSON.stringify(data));
        });  



		
 
 
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.reviews = data;
            $('#loading').css('display', 'none');  
	 
        })
		.error(function (data, status, headers, config) { 
	 
        });       
		
		} 
		}
		



		$scope.replyUser = function(id){ 
			$scope.enquiry_id = id;
			$('#enquiry_id').val(id);
      $('#replyModal').modal('show');
		}
		


		$scope.reply = function()
		{

			 $('#loading').css('display', 'block');
					   $scope.message = $('#message').val();
		   $scope.enquiry_id =  $('#enquiry_id').val();
		   $scope.request_data = {"enquiry_id" : $scope.enquiry_id, "message" : $scope.message};
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/freshdesk/ticket_reply',
            data: $scope.request_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            $scope.data = data;
             toastr.success(data.message, 'Success');
             $('#loading').css('display', 'none');  
           //  location.reload();			 
		console.log(JSON.stringify(data));
		     $('#replyModal').modal('show');
             $('#message').val('');
             location.reload();	
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
             $('#loading').css('display', 'none');  
             console.log(JSON.stringify(data));
             $('#replyModal').modal('show');
             $('#message').val('');
 
        }); 
		}

		
		
	});
		
		  
		
		
		 