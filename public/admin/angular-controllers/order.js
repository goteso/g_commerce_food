
app.config(function($routeProvider) {
    $routeProvider 
    .when("/regular", {
        templateUrl : APP_URL+"/order-regular.html",
        controller : "regularOrdersController as ctrl"
    })
    .when("/catering", {
        templateUrl : APP_URL+"/order-catering.html",
        controller : "cateringOrdersController as ctrl"
    })
    .otherwise({
            redirectTo: '/regular'
        });	;
});
		
		
		 
//==========================================================REGULAR ORDER CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('regularOrdersController', function($http,$scope,$window,$log, toastr) {
		  
	 var pro = this; 
	    $('#loading').css('display', 'block');
       $('.innerData').css('display', 'none'); 
	    var auth_user_id = document.getElementById('auth_user_id').value;
	   
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&status=any&order_type=regular',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.ordersCount =  data ;			
			pro.orders =  data.data ; 
           console.log(JSON.stringify(data.data)); 


            $('#loading').css('display', 'none');
             $('.innerData').css('display', 'block'); 
        })
		.error(function (data, status, header, config) {            
         console.log(JSON.stringify(data));        
	    }); 
			
 
 
 
        pro.getOrdersByStatus = function(value , type){
 
					 
					 	if(pro.orderId == undefined || pro.orderId == ''){
				pro.orderId = ' ';
			}
 
			
 
 
			pro.order_status = value;
			pro.order_type = type;
			   var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&order_type='+type+'&status='+pro.order_status+'&order_id='+pro.orderId,  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		 pro.ordersCount =  data ;
			pro.orders =  data.data ; 
          
         console.log(JSON.stringify(data.data)); 

            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
         console.log(JSON.stringify(data));       
	    }); 
			
		}
		
		 //========= FUNCTIONS FOR GET ORDER ====================================================================================== 
		  pro.orderSearch = function(query){ 
		        return $http.get(APP_URL+"/api/v1/orders?status=any&order_type=regular&auth_user_id="+auth_user_id+"&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    pro.selectedOrderChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	           pro.orderId =  values.order_id;  
			  
				 pro.filterOrder(pro.orderId);
            }
			
			 pro.searchOrderChange = function(text){  
				   pro.order_id = ''; 
				    //pro.filterOrder();
			}
  
  
  	// FUNCTION FOR FILTER BY ORDER ID  ============================================================== 
		
		pro.filterOrder =  function(orderId){
			 
			if(pro.order_status == undefined || pro.order_status == ''){
				pro.order_status = 'any';
			}
			
			
			console.log(APP_URL+'/api/v1/orders?include_count_blocks=true&order_id='+orderId+'&auth_user_id='+auth_user_id+'&status='+pro.order_status+'&order_type=regular');
			    $('#loading').css('display', 'block');
             $('.innerData').css('display', 'none'); 
			  var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&order_id='+orderId+'&auth_user_id='+auth_user_id+'&status='+pro.order_status+'&order_type=regular',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
			  pro.ordersCount =  data ;			
			pro.orders =  data.data ; 
           console.log("ffdg"+JSON.stringify(data.data)); 


            $('#loading').css('display', 'none');
             $('.innerData').css('display', 'block'); 
        })
		.error(function (data, status, header, config) {            
         console.log(JSON.stringify(data));        
	    }); 
		}
  
  // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
        $('.innerData').css('display', 'none'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&include_count_blocks=true&auth_user_id='+auth_user_id+'&status='+pro.order_status+'&order_type='+pro.order_type,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.orders = data.data;
            $('#loading').css('display', 'none'); 
             $('.innerData').css('display', 'block');  
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
  
  
  
  
       	
			
			
			
			
  
	
		
		
	});







//==========================================================CATERING ORDER CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('cateringOrdersController', function($http,$scope,$window,$log, toastr) {
		  
	 var pro = this; 
	    $('#loading').css('display', 'block');
       $('.innerData').css('display', 'none'); 
	    var auth_user_id = document.getElementById('auth_user_id').value;
	   
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&status=any&order_type=catering',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			  pro.ordersCount =  data ;			
			pro.orders =  data.data ; 
           console.log(JSON.stringify(data)); 


            $('#loading').css('display', 'none');
             $('.innerData').css('display', 'block'); 
        })
		.error(function (data, status, header, config) {            
     console.log(JSON.stringify(data));     
	    }); 
			
 
 
 
        pro.getOrdersByStatus = function(value , type){ 
 
	 	if(pro.orderId == undefined || pro.orderId == ''){
				pro.orderId = ' ';
			}
 
			
 
 			pro.order_status = value;
			
			  var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&auth_user_id='+auth_user_id+'&order_type='+type+'&status='+pro.order_status+'&order_id='+pro.orderId,  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			  pro.ordersCount =  data ;			
			pro.orders =  data.data ; 
           console.log(JSON.stringify(data)); 


            $('#loading').css('display', 'none');
        })
		.error(function (data, status, header, config) {            
       console.log(JSON.stringify(data));      
	    }); 
			
		}
  
  
  
   //========= FUNCTIONS FOR GET ORDER ====================================================================================== 
		  pro.orderSearch = function(query){ 
		        return $http.get(APP_URL+"/api/v1/orders?status=any&order_type=catering&auth_user_id="+auth_user_id+"&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    pro.selectedOrderChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	           pro.orderId =  values.order_id;  
			  
				 pro.filterOrder(pro.orderId);
            }
			
			 pro.searchOrderChange = function(text){  
				   pro.order_id = ''; 
				    //pro.filterOrder();
			}
  
  
  	// FUNCTION FOR FILTER BY ORDER ID  ============================================================== 
		
		pro.filterOrder =  function(orderId){
			 
			if(pro.order_status == undefined || pro.order_status == ''){
				pro.order_status = 'any';
			}
			
			
			console.log(APP_URL+'/api/v1/orders?include_count_blocks=true&order_id='+orderId+'&auth_user_id='+auth_user_id+'&status='+pro.order_status+'&order_type=catering');
			    $('#loading').css('display', 'block');
             $('.innerData').css('display', 'none'); 
			  var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&order_id='+orderId+'&auth_user_id='+auth_user_id+'&status='+pro.order_status+'&order_type=catering',  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
			  pro.ordersCount =  data ;			
			pro.orders =  data.data ;  
           console.log("ffdg"+JSON.stringify(data.data)); 


            $('#loading').css('display', 'none');
             $('.innerData').css('display', 'block'); 
        })
		.error(function (data, status, header, config) {            
         console.log(JSON.stringify(data));        
	    }); 
		}
		
		
  // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
        $('.innerData').css('display', 'none'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&include_count_blocks=true&auth_user_id='+auth_user_id+'&status='+pro.order_status,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
              pro.ordersCount =  data ;			
			pro.orders =  data.data ; 
            $('#loading').css('display', 'none'); 
             $('.innerData').css('display', 'block');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
		
	});


 


