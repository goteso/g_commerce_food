 
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userController', function ($http,$scope,$window,toastr) {
		
        var pro = this; 
        $('#loading').css('display', 'block'); 
         $('.innerData').css('display', 'none'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=3',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');  
             $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
		     $('#loading').css('display', 'none');
        });       
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteUser = function(userId) { 
			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
                	$('#loading').css('display', 'none');
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
					$('#loading').css('display', 'none');
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
			  $('.innerData').css('display', 'none'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&user_type=3',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');  
             $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
	
 	});
	
	
	
	
	
 	
 //====================================================== ADD CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userAddController', function ($http, $scope, $window,toastr) {
 
 var auth_user_id = document.getElementById('auth_user_id').value;
	     // get basic form to add product ============================================================== clean done
	     
 
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user?user_type=3&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addUser  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
			$('#loading').css('display', 'none');
		     document.getElementById("res").value = JSON.stringify(data);
        });  




		
		  
		
		
		
		   // FUNCTION FOR STORE USER  ============================================================== clean done
	     
		
           $scope.storeUser = function(){ 

           $('#loading').css('display', 'block');
		   
		    for(var i=0;i<$scope.addUser[0].fields.length;i++){ 
			     if($scope.addUser[0].fields[i].identifier == 'user_type'){
				    $scope.addUser[0].fields[i].value = '3';
			     } 
              }
			   var photo = $('#photo').val();
 
 for(var i=0;i<$scope.addUser[0].fields.length;i++){
 if($scope.addUser[0].fields[i].identifier == 'photo'){
	 $scope.addUser[0].fields[i].value = photo;
	  // document.getElementById('preview_image').src = APP_URL+'/images/items/'+item_photo;
	   //console.log("this is deepu path - "+APP_URL+'/images/items/' + $('#item_photo').val(data));
 } 
 }

 console.log(" Post Data = "+JSON.stringify($scope.addUser));
  
			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/user?auth_user_id='+auth_user_id+'',
            data:  $scope.addUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;
             
			    
$('#loading').css('display', 'none');
			        if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 
					$scope.customer_id = data.data.user_id
			  $window.location.href = 'restaurant/'+$scope.customer_id;
					}
            else { toastr.error(data.message, 'Error'); return false; }


			document.getElementById("res").value = JSON.stringify(data);
			
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
             $('#loading').css('display', 'none');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };			   
		  
	});



 
	
