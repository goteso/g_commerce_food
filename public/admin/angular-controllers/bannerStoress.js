
	
	
	//======================================================LOGO UPLOAD CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('bannerController', function ($http,$scope,$window,toastr,$log) {
		
		  var auth_user_id = document.getElementById('auth_user_id').value; 

		 
	
            $('#loading').css('display', 'block');
	   $('.innerData').css('display', 'none'); 
	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/banners?auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.banners = data;
            $('#loading').css('display', 'none');  
               $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
		
		
		 // FUNCTION FOR DELETE  COUPON  ============================================================== clean done
	$scope.deleteBanner = function(bannerId) {
		    
			$scope.banner_id1 = bannerId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/banners/'+$scope.banner_id1,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
			   

			       if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		 //========= FUNCTIONS FOR GET STORE====================================================================================== 
		 
		 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?auth_user_id="+auth_user_id+"&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values) { 
		        
		        console.log( APP_URL+"/api/v1/stores?auth_user_id="+auth_user_id );
                $log.info('Item changed to ' + JSON.stringify(values)); 
 			
	            $scope.linked_store_id =  values.store_id;  		
            }
			
			$scope.searchStoreChange = function(text){  
			    console.log( APP_URL+"/api/v1/stores?auth_user_id="+auth_user_id );
				   $scope.linked_store_id = ''; 
			};
			
			
			
 
    $scope.store_banner = function(id,value){
      
   
    			 $scope.linked_id = $('#linked_store_id').val();
    		 
    	
	 $scope.banner_image = $('#item_photo').val(); 
	 $scope.type = $('#banner_type').val();
	 alert(auth_user_id);
	 $scope.banner_data = {"photo":$scope.banner_image, "type":$scope.type,  "linked_id":$scope.linked_id};
	 console.log(JSON.stringify($scope.banner_data));
	  
	 var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/banners?auth_user_id='+auth_user_id,
            data:  $scope.banner_data,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;

			  if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); location.reload(); }
            else { toastr.error(data.message, 'Error'); }
   	 console.log(JSON.stringify(data));

 
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
			  	 console.log(JSON.stringify(data)); 
        });  
 
	 
 }
 
 
 
 
 
  $scope.editBanner = function(values){ 
 $scope.store_title = values.store_title; 	  
$scope.banner_id = values.id;  
		 $scope.item_photo1 = values.photo;
           $scope.banner_edit_type = values.type;
		   $scope.linked_edit_store_id = values.linked_id;  
		     $('#editBan').modal('show');    
		
 	};
	
	  //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeEditSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?auth_user_id="+auth_user_id+"&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedEditStoreChange = function(values) {
              $log.info('Item changed to ' + JSON.stringify(values)); 
                 $scope.store_id =   values.store_id; 
               if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.linked_edit_store_id = $scope.store_id;
			    }				 
        }; 			
              
			
			$scope.searchEditStoreChange = function(text){  
				   $scope.linked_edit_store_id = ''; 
			};
	
	
	
	
	
	
	 $scope.update_banner = function(id){
		 $scope.banner_id = $scope.banner_id;
	 $scope.banner_image1 = $('#item_photo1').val(); 
	 $scope.edit_type = $('#banner_edit_type').val();
	 $scope.linked_edit_store_id = $('#linked_edit_store_id').val();
	 $scope.banner_data1 = {"photo":$scope.banner_image1, "type":$scope.edit_type,  "linked_id":$scope.linked_edit_store_id};
	 console.log(JSON.stringify($scope.banner_data1));
	  
	 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/banners/'+$scope.banner_id,
            data:  $scope.banner_data1,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;



			  if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 	 location.reload(); }
            else { toastr.error(data.message, 'Error');
 
             }
   
		
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
	 
 }
 
 
	});
