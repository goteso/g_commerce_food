 
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userController', function ($http,$scope,$window,toastr) {
		
        var pro = this; 
        $('#loading').css('display', 'block');
         $('.innerData').css('display', 'none');  
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=2',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');
             $('.innerData').css('display', 'block');   
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteUser = function(userId) { 
			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		           
         //block a user with reason  
           		$scope.blockUser = function(id){ 
		 	$scope.user_id = id;
		 	$('#user_id').val(id);
      $('#replyModal').modal('show');
		}
		


		$scope.block_user = function()
		{
           $('#loading').css('display', 'block');
		   $scope.message = $('#message').val();
		   $scope.user_id =  $('#user_id').val();
		   $scope.request_data = { "message" : $scope.message};


		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/block_user/'+$scope.user_id,
            data: $scope.request_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
            $scope.data = data;
             toastr.success(data.message, 'Success');
             $('#loading').css('display', 'none');  
           //  location.reload();			 
		     console.log(JSON.stringify(data));
		     $('#replyModal').modal('hide');
             $('#message').val('');
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
             $('#loading').css('display', 'none');  
             console.log(JSON.stringify(data));
             $('#replyModal').modal('hide');
             $('#message').val('');
 
        }); 
		}  









	 //UNblock a user with reason  
      $scope.unblockUser = function(id){ 
	  $scope.user_id = id;
	  $('#user_id_unblock').val(id);
      $('#unblockModal').modal('show');
		}
		


		$scope.unblock_user = function()
		{
           $('#loading').css('display', 'block');
            $('.innerData').css('display', 'none'); 
		   $scope.message = $('#message_unblock').val();
		   $scope.user_id_ =  $('#user_id_unblock').val();
		   $scope.request_data = { "message" : $scope.message};


		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/unblock_user/'+$scope.user_id,
            data: $scope.request_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
            $scope.data = data;
             toastr.success(data.message, 'Success');
             $('#loading').css('display', 'none');  
       		  $('.innerData').css('display', 'block'); 
		     console.log(JSON.stringify(data));
		     $('#unblockModal').modal('hide');
             $('#message_unblock').val('');
                location.reload();	
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
             $('#loading').css('display', 'none');  
             console.log(JSON.stringify(data));
             $('#unblockModal').modal('hide');
             $('#message').val('');     location.reload();	
 
        }); 
		}  





	 //update_customer_type
           		$scope.update_customer_type = function(user_id , customer_type){ 
           			 
		 	var customer_type =$('#customer_type'+user_id).val();
       
		  
           $('#loading').css('display', 'block');
		   $('.innerData').css('display', 'none'); 
		   $scope.user_id =  user_id;
		    $scope.customer_types =  customer_type;
		   $scope.request_data = { "customer_type" : $scope.customer_types};


		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update_customer_type/'+$scope.user_id,
            data: $scope.request_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
            $scope.data = data;
             toastr.success(data.message, 'Success');
             $('#loading').css('display', 'none');  $('.innerData').css('display', 'block');  
       		 
		     console.log(JSON.stringify(data));
		 
               return false;	
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
             $('#loading').css('display', 'none');  
             console.log(JSON.stringify(data));
       
   return false;	
        }); 
		 //	$('#user_id_unblock').val(id);
     // $('#unblockModal').modal('show');
		}
		

 



		
		
		
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
			  $('.innerData').css('display', 'none'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&user_type=2',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.users = data;
            $('#loading').css('display', 'none');  
             $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
 	});
	
	
	
	
	
 //====================================================== ADD CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userAddController', function ($http, $scope, $window,toastr) {
 
 var auth_user_id = document.getElementById('auth_user_id').value;
  
 
 $scope.maxDate = new Date().toDateString();
 
	     // get basic form to add product ============================================================== clean done
	     
      $('#loading').css('display', 'block'); 
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user?user_type=2&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.addUser  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  




		
		  
		
		
		
		   // FUNCTION FOR STORE USER  ============================================================== clean done
	     
		
           $scope.storeUser = function(){ 

           	$('#loading').css('display', 'block');
		      for(var i=0;i<$scope.addUser[0].fields.length;i++){ 
			     if($scope.addUser[0].fields[i].identifier == 'user_type'){
				    $scope.addUser[0].fields[i].value = '2';
			     }
              }


				 var photo = $('#photo').val();
				 
				 for(var i=0;i<$scope.addUser[0].fields.length;i++){
				 if($scope.addUser[0].fields[i].identifier == 'photo'){
					 $scope.addUser[0].fields[i].value = photo; 
				 } 
				}
 
 

console.log("POST DATA = "+JSON.stringify($scope.addUser));

 

			var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/user?auth_user_id='+auth_user_id,
            data:  $scope.addUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;
            	console.log(JSON.stringify(data));



            $('#loading').css('display', 'none');
			   		         
			   		         if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			 $scope.customer_id = data.data.user_id

		
			  $window.location.href = 'customer/'+$scope.customer_id;
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
             $('#loading').css('display', 'none');
             	console.log(JSON.stringify(data));
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };			   
		  
	});



 
	

	
//=========================================================EDIT CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('userEditController', function ($http, $scope, $window,toastr) {
 
 var auth_user_id = document.getElementById('auth_user_id').value;
  $scope.maxDate = new Date().toDateString();
  $scope.customer_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
	 
	        $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.customer_id+"?user_type=2&auth_user_id="+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.editUser  = data;
			 $('#loading').css('display', 'none');  
			  for(var k=0;k<=$scope.editUser[0].fields.length;k++){  
	           if($scope.editUser[0].fields[k].identifier == 'photo'){ 		   
			var item_data_image =  $scope.editUser[0].fields[k].value ; 			 
			document.getElementById('item_photo').value = item_data_image; 
		} 
	 }
	 
            
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
		     $('#loading').css('display', 'none');
        });  


		 
	 //==========================function for update user======================================
		   $scope.updateUser = function(){ 
		   	 var item_photo = $('#item_photo').val(); 
         for(var j=0;j<$scope.editUser[0].fields.length;j++){
               if($scope.editUser[0].fields[j].identifier == 'photo'){
	               $scope.editUser[0].fields[j].value = item_photo; 
                }  
 
              }
  
			    
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.customer_id+'?auth_user_id='+auth_user_id,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  

        	 console.log(JSON.stringify(data));
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

 
            $('#loading').css('display', 'none');    
			 document.getElementById("res").value = JSON.stringify(data); 
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		      document.getElementById("res").value = JSON.stringify(data);
		      $('#loading').css('display', 'none');
        });  
 
		   };



 //==========================function for RESET PASSWORD======================================
           $scope.resetPass = function(){ 
		    
			$scope.pass = $('#password').val();
			$scope.confirm_pass = $('#confirm_password').val();
			if($scope.pass != $scope.confirm_pass){
				toastr.error('Password does not match', 'Error');
			}
			else{
				
				$scope.passwordValue = { "password":$scope.pass };
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update-password/'+$scope.customer_id,
            data:$scope.passwordValue,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	 console.log(JSON.stringify(data));
            $scope.data  = data; 
            $('#loading').css('display', 'none');
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
                else { toastr.error(data.message, 'Error'); return false; }
   
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
		      $('#loading').css('display', 'none');
        });  
		   
			}
           };
           
           
           
           
           
           

		
		
		
		

		   
	});



