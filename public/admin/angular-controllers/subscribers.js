app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/subscribe.html",
        controller : "subscribeController"
    })
    .when("/unsubs", {
        templateUrl : APP_URL+"/unsubscribe.html",
        controller : "unsubscribeController"
    })
    .otherwise({
            redirectTo: '/'
        });	;
});



app.controller('subscribeController', function ($http,$scope,$window,toastr,$location) {
    
        var auth_user_id = document.getElementById('auth_user_id').value;
        // var pro = this; 
        $('#loading').css('display', 'block'); 
         $('.innerData').css('display', 'none'); 

        
        var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/newsletters?status=1',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.subscribers = data.data;
              $scope.unsubscribers_count = data.unsubscriber_count;
            $scope.subscribers_count = data.subscribers_count;

            console.log("unsubscribers_count ="+$scope.unsubscribers_count);
            console.log("subscribers_count ="+$scope.subscribers_count);
            $('#loading').css('display', 'none');  
             $('.innerData').css('display', 'block'); 

            console.log("result"+JSON.stringify($scope.subscribers));
        })
        .error(function (data, status, headers, config) { 
             document.getElementById("res").value = JSON.stringify(data);
        });       



          $scope.unsubscribe = function(email)
         { 
 
           post_data = { "email" : email };
     

        if (confirm("Are you sure?")) { 

           var request = $http({
                       method: "POST",
                       url: APP_URL+'/api/v1/newsletters/unsubscribe',
                       data:  post_data,
                       headers: { 'Accept':'application/json' }
           });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            data = data;
           if(data.status_code == 1){
               toastr.success(data.message, 'Success');
              location.reload();    
           }
           else{
            toastr.error(data.message, 'Error'); 
           }           
             
        })
        .error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
            console.log("Error = "+JSON.stringify(data));
        });  

        }}


















        
        // FUNCTION FOR PAGINATION  ============================================================== clean done
        $scope.pagination = function(api_url){ 
             if(api_url == null || api_url== ''){
                 return false;
             }
             else{
             $('#loading').css('display', 'block'); 
              $('.innerData').css('display', 'none'); 

        
        var request = $http({
            method: "GET",
            url: api_url,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.subscribers = data.data;
            $('#loading').css('display', 'none'); 
             $('.innerData').css('display', 'block'); 
 
            document.getElementById("res").value = JSON.stringify(data);
        })
        .error(function (data, status, headers, config) { 
             document.getElementById("res").value = JSON.stringify(data);
        });       
        
        } 
        }



});



















app.controller('unsubscribeController', function ($http,$scope,$window,toastr) {
        
        var auth_user_id = document.getElementById('auth_user_id').value;
        // var pro = this; 
        $('#loading').css('display', 'block'); 
         $('.innerData').css('display', 'none'); 

        
        var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/newsletters?status=0',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.unsubscribers = data.data;
 
            $('#loading').css('display', 'none');
             $('.innerData').css('display', 'block'); 
  

                        $scope.unsubscribers_count = data.unsubscriber_count;
            $scope.subscribers_count = data.subscribers_count;


            console.log("result"+JSON.stringify($scope.unsubscribers));
            document.getElementById("res").value = JSON.stringify(data);
        })
        .error(function (data, status, headers, config) { 
             document.getElementById("res").value = JSON.stringify(data);
        });     








          $scope.subscribe = function(email)
         { 
         if (confirm("Are you sure?")) { 
           post_data = { "email" : email };
     
           var request = $http({
                       method: "POST",
                       url: APP_URL+'/api/v1/newsletters_subscribe_again',
                       data:  post_data,
                       headers: { 'Accept':'application/json' }
           });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            data = data;
           if(data.status_code == 1){
               toastr.success(data.message, 'Success');
              location.reload();    
           }
           else{
            toastr.error(data.message, 'Error'); 
           }           
             
        })
        .error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
            console.log("Error = "+JSON.stringify(data));
        });  

        }}

        
        // FUNCTION FOR PAGINATION  ============================================================== clean done
        $scope.pagination = function(api_url){ 
             if(api_url == null || api_url== ''){
                 return false;
             }
             else{
             $('#loading').css('display', 'block'); 
              $('.innerData').css('display', 'none'); 

        
        var request = $http({
            method: "GET",
            url: api_url,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.unsubscribers = data.data;
            $('#loading').css('display', 'none');  
             $('.innerData').css('display', 'block'); 

            document.getElementById("res").value = JSON.stringify(data);
        })
        .error(function (data, status, headers, config) { 
             document.getElementById("res").value = JSON.stringify(data);
        });       
        
        } 
        }
















});








