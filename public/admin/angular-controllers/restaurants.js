
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/restaurant-menu.html",
        controller : "menuController as ctrl"
    })
    .when("/categories", {
        templateUrl : APP_URL+"/restaurant-categories.html",
        controller : "categoryController as ctrl"
    })
	.when("/items", {
        templateUrl : APP_URL+"/restaurant-items.html",
        controller : "itemsController as ctrl"
    })
    .otherwise({
            redirectTo: '/'
        });	;
});
		
		
//====================================================== ADD ITEM CONTROLLER=================================================================================
//==============================================================================================================================================================
 /*app.controller('storeController', function ($http, $scope, $window,toastr, $log, $q ,$location) {
	 
	   var pro = this; 
	  //========= FUNCTIONS FOR GET STORE====================================================================================== 
		  pro.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		   pro.selectedStoreChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            pro.store_id =  values.store_id;  
                pro.filterStore();				
            }
			
			pro.searchStoreChange = function(text){  
				   pro.store_id = ''; 
				   pro.filterStore();
			};
			
			
			
 	
			 
			
			pro.filterStore = function(){
				//$location.path('/categories');
				 $scope.url = window.location.href;
			 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 1;
 
 var str = parts[index];
 
 $scope.setUrl = str ;   
	 //$location.path('/');
			 
				var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?store_id='+pro.store_id+'&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. *
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });    
		
			}
		
	 
 });*/
 
 app.controller('restaurantController', function ($http, $scope, $window,toastr, $log, $q ) {
 var auth_user_id = document.getElementById('auth_user_id').value;
	     // get basic form to add product ============================================================== clean done
	      var pro = this; 
	  //========= FUNCTIONS FOR GET STORE====================================================================================== 
	  
	  
		  pro.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		   pro.selectedStoreChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            pro.store_id =  values.store_id;  
               		 pro.categoriesFilter();
					 pro.menu();
					  pro.items();
            }
			
			pro.searchStoreChange = function(text){  
				   pro.store_id = ''; 
				  pro.categoriesData = '';
				  pro.stores = '';
				  pro.products = '';
			};
			
			
			
			
    
	
	
	pro.filterCat = function(){
	console.log(APP_URL+'/api/v1/category?store_id='+pro.store_id+'&auth_user_id='+auth_user_id+'&include_items=true&include_meta=true&request_type=input_field');
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?store_id='+pro.store_id+'&auth_user_id='+auth_user_id+'&include_items=true&include_meta=true&request_type=input_field',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data; 
            $('#loading').css('display', 'none');  
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
	
        });       
	}
	
	
	
	
	
	
		 
 
  pro.categoriesFilter = function(){ 
	  
	  if(pro.store_id == '' || pro.store_id == undefined){
		 return;
	  }
	  else{
        $('#loading').css('display', 'block'); 
		
		pro.filterCat();
	  }
       
	
	
  }
					
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&store_id='+pro.store_id+'&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
		// FUNCTION FOR DELETE CATEGORY  ============================================================== clean done
	pro.deleteCategoryId = function(categoryId) {
		    
			$scope.category_id = categoryId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/category/'+$scope.category_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();	
					}
				else{
					toastr.error(data.message,'Error!');
				}	
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		
		// FUNCTION FOR CHANGE STATUS OF CATEGORY  ============================================================== clean done
	pro.switchStatus = function(value, categoryId) {
		
		pro.category_id = categoryId; 
		pro.status_value = value;
		
		pro.category_status_value = { "status": pro.status_value}; 
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/category/update-status/'+pro.category_id,
                    data: pro.category_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					 console.log(JSON.stringify(data)); 
					}
					else{toastr.error(data.message,'Error!'); console.log(JSON.stringify(data)); }
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	
	
		//====================FUNCTION FOR GET PARENT CATEGORY API==========================================================================
		pro.categorySearch = function(query) {  
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&include_items=true&include_meta=true&search=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };
  
        pro.selectedCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
              $scope.category_id =   data.category_id;  
        } 
		
		
		$scope.searchCategoryChange = function(text){  
				   $scope.category_id = ''; 
			};
		
		
		
 
			
			
		
		 $scope.uploadFile = function () {
			 
            var file = $scope.myFile;
            var uploadUrl = APP_URL + "/api/v1/users/upload-profile-image", //Url of webservice/api/server
                promise = fileUploadService.uploadFileToUrl(file, uploadUrl);
 
            promise.then(function (response) {
				 
                $scope.serverResponse = response;
            }, function () {
				 
                $scope.serverResponse = 'An error has occurred';
            })
        };
		
		
		 pro.editCategory = function(){ 
		 
		 }
		
		//========= FUNCTIONS FOR STORE A CATEGORY=============================================================================================== 
		 pro.storeCategory = function(){ 
           pro.category_photo = $('#file_name').val();
		   pro.category_title = $('#category_title').val();
		   pro.parent_id = $('#category_id').val();
           pro.store_id_value = $('#store_id').val();			
		   pro.category_data = {"category_title" : pro.category_title, "parent_id" : pro.parent_id, "store_id" : pro.store_id_value, "category_photo" : pro.category_photo };

		   console.log("Store Category Post ="+JSON.stringify(pro.category_data));


		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/category',
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
             if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			  location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A CATEGORY FORM=============================================================================================== 
		 pro.editCategory = function(values){  
		 console.log(JSON.stringify(values));
		 pro.category_photo = values.category_photo;
           pro.category_id = values.category_id;
		   pro.category_edit_title = values.category_title;
           pro.parent_category_title = values.parent_category_title;		   
		    pro.parent_id = values.parent_id;
			pro.store_edit_id = values.store_id;
			pro.store_edit_title = values.store_title;
		     $('#editCat').modal('show');   
		
		 
		 
		 
		  pro.editCategorySearch = function(query) { 
		   
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&parents_only=true&exclude_id="+pro.category_id +"&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
			console.log(response.data.data.data);
                return response.data.data.data;
            })
        };
  
        pro.selectedEditCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
			//alert(data.category_id);
                 pro.parent_category_id =   data.category_id; 
               if(pro.parent_category_id != undefined || pro.parent_category_id != null){
				   pro.parent_id = pro.parent_category_id;
			    } 
		  };
		  
		  pro.searchEditCategoryChange = function(text){  
				   pro.parent_id = ''; 
			};
		  
		  
		  
		 }
 
		 
		 //========= FUNCTIONS FOR UPDATE A CATEGORY=============================================================================================== 
		 pro.updateCategory = function(id){ 
           pro.category_id = id; 
		   pro.category_photo = $('#file_name1').val();
		   pro.store_edit_id = $('#store_id').val();	
		   pro.category_edit_title = $('#category_edit_title').val();
		   pro.parent_id = $('#category_edit_id').val();
		  
		   pro.category_data = {"category_title" : pro.category_edit_title, "parent_id" : pro.parent_id,"store_id" : pro.store_edit_id , "category_photo" : pro.category_photo};
		    
		    
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/category/'+pro.category_id,
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
            if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


		 
              location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		pro.menu = function(){
			
			if(pro.store_id == '' && pro.store_id == undefined){
				return;
			}
			else{
				
				
			 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores/'+pro.store_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            pro.stores  = data.data;
			
			 
		 
            $('#loading').css('display', 'none');  
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
			
			
			
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?auth_user_id='+auth_user_id+'&store_id='+pro.store_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           pro.categoriesData = data.data;   
		   console.log(JSON.stringify(data.data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		}
			 
		}
		
		
		
		
		
		
		
		
		
		
		
		
		pro.items = function(){
		
		//alert(pro.store_id);
		if(pro.store_id == '' && pro.store_id == undefined){
				return;
			}
			
			else{
			var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?store_id='+pro.store_id+'&auth_user_id='+auth_user_id+'&status=active',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
			
			
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
		}
		}
	// FUNCTION FOR CHANGE STATUS OF ITEM  ============================================================== clean done
	pro.switchStatus = function(value, itemId) {
		
		pro.item_id = itemId;
		pro.status_value = value;
		pro.item_status_value = { "item_active_status":pro.status_value};
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/items/item-active-status/update/'+pro.item_id,
                    data: pro.item_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					}
					else{toastr.error(data.message,'Error!');}
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteItem = function(itemId) {
		    
			$scope.item_id = itemId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/items/'+$scope.item_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&store_id='+pro.store_id+'&auth_user_id='+auth_user_id+'&status=active',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
  
  
 });
	
	app.controller('menuController', function ($http, $scope, $window,toastr, $log, $q ) {
 
	     // get basic form to add product ============================================================== clean done
	     	var auth_user_id = document.getElementById('auth_user_id').value;
   var pro = this; 
   
   
			$scope.store_value_id = localStorage.getItem('storeId');
			
			//alert($scope.store_value_id);
			
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores/'+$scope.store_value_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            pro.stores  = data.data.data;
			
			 
		 
            $('#loading').css('display', 'none');  
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
			
			
			
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?auth_user_id='+auth_user_id+'&request_type=input_field',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           $scope.categories = data.data.data;   
		   console.log(JSON.stringify(data.data.data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
		 $scope.searchData = function (query) { 
		    var search_term = query.toUpperCase(); 
		    $scope.people = [];
		    angular.forEach($scope.categories, function (item) {
			    if (item.label.toUpperCase().indexOf(search_term) >= 0)
				    $scope.people.push(item); 
		        });
				console.log(JSON.stringify($scope.people));
           return $scope.people;
		   
	    };
		
	 
		
 
		
		 
	
		
	 
			
			 
// FUNCTION FOR ADD ITEM  ==============================================================  
	     
		
           $scope.storeItem = function(){ 
	 

	  var item_photo = $('#item_photo').val();
 
 for(var i=0;i<$scope.addItem[0].fields.length;i++){
 if($scope.addItem[0].fields[i].identifier == 'item_photo'){
	 $scope.addItem[0].fields[i].value = item_photo;
 } 
 
 if($scope.addItem[0].fields[i].type == 'api'){
 if($scope.addItem[0].fields[i].identifier == 'vendor_id'){
	 $scope.addItem[0].fields[i].value = $scope.vendor_id;
 } 
 if($scope.addItem[0].fields[i].identifier == 'store_id'){
	 $scope.addItem[0].fields[i].value = $scope.store_id;
 } 
 }
 
  
}
           	console.log(JSON.stringify($scope.addItem));
 

			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/form/item?auth_user_id='+auth_user_id+'&meta_type='+$scope.meta_type,
            data:  $scope.addItem,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;
			
			if(data.status_code == 1){
			toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');   
            	console.log(JSON.stringify(data)); 
			 
			$scope.item_id = data.data.id
			  $window.location.href = 'item/'+$scope.item_id+'?'+$scope.meta_type;
			}
			else{toastr.error(data.message, 'Error');}
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };			   
		  
	});





	
	
	
	
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('categoryController', function ($http,$scope,$window,toastr,$location,$log) {
		var auth_user_id = document.getElementById('auth_user_id').value;
		    var pro = this; 
		  
			 pro.store_id = $('#store_id').val();
 
  
      
        $('#loading').css('display', 'block'); 
		//alert(APP_URL+'/api/v1/category?store_id='+pro.store_id+'&auth_user_id='+auth_user_id);
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?store_id='+pro.store_id+'&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
       
	
	
	
					
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&store_id='+pro.store_id+'&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
		// FUNCTION FOR DELETE CATEGORY  ============================================================== clean done
	pro.deleteCategoryId = function(categoryId) {
		    
			$scope.category_id = categoryId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/category/'+$scope.category_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();	
					}
				else{
					toastr.error(data.message,'Error!');
				}	
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		
		// FUNCTION FOR CHANGE STATUS OF CATEGORY  ============================================================== clean done
	pro.switchStatus = function(value, categoryId) {
		
		pro.category_id = categoryId; 
		pro.status_value = value;
		
		pro.category_status_value = { "status": pro.status_value}; 
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/category/update-status/'+pro.category_id,
                    data: pro.category_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					 console.log(JSON.stringify(data)); 
					}
					else{toastr.error(data.message,'Error!'); console.log(JSON.stringify(data)); }
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	
	
		//====================FUNCTION FOR GET PARENT CATEGORY API==========================================================================
		pro.categorySearch = function(query) {  
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&parents_only=true&search=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };
  
        pro.selectedCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
              $scope.category_id =   data.category_id;  
        } 
		
		
		$scope.searchCategoryChange = function(text){  
				   $scope.category_id = ''; 
			};
		
		
		
 
			
			
		
		 $scope.uploadFile = function () {
			 //alert(JSON.stringify($scope.myFile));
            var file = $scope.myFile;
            var uploadUrl = APP_URL + "/api/v1/users/upload-profile-image", //Url of webservice/api/server
                promise = fileUploadService.uploadFileToUrl(file, uploadUrl);
 
            promise.then(function (response) {
				 
                $scope.serverResponse = response;
            }, function () {
				 
                $scope.serverResponse = 'An error has occurred';
            })
        };
		
		
		 pro.editCategory = function(){ 
		 
		 }
		
		//========= FUNCTIONS FOR STORE A CATEGORY=============================================================================================== 
		 pro.storeCategory = function(){ 
           pro.category_photo = $('#file_name').val();
		   pro.category_title = $('#category_title').val();
		   pro.parent_id = $('#category_id').val();
           pro.store_id_value = pro.store_id;			
		   pro.category_data = {"category_title" : pro.category_title, "parent_id" : pro.parent_id, "store_id" : pro.store_id_value, "category_photo" : pro.category_photo };

		   console.log("Store Category Post ="+JSON.stringify(pro.category_data));


		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/category',
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
             if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			  location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A CATEGORY FORM=============================================================================================== 
		 pro.editCategory = function(values){  
		 console.log(JSON.stringify(values));
		 pro.category_photo = values.category_photo;
           pro.category_id = values.category_id;
		   pro.category_edit_title = values.category_title;
           pro.parent_category_title = values.parent_category_title;		   
		    pro.parent_id = values.parent_id;
			pro.store_edit_id = values.store_id;
			pro.store_edit_title = values.store_title;
		     $('#editCat').modal('show');   
		
		 
		 
		 
		  pro.editCategorySearch = function(query) { 
		   
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&parents_only=true&exclude_id="+pro.category_id +"&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
			console.log(response.data.data.data);
                return response.data.data.data;
            })
        };
  
        pro.selectedEditCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
			//alert(data.category_id);
                 pro.parent_category_id =   data.category_id; 
               if(pro.parent_category_id != undefined || pro.parent_category_id != null){
				   pro.parent_id = pro.parent_category_id;
			    } 
		  };
		  
		  pro.searchEditCategoryChange = function(text){  
				   pro.parent_id = ''; 
			};
		  
		  
		  
		 }
 
		 
		 //========= FUNCTIONS FOR UPDATE A CATEGORY=============================================================================================== 
		 pro.updateCategory = function(id){ 
           pro.category_id = id; 
		   pro.category_photo = $('#file_name1').val();
		   pro.store_edit_id = pro.store_id;	
		   pro.category_edit_title = $('#category_edit_title').val();
		   pro.parent_id = $('#category_edit_id').val();
		  
		   pro.category_data = {"category_title" : pro.category_edit_title, "parent_id" : pro.parent_id,"store_id" : pro.store_edit_id , "category_photo" : pro.category_photo};
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/category/'+pro.category_id,
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
            if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


		 
              location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		
	
 	});
	
	
	
	//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('itemsController', function ($http,$scope,$window,toastr,$location) {
		var auth_user_id = document.getElementById('auth_user_id').value; 
		 var pro = this; 
		 pro.store_id = $('#store_id').val();
   
          
			 
       
        $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?store_id='+pro.store_id+'&auth_user_id='+auth_user_id+'&status=active',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
			
			
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	// FUNCTION FOR CHANGE STATUS OF ITEM  ============================================================== clean done
	pro.switchStatus = function(value, itemId) {
		
		pro.item_id = itemId;
		pro.status_value = value;
		pro.item_status_value = { "item_active_status":pro.status_value};
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/items/item-active-status/update/'+pro.item_id,
                    data: pro.item_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					}
					else{toastr.error(data.message,'Error!');}
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteItem = function(itemId) {
		    
			$scope.item_id = itemId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/items/'+$scope.item_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_user_id='+auth_user_id+'&status=active',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
	
 	});
	
	
	
	
	
	
