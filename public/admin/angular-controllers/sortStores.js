
//======================================================VIEW STORES LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	app.controller('sortStoreController', function ($http,$scope,$window,toastr,$log) { 

        //$('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores-sorted',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.stores = data; 
			$scope.storesData = data.data;
           $scope.storesDataList = $scope.storesData ;
            console.log("Result = "+JSON.stringify($scope.storesDataList));
			 /* $scope.storesData = data.data;
			 $scope.storesDataList = {
		stores : $scope.storesData
	};
	
	$scope.storesData1 = { 
        d : {"d1": $scope.storesDataList.stores}
    };*/
	
	
  // console.log(JSON.stringify($scope.storesData1));
	 
        })
		.error(function (data, status, headers, config) { 
			console.log(JSON.stringify(data));
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	 
	
	

		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&?exclude_busy_store=false',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.stores = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
	
	
		//========= FUNCTIONS FOR GET STORE====================================================================================== 
		  $scope.storeSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/stores?search_text="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedStoreChange = function(values, data) {
				$scope.storeData = values;
                 $log.info('Item changed to ' + JSON.stringify($scope.storeData)); 
                $scope.store_id =  values.store_id  ;  
            
            }
			
	      $scope.searchStoreChange = function(text,data){  
				   $scope.store = ''; 
			}
			
			
	
	
	
	
			//========= FUNCTIONS FOR GET STORE====================================================================================== 
			$scope.addSelectedStore = function() {  
		
		      var alreadyInArray = '';
                var sortIndex = 0;

                if ($scope.storesDataList.length == 0) { 
                    $scope.storesDataList.push($scope.storeData); 
                      console.log("rfrefryyyy"+JSON.stringify($scope.storesDataList)); 
					 $scope.sortArray();
                    return;
				}

                for (var i = 0; i < $scope.storesDataList.length; i++) {
                    if ($scope.storesDataList[i].store_id == $scope.storeData.store_id) {
                        toastr.error('Store already Selected','Error!');
						  alreadyInArray = 'YES';
                        sortArray = i;
                    }
                }

				
                if (alreadyInArray == 'YES') {
                    toastr.error('Store already Selected','Error!');
                    return;
                } 
				
				else { 
				 $scope.storesData1 = { 
						selected: null,
						d : {"d1": []}
					};	
                    $scope.storesDataList.push($scope.storeData); 
				
                    console.log("rfrefregfer"+JSON.stringify($scope.storesDataList)); 
				  $scope.sortArray(); 
                } 
	}
	
	//========= FUNCTIONS FOR GET STORE=====================================================================================
	
 	
		
	
		
 	// FUNCTION FOR SORT OR ADD TABLE ARRAY  ============================================================== clean done
  $scope.sortArray = function(){
	for(var j = 0; j<= $scope.storeData.length; j++) {
        $scope.storesDataList.push($scope.storeData[j]);  
		console.log("iiii"+JSON.stringify($scope.storesDataList));

    }
 
    // Model to JSON for demo purpose
    $scope.$watch('storesDataList', function(model) {  
        $scope.modelAsJson = angular.toJson(model, true);
		console.log("iiii"+JSON.stringify(model)); 
		 
    }, true);
  }

 
  // FUNCTION FOR DELETE STORE  ============================================================== clean done
	$scope.deleteStore = function(storeId,index) {
		    
			$scope.store_id = storeId; 
			if (confirm("Are you sure?")) {
		   $scope.storesData1.d.d1.splice(index, 1)
			
		    }
		
        };
		
 
 
 
   // FUNCTION FOR UPDATE SORT STORE LIST  ============================================================== clean done
 
 $scope.addStore = function(){
			   
		console.log("API CALLED = "+JSON.stringify($scope.storesDataList)); 
		    var request = $http({
                 method: "PUT",
                 url: APP_URL+'/api/v1/stores-sorted',
                 data:  $scope.storesDataList,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.data = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success');  
			  }
            else { toastr.error(data.message, 'Error'); return false; }


				
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) {
                 toastr.error(data.message, 'Error');				
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
		}
		
		
		
		$('table tbody').sortable({
    helper: fixWidthHelper
}).disableSelection();
function fixWidthHelper(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
}
	
	/*
	 sortable_tables.sorting_field_table(); 
	 var sortable_tables =
{
    sorting_field_table: function()
    {
        $('.fixed-width-table tbody').sortable({
            helper: sortable_tables.fixWidthHelper
        }).disableSelection();
    },

    fixWidthHelper: function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    }
}*/
 

 $scope.sortableOptions = {
   
   drop: function(e, ui) { 
    console.log(arguments[0]);
    console.log("111111");
      console.log(e);
   
   },
   stop: function (e, ui) {
	     console.log($scope.storesDataList);
              for (var index in $scope.storesDataList) {
               
                  $scope.storesDataList[index].sort_index = Number(index) + 1;
              }
              console.log("ddgdg"+ JSON.stringify($scope.storesDataList));
               $scope.addStore();
              },
   axis: 'y' 
   
    
}
 	});
	
	
	
	