
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('policyController', function($http, $scope , $window, $log, $q, $timeout, toastr) {
 
          
		    var auth_user_type = document.getElementById('auth_user_type').value;
  $scope.auth_user_type = auth_user_type;
  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/setting?keys=privacy_policy',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.termsData  = data.data[0];
			$scope.terms_id = $scope.termsData.id;
			$scope.termsValue = $scope.termsData.key_value;
            $('#loading').css('display', 'none');  
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
 
 $scope.updateTerms = function(){
	 
	
	 $scope.key_value = $('#policy').val(); 
	 $scope.key_id = $scope.terms_id; 
	 $scope.key_data = {"key_value":$scope.key_value};
	 
	 
	 console.log(JSON.stringify($scope.key_data));
	  
	 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/setting/'+$scope.key_id,
            data:  $scope.key_data,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;

			     if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); }

            $('#loading').css('display', 'none');  
			//location.reload();
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {
			 toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
	 
 }

		 	 
		 
		 
	 
		
   });