var app = angular.module("mainApp", [])



//========================================================== ORDER DETAIL CONTROLLER================================================================================================
//==================================================================================================================================================================================
    app.controller('invoiceController', function($http,$scope,$window,$log) {
    	
 
		$scope.order_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
		   
      //========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
			
	    $http.get(APP_URL+"/api/v1/orders/"+$scope.order_id)
    .then(function(response) {
        $scope.invoiceData =  response.data ; 
        setTimeout(function () {
             $scope.print();
               }, 1000);
	
		 document.getElementById('res').value = JSON.stringify(response.data); 
    });
	
	
	   
	 $scope.print = function(){
	 	$window.print();
	 }

      
   
});





 