 //====================================================== REVIEW CONTROLLER=================================================================================
 //==============================================================================================================================================================
 app.controller('reviewsController', function($http, $scope, $window, toastr, $log) {
 
     var auth_user_id = document.getElementById('auth_user_id').value;

     $('#loading').css('display', 'block');
	 $('.innerData').css('display', 'none');

//=============== GET A ORDER REVIEW  LIST=========================================================================

     var request = $http({
         method: "GET",
         url: APP_URL +'/api/v1/order-review?status=any&auth_user_id='+auth_user_id,
         data: '',
         headers: {
             'Accept': 'application/json'
         }
     });

     /* Check whether the HTTP Request is successful or not. */
     request.success(function(data) {
             $scope.reviews = data;
             $('#loading').css('display', 'none');
			 $('.innerData').css('display', 'block');
             document.getElementById("res").value = JSON.stringify(data);
         })
         .error(function(data, status, headers, config) {
             document.getElementById("res").value = JSON.stringify(data);
         });




     //=============== FUNCTION FOR DELETE A REVIEW  =========================================================================

     $scope.deleteReview = function(id) {
         $scope.review_id = id;
         if (confirm("Are you sure?")) {
             var request = $http({
                 method: "DELETE",
                 url: APP_URL+'/api/v1/order-review/'+$scope.review_id,
                 data: '',
                 headers: {
                     'Accept': 'application/json'
                 }
             });

             /* Check whether the HTTP Request is successful or not. */
             request.success(function(data) {
                     $scope.data = data;
                     if (data.status_code == 1) {
                         toastr.success(data.message, 'Success!');
                         location.reload();
                     } else {
                         toastr.error(data.message, 'Error!');
                     }
                 })
                 .error(function(data, status, headers, config) {
                     console.log(JSON.stringify(data));
                     toastr.error('Error Occurs', 'Error!');
                 });
         }

     };


     // FUNCTION FOR PAGINATION  ============================================================== clean done
     $scope.pagination = function(api_url) {
         if (api_url == null || api_url == '') {
             return false;
         } else {
             $('#loading').css('display', 'block');
			 $('.innerData').css('display', 'none');

             var request = $http({
                 method: "GET",
                 url: api_url,
                 data: '',
                 headers: {
                     'Accept': 'application/json'
                 }
             });

             /* Check whether the HTTP Request is successful or not. */
             request.success(function(data) {
                     $scope.reviews = data;
                     $('#loading').css('display', 'none');
					 $('.innerData').css('display', 'block');
                     document.getElementById("res").value = JSON.stringify(data);
                 })
                 .error(function(data, status, headers, config) {
                     document.getElementById("res").value = JSON.stringify(data);
                 });

         }
     }




     //========= FUNCTIONS FOR GET STORE LIST====================================================================================== 
     $scope.storeSearch = function(query) {
         return $http.get(APP_URL + "/api/v1/stores?auth_user_id=" + auth_user_id + "&search=" + query, {
                 params: {
                     q: query
                 }
             })
             .then(function(response) {
                 console.log(JSON.stringify(response.data.data.data));
                 return response.data.data.data;
             })
     };

     $scope.selectedStoreChange = function(values, data) {
         $log.info('Item changed to ' + JSON.stringify(values));
         $scope.store_id = values.store_id;
         $scope.filterStore();
         /*if($scope.store_id != undefined || $scope.store_id != null){
				   $scope.store = $scope.store_id;
			    }  
            */
     }
     $scope.searchStoreChange = function(text, data) {
         $scope.store_id = '';
         $scope.filterStore();
     }




 //========= FUNCTIONS FOR FILTER A STORE LIST======================================================================================

     $scope.filterStore = function() {
		 $('#loading').css('display', 'block');
$('.innerData').css('display', 'none');
         var request = $http({
             method: "GET",
             url: APP_URL + '/api/v1/order-review?store_id=' + $scope.store_id,
             data: '',
             headers: {
                 'Accept': 'application/json'
             }
         });

         /* Check whether the HTTP Request is successful or not. */
         request.success(function(data) {
                 $scope.reviews = data;
                 $('#loading').css('display', 'none');
				 $('.innerData').css('display', 'block');
                 document.getElementById("res").value = JSON.stringify(data);
             })
             .error(function(data, status, headers, config) {
                 document.getElementById("res").value = JSON.stringify(data);
             });
     }

	 
	 
	 
	 $scope.switchStatus = function(status, reviewId){
		 
		 $scope.reviewStatus = { "status":status};
		 var request = $http({
             method: "PUT",
             url: APP_URL + '/api/v1/order-review/'+reviewId+'/update_status',
             data: $scope.reviewStatus,
             headers: {
                 'Accept': 'application/json'
             }
         });

         /* Check whether the HTTP Request is successful or not. */
         request.success(function(data) {
                 $scope.data = data;
                 $('#loading').css('display', 'none');
				 $('.innerData').css('display', 'block');
                  console.log(JSON.stringify(data));
             })
             .error(function(data, status, headers, config) {
                console.log(JSON.stringify(data));
             });
		 
	 }
 });