app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/store.html",
        controller : "storeController"
    })
    .when("/manager", {
        templateUrl : APP_URL+"/manager.html",
        controller : "managerController"
    }) ;
});
	
	
	
 


//======================================================VIEW STORES LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('storeController', function ($http,$scope,$window,toastr) { 

        $('#loading').css('display', 'block'); 
        $('.innerData').css('display', 'none'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?exclude_busy_store=false&catering_status=any',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.stores = data;
			 $scope.storesData = data.data;
			 
             $('#loading').css('display', 'none');  
		  $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
			
			 $scope.storesData1 = { 
	    selected: null,
        data: {"data": []}
    };
 

	for (var i = 0; i <= $scope.storesData.data.length; ++i) {
        $scope.storesData1.data.data.push($scope.storesData.data[i]);   
		
    }

	 
    // Model to JSON for demo purpose
    $scope.$watch('storesData1', function(model) {  
        $scope.modelAsJson = angular.toJson(model, true);
		console.log("iiii"+JSON.stringify(model.data.data)); 
		 
    }, true);

	
	 
        })
		.error(function (data, status, headers, config) {
		$('#loading').css('display', 'none'); 
			console.log(JSON.stringify(data));
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	 
	
	
	
	// FUNCTION FOR DELETE STORE  ============================================================== clean done
	$scope.deleteStore = function(storeId) {
		    
			$scope.store_id = storeId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/stores/'+$scope.store_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) {
                $('#loading').css('display', 'none'); 
				    $scope.data = data;
			     

			         if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
					$('#loading').css('display', 'none');
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&?exclude_busy_store=false&catering_status=any',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.stores = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		 
 	$scope.switch = function(value,id){
 	    
	    $scope.store_id = id;
	    $scope.catering_value = value;
	     $scope.catering_data = {"catering_status":$scope.catering_value};
	     var request = $http({
                 method: "PUT",
                 url: APP_URL+'/api/v1/stores/catering-status/'+$scope.store_id,
                 data:  $scope.catering_data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.stores_catering = data;   
				 if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) {
                 toastr.error(data.message, 'Error');				
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
	    
	}
	
	
	
	
 	});
	
	
	
	
	
	
//======================================================ADD STORE CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('addStoreController', function ($http,$scope,$window,toastr,$log,$q) {
		
		
		  $scope.locationpickerOptions = {
                        location: {
                            latitude: 25.286106,
                            longitude: 51.534817
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,
                        radius: 0,
						//enableReverseGeocode: false,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true 
					   
					   
                    };
		 
		 
					  // $('#us3-address').val() = $('#address').val();	
		 
		 
		 //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.vendor_id =  values.user_id;  
            }
			
			 $scope.searchVendorChange = function(text){  
				   $scope.vendor_id = ''; 
			}
			
			//========= FUNCTIONS FOR GET MANAGER FOR STORE====================================================================================== 
		  $scope.managerSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=4&search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedManagerChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.manager_id =  values.user_id;  
            }
			
			 $scope.searchManagerChange = function(text){  
				   $scope.manager_id = ''; 
			}
		 
         //$('#loading').css('display', 'block');  
		$scope.addStore = function(){
			  
           $scope.featured = $('#featured').val();	
		   if(!$scope.featured){
			   toastr.error('Choose Featured', 'Error'); return false; 
		   }
			
			$scope.store_title = $('#title').val(); 
			$scope.commission = $('#commission').val();
		    $scope.catering_commission = $('#catering_commission').val();
			$scope.manager_id = $('#manager_id').val(); 
			$scope.store_tax = $('#store_tax').val(); 
			$scope.minimum_delivery = $('#minimum_delivery').val(); 
			$scope.vendor_id = $('#vendor_id').val(); 		
			$scope.latitude = $('#us3-lat').val(); 
			$scope.longitude = $('#us3-lon').val();  
             $scope.address = $('#address').val();			
			 $scope.store_photo = $('#file_name').val();
			$scope.storeData = { "store_title" : $scope.store_title, "minimum_delivery" : $scope.minimum_delivery, "store_tax" : $scope.store_tax, "commission" : $scope.commission, "catering_commission" : $scope.catering_commission, "manager_id" : $scope.manager_id, "vendor_id" : $scope.vendor_id, "longitude" : $scope.longitude, "latitude" : $scope.latitude,  "address" : $scope.address , "featured" : $scope.featured, "store_photo" : $scope.store_photo};
		 
		 console.log(JSON.stringify( $scope.storeData)); 
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores',
                 data:  $scope.storeData,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
            	$('#loading').css('display', 'none');
                $scope.stores = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 
			    $scope.id  = data.data.id;
			   $window.location.href = 'store/'+$scope.id;
			  }
            else { toastr.error(data.message, 'Error'); return false; }


				
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) {
		    	$('#loading').css('display', 'none');
                 toastr.error(data.message, 'Error');				
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
		}
 	});
		
 
  
 
 	
//======================================================EDIT STORE CONTROLLER====================================================================================================
//==============================================================================================================================================================================
 
	
	app.controller('editStoreController', function ($http,$scope,$window,toastr,$location,$log, $q,$timeout) {

		 	var auth_user_id = document.getElementById('auth_user_id').value;
		 
         $('#loading').css('display', 'block'); 

		 
		  $('.innerData').css('display', 'none'); 
		
			
			
			
			
		 $scope.store_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 
		 
		$http.get(APP_URL+'/api/v1/stores/'+$scope.store_id)
            .success(function(data, status, headers, config) { 
		    $scope.store = data.data[0];  
		    $scope.lat = $scope.store.latitude;
             $scope.lon = $scope.store.longitude;			
             $scope.vendor = $scope.store.vendor_id; 
			  $scope.manager = $scope.store.manager_id; 	 
			 
			  $('#loading').css('display', 'none'); 
				 $('.innerData').css('display', 'block'); 


			   $scope.locationpickerOptions.center.latitude = $scope.lat;
                $scope.locationpickerOptions.center.longitude = $scope.lon;

				
				$('#us3-edit-lat').val($scope.locationpickerOptions.center.latitude);
				$('#us3-edit-lon').val($scope.locationpickerOptions.center.longitude);
				
				
				$scope.locationpickerOptions.inputBinding.latitudeInput = $('#us3-edit-lat');
				$scope.locationpickerOptions.inputBinding.longitudeInput = $('#us3-edit-lon');
				 
			  console.log(JSON.stringify(data.data[0]));
			  
  
	    }) 
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
			 //========= FUNCTIONS FOR LOCATION FOR STORE====================================================================================== 
		 $scope.locationpickerOptions = { 
                        center: {
                          latitude: null,
                            longitude:null
                        },
                        inputBinding: { 
                            locationNameInput: $('#us3-edit-address') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true,
					    
                    };
		//========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 
         var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/form/meta',
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.storeMeta = data;    
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify($scope.storeMeta);
            })
		    .error(function (data, status, headers, config) { 
		        document.getElementById("res").value = JSON.stringify(data);
            });   

 
		
	 
					
					 //========= FUNCTIONS FOR GET VENDOR FOR STORE A AREA====================================================================================== 
		  $scope.vendorSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=3&search="+query, {params: {q: query}})
                .then(function(response){  
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedVendorChange = function(values) { 
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.vendor_id =  values.user_id ; 				
			    if($scope.vendor_id != undefined || $scope.vendor_id != null){
				   $scope.vendor = $scope.vendor_id;
			    }
            }
			
			 $scope.searchVendorChange = function(text){ 
				   $scope.vendor = ''; 
			}
			//========= FUNCTIONS FOR GET MANAGER FOR STORE====================================================================================== 
		  $scope.managerSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=4&search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedManagerChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.manager_id =  values.user_id; 
                 if($scope.manager_id != undefined || $scope.manager_id != null){
				   $scope.manager = $scope.manager_id;
			    }				
            }
			 $scope.searchManagerChange = function(text){ 
				   $scope.manager = ''; 
			}
			
			
			
			
			
				//========= FUNCTIONS FOR GET CUISINE FOR STORE====================================================================================== 
		  /*$scope.cuisineSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/store-filter-options?type=cuisines&search="+query, {params: {q: query}})
                .then(function(response){  
				console.log(JSON.stringify(response.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedCuisineChange = function(values,data) {
                $log.info('Item changed to ' + JSON.stringify(values));
 data.value=values.value;
	            $scope.cuisine_value =  data.value; 
                 				
            }
			 $scope.searchCuisinesChange = function(text){ 
				   $scope.cuisine_value = ''; 
			}
			
			
			*/
			
			
		 
			  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/store-filter-options?type=cuisines&exclude_value=true',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           $scope.cuisinesList = data.data;   
		   console.log(JSON.stringify(data.data));
        })
		.error(function (data, status, headers, config) { 
			$('#loading').css('display', 'none');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 
		
			 $scope.searchCuisineData = function (query) { 
		    var search_term = query.toUpperCase(); 
		    $scope.cuisine = [];
		    angular.forEach($scope.cuisinesList, function (item) { 
			    if (item.label.toUpperCase().indexOf(search_term) >= 0)  
				    $scope.cuisine.push(item); 
		        });
				console.log(JSON.stringify($scope.cuisine));
               return $scope.cuisine;
		   
	    };
		
			
			

			
			
			
			
			
		 //========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 
		$scope.updateStoreData = function(){  
		 
		
		
		           $scope.featured = $('#featured').val();	 
		   if(!$scope.featured){
			   toastr.error('Choose Featured', 'Error'); return false; 
		   }
			$scope.store_title = $('#title').val(); 
			$scope.commission = $('#commission').val(); 
			$scope.catering_commission = $('#catering_commission').val();
			$scope.manager_id = $('#manager_id').val(); 
			$scope.vendor_id = $('#vendor_id').val();   
			$scope.store_tax = $('#store_tax').val(); 
			$scope.latitude = $('#us3-edit-lat').val(); 
			$scope.longitude = $('#us3-edit-lon').val();   
			$scope.address = $('#address2').val();   			
			 $scope.store_photo = $('#file_name').val();
			$scope.storeData = [{ "store_title" : $scope.store_title, "store_tax" : $scope.store_tax, "commission" : $scope.commission, "catering_commission" : $scope.catering_commission, "manager_id" : $scope.manager_id, "vendor_id" : $scope.vendor_id, "longitude" : $scope.longitude, "latitude" : $scope.latitude,  "address" : $scope.address,"featured" : $scope.featured, "store_photo" : $scope.store_photo}];
			
			
   
  
		 
				 console.log("post data ="+JSON.stringify($scope.storeData));
 
		  
		   $scope.updateStoreDataValue = {"basic_data" : $scope.storeData , "meta_data" : $scope.storeMeta }
		  
				 console.log(JSON.stringify($scope.updateStoreDataValue));

	  
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/submit_store_edit_forms?auth_user_id='+auth_user_id,
                 data:  $scope.updateStoreDataValue,
                 headers: { 'Accept':'application/json' }
            }); 
            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
            	$('#loading').css('display', 'none');
                $scope.data = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

				 console.log(JSON.stringify(data));
             // $('#loading').css('display', 'none');  
			     
            })
		    .error(function (data, status, headers, config) { 
		    	$('#loading').css('display', 'none');
		     
		        console.log(JSON.stringify(data));
            });       
	
		}
		
		 

				//========= FUNCTIONS FOR UPDATE  META DATA FOR STORE====================================================================================== 
		
		   $scope.updateMetaStore = function(){  
		       
		 
		  console.log("Post meta form ="+JSON.stringify($scope.storeMeta));
		  return;
		  var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/form/meta',
                 data: $scope.storeMeta,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
            	$('#loading').css('display', 'none');
                $scope.data = data;  
		

		    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            return false;
				 location.reload();
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify($scope.store);
            })
		    .error(function (data, status, headers, config) {  
		    	$('#loading').css('display', 'none');
			 toastr.error(data.message, 'Error');
		        document.getElementById("res").value = JSON.stringify(data);
            });   
		    }
  
		
  
 
 
 });
 
 
 
 
 
 
 
 
 
 //======================================================VIEW STORE MANAGER LIST CONTROLLER==========================================================================================================
//===================================================================================================================================================================================================
 
	
	app.controller('managerController', function ($http,$scope,$window,toastr) { 
        //$('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/users?user_type=4',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.manager = data;
           // $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	  $scope.deleteUser = function(userId) { 
			$scope.user_id = userId; 
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/users/'+$scope.user_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();		
					}		
                    else{
					   toastr.error(data.message,'Error!');
				    }					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&user_type=4',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            $scope.manager = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
	
 	});
	
	
	