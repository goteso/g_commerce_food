app.config(function($routeProvider) {
    $routeProvider 
    .when("/", {
        templateUrl : APP_URL+"/store-edit.html",
        controller : "storeEditController"
    }) 
    .when("/store-manager", {
        templateUrl : APP_URL+"/store-manager.html",
        controller : "storeManagerController"
    }) 
    .when("/working-hours", {
        templateUrl : APP_URL+"/store-working-hours.html",
        controller : "workingHoursController"
    }) 
	 .when("/deleivery-locations", {
        templateUrl : APP_URL+"/store-deleivery-locations.html",
        controller : "deliveryLocationController"
    });
});
	
	
	
	
		app.controller('storeDataController', function ($http,$scope,$window,toastr,$location,$log, $q,$timeout) {

		 	var auth_user_id = document.getElementById('auth_user_id').value;
		 	
		 		$scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.store_id = str.replace("#", ""); 

		 	$http.get(APP_URL+'/api/v1/stores/'+$scope.store_id)
            .success(function(data, status, headers, config) { 
		    $scope.storeInfo = data.data;  
			console.log(JSON.stringify(data.data));
            });
		 	
		 	
		});
 


//======================================================EDIT STORE CONTROLLER====================================================================================================
//==============================================================================================================================================================================
 
	
	app.controller('storeEditController', function ($http,$scope,$window,toastr,$location,$log, $q,$timeout) {

		 	var auth_user_id = document.getElementById('auth_user_id').value;
		 
         $('#loading').css('display', 'block'); 
 
		  $('.innerData').css('display', 'none'); 
		
			$scope.url = $window.location.href; 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];

$scope.store_id =  str.replace("#", "")
  var parts2 = str.split('?'); 
   
   if(parts2.length<=1){
	   $scope.store_id =  str.replace("#", "")
   }
   else{
	    var str2 = parts2[0];
 $scope.store_id = str2; 
   }
 
		 //$scope.store_id = window.location.href.substr(window.location.href.lastIndexOf('/') + 1); 

		 
		$http.get(APP_URL+'/api/v1/stores/'+$scope.store_id)
            .success(function(data, status, headers, config) { 
		    $scope.store = data.data[0];  
		    $scope.lat = $scope.store.latitude;
             $scope.lon = $scope.store.longitude;			
             $scope.vendor = $scope.store.vendor_id; 
			  $scope.manager = $scope.store.manager_id; 	 
			   console.log(JSON.stringify(data.data[0])); 
			  $('#loading').css('display', 'none'); 
				 $('.innerData').css('display', 'block'); 


			  
				
				$('#us3-edit-lat').val($scope.lat);
				$('#us3-edit-lon').val($scope.lon);
				
			 
				
				$('#us3-edit-lat').val() = $('#us3-edit-lat1').val();
				
				
				
				// $scope.locationpickerOptions.center.latitude = $scope.lat;
               // $scope.locationpickerOptions.center.longitude = $scope.lon;

				 $scope.locationpickerOptions.inputBinding.latitudeInput = $('#us3-edit-lat1');
				  $scope.locationpickerOptions.inputBinding.longitudeInput = $('#us3-edit-lon2');
				 
			
			  
  
	    }) 
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		 
		//========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 
         var request = $http({
                 method: "GET",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/form/meta',
                 data:  '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.storeMeta = data;    
             // $('#loading').css('display', 'none');  
			    console.log("meta"+JSON.stringify(data));
            })
		    .error(function (data, status, headers, config) { 
		        document.getElementById("res").value = JSON.stringify(data);
            });   

 
		
	 	 //========= FUNCTIONS FOR LOCATION FOR STORE====================================================================================== 
		/* $scope.locationpickerOptions = { 
                        center: {
                          latitude: null,
                            longitude:null
                        },
                        inputBinding: { 
                            locationNameInput: $('#us3-edit-address') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true,
					    
                    };*/




                      $scope.locationpickerOptions = {
                        location: {
                            latitude: $('#us3-edit-lat').val(),
                            longitude: $('#us3-edit-lon').val()
                        },
                        inputBinding: { 
						 latitudeInput: $('#us3-edit-lat1'),
                            longitudeInput: $('#us3-edit-lon1'),
                            radiusInput: $('#us3-edit-radius'),
                            locationNameInput: $('#us3-edit-address') 
                        },
						zoom: 15,
                        radius: 0,
						//enableReverseGeocode: false,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true 
					   
					   
                    };


					
					 
			
			
			
				//========= FUNCTIONS FOR GET CUISINE FOR STORE====================================================================================== 
		  /*$scope.cuisineSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/store-filter-options?type=cuisines&search="+query, {params: {q: query}})
                .then(function(response){  
				console.log(JSON.stringify(response.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedCuisineChange = function(values,data) {
                $log.info('Item changed to ' + JSON.stringify(values));
 data.value=values.value;
	            $scope.cuisine_value =  data.value; 
                 				
            }
			 $scope.searchCuisinesChange = function(text){ 
				   $scope.cuisine_value = ''; 
			}
			
			
			*/
			
			
		 
			  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/store-filter-options?type=cuisines&exclude_value=true',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           $scope.cuisinesList = data.data;   
		   console.log(JSON.stringify(data.data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 
		
			 $scope.searchCuisineData = function (query) { 
		    var search_term = query.toUpperCase(); 
		    $scope.cuisine = [];
		    angular.forEach($scope.cuisinesList, function (item) { 
			    if (item.label.toUpperCase().indexOf(search_term) >= 0)  
				    $scope.cuisine.push(item); 
		        });
				console.log(JSON.stringify($scope.cuisine));
               return $scope.cuisine;
		   
	    };
		
			
			

			
			
			
			$scope.switch = function(value,id){ 
				 $scope.catering_status = value;
			}
			
		 //========= FUNCTIONS FOR GET META DATA FOR STORE====================================================================================== 
		$scope.updateStoreData = function(){  
		 
		 if($scope.catering_status == '' || $scope.catering_status == undefined){
			 $scope.catering_status = 0;
		 }
		 
		 
		           $scope.featured = $('#featured').val();	 
		   if(!$scope.featured){
			   toastr.error('Choose Featured', 'Error'); return false; 
		   }
			$scope.store_title = $('#title').val(); 
			$scope.commission = $('#commission').val(); 
			$scope.catering_commission = $('#catering_commission').val();
			$scope.manager_id =  $scope.manager; 
			$scope.vendor_id =  $scope.vendor;   
			$scope.minimum_delivery = $('#minimum_delivery').val();   
			$scope.store_tax = $('#store_tax').val(); 
			$scope.latitude = $('#us3-edit-lat').val(); 
			$scope.longitude = $('#us3-edit-lon').val();   
			$scope.address = $('#address2').val();   			
			 $scope.store_photo = $('#store_file_name').val();
			$scope.storeData = [{ "store_title" : $scope.store_title, "minimum_delivery" : $scope.minimum_delivery,"catering_status":$scope.catering_status, "store_tax" : $scope.store_tax, "commission" : $scope.commission, "catering_commission" : $scope.catering_commission, "manager_id" : $scope.manager_id, "vendor_id" : $scope.vendor_id, "longitude" : $scope.longitude, "latitude" : $scope.latitude,  "address" : $scope.address,"featured" : $scope.featured, "store_photo" : $scope.store_photo}];
			
			
   
  
		 
				 console.log("post data ="+JSON.stringify($scope.storeData));
 
		  
		   $scope.updateStoreDataValue = {"basic_data" : $scope.storeData , "meta_data" : $scope.storeMeta }
		  
				 console.log(JSON.stringify($scope.updateStoreDataValue));

	  
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/submit_store_edit_forms?auth_user_id='+auth_user_id,
                 data:  $scope.updateStoreDataValue,
                 headers: { 'Accept':'application/json' }
            }); 
            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) {
                $scope.data = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

				 console.log(JSON.stringify(data));
             // $('#loading').css('display', 'none');  
			     
            })
		    .error(function (data, status, headers, config) { 
		     
		        console.log(JSON.stringify(data));
            });       
	
		}
		
		 

				//========= FUNCTIONS FOR UPDATE  META DATA FOR STORE====================================================================================== 
		
		   $scope.updateMetaStore = function(){  
		       
		 
		  console.log("Post meta form ="+JSON.stringify($scope.storeMeta));
		  return;
		  var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores/'+$scope.store_id+'/form/meta',
                 data: $scope.storeMeta,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.data = data;  
		

		    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            return false;
				 location.reload();
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify($scope.store);
            })
		    .error(function (data, status, headers, config) {  
			 toastr.error(data.message, 'Error');
		        document.getElementById("res").value = JSON.stringify(data);
            });   
		    }
  
		
  
 
 
 });
 
 
 
 
 


 //=========================================================EDIT CUSTOMER CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('storeManagerController', function ($http, $scope, $window,toastr) {
 
 
 	$scope.url = window.location.href; 
 var store_value = $scope.url;
 var store_parts = store_value.split('/');
 var store_index = store_parts.length - 2;
 var store_str = store_parts[store_index];
 
  var store_parts2 = store_str.split('?'); 
   
   if(store_parts2.length<=1){
	   $scope.store_id =  store_str.replace("#", "")
   }
   else{
	    var store_str2 = store_parts2[0];
 $scope.store_id = store_str2; 
   }
   
   
   
   
  var url_val = window.location.href.substr(window.location.href.lastIndexOf('?') + 1); 
 
    var parts = url_val.split('/');
 var index = parts.length - 2;
 var str = parts[index]; 
    var id  = str.replace("#", ""); 
	
     var parts2 = id.split('=');
	 var str2 = parts2[1];
	 
	 
   if(str2 == 0){
	   $scope.manager_id = '';
   }
else{
	$scope.manager_id = str2;
   
}	 
	   
	  if($scope.manager_id){
	        $('#loading').css('display', 'block');
 
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.manager_id+"?user_type=4",
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.editUser  = data;
            $('#loading').css('display', 'none'); 

       for(var k=0;k<=$scope.editUser[0].fields.length;k++){  
	           if($scope.editUser[0].fields[k].identifier == 'photo'){ 		   
			var item_data_image =  $scope.editUser[0].fields[k].value ; 			 
			document.getElementById('item_photo').value = item_data_image; 
		} 
	 }			
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  

	  }
		 
	 //==========================function for update user======================================
		   $scope.updateUser = function(){
			    
				
				 var item_photo = $('#item_photo').val(); 
         for(var j=0;j<$scope.editUser[0].fields.length;j++){
               if($scope.editUser[0].fields[j].identifier == 'photo'){
	               $scope.editUser[0].fields[j].value = item_photo; 
                }  
 
              }
			  
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.manager_id,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 
          

              if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


            $('#loading').css('display', 'none');    
			 document.getElementById("res").value = JSON.stringify(data); 
        })
		.error(function (data, status, headers, config) {  	
		
            
                if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
		      document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };		


//==========================function for RESET PASSWORD======================================
           $scope.resetPass = function(){ 
		    
			$scope.pass = $('#password').val();
			$scope.confirm_pass = $('#confirm_password').val();
			if($scope.pass != $scope.confirm_pass){
				toastr.error('Password does not match', 'Error');
			}
			else{
				
				$scope.passwordValue = { "password":$scope.pass };
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update-password/'+$scope.manager_id,
            data:$scope.passwordValue,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	 console.log(JSON.stringify(data));
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
                else { toastr.error(data.message, 'Error'); return false; }
   
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
		   
			}
           };		   
	});







//======================================================WORKING HOURS CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('workingHoursController', function ($http,$scope,$window,toastr, $filter, mdcDateTimeDialog) { 

	
	var auth_user_id = document.getElementById('auth_user_id').value;
	
		$scope.url = window.location.href; 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 var value2 = str.replace("#", ""); 
var parts2 = value2.split('?');
var str2 =  parts2[0];
 $scope.store_id = str2; 
	
	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/store-working-hours?auth_user_id='+auth_user_id+'&store_id='+$scope.store_id, 
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.tableData = data.data;
			 	$scope.stores = angular.copy($scope.tableData);
				console.log(JSON.stringify(data));
				 /*for(var j=0;j<=$scope.stores.length;j++){
					$scope.storeId = $scope.stores[j].id;
				} */
		$scope.enableEdit = [];
		
            $('#loading').css('display', 'none');  
        })
		.error(function (data, status, headers, config) { 
		        console.log(JSON.stringify(data)); 
        });       
		
		 
	
		
		$scope.addWorkingHours = function(){
			 
		var store = { weekday:"", start_time :"", end_time:"", disableEdit:true};
		 
		$scope.stores.push(store);
		 $scope.enabledEdit[$scope.stores.length-1] = true;
		 console.log($scope.stores.length-1);
		}
		
		 
		
		$scope.editWorkigHours = function(working_id, index){
			$scope.working_id = working_id;
		   console.log("edit index"+index);
		   //$scope.enabledEdit[index] = true;
		   
		   
		    $scope.weekday_edit = document.getElementsByName('weekday' + index)[0].value; 
         $scope.start_time_edit = document.getElementsByName('start_time' + index)[0].value; 
		 $scope.end_time_edit = document.getElementsByName('end_time' + index)[0].value; 
		 $scope.workingHourDataEdit = {  "store_id":$scope.store_id,  "start_time": $scope.start_time_edit,  "end_time": $scope.end_time_edit, "weekday": $scope.weekday_edit  }
		 
		 
		 
		   	var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/store-working-hours/'+$scope.working_id, 
            data:  $scope.workingHourDataEdit,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.data = data;
			    if(data.status_text == 'Success') { 
				toastr.success(data.message, 'Success'); 
				console.log(JSON.stringify(data));   
				// $scope.enabledEdit[index] = true;
				 }
            else { toastr.error(data.message, 'Error'); return false; }
			
				 
            $('#loading').css('display', 'none');  
        })
		.error(function (data, status, headers, config) { 
		    console.log(JSON.stringify(data)); 
        });       
		 
		 
		 
		}
		
		
		
		
		// FUNCTION FOR ADD STORE WORKING HOURS  ============================================================== clean done
		$scope.submitWorkingHours = function(index){
			 
		  $scope.weekday = document.getElementsByName('weekday' + index)[0].value; 
         $scope.start_time = document.getElementsByName('start_time' + index)[0].value; 
		 $scope.end_time = document.getElementsByName('end_time' + index)[0].value; 
		 $scope.workingHourData = {  "store_id":$scope.store_id,  "start_time": $scope.start_time,  "end_time": $scope.end_time, "weekday": $scope.weekday  }
   
			console.log(angular.toJson($scope.workingHourData));
			
				var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/store-working-hours', 
            data:  $scope.workingHourData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.data = data;
			    if(data.status_text == 'Success') { 
				toastr.success(data.message, 'Success'); 
				console.log(JSON.stringify(data)); 
				 
				  $('input[name="' + 'id' + index + '"]').val(data.data.id) ;
				  
				  $scope.storeId = $('input[name="' + 'id' + index + '"]').val();
				 $('#btnAdd').hide();
				 location.reload();
				// $scope.enabledEdit[index] = true;
				 }
            else { toastr.error(data.message, 'Error'); return false; }
			
				
            $('#loading').css('display', 'none');  
        })
		.error(function (data, status, headers, config) { 
		      console.log(JSON.stringify(data)); 
        });       
		 
		
		}
	
	
	
	
	// FUNCTION FOR DELETE STORE WORKING HOURS ============================================================== clean done
		
		$scope.deleteWorkigHours = function(working_id, index){ 
 	
		  $scope.working_id = working_id;
		 if($scope.working_id == '' || $scope.working_id == undefined || $scope.working_id == null){
			  $scope.stores.splice(index,1);
		 }
		 
		 else{
		  
		   if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/store-working-hours/'+$scope.working_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data; 

			         if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 
		   $scope.stores.splice(index,1);}
            else { toastr.error(data.message, 'Error'); return false; }


                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			           console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		 }
		
		}
		
		

	
	
 	});
	
	
	
	
	
	
//======================================================ADD STORE CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('deliveryLocationController', function ($http,$scope,$window,toastr,$log,$q) {
		
		var auth_user_id = document.getElementById('auth_user_id').value;
		 
		 	$scope.url = window.location.href; 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 
var value2 = str.replace("#", ""); 
var parts2 = value2.split('?');
var str2 =  parts2[0];
 $scope.store_id = str2; 
		   
		    $('#loading').css('display', 'block');  
			   $('.innerData').css('display', 'none');  
		 
		 /*---------------function for get stores-------------------------*/
		 

		 console.log("URL = "+APP_URL+'/api/v1/store-delivery-settings?auth_user_id='+auth_user_id+'&store_id='+$scope.store_id);
		 	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/store-delivery-settings?auth_user_id='+auth_user_id+'&store_id='+$scope.store_id, 
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
			console.log(JSON.stringify(data));
             $scope.tableData = data.data;
			 	$scope.locations = angular.copy($scope.tableData);
				console.log(JSON.stringify($scope.locations));
				   
		$scope.enableEdit = [];
		
            $('#loading').css('display', 'none');
   $('.innerData').css('display', 'block');  			
        })
		.error(function (data, status, headers, config) { 
		       console.log(JSON.stringify(data)); 
        });       
		
		
		
		
		
		
	var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/areas?auth_user_id='+auth_user_id, 
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.areas = data.data.data; 
				console.log(JSON.stringify($scope.areas)); 
            $('#loading').css('display', 'none');  
        })
		.error(function (data, status, headers, config) { 
		       console.log(JSON.stringify(data)); 
        });       
		
		 
	
		
		$scope.addDeliveryLocation = function(){
			 
		var store = { area_id:"", start_time :"", end_time:"", delivery_fee:"", disableEdit:true};
		 
		$scope.locations.push(store);
		 $scope.enabledEdit[$scope.locations.length-1] = true;
		 console.log($scope.locations.length-1);
		}
		
		 
		
		$scope.editDeliveryLocation = function(location_id, index){
			$scope.location_id = location_id;
		   console.log("edit index"+index);
		   //$scope.enabledEdit[index] = true;
		   
		   
		    $scope.area_id_edit = document.getElementsByName('area_id' + index)[0].value; 
         $scope.start_time_edit = document.getElementsByName('start_time' + index)[0].value; 
		 $scope.end_time_edit = document.getElementsByName('end_time' + index)[0].value; 
		  $scope.delivery_fee_edit = document.getElementsByName('delivery_fee' + index)[0].value; 
		 $scope.deliveryLocationDataEdit = {  "store_id":$scope.store_id,  "start_time": $scope.start_time_edit,  "end_time": $scope.end_time_edit, "area_id": $scope.area_id_edit, "delivery_fee": $scope.delivery_fee_edit}
		 
		 
		 
		   	var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/store-delivery-settings/'+$scope.location_id, 
            data:  $scope.deliveryLocationDataEdit,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.data = data;
			    if(data.status_text == 'Success') { 
				toastr.success(data.message, 'Success'); 
				console.log(JSON.stringify(data));   
				// $scope.enabledEdit[index] = true;
				 }
            else { toastr.error(data.message, 'Error'); return false; }
			
				
            $('#loading').css('display', 'none');  
        })
		.error(function (data, status, headers, config) { 
		       console.log(JSON.stringify(data)); 
        });       
		 
		 
		 
		}
		
		
		
		
		// FUNCTION FOR ADD STORE WORKING HOURS  ============================================================== clean done
		$scope.submitDeliveryLocation = function(index){
			 
		  $scope.area_id = document.getElementsByName('area_id' + index)[0].value; 
         $scope.start_time = document.getElementsByName('start_time' + index)[0].value; 
		 $scope.end_time = document.getElementsByName('end_time' + index)[0].value; 
		  $scope.delivery_fee = document.getElementsByName('delivery_fee' + index)[0].value; 
		 $scope.workingHourData = {  "store_id":$scope.store_id,  "start_time": $scope.start_time,  "end_time": $scope.end_time, "area_id": $scope.area_id , "delivery_fee": $scope.delivery_fee }
   
			console.log(angular.toJson($scope.workingHourData));
			
				var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/store-delivery-settings', 
            data:  $scope.workingHourData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.data = data;
			    if(data.status_text == 'Success') { 
				toastr.success(data.message, 'Success'); 
				console.log(JSON.stringify(data)); 
				 
				  $('input[name="' + 'id' + index + '"]').val(data.data.id) ;
				  
				  $scope.storeId = $('input[name="' + 'id' + index + '"]').val();
				 $('#btnAdd').hide();
				 location.reload();
				// $scope.enabledEdit[index] = true;
				 }
            else { toastr.error(data.message, 'Error'); return false; }
			
				
            $('#loading').css('display', 'none');  
        })
		.error(function (data, status, headers, config) { 
		       console.log(JSON.stringify(data)); 
        });       
		 
		
		}
	
	
	
	
	// FUNCTION FOR DELETE STORE WORKING HOURS ============================================================== clean done
		
		$scope.deleteDeliveryLocation = function(location_id, index){ 
 	
		  $scope.location_id = location_id;
		 if($scope.location_id == '' || $scope.location_id == undefined || $scope.location_id == null){
			  $scope.locations.splice(index,1);
		 }
		 
		 else{
		  
		   if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/store-delivery-settings/'+$scope.location_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data; 

			         if(data.status_text == 'Success') {   console.log(JSON.stringify(data));  toastr.success(data.message, 'Success'); 
		   $scope.locations.splice(index,1);}
            else {    console.log(JSON.stringify(data)); toastr.error(data.message, 'Error'); return false; }


                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		 }
		
		}
 	});
		
 
  
 
 
	