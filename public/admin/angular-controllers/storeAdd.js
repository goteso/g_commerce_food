
 
	
//======================================================ADD STORE CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('addStoreController', function ($http,$scope,$window,toastr,$log,$q) {
		
		 var auth_user_id = document.getElementById('auth_user_id').value;
		 
		 $scope.restaurant_id = window.location.href.substr(window.location.href.lastIndexOf('?') + 1); 
		 
		 
		  $scope.locationpickerOptions = {
                        location: {
                            latitude: 25.286106,
                            longitude: 51.534817
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,
                        radius: 0,
						//enableReverseGeocode: false,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true 
					   
					   
                    };
		 
		 
					  // $('#us3-address').val() = $('#address').val();	
		 
		 
		 
			
			//========= FUNCTIONS FOR GET MANAGER FOR STORE====================================================================================== 
		  $scope.managerSearch = function(query){
		        return $http.get(APP_URL+"/api/v1/users?user_type=4&search="+query, {params: {q: query}})
                .then(function(response){ 
				//alert(JSON.stringify(response.data.data.data));
                    return response.data.data.data;
                }) 
            };
		  
		    $scope.selectedManagerChange = function(values) {
                $log.info('Item changed to ' + JSON.stringify(values));  
	            $scope.manager_id =  values.user_id;  
            }
			
			 $scope.searchManagerChange = function(text){  
				   $scope.manager_id = ''; 
			}
		 
         //$('#loading').css('display', 'block');  
		$scope.addStore = function(){
			  
           $scope.featured = $('#featured').val();	
		   if(!$scope.featured){
			   toastr.error('Choose Featured', 'Error'); return false; 
		   }
			 
			$scope.store_title = $('#title').val(); 
			$scope.commission = $('#commission').val();
		    $scope.catering_commission = $('#catering_commission').val();
			$scope.manager_id = '0';
			$scope.store_tax = $('#store_tax').val(); 		
			$scope.minimum_delivery = $('#minimum_delivery').val(); 
			$scope.vendor_id = $scope.restaurant_id; 		
			$scope.latitude = $('#us3-lat').val(); 
			$scope.longitude = $('#us3-lon').val();  
             $scope.address = $('#address').val();			
			 $scope.store_photo = $('#file_name').val();
			$scope.storeData = { "minimum_delivery" : $scope.minimum_delivery, "store_title" : $scope.store_title, "catering_status":$scope.catering_status, "store_tax" : $scope.store_tax, "commission" : $scope.commission, "catering_commission" : $scope.catering_commission, "manager_id" : $scope.manager_id, "vendor_id" : $scope.vendor_id, "longitude" : $scope.longitude, "latitude" : $scope.latitude,  "address" : $scope.address , "featured" : $scope.featured, "store_photo" : $scope.store_photo};
		 console.log(APP_URL+'/api/v1/stores');
		 console.log(JSON.stringify( $scope.storeData)); 
	 
		    var request = $http({
                 method: "POST",
                 url: APP_URL+'/api/v1/stores',
                 data:  $scope.storeData,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.stores = data;  
               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); 
			    $scope.id  = data.data.id;
			   $window.location.href = 'store_info/'+$scope.id+'?id='+$scope.manager_id;
			  }
            else { toastr.error(data.message, 'Error'); return false; }


				
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) {
                 toastr.error(data.message, 'Error');				
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
		}
 	});
		
 
  
 
 