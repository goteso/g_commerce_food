
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/restaurant-edit.html",
        controller : "restaurantEditController"
    })
    .when("/stores", {
        templateUrl : APP_URL+"/restaurant-stores.html",
        controller : "restaurantStoresController"
    }) 
    .otherwise({
            redirectTo: '/'
        });	;
});
		
		

	
//=========================================================EDIT RESTAURANT CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('restaurantEditController', function ($http, $scope, $window,toastr) {
 
 var auth_user_id = document.getElementById('auth_user_id').value;
 
  $scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.restaurant_id = str.replace("#", ""); 

  
   
	 
	        $('#loading').css('display', 'block');
  
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.restaurant_id+'?auth_user_id='+auth_user_id+'&user_type=3',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.editUser  = data;
			console.log(JSON.stringify(data));
            $('#loading').css('display', 'none');  
			 for(var k=0;k<=$scope.editUser[0].fields.length;k++){  
	           if($scope.editUser[0].fields[k].identifier == 'photo'){ 		   
			var item_data_image =  $scope.editUser[0].fields[k].value ; 			 
			document.getElementById('item_photo').value = item_data_image; 
		} 
			 }
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  


		 
	 //==========================function for update user======================================
		   $scope.updateUser = function(){
			    
				
				 var item_photo = $('#item_photo').val(); 
         for(var j=0;j<$scope.editUser[0].fields.length;j++){
               if($scope.editUser[0].fields[j].identifier == 'photo'){
	               $scope.editUser[0].fields[j].value = item_photo; 
                }  
 
              }
			  
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.restaurant_id+'?auth_user_id='+auth_user_id,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 
           

               if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }

            
            $('#loading').css('display', 'none');    
			 document.getElementById("res").value = JSON.stringify(data); 
        })
		.error(function (data, status, headers, config) {  	
		
             toastr.error(data.message, 'Error');
		      document.getElementById("res").value = JSON.stringify(data);
        });  
 
		   };			   
		   
		   
		   
		   //==========================function for RESET PASSWORD======================================
           $scope.resetPass = function(){ 
		    
			$scope.pass = $('#password').val();
			$scope.confirm_pass = $('#confirm_password').val();
			if($scope.pass != $scope.confirm_pass){
				toastr.error('Password does not match', 'Error');
			}
			else{
				
				$scope.passwordValue = { "password":$scope.pass };
			 var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/users/update-password/'+$scope.restaurant_id,
            data:$scope.passwordValue,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
        	 console.log(JSON.stringify(data));
            $scope.data  = data; 
             if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
                else { toastr.error(data.message, 'Error'); return false; }
   
        })
		.error(function (data, status, headers, config) {   
             toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
		   
			}
           };
	});


	
	
	
	
	
 
 app.controller('restaurantStoresController', function ($http, $scope, $window,toastr, $log, $q ) {
	 
	  var auth_user_id = document.getElementById('auth_user_id').value;
 
  $scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 $scope.restaurant_id = str.replace("#", ""); 
 
 

 console.log('APP_URL = '+APP_URL+'/api/v1/stores?exclude_busy_store=false&catering_status=any&vendor_id='+$scope.restaurant_id);
   $('#loading').css('display', 'block'); 
        $('.innerData').css('display', 'none'); 
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?exclude_busy_store=false&catering_status=any&vendor_id='+$scope.restaurant_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.stores = data;
			 $scope.storesData = data.data;
			 
             $('#loading').css('display', 'none');  
		  $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
			
			 $scope.storesData1 = { 
	    selected: null,
        data: {"data": []}
    };
 

	for (var i = 0; i <= $scope.storesData.data.length; ++i) {
        $scope.storesData1.data.data.push($scope.storesData.data[i]);   
		
    }

	 
    // Model to JSON for demo purpose
    $scope.$watch('storesData1', function(model) {  
        $scope.modelAsJson = angular.toJson(model, true);
		console.log("iiii"+JSON.stringify(model.data.data)); 
		 
    }, true);

	
	 
        })
		.error(function (data, status, headers, config) { 
			console.log(JSON.stringify(data));
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	 
	
	
	
	// FUNCTION FOR DELETE STORE  ============================================================== clean done
	$scope.deleteStore = function(storeId) {
		    
			$scope.store_id = storeId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/stores/'+$scope.store_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.data = data;
			     

			         if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		// FUNCTION FOR PAGINATION  ============================================================== clean done
		$scope.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&?exclude_busy_store=false&catering_status=any',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
             $scope.stores = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		 
 	$scope.switch = function(value,id){
 	    
	    $scope.store_id = id;
	    $scope.catering_value = value;
	     $scope.catering_data = {"catering_status":$scope.catering_value};
	     var request = $http({
                 method: "PUT",
                 url: APP_URL+'/api/v1/stores/catering-status/'+$scope.store_id,
                 data:  $scope.catering_data,
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
                $scope.stores_catering = data;   
				 if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }
             // $('#loading').css('display', 'none');  
			    document.getElementById("res").value = JSON.stringify(data);
            })
		    .error(function (data, status, headers, config) {
                 toastr.error(data.message, 'Error');				
		        document.getElementById("res").value = JSON.stringify(data);
            });       
	
	    
	}
	
	
	
  
	
 	});
	
	
	
	
	
	
