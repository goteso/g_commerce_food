
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('faqController', function($http, $scope , $window, $log, $q, $timeout, toastr,$sce) {
 
 $scope.trustAsHtml = $sce.trustAsHtml;
 
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		   $('.innerData').css('display', 'none'); 
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/faqs',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.faqs = data;
            $('#loading').css('display', 'none');  
			   $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
			   $('.innerData').css('display', 'none');  
		
		var request = $http({
            method: "GET",
            url: api_url,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.faqs = data;
            $('#loading').css('display', 'none');  
               $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
		//========= FUNCTIONS FOR STORE A FAQ=============================================================================================== 
		 pro.storeFaq = function(){ 
 
		   pro.question = $('#question').val();
		   pro.answer = $('#answer').val();
		   pro.faq_data = {question : pro.question , answer : pro.answer};
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/faqs',
            data:  pro.faq_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {  
            pro.data = data;
			if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		  $scope.AppendText = function() {
     var myEl = angular.element( document.querySelector( '#divID' ) );
     myEl.append('Hi<br/>');     
    }
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A FAQ FORM=============================================================================================== 
		 pro.editFaq = function(id,question,answer){  
           pro.faq_id = id;
		    pro.question = question;
            pro.answer = answer;			
		   $('#edit').modal('show');   
		 }
		 
		 
		 
		 
		 
		 
		 //========= FUNCTIONS FOR UPDATE A FAQ=============================================================================================== 
		 pro.updateFaq = function(id){ 
           pro.faq_id = id; 
		   pro.question = $('#edit_question').val();
		   pro.answer = $('#edit_answer').val();  
		   pro.faq_data = {question : pro.question , answer : pro.answer};
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/faqs/'+pro.faq_id,
            data:  pro.faq_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
             if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}	 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 // FUNCTION FOR DELETE FAQ  ============================================================== clean done
	pro.deleteFaq = function(faqId) {
		    
			pro.faq_id = faqId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/faqs/'+pro.faq_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		//FUNCTION FOR VIEW A FAQ=======================================================================
		
		
		//========= FUNCTIONS FOR EDIT  A FAQ FORM=============================================================================================== 
		 pro.viewFaq = function(id,question,answer){  
           pro.faqId = id;
		    pro.questionView = question;
            pro.answerView = answer;			
		   $('#vieFaq').modal('show');   
		 }
		 
		
   });