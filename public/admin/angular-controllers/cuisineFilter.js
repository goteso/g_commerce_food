
//==========================================================FAQ CONTROLLER=======================================================================================================
//====================================================================================================================================================================================
   app.controller('cuisineController', function($http, $scope , $window, $log, $q, $timeout, toastr,$sce) {
 
 $scope.trustAsHtml = $sce.trustAsHtml;
 
        var pro = this; 
		 $('#loading').css('display', 'block'); 
		   $('.innerData').css('display', 'none'); 
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/store-filter-options?type=cuisines',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.cuisines = data;
            $('#loading').css('display', 'none');  
			   $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
		
				
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
			   $('.innerData').css('display', 'none');  
		
		var request = $http({
            method: "GET",
            url: api_url,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.cuisines = data;
            $('#loading').css('display', 'none');  
               $('.innerData').css('display', 'block'); 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
		//========= FUNCTIONS FOR STORE A FAQ=============================================================================================== 
		 pro.storeCuisines = function(){ 
 
		   pro.cuisine_indentifier = $('#cuisine_indentifier').val();
		   pro.cuisine_value = $('#cuisine_value').val();
		   pro.cuisine_data = {"identifier" : pro.cuisine_indentifier , "value" : pro.cuisine_value, "type":"cuisines"};
		  
		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/store-filter-options',
            data:  pro.cuisine_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) {  
            pro.data = data;
			if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		  $scope.AppendText = function() {
     var myEl = angular.element( document.querySelector( '#divID' ) );
     myEl.append('Hi<br/>');     
    }
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A FAQ FORM=============================================================================================== 
		 pro.editFaq = function(id,question,answer){  
           pro.faq_id = id;
		    pro.question = question;
            pro.answer = answer;			
		   $('#edit').modal('show');   
		 }
		 
		 
		 
		 
		 
		 
		 //========= FUNCTIONS FOR UPDATE A FAQ=============================================================================================== 
		 pro.updateFaq = function(id){ 
           pro.faq_id = id; 
		   
		    pro.cuisine_indentifier = $('#edit_cuisine_indentifier').val();
		   pro.cuisine_value = $('#edit_cuisine_value').val();
		   pro.edit_cuisine_data = {"identifier" : pro.cuisine_indentifier , "value" : pro.cuisine_value, "type":"cuisines"};
		   
		 
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/faqs/'+pro.faq_id,
            data:  pro.edit_cuisine_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
             if(data.status_code == 1){
             toastr.success(data.message, 'Success');
             location.reload();			 
			}
			else{toastr.error(data.message, 'Error');}	 
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 // FUNCTION FOR DELETE FAQ  ============================================================== clean done
	pro.deleteCuisine = function(cuisineId) {
		    
			pro.cuisine_id = cuisineId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/store-filter-options/'+pro.cuisine_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		//FUNCTION FOR VIEW A FAQ=======================================================================
		
		
		 
		
   });