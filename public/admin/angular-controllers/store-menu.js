
app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/store-menu.html",
        controller : "menuController as ctrl"
    })
    .when("/categories", {
        templateUrl : APP_URL+"/store-categories.html",
        controller : "categoryController as ctrl"
    })
	.when("/items", {
        templateUrl : APP_URL+"/store-items.html",
        controller : "itemsController as ctrl"
    })
    .otherwise({
            redirectTo: '/'
        });	;
});
		
		
//====================================================== STORE MENU CONTROLLER=================================================================================
//==============================================================================================================================================================
  
	app.controller('menuController', function ($http, $scope, $window,toastr, $log, $q ) {
 
	     // get basic form to add product ============================================================== clean done
	     	var auth_user_id = document.getElementById('auth_user_id').value;
  

  var pro = this; 
   
   
   
    $scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 pro.store_id = str.replace("#", ""); 
  
			
         $('#loading').css('display', 'block');
		  
		 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores/'+pro.store_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            pro.stores  = data.data;
			 console.log(JSON.stringify(data));
            $('#loading').css('display', 'none');  
			
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 
			
			
			
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?auth_user_id='+auth_user_id+'&request_type=input_field&include_items=true&store_id='+pro.store_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
           pro.categories = data.data.data;   
		   console.log(JSON.stringify(data.data.data));
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
		
		
		   
		  
	});





	
	
	
	
//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('categoryController', function ($http,$scope,$window,toastr,$location,$log) {
		
	
 
 
		var auth_user_id = document.getElementById('auth_user_id').value;
		    var pro = this; 
		  
			 	  $scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 pro.store_id = str.replace("#", ""); 
  
      
        $('#loading').css('display', 'block');  
		
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/category?store_id='+pro.store_id+'&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		
       
	
	
	
					
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&store_id='+pro.store_id+'&auth_user_id='+auth_user_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.categories = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
		
		
		
		// FUNCTION FOR DELETE CATEGORY  ============================================================== clean done
	pro.deleteCategoryId = function(categoryId) {
		    
			$scope.category_id = categoryId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/category/'+$scope.category_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
                       location.reload();	
					}
				else{
					toastr.error(data.message,'Error!');
				}	
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		
		
		// FUNCTION FOR CHANGE STATUS OF CATEGORY  ============================================================== clean done
	pro.switchStatus = function(value, categoryId) {
		
		pro.category_id = categoryId; 
		pro.status_value = value;
		
		pro.category_status_value = { "status": pro.status_value}; 
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/category/update-status/'+pro.category_id,
                    data: pro.category_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					 console.log(JSON.stringify(data)); 
					}
					else{toastr.error(data.message,'Error!'); console.log(JSON.stringify(data)); }
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	
	
		//====================FUNCTION FOR GET PARENT CATEGORY API==========================================================================
		pro.categorySearch = function(query) {  
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&parents_only=true&store_id="+pro.store_id+"search=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };
  
        pro.selectedCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
              $scope.category_id =   data.category_id;  
        } 
		
		
		$scope.searchCategoryChange = function(text){  
				   $scope.category_id = ''; 
			};
		
		
		
 
			
			
		
		 $scope.uploadFile = function () {
			 alert(JSON.stringify($scope.myFile));
            var file = $scope.myFile;
            var uploadUrl = APP_URL + "/api/v1/users/upload-profile-image", //Url of webservice/api/server
                promise = fileUploadService.uploadFileToUrl(file, uploadUrl);
 
            promise.then(function (response) {
				 
                $scope.serverResponse = response;
            }, function () {
				 
                $scope.serverResponse = 'An error has occurred';
            })
        };
		
		
		 pro.editCategory = function(){ 
		 
		 }
		
		//========= FUNCTIONS FOR STORE A CATEGORY=============================================================================================== 
		 pro.storeCategory = function(){ 
           pro.category_photo = $('#file_name').val();
		   pro.category_title = $('#category_title').val();
		   pro.parent_id = $('#category_id').val();
           pro.store_id_value = pro.store_id;			
		   pro.category_data = {"category_title" : pro.category_title, "parent_id" : pro.parent_id, "store_id" : pro.store_id_value, "category_photo" : pro.category_photo };

		   console.log("Store Category Post ="+JSON.stringify(pro.category_data));


		   var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/category',
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
             if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


			  location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		 
		 
		 
		 
		 
		 
		 
		
		
		//========= FUNCTIONS FOR EDIT  A CATEGORY FORM=============================================================================================== 
		 pro.editCategory = function(values){  
		 console.log(JSON.stringify(values));
		 pro.category_photo = values.category_photo;
           pro.category_id = values.category_id;
		   pro.category_edit_title = values.category_title;
           pro.parent_category_title = values.parent_category_title;		   
		    pro.parent_id = values.parent_id;
			pro.store_edit_id = values.store_id;
			pro.store_edit_title = values.store_title;
			
		 $('#file_name1').val(pro.category_photo);
		     $('#editCat').modal('show');   
		
		 
		 
		 
		  pro.editCategorySearch = function(query) { 
		   
            return $http.get(APP_URL + "/api/v1/category?auth_user_id="+auth_user_id+"&parents_only=true&store_id="+pro.store_id+"&exclude_id="+pro.category_id +"&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
			console.log(response.data.data.data);
                return response.data.data.data;
            })
        };
  
        pro.selectedEditCategoryChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
			//alert(data.category_id);
                 pro.parent_category_id =   data.category_id; 
               if(pro.parent_category_id != undefined || pro.parent_category_id != null){
				   pro.parent_id = pro.parent_category_id;
			    } 
		  };
		  
		  pro.searchEditCategoryChange = function(text){  
				   pro.parent_id = ''; 
			};
		  
		  
		  
		 }
 
		 
		 //========= FUNCTIONS FOR UPDATE A CATEGORY=============================================================================================== 
		 pro.updateCategory = function(id){ 
           pro.category_id = id; 
		   pro.category_photo = $('#file_name1').val();
		   pro.store_edit_id = pro.store_id;	
		   pro.category_edit_title = $('#category_edit_title').val();
		   pro.parent_id = $('#category_edit_id').val();
		  
		   pro.category_data = {"category_title" : pro.category_edit_title, "parent_id" : pro.parent_id,"store_id" : pro.store_edit_id , "category_photo" : pro.category_photo};
		   var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/category/'+pro.category_id,
            data:  pro.category_data,
            headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
           request.success(function (data) { 
            pro.data = data;
            if(data.status_code == 1){

                    if(data.status_text == 'Success') { toastr.success(data.message, 'Success'); }
            else { toastr.error(data.message, 'Error'); return false; }


		 
              location.reload();	
			}
			else{
             toastr.error(data.message, 'Error'); 
			}			
        })
		.error(function (data, status, headers, config) {  
             toastr.error(data.message, 'Error');
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		 }
		
	
 	});
	
	
	
	//======================================================VIEW ITEMS LIST CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('itemsController', function ($http,$scope,$window,toastr,$location) {
		var auth_user_id = document.getElementById('auth_user_id').value; 
		 var pro = this; 
			  $scope.url = window.location.href;
 
 var value = $scope.url;
 var parts = value.split('/');
 var index = parts.length - 2;
 var str = parts[index];
 pro.store_id = str.replace("#", ""); 
   
          
			 
       
        $('#loading').css('display', 'block'); 
		console.log(APP_URL+'/api/v1/items?store_id='+pro.store_id+'&auth_user_id='+auth_user_id+'&status=any');
		var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/items?store_id='+pro.store_id+'&auth_user_id='+auth_user_id+'&status=any',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
			
			
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
	
	
	
	// FUNCTION FOR CHANGE STATUS OF ITEM  ============================================================== clean done
	pro.switchStatus = function(value, itemId) {
		
		pro.item_id = itemId;
		pro.status_value = value;
		pro.item_status_value = { "item_active_status":pro.status_value};
		var request = $http({
                    method: "PUT",
                    url: APP_URL+'/api/v1/items/item-active-status/update/'+pro.item_id,
                    data: pro.item_status_value,
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
					if(data.status_code == 1){
			        toastr.success(data.message,'Success!');
					}
					else{toastr.error(data.message,'Error!');}
                       //location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
	};
	
	// FUNCTION FOR DELETE ITEM  ============================================================== clean done
	pro.deleteItem = function(itemId) {
		    
			$scope.item_id = itemId;
			
			if (confirm("Are you sure?")) {
		        var request = $http({
                    method: "DELETE",
                    url: APP_URL+'/api/v1/items/'+$scope.item_id,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    pro.data = data;
			        toastr.success(data.message,'Success!');
                       location.reload();					
			    })
				.error(function (data, status, headers, config) { 
			        document.getElementById("res").value = JSON.stringify(data); 
				    toastr.error('Error Occurs','Error!');
                }); 
		    }
		
        };
		
		
		 // FUNCTION FOR PAGINATION  ============================================================== clean done
		pro.pagination = function(api_url){ 
			 if(api_url == null || api_url== ''){
				 return false;
			 }
			 else{
			 $('#loading').css('display', 'block'); 
		
		var request = $http({
            method: "GET",
            url: api_url+'&auth_user_id='+auth_user_id+'&status=any',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {
            pro.products = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) { 
		     document.getElementById("res").value = JSON.stringify(data);
        });       
		
		} 
		}
  
	
 	});
	
	
	
	
	
	
