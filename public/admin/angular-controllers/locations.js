 //==========================================================LOCATION CONTROLLER=======================================================================================================
 //====================================================================================================================================================================================
 app.controller('locationController', function($http, $scope, $window, $log, $q, $timeout, toastr) {

     var pro = this;
     $('#loading').css('display', 'block');

     var request = $http({
         method: "GET",
         url: APP_URL + '/api/v1/locations',
         data: '',
         headers: {
             'Accept': 'application/json'
         }
     });

     /* Check whether the HTTP Request is successful or not. */
     request.success(function(data) {
             pro.locations = data;
             $('#loading').css('display', 'none');
             document.getElementById("res").value = JSON.stringify(data);
         })
         .error(function(data, status, headers, config) {
             document.getElementById("res").value = JSON.stringify(data);
         });



     // FUNCTION FOR PAGINATION  ============================================================== clean done
     pro.pagination = function(api_url) {
         if (api_url == null || api_url == '') {
             return false;
         } else {
             $('#loading').css('display', 'block');

             var request = $http({
                 method: "GET",
                 url: api_url,
                 data: '',
                 headers: {
                     'Accept': 'application/json'
                 }
             });

             /* Check whether the HTTP Request is successful or not. */
             request.success(function(data) {
                     pro.locations = data;
                     $('#loading').css('display', 'none');
                     document.getElementById("res").value = JSON.stringify(data);
                 })
                 .error(function(data, status, headers, config) {
                     document.getElementById("res").value = JSON.stringify(data);
                 });

         }
     }




     // FUNCTION FOR DELETE LOCATION  ============================================================== clean done
     pro.deleteLocationId = function(locationId) {

         $scope.location_id = locationId;

         if (confirm("Are you sure?")) {
             var request = $http({
                 method: "DELETE",
                 url: APP_URL + '/api/v1/locations/' + $scope.location_id,
                 data: '',
                 headers: {
                     'Accept': 'application/json'
                 }
             });

             /* Check whether the HTTP Request is successful or not. */
             request.success(function(data) {
                     pro.data = data;
                     if (data.status_code == 1) {
                         toastr.success(data.message, 'Success!');
                         location.reload();
                     } else {
                         toastr.error(data.message, 'Error!');
                     }
                 })
                 .error(function(data, status, headers, config) {
                     document.getElementById("res").value = JSON.stringify(data);
                     toastr.error('Error Occurs', 'Error!');
                 });
         }

     };


     //====================FUNCTION FOR GET PARENT LOCATION API==========================================================================
     pro.locationSearch = function(query) {
         return $http.get(APP_URL + "/api/v1/locations?parents_only=true&search=" + query, {
                 params: {
                     q: query
                 }
             })
             .then(function(response) {
                 return response.data.data.data;
             })
     };

     pro.selectedLocationChange = function(data) {
         $log.info('Item changed to ' + JSON.stringify(data));
         $scope.location_id = data.location_id;
     }

     pro.searchLocationChange = function(text) {
         $scope.location_id = '';
     };

     //========= FUNCTIONS FOR STORE A LOCATION=============================================================================================== 
     pro.storeLocation = function() {

         pro.location_title = $('#location_title').val();
         pro.parent_location_id = $('#location_id').val();
         pro.location_data = {
             title: pro.location_title,
             parent_id: pro.parent_location_id
         };
         var request = $http({
             method: "POST",
             url: APP_URL + '/api/v1/locations',
             data: pro.location_data,
             headers: {
                 'Accept': 'application/json'
             }
         });

         /* Check whether the HTTP Request is successful or not. */
         request.success(function(data) {
                 pro.data = data;
                 toastr.success(data.message, 'Success');
                 location.reload();
                 document.getElementById("res").value = JSON.stringify(data);
             })
             .error(function(data, status, headers, config) {
                 toastr.error(data.message, 'Error');
                 document.getElementById("res").value = JSON.stringify(data);
             });
     }




     //========= FUNCTIONS FOR EDIT  A LOCATION FORM=============================================================================================== 
     pro.editLocation = function(id, title, parent_id, parent_title) {
         pro.location_id = id;
         pro.location_title = title;
         pro.parent_id = parent_id;
         pro.parent_location_title = parent_title;
         $('#edit').modal('show');
     }



     pro.editLocationSearch = function(query) {
         return $http.get(APP_URL + "/api/v1/locations?parents_only=true&exclude_id=" + pro.location_id + "&search=" + query, {
                 params: {
                     q: query
                 }
             })
             .then(function(response) {
                 return response.data.data.data;
             })
     };


     pro.selectedEditLocationChange = function(data) {
         $log.info('Item changed to ' + JSON.stringify(data));
         pro.parent_location_id = data.location_id;
         if (pro.parent_location_id != undefined || pro.parent_location_id != null) {
             pro.parent_id = pro.parent_location_id;
         }
     }

     pro.searchEditLocationChange = function(text) {
         pro.parent_id = '';
     };

     //========= FUNCTIONS FOR UPDATE A LOCATIONS=============================================================================================== 
     pro.updateLocation = function(id) {
         pro.location_id = id;
         pro.location_title = $('#location_edit_title').val();
         pro.parent_id = $('#location_edit_id').val();
         pro.location_data = {
             "title": pro.location_title,
             "parent_id": pro.parent_id
         };
         //alert(JSON.sringify(pro.location_data));
         var request = $http({
             method: "PUT",
             url: APP_URL + '/api/v1/locations/' + pro.location_id,
             data: pro.location_data,
             headers: {
                 'Accept': 'application/json'
             }
         });

         /* Check whether the HTTP Request is successful or not. */
         request.success(function(data) {
                 pro.data = data;
                 if (data.status_code == 1) {
                     toastr.success(data.message, 'Success');
                     location.reload();
                 } else {
                     toastr.error(data.message, 'Error');
                 }

             })
             .error(function(data, status, headers, config) {
                 toastr.error(data.message, 'Error');
                 document.getElementById("res").value = JSON.stringify(data);
             });
     }

 });