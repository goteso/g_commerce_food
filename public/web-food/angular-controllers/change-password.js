
app.controller('passwordController', function($http,$scope,toastr, $window) {
	 
	
	$scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
     $scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
     $scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
     $scope.customerDataEmail = localStorage.getItem('login_customer_email');

     //$scope.customerDataId = localStorage.getItem('customerDataId');
     if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
         console.log("Login Not Set");
         return false;
     } else {  
	 
	 
	 $scope.changePassword = function(){
	 
	$scope.old_password = $('#old_password').val();
	$scope.new_password = $('#new_password').val();
	$scope.confirm_new_password = $('#confirm_new_password').val();
	
	
		
	if($scope.new_password != $scope.confirm_new_password){
		 toastr.error('Password does not match', 'Error');
		 return false;
	}
	
	else{
	$scope.passwordData = { "user_id":$scope.customerDataId,   "new_password":$scope.new_password,   "old_password":$scope.old_password};
	 var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/change-password',
            data:  $scope.passwordData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
			
			if(data.status_code == 1){ 
                toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');   
            
			 location.reload();
			}
			else{
      // $window.location.href = APP_URL+"/user_login";
	   toastr.error(data.message, 'Error');
			 
			}
			document.getElementById("res").value = JSON.stringify(data);
              
        })
		.error(function (data, status, headers, config) {
			  
		$window.location.href = APP_URL+"/admin_login";
        });  
	}
	}
	
	 }
});  
 

 