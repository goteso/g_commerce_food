 
 app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : APP_URL+"/web-index.html",
        controller : "indexController"
    })
    .when("/stores", {
        templateUrl : APP_URL+"/web-stores.html",
        controller : "storesController"
    })
    .when("/items", {
        templateUrl : APP_URL+"/cart-items.html",
        controller : "orderController as main"
    })
    .when("/checkout", {
        templateUrl : APP_URL+"/checkout.html",
        controller : "checkoutController as main"
    })
});
		
		
		
		 

		
app.factory('storeData', function() {
			 var sData = {}
			 function set(data){
				 sData = data;
			 }
			 function get(){
				 return sData;
			 }
			 return{
				 set:set,
				 get:get
			 }
		});
		
		
		
		app.controller('indexController', function ($http, $scope, $window,toastr,$log,storeData,$location) {
			localStorage.setItem('item_type', '');
			//====================FUNCTION FOR GET FEATURED STORES IMAGES ==========================================================================
			
			$scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
			$scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
			$scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
			$scope.customerDataEmail = localStorage.getItem('login_customer_email');
			  $scope.storesData = localStorage.getItem('');
			  
			  
			 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?fetured=1&per_page=6',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.featuredRest  = data.data.data;
            $('#loading').css('display', 'none');  
		console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
		
		
				//====================FUNCTION FOR GET AREA API==========================================================================
		$scope.areaSearch = function(query) { 
            return $http.get(APP_URL + "/api/v1/areas?search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
			console.log(JSON.stringify(response.data.data.data));
                return response.data.data.data;
				
            })
        };
  
        $scope.selectedAreaChange = function(data) { 
		 localStorage.setItem('area_title', '');
            $log.info('Item changed to ' + JSON.stringify(data)); 
			 $scope.area_title = data.title; 
                 $scope.area_id = data.area_id;  
				 $scope.area_lat = data.latitude; 
				 $scope.area_lon = data.longitude;  
				  localStorage.setItem('area_title', $scope.area_title);
        } 
		
		 $scope.searchAreaChange = function(text){  
				   $scope.area_id = ''; 
			};
		
		
		
		
		//====================FUNCTION FOR GET CUSINIES API==========================================================================
		$scope.cuisinesSearch = function(query) { 
            return $http.get(APP_URL + "/api/v1/store-filter-options?type=cuisines&search_text=" + query , {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
			console.log(JSON.stringify(response.data.data.data));
                return response.data.data.data;
				
            })
        };
  
        $scope.selectedCuisinesChange = function(data) { 
            $log.info('Item changed to ' + JSON.stringify(data)); 
                 $scope.cuisines = data.value;  
				
        } 
		
		 $scope.searchCuisinesChange = function(text){			 
				   $scope.cuisines = text; 
			};
		
		
		$scope.catering_status = 0;
		
		if($scope.catering_status == 0){
			$scope.item_type_value = 'regular';
		}
		
		else{
		$scope.item_type_value = 'catering';}
				localStorage.setItem('item_type', $scope.item_type_value);
		$scope.class = "active";
		$scope.class1 = " ";
		
		$scope.getItemType = function(value){
				localStorage.setItem('item_type', value);
		   // alert(value);
			 //localStorage.setItem('order_type', '');
			 localStorage.getItem('item_type')
			if(value == 'catering'){
				
				$scope.catering_status = 1;
				
				 localStorage.setItem('order_type', value);
				$scope.order_type = localStorage.getItem('order_type');
				$scope.class = " ";
				$scope.class1 = "active";
			} 
			else{
				$scope.catering_status = 0;
				$scope.class = "active";
	          	$scope.class1 = " ";
			}
			
			
		}
		
		
		
		
		
		$scope.searchStore = function () { 
		 
		
		 if($scope.area_id == '' || $scope.area_id == undefined || $scope.area_id == null){
				toastr.error('Select Area','Error!');
				return;
			}
			
			
			else{
				 
			 var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/stores?page=1&orderby=distance&latitude='+$scope.area_lat+'&longitude='+$scope.area_lon+'&exclude_busy_stores=false&catering_status='+$scope.catering_status+'&meta_filters=cuisines:'+$scope.cuisines,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.storeData  = data.data; 
      $scope.storeDetail = {"store":$scope.storeData, "area_lat":$scope.area_lat, "area_lon":$scope.area_lon, "catering_status":$scope.catering_status,"meta_filters" :$scope.cuisines}
      
      	localStorage.setItem('store_data', angular.toJson($scope.storeDetail));
	    storeData.set($scope.storeDetail); 
	     
	    	
	   $location.path("/stores");
    
	
            $('#loading').css('display', 'none');  
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {  
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
		}
		};
	
	
	 $scope.getItems = function(store,url){ 
		   
			   localStorage.setItem('store_id', JSON.stringify(store.store_id));
			   localStorage.setItem('store_title', JSON.stringify(store.store_title));
			   localStorage.setItem('store_photo', JSON.stringify(store.store_photo));
			   $location.path(url);
		   }
		   
	
		
		
		});
		
 //====================================================== REVIEW CONTROLLER=================================================================================
//==============================================================================================================================================================
 
	
	app.controller('storesController', function ($http, $scope, $window,toastr, storeData,$location) {
 
 
 $scope.area_title = localStorage.getItem('area_title');
 	$scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
			$scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
			$scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
			$scope.customerDataEmail = localStorage.getItem('login_customer_email');
		 
		   $('#loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 
		 
		   $scope.storesData = localStorage.getItem('store_data');
	    console.log($scope.area_title);
		 $scope.storesListData = JSON.parse($scope.storesData);
		 $scope.storesList = $scope.storesListData.store;
		 
		  $scope.storesListData1 = storeData.get();
		 $scope.cuisines_meta = $scope.storesListData.meta_filters; 
		 console.log($scope.storesListData);
       console.log($scope.storesList);

		
  $('#loading').css('display', 'none'); 
 $('.innerData').css('display', 'block'); 
     
		
		 //================================ FUNCTION FOR GET CUSINIES FILTER LIST  ================================================================================
	 
    	   
		        var request = $http({
                    method: "GET",
                    url: APP_URL+'/api/v1/store-filter-options?type=cuisines',
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.cusiniesList = data.data;
					 				
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data));  
                }); 
		   
		   
		   
		   
		   
		   
		   	   $scope.cuisinesFilerData = []; 
			    $scope.cuisinesValue =  $scope.cuisines_meta;
				   $scope.cuisinesFilerData.push($scope.cuisinesValue);   
			     //$scope.c = { "check" : true}; 
		   $scope.cuisinesFiler = function(value2,value1){ 
				  //$scope.c = {"check" : value}; 
				    var already = '';
			   if(value2 == true){
				   
				   
				    angular.forEach($scope.cuisinesFilerData, function(value){ 
						
				   if(value1 == value){ 
				    already = 'yes';
				   }
					});
					
					if(already == 'yes'){ 
						return;
					}
				   else{
				   $scope.cuisinesValue =  value1;
				   $scope.cuisinesFilerData.push($scope.cuisinesValue);  
				   $scope.storesFilter();
				   }
					 
			   }
			   else{
				    
				   $scope.cuisinesValue = value1;
				   $scope.cuisinesFilerData.splice($scope.cuisinesValue, 1);  
				    $scope.storesFilter();
			   }
			     
			  
		
		
		   }
		   
		   
		   
		   
		    //================================ FUNCTION FOR GET FILTER STORE LIST  ================================================================================
		   $scope.storesFilter = function(){
			  
			    $('#loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 
		     var request = $http({
                    method: "GET",
                    url: APP_URL+'/api/v1/stores?page=1&orderby=distance&latitude='+$scope.storesListData.area_lat+'&longitude='+$scope.storesListData.area_lon+'&exclude_busy_stores=false&catering_status='+$scope.storesListData.catering_status+'&meta_filters=cuisines:'+$scope.cuisinesFilerData,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.storesList = data.data;
					 $('#loading').css('display', 'none'); 
                       $('.innerData').css('display', 'block'); 
					console.log(JSON.stringify($scope.storesList));
					 				
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
	}
	
	
		 
		   
		   //================================ FUNCTION FOR GET SORTNG STORE LIST  BY POPULAR STORE================================================================================
		   $scope.popularSort = function(){ 
		    $('#loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 
		 var request = $http({
                    method: "GET",
                    url: APP_URL+'/api/v1/stores?page=1&orderby=popular_count&latitude='+$scope.storesListData.area_lat+'&longitude='+$scope.storesListData.area_lon+'&exclude_busy_stores=false&catering_status='+$scope.storesListData.catering_status+'&meta_filters=cuisines:'+$scope.cuisinesFilerData,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.storesList = data.data;
					 $('#loading').css('display', 'none'); 
 $('.innerData').css('display', 'block'); 
					console.log(JSON.stringify($scope.storesList));
					 				
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		   }
		   
		   
		   //================================ FUNCTION FOR GET SOTING STORE LIST  BY NEARBY LOCATION================================================================================
		   $scope.nearBySort = function(){
			    $('#loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 
 
		 var request = $http({
                    method: "GET",
                    url: APP_URL+'/api/v1/stores?page=1&orderby=distance&latitude='+$scope.storesListData.area_lat+'&longitude='+$scope.storesListData.area_lon+'&exclude_busy_stores=false&catering_status='+$scope.storesListData.catering_status+'&meta_filters=cuisines:'+$scope.cuisinesFilerData,
                    data:  '',
                    headers: { 'Accept':'application/json' }
                });

               /* Check whether the HTTP Request is successful or not. */
                request.success(function (data) { 
				    $scope.storesList = data.data;
					 $('#loading').css('display', 'none'); 
 $('.innerData').css('display', 'block'); 
					console.log(JSON.stringify($scope.storesList));
					 				
			    })
				.error(function (data, status, headers, config) { 
			        console.log(JSON.stringify(data)); 
				    toastr.error('Error Occurs','Error!');
                }); 
		
		   }
		   
		   
		   
		   
		   $scope.getStoreItems = function(store,url){ 
		  
		   
		   if(store.busy_status == 0){
			   
			   $scope.store_status = store.busy_status_title;
		   }
			   
			   else{
			   if(store.close_status == 0){
				   $scope.store_status = store.close_status_title;
			   }
			   else{
				   $scope.store_status = store.close_status_title;
			   } 
		   }
		   
			   localStorage.setItem('store_id', JSON.stringify(store.store_id));
			   localStorage.setItem('store_title', JSON.stringify(store.store_title));
			   localStorage.setItem('store_photo', JSON.stringify(store.store_photo));
			   localStorage.setItem('store_address', JSON.stringify(store.address));
			    localStorage.setItem('store_status', JSON.stringify($scope.store_status));
				
			 
			 
			   $location.path(url);
			   
			   
		   }
	
		
	});
		
		
		
		
		
		
		 
app.factory('cartStorage', function() {
    var _cart = {
        items: [], 
		payment:{},
		coupons:{},
    };
    var service = {
        get cart() {
            return _cart;
        }
    }
    return service;
});




		
		//==========================================================ADD ORDER CONTROLLER====================================================================================================
//==================================================================================================================================================================================
    app.controller('orderController', function($http,$scope,$window,$log, toastr, cartStorage,$location, $mdDialog) {
		
		
		    $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
			$scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
			$scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
			$scope.customerDataEmail = localStorage.getItem('login_customer_email');
			
     	 
		  $scope.store_id = localStorage.getItem('store_id');
		  $scope.store_title = JSON.parse(localStorage.getItem('store_title'));
		  $scope.store_photo =  JSON.parse(localStorage.getItem('store_photo'));
		    $scope.store_address =  JSON.parse(localStorage.getItem('store_address'));
			  $scope.store_status =  JSON.parse(localStorage.getItem('store_status')); 
		  console.log($scope.store_id);
		   console.log($scope.store_title);
		    console.log($scope.store_photo);
			  console.log($scope.store_address);
		    console.log($scope.store_status);
	//$scope.store_id = window.location.href.substr(window.location.href.lastIndexOf('?') + 1);
	  $scope.item_type = localStorage.getItem('item_type'); 
			 if($scope.store_id == '' || $scope.store_id == undefined || $scope.store_id == null || $scope.store_id == 'null'){
				 $location.path('/');
			 }
        var _this = this;
       
	    	
		$('#menuData').css('display', 'none'); 
		$('.menuData').css('display', 'none');
		 _this.cartStorage = cartStorage.cart;
        _this.total = 0;
		 $('#item-loading').css('display', 'block'); 
		//$(".promoCode").css('display', 'none');  
 
		
        $('.order-content').css('display', 'none');
		
		
	 
		
		//========= ONLOAD GET ADD ORDER FORM  VIA CALLING API==================================================================
		console.log(APP_URL+'/api/v1/category/?store_id='+$scope.store_id+'&include_items=true&include_meta=true&request_type=input_field&type='+$scope.item_type+'&exclude_empty_categories=true');
		 var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/category/?store_id='+$scope.store_id+'&include_items=true&include_meta=true&request_type=input_field&type='+$scope.item_type+'&exclude_empty_categories=true',
             data: '',
             headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
           //console.log(JSON.stringify(data)); 
			 _this.addOrder = data.data.data;
			 console.log(JSON.stringify(_this.addOrder)); 
  $('#item-loading').css('display', 'none'); 
 $('.menuData').css('display', 'block'); 		 
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
		
		
		
	//========= FUNCTIONS FOR GET OFFERS  LIST ===============================================================================================
    //===========================================================================================================================================
	
	  _this.offers = function(){ 
	   $('#offer-loading').css('display', 'block'); 
 $('.offerData').css('display', 'none'); 		
		  var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/coupons?store_id='+$scope.store_id+'&include_expired=false',
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.offersdata = data.data;
			  $('#offer-loading').css('display', 'none'); 
 $('.offerData').css('display', 'block'); 		
            console.log(JSON.stringify(data));   
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
	  };
	  
	  
	  
	  
	  //========= FUNCTIONS FOR GET REVIEW  LIST ===============================================================================================
    //===========================================================================================================================================
	
	  _this.reviews = function(){
		  $('#review-loading').css('display', 'block'); 
 $('.reviewData').css('display', 'none'); 		
		  var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/order-review?store_id='+$scope.store_id,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.reviewsdata = data.data;
			  $('#review-loading').css('display', 'none'); 
 $('.reviewData').css('display', 'block'); 		
            console.log(JSON.stringify(data));   
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
	  }
	  
	  
	  
	  //========= FUNCTIONS FOR GET OFFERS  LIST ===============================================================================================
    //===========================================================================================================================================
	
	  _this.info = function(){
		   $('#info-loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 		
		  var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/stores/'+$scope.store_id,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.infodata = data.data;
			  $('#info-loading').css('display', 'none'); 
 $('.innerData').css('display', 'block'); 		
            console.log(JSON.stringify(data));   
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
	  }
	
	
	 
	   
	/*	_this.itemsLocalData = localStorage.getItem('items_data');
       _this.paymentLocalData = localStorage.getItem('payment_data'); 
		if(_this.itemsLocalData){
		 _this.cartStorage.items= JSON.parse(_this.itemsLocalData);
         _this.cartStorage.payment= JSON.parse(_this.paymentLocalData);		 
		 
		  console.log(_this.cartStorage.items);
		}*/
		
		
		//_this.cartStorage.items = localStorage.getItem('items_data');
		//console.log(_this.cartStorage.items);
		//_this.cartStorage.payment = localStorage.getItem('payment_data');
     	 
		/*_this.paymentsData = localStorage.getItem('payment_data');
			 console.log(_this.paymentsData);
			   _this.cartStorage.payment = angular.toJson(_this.paymentsData.data);
			  //_this.cartStorage.items = _this.paymentsData.data[0].items;
			 //   console.log(_this.cartStorage.items);
			    console.log(_this.cartStorage.payment);
			   // _this.cartStorage.payment = _this.paymentsData; */
			 
		
       
			   $scope.couponsData = _this.cartStorage.coupons;  	 
			 $scope.loyalty_points = $scope.couponsData.loyalty_points;
			 if($scope.loyalty_points == 1){
				 _this.points = true;
			 }
			
		/* $scope.coupon_code = $scope.couponsData.coupon_code;  
			  _this.coupon_code_value = $scope.coupon_code;*/
			  
			  	 
				
				
		 
		
		
		
		//========= ONLOAD GET ADD ORDER FORM  VIA CALLING API========================================================================
		/* var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders/add/form/',
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. *
        request.success(function (data) { 
           //console.log(JSON.stringify(data)); 
			 _this.addOrder = data.categories_products;
            _this.addOrderMeta = data.order_meta_fields;
            _this.UserListData = data.order_meta_fields.fields.fields;
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
		
		*/
		  _this.points_title = localStorage.getItem('points_title'); 
	  _this.orderCalculate = function() { 
	   $('#payment-loading').css('display', 'block');  
	   $(".cart").hide();
  $(".main-cart").hide();	
 
			  $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				  
				  _this.cartStorage.coupons = $scope.order_items.data;
   
				  //localStorage.setItem('coupon_code_data', $scope.coupon_code); 
//localStorage.setItem('loyalty_points_data', $scope.loyalty_points);				  
				  console.log(JSON.stringify($scope.order_items));
				  	localStorage.setItem('items_data', JSON.stringify($scope.order_items));
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
					localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
				_this.points_title = _this.paymentData.points_title;
				
				localStorage.setItem('points_title', _this.points_title);
                $(".cart").hide();
                $(".main-cart").show();	
                $(".promoCode").show();	
                 $('#payment-loading').css('display', 'none');  			
				console.log(JSON.stringify(_this.paymentData));
				  _this.cartStorage.payment = _this.paymentData;
				 
				 console.log(JSON.stringify(_this.paymentData.data[0].items));
				 //localStorage.setItem('payment_data', JSON.stringify(_this.paymentData.data[0].data));
				  //localStorage.setItem('items_data', JSON.stringify(_this.paymentData.data[0].items));
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
		 
		}; 
		
		
		
		
		
	
		
		//========= FUNCTIONS FOR APPLY COUPON ====================================================================================
	//=================================================================================================================================
		
		 _this.applyCoupon = function() {
		
		 $scope.coupon_code = $('#coupon_code').val();
		 _this.coupon_code_value = $('#coupon_code').val();  
if($scope.coupon_code == '' || $scope.coupon_code == undefined ){
	return;
}	
		  $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				 
				 _this.cartStorage.coupons = $scope.order_items.data;
				 localStorage.setItem('coupon_code_data', $scope.coupon_code); 
localStorage.setItem('loyalty_points_data', $scope.loyalty_points);	
				 $('#payment-loading').css('display', 'block');
 $(".main-cart").hide();			
		_this.pointsDisabled = true; 
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
				localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
                $(".cart").hide();
                $(".main-cart").show();	
$('#payment-loading').css('display', 'none');  				
				console.log(JSON.stringify(_this.paymentData)); 
				_this.messagePadding = '2px 8px';
				_this.paymentData.couponMessage =  _this.paymentData.data[0].discount_response.message; 
				if(_this.paymentData.data[0].discount_response.status_code == 0){
					_this.messageBg = '#de8583';
				}
				else{
					_this.messageBg = '#e8f7d8';
				}
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
		
		 }
		 
		 //========= FUNCTIONS FOR APPLY COUPON ====================================================================================
	//=================================================================================================================================
		
		 _this.cancelCoupon = function() { 
		 $('#payment-loading').css('display', 'block'); 
$(".main-cart").hide();			 
		 _this.paymentData.couponMessage = '';
		  $('#coupon_code').val('');
		 $scope.coupon_code = ''; 
		_this.coupon_code_value = $scope.coupon_code;  
		
		
		_this.pointsDisabled = false;
		 
		  
		/*_this.coupon_code_value = ''; 
		 $scope.coupon_code = '';*/
		  $scope.order_items = _this.cartStorage.items;
		  
		  
			$scope.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				 
			 _this.cartStorage.coupons = $scope.order_items.data;
				 localStorage.setItem('coupon_code_data', $scope.coupon_code); 
localStorage.setItem('loyalty_points_data', $scope.loyalty_points);	
				 
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
				 $(".cart").hide();
				_this.messagePadding = '0px';
				_this.messageBg = '';
				_this.paymentData.couponMessage = ''; 
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
				
				localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
                $(".cart").hide();
                $(".main-cart").show();		
$('#payment-loading').css('display', 'none');  				
				console.log(JSON.stringify(_this.paymentData)); 
				
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
		
		 }
		 
		 
		 
		 //========= FUNCTIONS FOR APPLY Points ====================================================================================
	//=================================================================================================================================
		
		 _this.changePonits = function() { 
		 $('#payment-loading').css('display', 'block');  
		 if (_this.points == true){
		 $scope.loyalty_points = 1;
		 
		 $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code, "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				  _this.cartStorage.coupons = $scope.order_items.data;
				  
				  localStorage.setItem('coupon_code_data', $scope.coupon_code); 
localStorage.setItem('loyalty_points_data', $scope.loyalty_points);	
 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
				
				localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
                $(".cart").hide();
                $(".main-cart").show();	
$('#payment-loading').css('display', 'none');  				
				console.log(JSON.stringify(_this.paymentData)); 
				if(_this.paymentData.data[0].discount_response.status_code == 0){
					_this.messagePadding1 = '4px 8px';
					_this.pointMessageBg = '#de8583'; 
				_this.paymentData.pointsMessage =  _this.paymentData.data[0].discount_response.message; 
				}
				else{
					_this.messagePadding = '4px 8px';
					_this.pointMessageBg = '#e8f7d8';
				_this.paymentData.pointsMessage =  _this.paymentData.data[0].discount_response.message; 
				}
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
			}
          else{
            $scope.loyalty_points = 0;
			$scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code, "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				  _this.cartStorage.coupons = $scope.order_items.data;
				  
				  localStorage.setItem('coupon_code_data', $scope.coupon_code); 
          localStorage.setItem('loyalty_points_data', $scope.loyalty_points);	
 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
				
							
				if(_this.paymentData.data[0].discount_response.status_code == 0){
					_this.messagePadding1 = '0px';
					_this.pointMessageBg = 'transparent'; 
				_this.paymentData.pointsMessage =  ''; 
				}
				else{
					_this.messagePadding = '0px';
					_this.pointMessageBg = '#transparent';
				_this.paymentData.pointsMessage =  ''; 
				}
				console.log(JSON.stringify(_this.paymentData)); 
				localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
                $(".cart").hide();
                $(".main-cart").show();	
$('#payment-loading').css('display', 'none');  	
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
          } 
		  
		
		 }
		 
		 
		//========= FUNCTIONS FOR ADD PRODUCTS TO CART ====================================================================================
	//=================================================================================================================================
	 
		 _this.showConfirm = function(ev,item) {
							// Appending dialog to document.body to cover sidenav in docs app
							var confirm = $mdDialog.confirm()
								  .title('Would you like to Change store?')
								  .textContent('All of the store items remove from cart.')
								  .ariaLabel('Lucky day')
								  .targetEvent(ev)
								  .ok('Confirm!')
								  .cancel('Cancel');

							$mdDialog.show(confirm).then(function() {
							  _this.cartStorage.items = [];  
							  _this.addToCart();
							});
						  };
		 

	//========= FUNCTIONS FOR ADD PRODUCTS TO CART ====================================================================================
	//=================================================================================================================================
	
	
        _this.addToCart = function(item) {
            $scope.item = item;
            $scope.id = item.item_id;
			 $scope.minimum_pax_value = item.minimum_pax;
			 
		  item.order_item_quantity = item.minimum_pax;
            $scope.item_id = {"item_id": $scope.id } 
			if(item.type == 'catering'){
				$('#cateringItemModal').modal('show');
				
				_this.item_data = item; 
			}
			else{
			 //CHECK VARIANTS EXIST OR NOT=====================================================================
			  
			//IF DOES NOT EXIT-----------------------------------------------------------------
            if (item.variant_exist == '0') {
                var alreadyInCart = '';
                var cartIndex = 0;

                if (_this.cartStorage.items.length == 0) {
					 _this.coupon_code_value = $('#coupon_code').val(); 
                    item.single_quantity_item_price = item.item_price;
                    item.quantity = 1;
					item.order_item_quantity = 1;
				    item.variants = [];
				
                    _this.cartStorage.items.push(item);  
				 _this.orderCalculate();
                    return;
				}

                for (var i = 0; i < _this.cartStorage.items.length; i++) {
                    if (_this.cartStorage.items[i].item_id == item.item_id) {
                        alreadyInCart = 'YES';
                        cartIndex = i;
                    }
                }

				
                if (alreadyInCart == 'YES') {
                     _this.cartStorage.items[cartIndex].quantity = _this.cartStorage.items[cartIndex].quantity + 1; 
					  _this.cartStorage.items[cartIndex].order_item_quantity = _this.cartStorage.items[cartIndex].order_item_quantity + 1; 
				 _this.orderCalculate();
                    return;
                } 
				
				else {
                   item.quantity = 1; 
					item.order_item_quantity = 1;
				    item.variants = [];
                    item.single_quantity_item_price = item.item_price; 
						if(_this.cartStorage.items[0].store_id == item.store_id ){
                    _this.cartStorage.items.push(item);  
					
					 _this.orderCalculate();
						}
						else{ 
		                   _this.showConfirm(item);
						}
					
                    document.getElementById("res").value = JSON.stringify(_this.cartStorage.items);
				
				   
                }
				
            }
 
			//IF VARIANTS EXIT------------------------------------------------------------------
			else { 
			
				//CALL API FOR GET PRODUCT VARIANTS===========================
				var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/items-variants?orderby=item_variant_value_title&item='+$scope.id,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.variantsdata = data.data;
            console.log(JSON.stringify(data)); 
            //$('#loading').css('display', 'none');
			 $("#variantsModal").modal('show');
                     $scope.variantsArray = [];
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
				 
            }

      
			
			}
		}
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
		 //==========================function for increase or decrease pax value============================

		 _this.increaseItemPax = function(item){ 
		    	  
			 item.order_item_quantity = item.order_item_quantity +1;  
		 	 //alert(item.minimum_pax ++);
		 	_this.item_data.order_item_quantity = item.order_item_quantity; 
			console.log(JSON.stringify(item));
		 }

 _this.decreaseItemPax = function(item){ 
 	 if(item.order_item_quantity <= item.minimum_pax){
 		return;
 	} 
		 	item.order_item_quantity =  item.order_item_quantity -1;  
		 	_this.item_data.order_item_quantity = item.order_item_quantity;
		 }
		 
		 
			 
			 //CHECK VARIANTS EXIST OR NOT=====================================================================
			 _this.variantCheck = function(item){
				  
console.log(JSON.stringify(item));				 
			 	$('#cateringItemModal').modal('hide');
			//IF DOES NOT EXIT-----------------------------------------------------------------
            if (item.variant_exist == '0') {
                var alreadyInCart = '';
                var cartIndex = 0;

                if (_this.cartStorage.items.length == 0) {
					 _this.coupon_code_value = $('#coupon_code').val(); 
                    item.single_quantity_item_price = item.item_price;
					
                    item.quantity = item.order_item_quantity;
					 item.order_item_quantity = item.order_item_quantity;
					
					console.log(JSON.stringify(item.order_item_quantity));	
				    item.variants = [];
				
                    _this.cartStorage.items.push(item);  
				 _this.orderCalculate();
                    return;
				}

                for (var i = 0; i < _this.cartStorage.items.length; i++) {
                    if (_this.cartStorage.items[i].item_id == item.item_id) {
                        alreadyInCart = 'YES';
                        cartIndex = i;
                    }
                }

				
                if (alreadyInCart == 'YES') {
					console.log(JSON.stringify(item.order_item_quantity));	
                     _this.cartStorage.items[cartIndex].quantity = _this.cartStorage.items[cartIndex].quantity; 
					  _this.cartStorage.items[cartIndex].order_item_quantity = _this.cartStorage.items[cartIndex].order_item_quantity; 
				 _this.orderCalculate();
                    return;
                } 
				
				else {
					console.log(JSON.stringify(item.order_item_quantity));	
                    item.quantity = item.order_item_quantity; 
					 item.order_item_quantity = item.order_item_quantity;
				    item.variants = [];
                    item.single_quantity_item_price = item.item_price; 
						if(_this.cartStorage.items[0].store_id == item.store_id ){
                    _this.cartStorage.items.push(item);  
					
					 _this.orderCalculate();
						}
						else{ 
		                   _this.showConfirm(item);
						}
					
                    document.getElementById("res").value = JSON.stringify(_this.cartStorage.items);
				
				   
                }
				
            }
 
			//IF VARIANTS EXIT------------------------------------------------------------------
			else { 
			
				//CALL API FOR GET PRODUCT VARIANTS===========================
				var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/items-variants?orderby=item_variant_value_title&item='+$scope.id,
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			 _this.variantsdata = data.data;
            console.log(JSON.stringify(data)); 
            //$('#loading').css('display', 'none');
			 $("#variantsModal").modal('show');
                     $scope.variantsArray = [];
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
			
				 
            }

        };

		
		  //$scope.variantsSelectedTitle = [];
		
		
		//FUNCTION FOR SHOW PRODUCT VARIANTS AS CHECKBOXES=======================================================================
       _this.getVariantValue = function(variants) { 
            $scope.variantsSelectedTitle = []; 
            var alreadyInVariants = ''; 
            var variantsIndex = 0; 
            $scope.variant = variants; 
            if ($scope.variantsArray.length == 0) {  
                $scope.variantsArray.push(variants); 
				console.log(JSON.stringify($scope.variantsArray)); 
                return;
            }

            for (var i = 0; i < $scope.variantsArray.length; i++) {  
			 
                if ($scope.variantsArray[i].item_variant_value_id == variants.item_variant_value_id) {  
                    alreadyInVariants = 'YES';
                    variantsIndex = i;
                }
            }

            if (alreadyInVariants == 'YES') {
				 
                $scope.variantsArray.splice(variantsIndex, 1); 
				console.log(JSON.stringify($scope.variantsArray)); 
                return;
				
            } 
			else { 
			
   			 
                $scope.variantsArray.push(variants); 
				
				console.log(JSON.stringify($scope.variantsArray)); 
                return;
				
            }
		}

		
		
		 //FUNCTION FOR SHOW PRODUCT VARIANTS AS RADIO BUTTONS=====================================================================
        _this.getVariantValue2 = function(variants) { 
		    $scope.variantsSelectedTitle = [];
            //alert(JSON.stringify(variants));
            var alreadyInVariants = '';
            var variantsIndex = 0; 
            if ($scope.variantsArray.length == 0) { 
               $scope.variantsArray.push(variants); 
			   console.log($scope.variant_item_price);
                return;
            }

            for (var i = 0; i < $scope.variantsArray.length; i++) { 
                if ($scope.variantsArray[i].product_variant_type_id == variants.product_variant_type_id) {
                   alreadyInVariants = 'YES';
                   variantsIndex = i;
                }
            }

            if(alreadyInVariants == 'YES') {
				 
                $scope.variantsArray.splice(variantsIndex, 1); 
                $scope.variantsArray.push(variants);   
                return;
            } 
			else {
				  $scope.variantsArray.push(variants); 
				 
                return;
            }
		}

        /* if (itemIndex > -1) {
                        _this.cartStorage.products.splice(itemIndex, 1);
    					_this.enableMe = true;
             }
    	 */

		 
		 

    $scope.variantsSelectedTitle = [];
		
		
	//FUNCTION FOR ADD PRODUCT VARIANTS TO CART============================================================================ 
        _this.addVariant = function() {
            // alert(JSON.stringify($scope.product)); 
            //$scope.value =  $('#v').val();
            //alert($scope.value);
            if ($scope.variantsArray == '') {
                alert('Please choose atleast one variant');
            } 
		    else {
			    $scope.item.variants = $scope.variantsArray;
                $scope.item.quantity = 1;
				$scope.item.order_item_quantity = 1;
                var item_price = $scope.item.item_price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].item_price) {
                        item_price = $scope.variantsArray[j].item_price;
                    }
                }

                var productToAdd = angular.copy($scope.item); 
                productToAdd.item_price = item_price;
                for (var j = 0; j < $scope.variantsArray.length; j++) {
                    if ($scope.variantsArray[j].item_variant_price_difference) {
                        productToAdd.item_price = parseInt(productToAdd.item_price) + parseInt($scope.variantsArray[j].item_variant_price_difference);
                    }
                }
                productToAdd.single_quantity_item_price = productToAdd.item_price  ;
         
                for (var j = 0; j < $scope.variantsArray.length; j++) {
					 
                    if ($scope.item.item_id == $scope.variantsArray[j].item_id) {
						 
                        $scope.variantsSelectedTitle.push($scope.variantsArray[j].item_variant_value_title);
						 
                    }
					
                    $scope.variantsSelectedTitleData = $scope.variantsSelectedTitle.toString();
					
					localStorage.setItem('variant_title', $scope.variantsSelectedTitleData);
                }
			
			        
		   
                _this.cartStorage.items.push(productToAdd);
				_this.orderCalculate();
                $("#variantsModal").modal('hide');
			
			    
            }
        }
	
		 




 



	//========= FUNCTIONS FOR GET DATA SEARCH LIST VIA API===============================================================================================
    //===========================================================================================================================================
        _this.querySearch = function(query, field) {  
            return $http.get(APP_URL + "/" + field.api, {
                params: {
                    q: query
                }
            })
            .then(function(response) { 
                return response.data.data.data;
            })
        };

	 
	
        _this.selectedValueChange = function(data,field ) { 
            $log.info('Data changed to ' + JSON.stringify(data));
            if (field.identifier == 'customer_id') {
                 field.value = data.value; 
            } 
            $scope.selected_values = '';
        }
		
		
	  
	  
	  
	  
	  
	  
	  _this.proceed = function(value){ 
		  
		  if($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined')
		  {
			  $('#loginModal').modal('show');
		  }
		  else{ 
		  
			   $location.path(value);
		  }
		  
	  }
	  
	  
	  _this.register = function(){
		  $('#registerModal').modal('show');
		  $('#loginModal').modal('hide');
	  }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 $scope.carouselTimer = 5000;
	 
	 
	 //==============================FUNCTION FOR GET ITEM DETIAL======================================
	 
	 $scope.itemInfo = function(item_id){ 
		 	var request = $http({
                method: "GET",
                url: APP_URL+'/api/v1/items/'+item_id,
                data: '',
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                $scope.singleItemData = data;  
                
             
            $scope.init();
        
        
                	  $("#itemInfoModal").modal('show');			
				console.log(JSON.stringify(data)); 
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
            
            
	 }
            
            $scope.init = function() {
        var options = {
            center: new google.maps.LatLng(40.7127837, -74.00594130000002),
            zoom: 13,
            disableDefaultUI: true    
        }
        $scope.map = new google.maps.Map(
            document.getElementById("map_canvas"), options
        );
            
            /* $timeout(function(){
      
            var latlng = new google.maps.LatLng(35.7042995, 139.7597564);
            var myOptions = {
                zoom: 8,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            $scope.map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); 
			
	 }*/
	 
            }
	

    });



//==========================================================CART CONTROLLER==========================================================================================================================
//========================================================================================================================================================================================================
    app.controller('cartController', function($http, cartStorage,$scope,$window, $log, $q, $timeout, toastr) {
		 
        var _this = this;
        _this.cartStorage = cartStorage.cart;
		 
	    //FUNCTION FOR INCREMENT item QUANTITY============================================================
        _this.increaseItemAmount = function(item) {
 $(".cart").hide();			
            item.price = item.base_price;
            var quantity = item.quantity++; 
			var quantity1 = item.order_item_quantity++;
		    $('#payment-loading').css('display', 'block');  
			  $('#payment-loading1').css('display', 'block');  
			  $scope.order_products = _this.cartStorage.items; 
			  
			  $scope.order_coupons = _this.cartStorage.coupons; 
			  
			   _this.paymentsData = localStorage.getItem('payment_data');
			   _this.cartStorage.payment = JSON.parse(_this.paymentsData);
			   _this.paymentData = _this.cartStorage.payment; 
			  console.log(JSON.stringify(_this.paymentData));
			  
			$scope.order_products.data = {"coupon_code":$scope.order_coupons.coupon_code,  "store_id":$scope.order_coupons.store_id, "loyalty_points":$scope.order_coupons.loyalty_points,"customer_id":$scope.order_coupons.customer_id,"items": $scope.order_products};
				 	
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();	
$('#payment-loading').css('display', 'none');
 $('#payment-loading1').css('display', 'none');    				
				console.log(JSON.stringify(_this.paymentData));
				_this.cartStorage.payment = _this.paymentData;
				localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
			document.getElementById('quantity').value = quantity;
            item.totalPrice = item.price * item.quantity;
		
        }

		
		//FUNCTION FOR DECREMENT PRODUCT QUANTITY IN CART============================================================
        _this.decreaseItemAmount = function(item) {
			if(item.order_item_quantity<= item.minimum_pax){
			return;}
			 $(".cart").hide();
			$('#payment-loading').css('display', 'block');
 $('#payment-loading1').css('display', 'block');  			
            item.quantity--;
			item.order_item_quantity--;
			 
		    $scope.order_products = _this.cartStorage.items;
			  $scope.order_coupons = _this.cartStorage.coupons; 
			  
			  _this.paymentData = _this.cartStorage.payment;
			$scope.order_products.data = {"coupon_code":$scope.order_coupons.coupon_code,  "store_id":$scope.order_coupons.store_id, "loyalty_points":$scope.order_coupons.loyalty_points,"customer_id":$scope.order_coupons.customer_id,"items": $scope.order_products}; 
			 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();		
$('#payment-loading').css('display', 'none');  			
 $('#payment-loading1').css('display', 'none');  	
				console.log(JSON.stringify(_this.paymentData));
				_this.cartStorage.payment = _this.paymentData;
				localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
				
            /* if (item.quantity <= 0) {
			    item.quantity = 0;
                item.addedToCart = false;
                item.showAddToCart = false;
                var itemIndex = _this.cartStorage.items.indexOf(item);
                
				if (itemIndex > -1) {
				    _this.cartStorage.items.splice(itemIndex, 1);
                    document.getElementById("res8").value = JSON.stringify(_this.cartStorage.items);
                    _this.enableMe = true;
				}
            }
			else */ if (item.order_item_quantity <= 0) {
			    item.order_item_quantity = 0;
                item.addedToCart = false;
                item.showAddToCart = false;
                var itemIndex = _this.cartStorage.items.indexOf(item);
                
				if (itemIndex > -1) {
				    _this.cartStorage.items.splice(itemIndex, 1);
                    document.getElementById("res8").value = JSON.stringify(_this.cartStorage.items);
                    _this.enableMe = true;
				}
            }
			
        }

		
		
		//FUNCTION FOR REMOVE PRODUCT FROM CART============================================================
        _this.removeFromCart = function(product) {
			 $(".cart").hide();
			$('#payment-loading').css('display', 'block'); 
 $('#payment-loading1').css('display', 'block');  			
            var itemIndex = _this.cartStorage.items.indexOf(product);
            _this.cartStorage.items.splice(itemIndex, 1); 
		   
		     $scope.order_products = _this.cartStorage.items;
			  $scope.order_coupons = _this.cartStorage.coupons; 
			   _this.paymentData = _this.cartStorage.payment;
			  
			$scope.order_products.data = {"coupon_code":$scope.order_coupons.coupon_code,  "store_id":$scope.order_coupons.store_id, "loyalty_points":$scope.order_coupons.loyalty_points,"customer_id":$scope.order_coupons.customer_id,"items": $scope.order_products}; 
			
			  
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_products.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
                $(".cart").show();
                $(".main-cart").hide();	
$('#payment-loading').css('display', 'none');  		
 $('#payment-loading1').css('display', 'none');  		
				console.log(JSON.stringify(_this.paymentData));
				_this.cartStorage.payment = _this.paymentData;
				localStorage.setItem('payment_data', JSON.stringify(_this.paymentData));
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });

            /*	   product.quantity = 0;
            product.addedToCart = false;
            product.showAddToCart = false;
            var itemIndex = _this.cartStorage.products.indexOf(product);
			alert(itemIndex);
            if (itemIndex > -1) {
                var a =  _this.cartStorage.products.splice(itemIndex, 1);
				alert(JSON.stringify(a));
				alert(JSON.stringify(_this.cartStorage.products)); */
                document.getElementById('res1').value = JSON.stringify(_this.cartStorage.products);
            //$scope.enableMe = true;
            // }
        }
		
	
		
    });

	
	/*=========================================================================================================================
	===========================================================================================================================*/
	 
	  app.controller('checkoutController', function($http,$scope,$window,$log, toastr,cartStorage,$location) {
      
	  $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
			$scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
			$scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
			$scope.customerDataEmail = localStorage.getItem('login_customer_email');
			console.log($scope.customerDataId);
			if($scope.customerDataId == '' || $scope.customerDataId == undefined || $scope.customerDataId == null || $scope.customerDataId == 'null'){
				 $location.reload;
			 }
			  $scope.store_id = localStorage.getItem('store_id');
			 
		  $scope.store_title = JSON.parse(localStorage.getItem('store_title'));
		  $scope.store_photo =  JSON.parse(localStorage.getItem('store_photo'));
		 
		 $scope.variantsSelectedTitleData =  localStorage.getItem('variant_title');
		 
				 
				 
			$scope.order_type = localStorage.getItem('order_type');
			console.log($scope.order_type);
				var _this = this;
        _this.cartStorage = cartStorage.cart;
			
			 _this.paymentsData = JSON.parse(localStorage.getItem('payment_data'));
			 console.log(_this.paymentsData);
			 
			   _this.cartStorage.items = _this.paymentsData.data[0].items;
			   console.log(_this.cartStorage.items);
			     console.log(_this.paymentsData);
			    _this.cartStorage.payment = _this.paymentsData;
			   // console.log(_this.cartStorage.payment);
			   //_this.cartStorage.coupons= ;
		
		 $scope.order_items = _this.cartStorage.items;
			 $scope.couponsData = _this.cartStorage.coupons; 
			 //$scope.store_id = $scope.couponsData.store_id;
			
			 if($scope.store_id == '' || $scope.store_id == undefined || $scope.store_id == null || $scope.store_id == 'null'){
				 $location.path('/');
			 }
			 
			  _this.points_title = localStorage.getItem('points_title'); 
			   $scope.couponsData.coupon_code = localStorage.getItem('coupon_code_data'); 
			  if($scope.couponsData.coupon_code == undefined || $scope.couponsData.coupon_code == 'undefined'){
				  $scope.couponsData.coupon_code = '';
			  };
              $scope.couponsData.loyalty_points =   localStorage.getItem('loyalty_points_data');	
				 
			 
			 $scope.loyalty_points = $scope.couponsData.loyalty_points;
			  if($scope.loyalty_points ==1){
				 _this.points = true;
			 }
			 
			 
			  $scope.coupon_code = $scope.couponsData.coupon_code; 
			
			  _this.coupon_code_value = $scope.coupon_code;
			    
			   
		$scope.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				 
				/*if($scope.coupon_code){ 
					$('.btn-apply').css('display','none'); 
		           $('.btn-cancel').css('display','block');
				   _this.pointsDisabled = true;
				}
				else if($scope.loyalty_points == 1){
					$('.btn-apply').css('display','block');
		           $('.btn-cancel').css('display','none');
				   _this.pointsDisabled = false;
				}*/
				 
				 
				 
			 
			 
	  var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders/add/form/',
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
           //console.log(JSON.stringify(data)); 
			 _this.addOrder = data.categories_products;
            _this.addOrderMeta = data.order_meta_fields;
            _this.UserListData = data.order_meta_fields.fields.fields;
            $('#loading').css('display', 'none');
            $('.order-content').css('display', 'block');
        })
		.error(function (data, status, header, config) {            
        	document.getElementById('res').value = JSON.stringify(data);        
	    }); 
		
		
	
        var _this = this; 
	     _this.cartStorage = cartStorage.cart;
		console.log(JSON.stringify(_this.cartStorage));
		
		
		
		
		
		$scope.changeDeliveryType = function(delivery_type_value,identifier){  
			$scope.deliveryType = identifier+'='+delivery_type_value;  
			$scope.deliveryValue = delivery_type_value;  
			
		}
		
		
		$scope.changeOrderType = function(order_type_value,identifier){ 
			$scope.orderType = identifier+"="+order_type_value; 
        $scope.orderValue = order_type_value;			
		}
		
		
		//========= FUNCTIONS FOR SELECT ADDRESS ==========================================================================================
	//=================================================================================================================================
         
		//FUNCTION FOR GET ADDRESSES======================================================================
		$scope.get_address = function(parent_identifier) { 
            var customer_id = $('#' + parent_identifier).val(); 
            if (customer_id == '') {
                alert('Please Select User First');
            } 
			else {
				
				
                $("#myModal").modal('show');

                /*get data from api**/
                $http.get(APP_URL + '/api/v1/address/?linked_id='+$scope.customerDataId)
                .success(function(data, status, headers, config) {
                    _this.data_addresses = data.data;
                    _this.currentpage_addresses = data.current_page;
                    _this.user_id = customer_id;
                })
                .error(function(data, status, headers, config) {
                    document.getElementById('res').value = JSON.stringify(data);
                });
            }
        };
		 
	    //FUNCTION FOR SELECT ADDREES VALUE INDEX=========================================================
        $scope.checkedIndex = function(values) { 
            $scope.selected_values = values; 
            //alert(JSON.stringify($scope.selected_values));
        }
 
    
	    //FUNCTION FOR PUT SELECTED ADDREES VALUE IN INPUT FIELD=========================================
        $scope.isSelected = function(field) {
            if (field.identifier == 'delivery_address') {
               //alert(JSON.stringify(field.id));
               field.value = JSON.stringify($scope.selected_values);
            }
           // return $scope.selected_values === values;
        }

		
		
		
		
		
		
		
		
		
		$scope.storeAddress = function(){
			$("#myModal").modal('hide');
			$("#address_add").modal('show');
		}
		
		$scope.add_address = function(){
		}
		
	
 $scope.selected = 0;
	
	//========= FUNCTIONS FOR SELECT PICKUP & DELIVERY DATE-TIME ================================================================================
	//============================================================================================================================================
	     
		//FUNCTION FOR GET PICKUP TIMESLOTS============================================================
        $scope.get_pickupTime = function() {
            $("#pickupModal").modal('show');

            $http.get(APP_URL + '/api/v1/timeslots')
            .success(function(data, status, headers, config) {
                _this.pickupDateData = data.data;
				console.log(JSON.stringify(_this.pickupDateData));
                $scope.selectedDay= _this.pickupDateData[0].day_name;
				 $scope.selectedDate = _this.pickupDateData[0].date;
                $scope.pickupDay2 = $scope.selectedDay;
                $scope.pickupDate2 = $scope.selectedDate; 				
            })
            .error(function(data, status, headers, config) {
                document.getElementById('res').value = JSON.stringify(data);
            });
        };


		//FUNCTION FOR GET SELECTED PICKUP DATE============================================================
        $scope.getPickupD = function(day,date) {
            $scope.pickupDay = day;
			$scope.pickupDate = date; 
        }
 
		//FUNCTION FOR SAVE SELECTED PICKUP DATE & TIME============================================================
        $scope.savePickup = function( id, from_time, to_time) { 
            if (!$scope.pickupDate) { 
                $scope.pickupDate = $scope.pickupDate2;
				$scope.pickupDay = $scope.pickupDay2; 
                var time = id; 
                $scope.timeShowFrom = from_time;
                $scope.timeShowTo = to_time;
                $scope.pickupTime = $scope.pickupDate; 
                _this.pickupTimeShow = $scope.pickupDate + ' , ' + $scope.timeShowFrom + ' - ' + $scope.timeShowTo; 
				
                document.getElementById('delivery_time').value = _this.pickupTimeShow;
				
               for(var i=0;i<=_this.addOrderMeta.fields.length;i++){ 
				  angular.forEach(_this.addOrderMeta.fields[i],function(key,value){
					   
					   if(key.identifier == 'delivery_time'){
						   key.value = _this.pickupTimeShow; 
					   }
				  });  
					
				   }
                $("#pickupModal").modal('hide'); 
				 
            } 
			else {
                var time = id;
                $scope.timeShowFrom = from_time;
                $scope.timeShowTo = to_time;
                $scope.pickupTime = $scope.pickupDate;
                _this.pickupTimeShow = $scope.pickupDate + ' , ' + $scope.timeShowFrom + ' - ' + $scope.timeShowTo;
				 
                document.getElementById('delivery_time').value = $scope.pickupTime;
				
				 for(var i=0;i<=_this.addOrderMeta.fields.length;i++){
					 angular.forEach(_this.addOrderMeta.fields[i],function(key,value){
					   
					   if(key.identifier == 'delivery_time'){
						   key.value = _this.pickupTimeShow; 
					   }
				  });  
				
                $("#pickupModal").modal('hide'); 
				}
            }
        };


		 
		
		
		
			//========= FUNCTIONS FOR APPLY COUPON ====================================================================================
	//=================================================================================================================================
		
		 _this.applyCoupon = function() {
		
		_this.coupon_code_value = $('#coupon_code').val();
		 $scope.coupon_code = $('#coupon_code').val(); 
		 if($scope.coupon_code == '' || $scope.coupon_code == undefined ){
	return;
}	
		  $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				 
				 _this.cartStorage.coupons = $scope.order_items.data;
				  
		_this.pointsDisabled = true;
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
              
				_this.paymentData.couponMessage =  _this.paymentData.data[0].discount_response.message; 
				if(_this.paymentData.data[0].discount_response.status_code == 0){
					_this.messageBg1 = '#de8583';
					_this.messagePadding1 = '2px 8px';
				}
				else{
					_this.messageBg1 = '#e8f7d8';
					_this.messagePadding1 = '2px 8px';
				}
				  $(".cart").hide();
                $(".main-cart").show();					 
				console.log(JSON.stringify(_this.paymentData)); 
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
		
		 }
		 
		 //========= FUNCTIONS FOR APPLY COUPON ====================================================================================
	//=================================================================================================================================
		
		 _this.cancelCoupon = function() { 
		
		_this.pointsDisabled = false;
		 $('#coupon_code').val('');
		 $scope.coupon_code = ''; 
		_this.coupon_code_value = $scope.coupon_code;  
		  $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code,  "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				 
				 _this.cartStorage.coupons = $scope.order_items.data; 
				 
				 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
                $(".cart").hide();
                $(".main-cart").show();					 
				console.log(JSON.stringify(_this.paymentData)); 
				_this.paymentData.couponMessage = ''; 
				_this.messageBg1 = 'transparent';
				_this.messagePadding1 = '0px';
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
		
		 }
		 
		 
		 
		 //========= FUNCTIONS FOR APPLY Points ====================================================================================
	//=================================================================================================================================
		
		 _this.changePonits = function() { 
		 if (_this.points == true){
		 $scope.loyalty_points = 1;
		  $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code, "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				  _this.cartStorage.coupons = $scope.order_items.data; 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
                $(".cart").hide();
                $(".main-cart").show();					 
				console.log(JSON.stringify(_this.paymentData)); 
				if(_this.paymentData.data[0].discount_response.status_code == 0){
					_this.pointMessageBg1 = '#e8f7d8';
					_this.pointMessagePadding1 = '2px 8px';
				_this.paymentData.pointsMessage1 =  _this.paymentData.data[0].discount_response.message; 
				}
				else{
					_this.pointMessageBg1 = '#e8f7d8';
					_this.pointMessagePadding1 = '2px 8px';
				_this.paymentData.pointsMessage1 =  _this.paymentData.data[0].discount_response.message; 
				}
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			}
          else{
            $scope.loyalty_points = 0;
			 $scope.order_items = _this.cartStorage.items;
			$scope.order_items.data = {"coupon_code":$scope.coupon_code, "store_id":$scope.store_id, "loyalty_points":$scope.loyalty_points,"customer_id":$scope.customerDataId,"items": $scope.order_items};
				  _this.cartStorage.coupons = $scope.order_items.data; 
			var request = $http({
                method: "POST",
                url: APP_URL + '/api/v1/orders/calculate',
                data: $scope.order_items.data,
                headers: {
                    'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
                _this.paymentData = data; 
				_this.cartStorage.payment = _this.paymentData;
                $(".cart").hide();
                $(".main-cart").show();					 
				console.log(JSON.stringify(_this.paymentData)); 
				if(_this.paymentData.data[0].discount_response.status_code == 0){
					_this.pointMessageBg1 = 'transparent'; 
					_this.pointMessagePadding1 = '0px';
				_this.paymentData.pointsMessage1 =  ''; 
				}
				else{
					_this.pointMessagePadding1 = '0px';
					_this.pointMessageBg1 = 'transparent';
				_this.paymentData.pointsMessage1 =  ''; 
				}
				
            })
			.error(function(data, status, headers, config) {  
			    document.getElementById("res").value = JSON.stringify(data);
            });
			
          } 
		 
		
		 }
		 
		 
		 
		
		//========= FUNCTIONS FOR PLACE ORDER ========================================================================================================
    //============================================================================================================================================
        $scope.placeOrder = function() {
            
            //$scope.msg = 'Data sent: ' + angular.toJson(_this.addOrderMeta);
            //$scope.msg1 = 'Data sent: ' + angular.toJson(_this.cartStorage.products);
 
	/*
            for (var j = 0; j < _this.addOrderMeta.fields.length; j++) {
                angular.forEach(_this.addOrderMeta.fields[j],function(key,value) { 
                	 
					if(key.identifier == 'customer_id'){
					key.value = $scope.customerDataId;
				   }
				   
				  
					  if($scope.deliveryType == key.display_show_rule){
						  key.show_bool = 1;
					  }
					  
					
                     $scope.required = key.required_or_not;
					
					
					if(key.show_bool == 1){
                    if($scope.required == '1') {
                        $scope.valuecheck = key.value; 
                       $scope.identifier = key.identifier; 
						
                    }
					} 
                });
            } */
var show_bool = '';
			for (var k = 0; k < _this.addOrderMeta.fields.length; k++) {
                angular.forEach(_this.addOrderMeta.fields[k],function(key,value) { 
                	 
					if(key.identifier == 'customer_id'){
					key.value = $scope.customerDataId;
				   }
				   
				   if(key.identifier == 'delivery_type'){
					   key.value = $scope.deliveryValue;
				   }
				    if(key.identifier == 'order_time'){
					   key.value = $scope.orderValue;
				   }
				 
				    
					if($scope.deliveryType == key.display_show_rule){ 
						  key.show_bool = 1; 
					  }
					  if($scope.orderType == key.display_show_rule){ 
						  key.show_bool = 1; 
					  }
					  
				if(key.show_bool == 1){ 
						  if(key.required_or_not == 1){
				   if(key.value == '' || key.value == undefined){ 
					   show_bool = 'yes';
                      $scope.setting_order_meta_type_title = key.setting_order_meta_type_title;	
	           			  
				   }
					}
				}
				   
				})
			}				
				 
					 
				 if(show_bool == 'yes'){ 
                     
                            toastr.error('Please enter ' + $scope.setting_order_meta_type_title , 'Error!');
                            return;
                        }  
               
  
			 
			    console.log(JSON.stringify(_this.addOrderMeta));
				//return;
          $scope.coupon_data = _this.cartStorage.coupons;
			 
			 $scope.coupon_code = $scope.coupon_data.coupon_code;
			 $scope.loyalty_points = $scope.coupon_data.loyalty_points;
            //$scope.total = _this.subTotal - $scope.coupon_discount;
       
            //alert(JSON.stringify($scope.total));
 
	  
            $scope.orderData = {
				 "customer_id":$scope.customerDataId,
                 "store_id":$scope.store_id,
               "coupon_code":  $scope.coupon_code, 
			    "type": $scope.order_type,
			   "loyalty_points": $scope.loyalty_points,
			   "payment_mode":"cash",
               "items": _this.cartStorage.items,
               "order_meta": { "fields" :_this.addOrderMeta.fields}
            };
           
		   
		   console.log(JSON.stringify($scope.orderData));
			//return;
 
            var request = $http({
                method: "POST",
                url: APP_URL+'/api/v1/orders',
                data: $scope.orderData,
                headers: {
                   'Accept': 'application/json'
                }
            });
            /* Check whether the HTTP Request is successful or not. */
            request.success(function(data) {
				$scope.data = data;
				if(data.status_code == 1){
                toastr.success(data.message, 'Success');
				localStorage.setItem('payment_data', '');
				 localStorage.setItem('store_id', '');
				localStorage.setItem('coupon_code_data', ''); 
					localStorage.setItem('store_data', '');
					localStorage.setItem('variant_title', '');
                 localStorage.setItem('loyalty_points_data', '');
				  _this.points_title = localStorage.getItem('');
				 $scope.order_type = localStorage.getItem('');
				 
                $window.location.href = 'order_detail?' + data.data[0].order_id; 
				
				
				}
				else{
					toastr.error(data.message, 'Error!');
					console.log(JSON.stringify(data));
				}
            })
			.error(function(data, status, headers, config) {
                toastr.error(data.message, 'Error!');
               console.log(JSON.stringify(data));
            });

        };


	
	
		
	  });

		 
		 
		 
		 
		 
		 
		 
		 
		 
			/*====================================================login controller=====================================================================
	===========================================================================================================================*/ 
		 
		 
		 
		 app.controller('loginController', function($http,$scope,$window,toastr,$location) {
		 
		

$scope.login = function(){
	
	 $scope.user_email = $('#user_email').val();
$scope.user_password = $('#user_password').val();

$scope.loginData = {"email":$scope.user_email, "password":$scope.user_password};
 
console.log(JSON.stringify($scope.loginData));

  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/login',
            data: $scope.loginData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){ 
            $scope.loginCustomer  = data; 
			$('#loginModal').modal('hide');
			$location.path('/checkout');
			localStorage.setItem('login_customer_id', JSON.stringify($scope.loginCustomer.user_data[0].user_id));
			localStorage.setItem('login_customer_photo', JSON.stringify($scope.loginCustomer.user_data[0].photo));
			localStorage.setItem('login_customer_first_name', $scope.loginCustomer.user_data[0].first_name);
			localStorage.setItem('login_customer_last_name', $scope.loginCustomer.user_data[0].last_name);
			localStorage.setItem('login_customer_email', $scope.loginCustomer.user_data[0].email);
			 
		 
		}
		else{
			toastr.error(data.message,'Error!') 
		}
			//document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });   
 		   
}

});



	/*====================================================login controller=====================================================================
	===========================================================================================================================*/ 
		 
		 
		 
		 app.controller('registerController', function($http,$scope,$window,toastr,$location) {
		 
		 $scope.registerUser = function(){

$scope.first_name = $('#first_name').val(); 
 $scope.last_name = $('#last_name').val(); 
 $scope.email = $('#email').val(); 
 $scope.phone = $('#mobile').val(); 
 $scope.password = $('#password').val(); 
 

$scope.registerData =   {
  "login_type":"email",
  "first_name":$scope.first_name,
  "facebook_id":"",
  "last_name":$scope.last_name,
  "email":$scope.email,
  "phone":$scope.phone,
  "password":$scope.password
};
 
console.log(JSON.stringify($scope.registerData));
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/register',
            data: $scope.registerData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){ 
            $scope.loginCustomer  = data; 
			toastr.success(data.message,'Success!') ;
      
       $('#registerModal').modal('hide');
		  $('#loginModal').modal('show');
		}
		else{
			toastr.error(data.message,'Error!') 
		}
			//document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });   
		 }
});


/*====================================================login controller=====================================================================
	===========================================================================================================================*/ 
		 
		 
		 
		 app.controller('addressController', function($http,$scope,$window,toastr,$location) {
$scope.locationpickerOptions = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,

                        radius: 0,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true
                    };
					


					
			//================================ FUNCTION FOR STORE NEW ADDRESS ================================================================================
    
                $scope.add_address = function() {
				 
					 $scope.address_type = 'customer'  ;
					 $scope.address_title = $('#address_title').val();
					 $scope.longitude = $('#us3-lon').val();
					 $scope.latitude = $('#us3-lat').val();
					 $scope.address_phone = $('#address_phone').val();
					 $scope.address_line1 = $('#address_line1').val();
					 $scope.address_line2 = $('#address_line2').val();
					 $scope.city = $('#city').val();
					 $scope.state = $('#state').val();
					 $scope.pincode = $('#pincode').val();
					 $scope.country = $('#country').val();

					 
					$scope.address_data = {"address_type": $scope.address_type, "linked_id": $scope.customerDataId, "address_title": $scope.address_title, "address_phone": $scope.address_phone, "address_line1": $scope.address_line1,
					"address_line2": $scope.address_line2, "latitude": $scope.latitude, "longitude": $scope.longitude, "city": $scope.city, "state": $scope.state, "pincode": $scope.pincode, "country": $scope.country}
				  
				    console.log(JSON.stringify($scope.address_data));
				 
					/*get data from api**/
					  var request = $http({
						method: "POST",
						url: APP_URL+'/api/v1/address',
						data: $scope.address_data,
						headers: {
							'Accept': 'application/json'
						}
					});

					/* Check whether the HTTP Request is successful or not. */
					request.success(function(data) { 
						$scope.data = data;
					    if(data.status_code == 1){
						   //toastr.success(data.message,'Success!');
						  //$("#myModal").modal('show');
			$("#address_add").modal('hide');
			$scope.get_address();
				        }
					    else{
						   toastr.error(data.message,'Error!');
					    }  
						//$window.location.href = 'product_edit/'+$scope.product_id;
					}).error(function(data, status, headers, config) {
						  toastr.error('Error Occurs','Error!');
						 document.getElementById("res2").value = JSON.stringify(data);
					});
				}
			});