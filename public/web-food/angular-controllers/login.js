 


app.controller('loginController', function($http,$scope,$window,toastr,$location) { 

            $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
			$scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
			$scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
			$scope.customerDataEmail = localStorage.getItem('login_customer_email');
			console.log($scope.customerDataPhoto);
// $window.localStorage.clear();
// localStorage.getItem('login_name')
//localStorage.getItem('login_name')


  if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
         console.log("Login Not Set");
		 $scope.login = function(){
	
 
 
 $scope.user_email = $('#user_email').val();
$scope.user_password = $('#user_password').val();

$scope.loginData = {"email":$scope.user_email, "password":$scope.user_password, "user_type":"2"};
 
console.log(JSON.stringify($scope.loginData));
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/login',
            data: $scope.loginData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){ 
            $scope.loginCustomer  = data; 
			toastr.success(data.message,'Success!') 
			localStorage.setItem('login_customer_id', JSON.stringify($scope.loginCustomer.user_data[0].user_id));
			localStorage.setItem('login_customer_photo', $scope.loginCustomer.user_data[0].photo);
			localStorage.setItem('login_customer_first_name', $scope.loginCustomer.user_data[0].first_name);
			localStorage.setItem('login_customer_last_name', $scope.loginCustomer.user_data[0].last_name);
			localStorage.setItem('login_customer_email', $scope.loginCustomer.user_data[0].email);
			 
			    $window.location.href = 'my-orders'; 
				
				
				  $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
			$scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
			$scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
			$scope.customerDataEmail = localStorage.getItem('login_customer_email');
			console.log($scope.customerDataPhoto);
		 
		}
		else{
			toastr.error(data.message,'Error!') 
		}
			//document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });   
 		   
}

         return false;
     } 
	 else {
        $window.location.href = 'my-orders'; 
		 console.log("Login Set"); 
	 }
});


 
