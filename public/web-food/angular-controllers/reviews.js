
//========================================================== review CONTROLLER==================================================================================
//==============================================================================================================================================================
 
	
	 app.controller('reviewController', function($http,$scope,$window) {
		     
 
			   $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
     $scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
     $scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
     $scope.customerDataEmail = localStorage.getItem('login_customer_email');
 
     if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
		 $window.location.href = APP_URL+'/web-login';
         console.log("Login Not Set");
         return false;
     } else {
		 
		 console.log($scope.customerDataId);
			  $('#loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 
		  
		//========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 
        var request = $http({ 
             method: "GET",   
             url: APP_URL+'/api/v1/order-review?user_id='+$scope.customerDataId,  
            data: '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
			$scope.reviewData =  data ; 
			
			  $('#loading').css('display', 'none'); 
 $('.innerData').css('display', 'block'); 
 
           	console.log(JSON.stringify(data));

 
        })
		.error(function (data, status, header, config) {            
        	console.log(JSON.stringify(data));        
	    }); 
			
			
			
			
			
	$scope.order_review_delete = function(reviewId, index) {
	 
	   if (confirm("Are you sure?")) {
                 
				 var request = $http({
                 method: "DELETE",
                 url: APP_URL+'/api/v1/order-review/'+reviewId,
                 data: '',
                 headers: { 'Accept':'application/json' }
            });

            /* Check whether the HTTP Request is successful or not. */
            request.success(function (data) { 
				$scope.data = data;
			   toastr.success(data.message,'Success!');
			   document.getElementById("res").value = JSON.stringify(data);
			    
			 }).error(function (data, status, headers, config) { 
			    toastr.error('Unknown Error Occurred','Error!');
			    document.getElementById("res").value = JSON.stringify(data);
				
        }); 
	   }
		
    };
	
	
 
 
	 }
  
		
	});



 