var app = angular.module("mainApp", ['ngMaterial', 'ngAnimate', 'ngMessages',  'toastr', 'ngRoute','angular-jquery-locationpicker','ui.bootstrap' ])


app.factory('SessionService', ['$http',
  function($http) {
    var footerService = {
      'footer': function() {
        $http({
          url: APP_URL+'/api/v1/setting',
          method: 'GET',
          data: '',
        }).then(function(data) {
			console.log(data); 
			 data;
        });
      } 
    };
    return footerService;
  }
]);



app.controller('logoutController', function($http,$scope,$window,toastr,$location) { 
  
 $scope.logout = function(){
	 
	  if (confirm("Are you sure?")) {
     $window.localStorage.clear();
	 $window.location.href = APP_URL;
	  }
 };
 
});




app.controller('footerController', function($http,$scope,toastr) { 
  
	   var request  =  $http({
          url: APP_URL+'/api/v1/setting',
          method: 'GET',
          data: '',
         headers: { 'Accept':'application/json' }
        }).then(function(data) { 
			$scope.footer = data.data.data;
			console.log(JSON.stringify($scope.footer));
			for(var i=0;i<=$scope.footer.length;i++){ 
				if($scope.footer[i].key_title == 'currency_symbol'){
				   $scope.symbol = $scope.footer[i].key_value; 
				}
				else if($scope.footer[i].key_title == 'email'){
				   $scope.footer_email = $scope.footer[i].key_value; 
				}
				else if($scope.footer[i].key_title == 'phone'){
				   $scope.footer_phone = $scope.footer[i].key_value; 
				}
				else if($scope.footer[i].key_title == 'facebook'){
				   $scope.footer_facebook_link = $scope.footer[i].key_value; 
				}
				else if($scope.footer[i].key_title == 'twitter'){
				   $scope.footer_twitter_link = $scope.footer[i].key_value; 
				}
				else if($scope.footer[i].key_title == 'google'){
				   $scope.footer_google_link = $scope.footer[i].key_value; 
				}
				else if($scope.footer[i].key_title == 'admin_email'){
				   $scope.admin_email = $scope.footer[i].key_value;
				}
			}
        });
   

});




app.controller('newsletterController', function($http,$scope,toastr) { 
 
		 //==========================function for update user======================================
		   $scope.submitNewsletter = function(){
			 
			    $scope.email = $('#newsletter-email').val();
			  $scope.newsletter_email = {"email" : $scope.email};
				
			    var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/newsletters',
            data:  $scope.newsletter_email,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;  
             if(data.status_code == 1) { toastr.success(data.message, 'Success'); $('#newsletter-email').val('');}
            else { toastr.error(data.message, 'Error'); }
             
            $('#loading').css('display', 'none');    
			 console.log(JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  	
		      toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
 
		   };			   
 
});




app.controller('headerController', function($http,$scope) { 
 
		
		
		  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/setting',
            data:  $scope.newsletter_email,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data;  
              for(var i=0;i<=$scope.data.data.length;i++){
				  if($scope.data.data[i].key_title == 'business_logo'){
					    $scope.logo = $scope.data.data[i].key_value; 
				  }
			  }
             
            $('#loading').css('display', 'none');    
			 console.log(JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  	
		      toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
		
		
	 $scope.customerDataId = localStorage.getItem('login_customer_id');
     $scope.customerDataFirstName = localStorage.getItem('login_customer_first_name'); 
     $scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
     $scope.customerDataEmail = localStorage.getItem('login_customer_email');
	  if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
         console.log("Login Not Set");
         $('#header-account').css('display','block');
		  $('#header-login').css('display','none');
     } else {
		  $('#header-account').css('display','none');
		  $('#header-login').css('display','block');
		 }

});
