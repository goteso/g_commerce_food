  
app.controller('registerController', function($http,$scope,$window,toastr,$location) { 

            $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
			$scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
			$scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
			$scope.customerDataEmail = localStorage.getItem('login_customer_email');
			
// $window.localStorage.clear();
// localStorage.getItem('login_name')
//localStorage.getItem('login_name')


  if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
         console.log("Login Not Set");
		 $scope.register = function(){
	
 
 
 $scope.first_name = $('#first_name').val(); 
 $scope.last_name = $('#last_name').val(); 
 $scope.email = $('#email').val(); 
 $scope.phone = $('#mobile').val(); 
 $scope.password = $('#password').val(); 
 

$scope.registerData =   {
  "login_type":"email",
  "first_name":$scope.first_name,
  "facebook_id":"",
  "last_name":$scope.last_name,
  "email":$scope.email,
  "phone":$scope.phone,
  "password":$scope.password
};
 
console.log(JSON.stringify($scope.registerData));
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/register',
            data: $scope.registerData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){ 
            $scope.loginCustomer  = data; 
			toastr.success(data.message,'Success!') ;
      
         $window.location.href = 'web-login'; 
		}
		else{
			toastr.error(data.message,'Error!') 
		}
			//document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });   
 		   
}

         return false;
     } 
	 else {
        $window.location.href = 'web-login'; 
		 console.log("Login Set"); 
	 }
});


 
