  
app.controller('accountController', function($http,$scope,toastr) { 

 $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
     $scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
     $scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
     $scope.customerDataEmail = localStorage.getItem('login_customer_email');

     //$scope.customerDataId = localStorage.getItem('customerDataId');
     if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
         console.log("Login Not Set");
         return false;
     } else {  

 $('#loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 
  var request = $http({
            method: "GET",
            url: APP_URL+'/v1/form/user/'+$scope.customerDataId,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.editUser  = data;
             $('#loading').css('display', 'none'); 
 $('.innerData').css('display', 'block'); 
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  

		
 
		
		 //==========================function for update user======================================
		   $scope.updateUser = function(){
			 
			    var photo = $('#item_photo').val();
				 
				 for(var i=0;i<$scope.editUser[0].fields.length;i++){
				 if($scope.editUser[0].fields[i].identifier == 'photo'){
					 $scope.editUser[0].fields[i].value = photo;
                       localStorage.setItem('login_customer_photo', photo);		
 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');					   
				 } 
				}
				
				console.log(JSON.stringify($scope.editUser));
				
				
			    var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/form/user/'+$scope.customerDataId,
            data:  $scope.editUser,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) {  
            $scope.data  = data; 
console.log(JSON.stringify($scope.data));
           if(data.status_text == 'Success') { toastr.success(data.message, 'Success');

 for(var i=0;i<=$scope.data[0].fields.length;i++){
				 if($scope.data[0].fields[i].identifier == 'first_name'){ 
			localStorage.setItem('login_customer_first_name', $scope.data[0].fields[i].first_name);
				 }
				 else if($scope.data[0].fields[i].identifier == 'photo'){
            localStorage.setItem('login_customer_photo', $scope.data[0].fields[i].photo);
				 }
				 else if($scope.data[0].fields[i].identifier == 'last_name'){
			localStorage.setItem('login_customer_last_name', $scope.data[0].fields[i].last_name);
				 }
				 else if($scope.data[0].fields[i].identifier == 'email'){
			localStorage.setItem('login_customer_email', $scope.data[0].fields[i].email);
				 }
			  }
			   
			  }
            else { toastr.error(data.message, 'Error'); }
              
			

            $('#loading').css('display', 'none');    
			 console.log(JSON.stringify(data)); 
        })
		.error(function (data, status, headers, config) {  	
		
            // toastr.error(data.message, 'Error');
		      console.log(JSON.stringify(data));
        });  
 
		   };			   
	 }
});


 