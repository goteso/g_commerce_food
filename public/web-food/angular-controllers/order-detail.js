 

app.controller('orderDetailController', function($http,$scope,toastr) { 

 $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
     $scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
     $scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
     $scope.customerDataEmail = localStorage.getItem('login_customer_email');
	 
	 	$scope.order_id = window.location.href.substr(window.location.href.lastIndexOf('?') + 1);
		

     //$scope.customerDataId = localStorage.getItem('customerDataId');
     if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
         console.log("Login Not Set");
         return false;
     } 
	 
	 
	 else {  


  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/orders/'+$scope.order_id,
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.orders  = data;
            $('#loading').css('display', 'none');  
			
			angular.forEach($scope.orders,function(key,value){
			if(key.type == 'store_details'){
				$scope.store_id = key.data[0].store_id;
				 
			}
			
			if(key.type == 'tasks'){
				$scope.driver_name = key.data[0].driver_name;
				 $scope.driver_id = key.data[0].driver_id;
			}
				 
			})
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  

		
 
		
		
		
		$scope.updateAction = function(value){ 
		 
			if(value == 'cancelled'){
				$('#loading').css('display', 'block');  
				$scope.order_status = {"order_status":value};
				
				if (confirm("Are you sure?")) {
				var request = $http({
            method: "PUT",
            url: APP_URL+'/api/v1/orders/'+$scope.order_id+'/status-update',
            data:  $scope.order_status,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
			 toastr.success(data.message,'Success!');
            $('#loading').css('display', 'none');  
			console.log(JSON.stringify(data));
			location.reload();
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  

			}
			}
			
			
			else if(value == 'track'){
			$('#orderTrack').modal('show');
			/*if($scope.driver_id == '' || $scope.driver_id == undefined){
				alert('No Driver is assigned');
			}*/
			}
			
			else{
				$('#feedbackModal').modal('show');
			}
			
			
			
			
			
		}
		
		
		
	$('.rating input').on('change', function() {
	var ratingValue = $('input[name=rating]:checked', '.rating').val();
    $('#rating').val(ratingValue);
});
 
	
	
		$scope.sendFeedback = function(){
			 $('#loading').css('display', 'block');  
			$scope.rating = $('#rating').val();
			$scope.review = $('#feedback').val();
			
		$scope.reviewData =  { "user_id":$scope.customerDataId, "store_id":$scope.store_id, "rating":$scope.rating,  "review":$scope.review,  "order_id":$scope.order_id}
console.log(JSON.stringify($scope.reviewData));
	 
			var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/order-review',
            data:  $scope.reviewData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
			
			if(data.status_code == 1){
				$('#feedbackModal').modal('hide');
			 toastr.success(data.message,'Success!');
            $('#loading').css('display', 'none'); 
location.reload();			
			}
			else{
				 toastr.error(data.message,'Error!');
			}
			console.log(JSON.stringify(data));
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
		
		}
		
		
 
	 
		   
    var mapOptions = {
				 zoom: 8,
             center: new google.maps.LatLng(30.65118399999999, 76.81360100000006),
       
         styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}] ,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
		 mapTypeControl: false,
		 streetViewControl: false
    }


  //Initializing the InfoWindow, Map and LatLngBounds objects.
            $scope.InfoWindow = new google.maps.InfoWindow();
            bounds = new google.maps.LatLngBounds(); 
     $scope.map = new google.maps.Map(document.getElementById('dvMap'), mapOptions); 
	 var position = new google.maps.LatLng(30.65118399999999, 76.81360100000006);
	  var marker = new google.maps.Marker({
            position: position, 
            map: $scope.map,
            icon: APP_URL+"/admin/assets/images/map-marker.png",  
            draggable: true,
            animation: google.maps.Animation.DROP,
            //label:'a'+i
        });
		
		/* (function (marker) {
                      
                    google.maps.event.addListener(marker, "click", function (e) {
                        $scope.InfoWindow.setContent("<div class='container-fluid'><div class='row'  ><div class='col-sm-3'><img style='border-radius:50px;height:50px;width:50px;margin-top:20px;' src='"+APP_URL+"/images/users/'></div><div class='col-sm-9'><h5 style='color:#6399bd'>"+$scope.driver_name+"</h5><p>  </p> </div></div></div>");
                        $scope.InfoWindow.open($scope.map, marker);
                    });
                })(marker);*/
				
		
		 bounds.extend(marker.position); 
            
  $scope.locationpickerOptions = {
                        location: {
                            latitude: 30.65118399999999,
                            longitude: 76.81360100000006
                        },
                        inputBinding: {
                            latitudeInput: $('#us3-lat'),
                            longitudeInput: $('#us3-lon'),
                            radiusInput: $('#us3-radius'),
                            locationNameInput: $('#us3-address') 
                        },
						zoom: 15,

                        radius: 0,
						enableReverseGeocode: false,
                        enableAutocomplete: true,
						 styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#7c93a3"},{"lightness":"-10"}]},{"featureType":"administrative.country","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#a0a4a5"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#62838e"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#dde3e3"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#3f4a51"},{"weight":"0.30"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"poi.attraction","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.government","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.sports_complex","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#bbcacf"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"lightness":"0"},{"color":"#bbcacf"},{"weight":"0.50"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"color":"#a9b4b8"}]},{"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"invert_lightness":true},{"saturation":"-7"},{"lightness":"3"},{"gamma":"1.80"},{"weight":"0.01"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#a3c7df"}]}],
						 
						markerIcon: APP_URL+"/admin/assets/images/map-marker.png",
                       markerDraggable: true,
                       markerVisible : true 
					   
					   
                    };
		 
		 
			
	}
	
	
		
	 
});


 