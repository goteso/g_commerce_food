
app.controller('transferPointsController', function($http,$scope,toastr, $window) {
	 
	
	$scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
     $scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
     $scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
     $scope.customerDataEmail = localStorage.getItem('login_customer_email');

     //$scope.customerDataId = localStorage.getItem('customerDataId');
     if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
         console.log("Login Not Set");
         return false;
     } else {  
	 
	 
	 $scope.transferPoints = function(){
	 
	$scope.email = $('#email').val();
	$scope.points = $('#points').val(); 
	 
	$scope.transferPointsData = { "user_id":$scope.customerDataId,  "type" : "transfer", "email":$scope.email,   "points":$scope.points};
	 var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/loyalty-points',
            data:  $scope.transferPointsData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.data  = data;
			
			if(data.status_code == 1){ 
                toastr.success(data.message, 'Success');
            $('#loading').css('display', 'none');   
            
			 location.reload();
			}
			else{
      // $window.location.href = APP_URL+"/user_login";
	   toastr.error(data.message, 'Error');
			 
			}
			console.log(JSON.stringify(data));
              
        })
		.error(function (data, status, headers, config) {
			  
		console.log(JSON.stringify(data));
        });  
	}
	 
	
	 }
});  
 

 