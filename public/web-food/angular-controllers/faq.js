  
app.controller('faqController', function($http,$scope,$sce) { 

$scope.trustAsHtml = $sce.trustAsHtml;
  var request = $http({
            method: "GET",
            url: APP_URL+'/api/v1/faqs',
            data:  '',
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
            $scope.faqData  = data;
            $('#loading').css('display', 'none');  
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 		   
 
});


 