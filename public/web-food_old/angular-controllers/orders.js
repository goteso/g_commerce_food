 //========================================================== ORDER CONTROLLER==================================================================================
 //==============================================================================================================================================================
 app.controller('OrderController', function($http, $scope, $window) {

 
 $('#loading').css('display', 'block'); 
 $('.innerData').css('display', 'none'); 
 
     $scope.customerDataId = localStorage.getItem('login_customer_id');
			 $scope.customerDataPhoto = localStorage.getItem('login_customer_photo');
     $scope.customerDataFirstName = localStorage.getItem('login_customer_first_name');
     $scope.customerDataLastName = localStorage.getItem('login_customer_last_name');
     $scope.customerDataEmail = localStorage.getItem('login_customer_email');
console.log($scope.customerDataPhoto);
     //$scope.customerDataId = localStorage.getItem('customerDataId');
     if ($scope.customerDataId == null || $scope.customerDataId == '' || $scope.customerDataId == 'undefined') {
		 $window.location.href = APP_URL+'/web-login';
         console.log("Login Not Set");
         return false;
     } else {
         console.log("Login Set");
         var request = $http({
             method: "GET",
             url: APP_URL+'/api/v1/orders?include_count_blocks=true&status=any&customer_id='+$scope.customerDataId,
             data: '',
             headers: {
                 'Accept': 'application/json'
             }
         });

         /* Check whether the HTTP Request is successful or not. */
         request.success(function(data) {
			 
			 $('#loading').css('display', 'none');  
                 $scope.orders = data;
				 
			 $('.innerData').css('display', 'block');
                 console.log(JSON.stringify(data));


             })
             .error(function(data, status, header, config) {
				 
                 document.getElementById('res').value = JSON.stringify(data);
             });

     }

     //========= ONLOAD GET ORDER DETAILS  VIA CALLING API================================================================== 




 });