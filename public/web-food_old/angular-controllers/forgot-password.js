  
app.controller('forgotPassController', function($http,$scope,$window,toastr) { 

$('#otp-block').css("display", "none");
$('#password-block').css("display", "none");

$scope.sendEmail = function(){
	 
$scope.user_email = $('#user_email').val(); 

$scope.userEmail = {"email":$scope.user_email}; 
 
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/forgot-password-email',
            data: $scope.userEmail,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){
            $scope.data  = data; 
			toastr.success(data.message,'Success!')
			
		$('#otp-block').css("display", "block");
		$('#email-block').css("display", "none");
			console.log(JSON.stringify(data));
		}
		else{
			toastr.error(data.message,'Error!')
		}
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        });  
 		   
}



$scope.verifyOtp = function(){ 
$scope.otp = $('#otp').val(); 

$scope.optData = {"email":$scope.user_email,"otp":$scope.otp}; 
 
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/forgot-password-verify-otp',
            data: $scope.optData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){
            $scope.data  = data; 
			$scope.user_id = data.user_id
			toastr.success(data.message,'Success!')
			
		$('#password-block').css("display", "block");
		$('#otp-block').css("display", "none");
			console.log(JSON.stringify(data));
		}
		else{
			toastr.error(data.message,'Error!')
		}
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        }); 
}

$scope.verifyOtp = function(){ 
$scope.otp = $('#otp').val(); 

$scope.optData = {"email":$scope.user_email,"otp":$scope.otp}; 
 
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/forgot-password-verify-otp',
            data: $scope.optData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){
            $scope.data  = data; 
			toastr.success(data.message,'Success!')
			
		$('#password-block').css("display", "block");
		$('#otp-block').css("display", "none");
			console.log(JSON.stringify(data));
		}
		else{
			toastr.error(data.message,'Error!')
		}
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        }); 
}


$scope.setPassword = function(){ 
$scope.password = $('#password').val(); 

$scope.passwordData = { "user_id":$scope.user_id, "password":$scope.password }; 
 
  var request = $http({
            method: "POST",
            url: APP_URL+'/api/v1/users/forgot-password-change-password',
            data: $scope.passwordData,
            headers: { 'Accept':'application/json' }
        });

        /* Check whether the HTTP Request is successful or not. */
        request.success(function (data) { 
		if(data.status_code == 1){
            $scope.data  = data; 
			toastr.success(data.message,'Success!') ;
			
		$window.location.href = 'web-login';
			console.log(JSON.stringify(data));
		}
		else{
			toastr.error(data.message,'Error!')
		}
			document.getElementById("res").value = JSON.stringify(data);
        })
		.error(function (data, status, headers, config) {
		     document.getElementById("res").value = JSON.stringify(data);
        }); 
}

});


 